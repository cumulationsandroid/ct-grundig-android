package com.cumulations.grundig.Ls9Sac;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.ActiveScenesListActivity;
import com.cumulations.grundig.DeviceDiscoveryActivity;
import com.cumulations.grundig.Gcast;
import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.Network.LSSDPDeviceNetworkSettings;
import com.cumulations.grundig.Network.NewSacActivity;
import com.cumulations.grundig.Network.WifiConnection;
import com.cumulations.grundig.PlayNewActivity;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.netty.BusProvider;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;

import java.util.List;

public class ConnectingToMainNetwork extends DeviceDiscoveryActivity implements LibreDeviceInteractionListner {
    private static final String PSK = "PSK";
    private static final String WEP = "WEP";
    private static final String OPEN = "Open";
    private static final int MAX_PRIORITY = 99999;
    private final WifiConnection wifiConnect = WifiConnection.getInstance();
    private WifiManager mWifiManager;
    private AlertDialog alertConnectingtoNetwork ;

    /**
     * Broadcast receiver for connection related events
     */
    private ConnectionReceiver connectionReceiver;
    private ProgressBar m_LoadingDialog;
    private TextView mMessageText;

    @Override
    protected void onDestroy() {
        try{
            if(mTaskHandlerForSendingMSearch.hasMessages(MSEARCH_TIMEOUT_SEARCH)) {
                mTaskHandlerForSendingMSearch.removeMessages(MSEARCH_TIMEOUT_SEARCH);
            }
            mTaskHandlerForSendingMSearch.removeCallbacks(mMyTaskRunnableForMSearch);
            unregisterReceiver(connectionReceiver);
        }catch(Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private boolean mRestartOfAllSockets = false;

    public static final String SAC_CURRENT_IPADDRESS = "sac_current_ipaddress";
    private String mSACConfiguredIpAddress = "";
    @SuppressLint("HandlerLeak")
    private final
    Handler mHandler = new Handler() {
        @Override

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            LibreLogger.d(this, "Handler Message Got " + msg.toString());
            if (msg.what == Constants.HTTP_POST_DONE_SUCCESSFULLY) {
                setTextWithMessage(getString(R.string.mConnectingToMainNetwork) + " " + wifiConnect.getMainSSID());
                mWifiManager.disconnect();
                LSSDPNodeDB.getInstance().clearDB();
                cleanUpcode(ConnectingToMainNetwork.this,false);
                connectToSpecificNetwork(wifiConnect.getMainSSID(), wifiConnect.getMainSSIDPwd(), wifiConnect.getMainSSIDSec());

            } else if (msg.what == Constants.CONNECTED_TO_MAIN_SSID_SUCCESS) {
                /* Removing Failed Callback */
                mHandler.removeMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL);

                if (getconnectedSSIDname(ConnectingToMainNetwork.this).equalsIgnoreCase(wifiConnect.getMainSSID())) {

                    LibreLogger.d(this, "Connected To Main SSID " + wifiConnect.getMainSSID());
                    LibreApplication.activeSSID = wifiConnect.getMainSSID();
                    //restartApp(getApplicationContext());
                    if (!mRestartOfAllSockets) {
                        LSSDPNodeDB.getInstance().clearDB();
                        LibreApplication.LOCAL_IP="";
                        LibreApplication.mCleanUpIsDoneButNotRestarted = false;
                        mRestartOfAllSockets = restartAllSockets(ConnectingToMainNetwork.this);
                        mHandler.sendEmptyMessageDelayed(Constants.SEARCHING_FOR_DEVICE,500);
                    }
                } else {
                    showAlertDialogForClickingWrongNetwork();
                }
            } else if (msg.what == Constants.HTTP_POST_FAILED) {
                Toast.makeText(ConnectingToMainNetwork.this, getString(R.string.httpPostFailed), Toast.LENGTH_LONG).show();
            } else if (msg.what == Constants.SEARCHING_FOR_DEVICE) {
                sendMSearchInIntervalOfTime();
                mHandler.sendEmptyMessageDelayed(Constants.TIMEOUT_FOR_SEARCHING_DEVICE, 70000);
                LibreLogger.d(this, "Searching For The Device " + LibreApplication.thisSACConfiguredFromThisApp);
                setTextWithMessage(getString(R.string.mSearchingTheDevce) + "\n" + LibreApplication.thisSACConfiguredFromThisApp);
            } else if (msg.what == Constants.TIMEOUT_FOR_SEARCHING_DEVICE) {
                closeProgressDialog();
                setTextWithMessage(getString(R.string.mTimeoutSearching) + "\n" + LibreApplication.thisSACConfiguredFromThisApp);
                showDialogifDeviceNotFound(getString(R.string.noDeviceFound));
                /*Call the Activity To PlayNewScreen */
               // onBackPressed();
            } else if (msg.what == Constants.CONFIGURED_DEVICE_FOUND) {
                closeProgressDialog();
                if(mHandler.hasMessages(Constants.TIMEOUT_FOR_SEARCHING_DEVICE))
                    mHandler.removeMessages(Constants.TIMEOUT_FOR_SEARCHING_DEVICE);
                setTextWithMessage(getString(R.string.mConfiguredSuccessfully) + "\n" + LibreApplication.thisSACConfiguredFromThisApp);
                callGoogleCastUpdateScreen();
            } else if (msg.what == Constants.CONNECTED_TO_DIFFERENT_SSID) {
                closeProgressDialog();
                showAlertDialogForClickingWrongNetwork();
            } else {

                ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (mWifi.isConnected()) {
                    if (getconnectedSSIDname(ConnectingToMainNetwork.this).contains(wifiConnect.getMainSSID())) {
                        mHandler.sendEmptyMessage(Constants.CONNECTED_TO_MAIN_SSID_SUCCESS);
                    } else if (!getconnectedSSIDname(ConnectingToMainNetwork.this).contains(Constants.WAC_SSID)) {
                        showAlertDialogForConnectedToWrongNetwork();
                    }
                }else{
                    alertBoxForNetworkOff();
                }
/*
                LibreError error = new LibreError("Not able to connect ", wifiConnect.getMainSSID());
                BusProvider.getInstance().post(error);
                Intent in = new Intent(ConnectingToMainNetwork.this, DMRDeviceListenerForegroundService.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
                stopService(in);
                try {
                    ActivityCompat.finishAffinity(ConnectingToMainNetwork.this);
                }catch(Exception e){
                    e.printStackTrace();
                }
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
                finish();*/
            }

        }
    };
    private AlertDialog alertDialogNetworkOff ;
    public void alertBoxForNetworkOff() {
        if (!ConnectingToMainNetwork.this.isFinishing()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ConnectingToMainNetwork.this);

            // set title
            alertDialogBuilder.setTitle(getString(R.string.wifiConnectivityStatus));

            // set dialog message
            alertDialogBuilder
                    .setMessage(getString(R.string.connectToWifi))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, close
                            // current activity
                            dialog.cancel();

                            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivityForResult(intent, 1235);

                        }
                    });
                  /*  .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            Toast.makeText(ConnectingToMainNetwork.this, getString(R.string.connectToWifi), Toast.LENGTH_SHORT).show();
                            ;
                            dialog.cancel();
                        }
                    });*/

            // create alert dialog
            if (alertDialogNetworkOff == null)
                alertDialogNetworkOff = alertDialogBuilder.create();

            // show it

            alertDialogNetworkOff.show();
        }
    }
    private final int MSEARCH_TIMEOUT_SEARCH = 2000;
    private Handler mTaskHandlerForSendingMSearch = new Handler();
    public boolean mBackgroundMSearchStoppedDeviceFound = false;
    private Runnable mMyTaskRunnableForMSearch = new Runnable() {
        @Override
        public void run() {
            LibreLogger.d(this, "My task is Sending 1 Minute Once M-Search");
            /* do what you need to do */
            if (mBackgroundMSearchStoppedDeviceFound)
                return;

            final LibreApplication application = (LibreApplication) getApplication();
            application.getScanThread().UpdateNodes();
            /* and here comes the "trick" */
            mTaskHandlerForSendingMSearch.postDelayed(this, MSEARCH_TIMEOUT_SEARCH);
        }
    };
    private void sendMSearchInIntervalOfTime() {
       mTaskHandlerForSendingMSearch.postDelayed(mMyTaskRunnableForMSearch, MSEARCH_TIMEOUT_SEARCH);

    }
    private void showDialogifDeviceNotFound(String Message) {
        if (!ConnectingToMainNetwork.this.isFinishing()) {
            alert = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(ConnectingToMainNetwork.this);

            builder.setMessage(Message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            alert.dismiss();

                            LSSDPNodes node = ScanningHandler.getInstance().getLSSDPNodeFromCentralDB(mSACConfiguredIpAddress);
                            if (node!=null && node.getgCastVerision()!=null){
                                Intent ledIntent = new Intent(ConnectingToMainNetwork.this, Gcast.class);
                                ledIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                ledIntent.putExtra("DeviceName", node.getFriendlyname());
                                ledIntent.putExtra("ip_address", mSACConfiguredIpAddress);
                                ledIntent.putExtra(Constants.FROM_ACTIVITY, ConnectingToMainNetwork.class.getSimpleName());
                                startActivity(ledIntent);
                                finish();
                                return;
                            }
                            if (ScanningHandler.getInstance().centralSceneObjectRepo.size() > 0) {
                                Intent newIntent = new Intent(ConnectingToMainNetwork.this, ActiveScenesListActivity.class);
                                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(newIntent);
                                finish();
                            } else {
                                Intent ssid = new Intent(ConnectingToMainNetwork.this,
                                        PlayNewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(ssid);
                                finish();
                            }
                        }
                    });

            if (alert == null) {
                alert = builder.show();
                TextView messageView = (TextView) alert.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }

            alert.show();

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1234) {
            LibreLogger.d(this, "came back from wifi list");
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (!mWifi.isConnected()) {
                alertBoxForNetworkOff();
            }else{
                restartApp(getApplicationContext());
            }
        }else if(requestCode == 1235){
            restartApp(getApplicationContext());
        }
    }
    private AlertDialog alertWrongNetwork;
    private void showAlertDialogForConnectedToWrongNetwork(){
            if (!ConnectingToMainNetwork.this.isFinishing()) {
                alertWrongNetwork = null;
                AlertDialog.Builder builder = new AlertDialog.Builder(ConnectingToMainNetwork.this);
                String Message =  String.format(getString(R.string.newrestartApp),
                        getconnectedSSIDname(ConnectingToMainNetwork.this), wifiConnect.getMainSSID());
            /*String Message = getResources().getString(R.string.mConnectedToSsid) + "\n" + getconnectedSSIDname(ConnectingToMainNetwork.this) + "\n" +
                    getString(R.string.restartTitle);*/
                builder.setMessage(Message)
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.gotoSettings), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                alertWrongNetwork.dismiss();
                                alertWrongNetwork = null;
                                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivityForResult(intent, 1234);

                            }
                        });
                if (alertWrongNetwork == null) {
                    alertWrongNetwork = builder.show();
               /* TextView messageView = (TextView) alert.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);*/
                }

                alertWrongNetwork.show();
            }
    }
    private void showAlertDialogForClickingWrongNetwork() {
        if (!ConnectingToMainNetwork.this.isFinishing()) {
            alert = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(ConnectingToMainNetwork.this);
            String Message =  String.format(getString(R.string.newrestartApp),
                    getconnectedSSIDname(ConnectingToMainNetwork.this), wifiConnect.getMainSSID());
            /*String Message = getResources().getString(R.string.mConnectedToSsid) + "\n" + getconnectedSSIDname(ConnectingToMainNetwork.this) + "\n" +
                    getString(R.string.restartTitle);*/
            builder.setMessage(Message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            alertConnectingtoNetwork.dismiss();
                            alertConnectingtoNetwork = null;
                            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivityForResult(intent, 1234);
                        }
                    });
            if (alertConnectingtoNetwork == null) {
                alertConnectingtoNetwork = builder.show();
               /* TextView messageView = (TextView) alert.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);*/
            }

            alertConnectingtoNetwork.show();

        }
    }

    @Override
    public void onBackPressed() {
        /*Intent ssid = new Intent(ConnectingToMainNetwork.this, SacActivityScreenForLaunchWifiSettings.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(ssid);
        finish();
        super.onBackPressed();*/
    }

    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

/* Setting Maximum Priority to Wifi Network */

    @Override
    public void newDeviceFound(LSSDPNodes node) {
        LibreLogger.d(this, "New Device Found and same as Configured Device" + node.getFriendlyname()
                + node.getgCastVerision());
        if (node != null) {
            if (LibreApplication.thisSACConfiguredFromThisApp.equals(node.getFriendlyname())) {
                LibreLogger.d(this, "Hurray !! New Device Found and same as Configured Device"
                        + node.getFriendlyname()
                + node.getgCastVerision());
                mSACConfiguredIpAddress=node.getIP();
                if(node.getgCastVerision()!=null && false) //hardcoded to avoid GCast related changes
                    mHandler.sendEmptyMessage(Constants.CONFIGURED_DEVICE_FOUND);
                else{
                    closeProgressDialog();
                    if(mHandler.hasMessages(Constants.TIMEOUT_FOR_SEARCHING_DEVICE))
                        mHandler.removeMessages(Constants.TIMEOUT_FOR_SEARCHING_DEVICE);
                    setTextWithMessage(getString(R.string.mConfiguredSuccessfully) + " " + LibreApplication.thisSACConfiguredFromThisApp);
                                /* Toast.makeText(getApplicationContext(),"Libre App is Restarting .." , Toast.LENGTH_SHORT).show();*/
                    showDialogifDeviceNotFound(getString(R.string.mConfiguredSuccessfully) + " " + LibreApplication.thisSACConfiguredFromThisApp);
                    /*restartApp(ConnectingToMainNetwork.this);*/
                   /* Intent ssid = new Intent(ConnectingToMainNetwork.this,
                            NewSacActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(ssid);
                    finish();*/
                }

            } else {
                LibreLogger.d(this, "New Device Found But not we configured Device" + node.getFriendlyname());
               // mSACConfiguredIpAddress=null;
            }
        }
    }

    @Override
    public void deviceGotRemoved(String ipaddress) {

    }
  /* Shfit Piriorty and Save */

    @Override
    public void messageRecieved(NettyData packet) {

    }

    private int getMaxPriority(final WifiManager wifiManager) {
        final List<WifiConfiguration> configurations = wifiManager.getConfiguredNetworks();
        int pri = 0;
        for (final WifiConfiguration config : configurations) {
            if (config.priority > pri) {
                pri = config.priority;
            }
        }
        return pri;
    }

    /* Converting a String to Quoted String
    * Syntax for Setting SSID in wifi Conf : / SSIDNAME /*/
    private String convertToQuotedString(String string) {
        if (TextUtils.isEmpty(string)) {
            return "";
        }
        final int lastPos = string.length() - 1;
        if (lastPos > 0 && (string.charAt(0) == '"' && string.charAt(lastPos) == '"')) {
            return string;
        }
        return "\"" + string + "\"";
    }

    private int shiftPriorityAndSave(final WifiManager wifiMgr) {
        final List<WifiConfiguration> configurations = wifiMgr.getConfiguredNetworks();
        final int size = configurations.size();
        for (int i = 0; i < size; i++) {
            final WifiConfiguration config = configurations.get(i);
            config.priority = i;
            wifiMgr.updateNetwork(config);
        }
        wifiMgr.saveConfiguration();
        return size;
    }

    private String getScanResultSecurity(String mSecurity) {
        final String[] securityModes = {WEP, PSK};
        for (int i = securityModes.length - 1; i >= 0; i--) {
            if (mSecurity.contains(securityModes[i])) {
                return securityModes[i];
            }
        }
        return OPEN;
    }

    /**
     * Start to connect to a specific wifi network
     */
    private void connectToSpecificNetwork(final String mNetworkSsidToConnect, final String mNetworkPassKeyToConnect, final String mNetworkSecurityToConnect) {


        new Thread(new Runnable() {

            @Override

            public void run() {
                ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
                if (networkInfo.isConnected() && wifiInfo.getSSID().replace("\"", "").equals(mNetworkSsidToConnect)) {
                    mHandler.sendEmptyMessage(Constants.CONNECTED_TO_MAIN_SSID_SUCCESS);
                    return;
                }
                final WifiConfiguration conf = new WifiConfiguration();
                conf.allowedAuthAlgorithms.clear();
                conf.allowedGroupCiphers.clear();
                conf.allowedPairwiseCiphers.clear();
                conf.allowedProtocols.clear();

                conf.allowedKeyManagement.clear();

                LibreLogger.d(this, "Nework Security  Scan Resut To Connect From Function " + getScanResultSecurity(mNetworkSecurityToConnect));
                LibreLogger.d(this, "Network Security To Connect " + mNetworkSecurityToConnect);
                conf.SSID = convertToQuotedString(mNetworkSsidToConnect);  //convertToQuotedString(mScanResult.SSID);
                // Make it the highest priority.
                int newPri = getMaxPriority(mWifiManager) + 1;
                if (newPri > MAX_PRIORITY) {
                    newPri = shiftPriorityAndSave(mWifiManager);
                }
                LibreLogger.d(this, "Network Security To Connect " + mNetworkSecurityToConnect +
                        "priority Generated is " + newPri);
                conf.status = WifiConfiguration.Status.ENABLED;
                conf.priority = newPri;
                switch (getScanResultSecurity(mNetworkSecurityToConnect)) {
                    case WEP:
                        conf.wepKeys[0] = "\"" + mNetworkPassKeyToConnect + "\"";
                        conf.wepTxKeyIndex = 0;
                        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                        break;
                    case PSK:
                        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                        conf.preSharedKey = "\"" + mNetworkPassKeyToConnect + "\"";
                        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                        conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                        conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                        conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // For WPA2
                        conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA); // For WPA
                        break;
                    case OPEN:
                        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                        conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.NONE);
                        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                        break;
                }

                int netId = mWifiManager.addNetwork(conf);
                LibreLogger.d(this, "Netid We got For the Ssid is " + mNetworkSsidToConnect +
                        "and Netid is " + netId);
                if (netId == -1) {
                    LibreLogger.d(this, "Failed to set the settings for  " + mNetworkSsidToConnect);
                    final List<WifiConfiguration> mWifiConfiguration = mWifiManager.getConfiguredNetworks();
                    for (int i = 0; i < mWifiConfiguration.size(); i++) {
                        String configSSID = mWifiConfiguration.get(i).SSID;
                        LibreLogger.d(this, "Config SSID" + configSSID + "Active SSID" + conf.SSID);
                        if (configSSID.equals(conf.SSID)) {
                            netId = mWifiConfiguration.get(i).networkId;
                            LibreLogger.d(this, "network id" + netId);
                            break;
                        }
                    }
                }
                connectionReceiver = new ConnectionReceiver(mNetworkSsidToConnect);
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
                intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
                intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
                intentFilter.addAction("android.net.wifi.STATE_CHANGE");
                intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
                intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
                registerReceiver(connectionReceiver, intentFilter);
                mHandler.sendEmptyMessageDelayed(Constants.CONNECTED_TO_MAIN_SSID_FAIL, 120000);
                boolean mCatchDisconnect = mWifiManager.enableNetwork(netId, true);
                LibreLogger.d(this, "Wifi MAnager calling enableNetwork" + mCatchDisconnect);
            }

        }).start();

    }

    private void setTextWithMessage(final String mMessage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessageText.setText(mMessage);
            }
        });
    }

    private void showProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_LoadingDialog.setVisibility(View.VISIBLE);
            }
        });
    }

    private void closeProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_LoadingDialog.setVisibility(View.GONE);
            }
        });
    }
    private void callGoogleCastUpdateScreen(){
        ActivityCompat.finishAffinity(this);
        if(LibreApplication.GCAST_UPDATE_AVAILABE_LIST_DATA.containsKey(mSACConfiguredIpAddress)){
            LibreApplication.GCAST_UPDATE_AVAILABE_LIST_DATA.remove(mSACConfiguredIpAddress);
        }
        startActivity(new Intent(ConnectingToMainNetwork.this, GoogleCastUpdateAfterSac.class)
                .putExtra(SAC_CURRENT_IPADDRESS, mSACConfiguredIpAddress));
        finish();

    }
    @Override
    public void onStartComplete() {
        super.onStartComplete();
        LibreLogger.d(this, "OnCreate Called");
        showProgressDialog();
        /*Get Connected Ssid Name */
        String mConnectedSsidName = getconnectedSSIDname(this);
        if (mConnectedSsidName.equalsIgnoreCase(wifiConnect.getMainSSID())) {
            LibreLogger.d(this, "OnCreate MainSSIDSuccess ");
            mHandler.sendEmptyMessage(Constants.CONNECTED_TO_MAIN_SSID_SUCCESS);
        } else {
            mHandler.sendEmptyMessage(Constants.HTTP_POST_DONE_SUCCESSFULLY);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        disableNetworkChangeCallBack();
        disableNetworkOffCallBack();
        registerForDeviceEvents(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connecting_to_main_network);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);


        m_LoadingDialog = (ProgressBar) findViewById(R.id.wifiConnectionLoader);
        mMessageText = (TextView) findViewById(R.id.mMessageStatus);

        disableNetworkChangeCallBack();
        disableNetworkOffCallBack();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_connecting_to_main_network, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

*/        return super.onOptionsItemSelected(item);
    }

    private class ConnectionReceiver extends BroadcastReceiver {

        private final String mNetworkSsidToConnect;

        ConnectionReceiver(String ssid) {
            mNetworkSsidToConnect = ssid;
        }

        @Override

        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
            LibreLogger.d(this, "OnRecieve Method: Wifi Info " + wifiInfo.getSSID());
            LibreLogger.d(this, "OnRecieve Method: mNetworkSSIDToConnect " + mNetworkSsidToConnect);
            LibreLogger.d(this, "OnRecieve Method: Network State " + networkInfo.getState());
            LibreLogger.d(this, "OnRecieve Method: Network Detailed State " + networkInfo.getDetailedState());
            LibreLogger.d(this, "Supplicant State " + wifiInfo.getSupplicantState());
            if (wifiInfo.getSupplicantState() == SupplicantState.INACTIVE) {
                LibreError error = new LibreError("Not able to connect ", wifiConnect.getMainSSID() +
                        "Authentication Error  , App Will be closed ");
                BusProvider.getInstance().post(error);
                if (mHandler.hasMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL))
                    mHandler.removeMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL);
                mHandler.sendEmptyMessageDelayed(Constants.CONNECTED_TO_MAIN_SSID_FAIL, 60000);
            }
            boolean mVariable;
            if ((android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)) {
                mVariable=true;
            }else{
                mVariable = networkInfo != null && (networkInfo.isConnected());
            }
            /*if (networkInfo != null && (networkInfo.isConnected())) */
            if(mVariable){
                if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
                    LibreLogger.d(this, " 1 OnRecieve Wifi Info " + wifiInfo.getSSID());
                    LibreLogger.d(this, " 1 OnRecieve mNetworkSSIDToConnect " + mNetworkSsidToConnect);
                    LibreLogger.d(this, " 1 OnRecieve Network State " + networkInfo.getState());
                    if (wifiInfo.getSSID().contains(mNetworkSsidToConnect)) {
                        mHandler.sendEmptyMessageDelayed(Constants.CONNECTED_TO_MAIN_SSID_SUCCESS,500);
                    } else if (!wifiInfo.getSSID().contains(Constants.WAC_SSID)) {
                        LibreError error = new LibreError("Not able to connect to ", wifiConnect.getMainSSID() +
                                "  but Connected To " + wifiInfo.getSSID());
                        BusProvider.getInstance().post(error);
                        if (mHandler.hasMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL))
                            mHandler.removeMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL);
                        mHandler.sendEmptyMessage(Constants.CONNECTED_TO_DIFFERENT_SSID);
                    }/*else if (wifiInfo.getSSID().contains(Constants.WAC_SSID)){
                        if (mHandler.hasMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL))
                            mHandler.removeMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL);
                        mHandler.sendEmptyMessage(Constants.HTTP_POST_DONE_SUCCESSFULLY);
                    }*/
                }
            }
        }

    }
}