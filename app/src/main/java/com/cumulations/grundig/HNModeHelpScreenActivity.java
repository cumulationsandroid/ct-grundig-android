package com.cumulations.grundig;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by anirudh on 19/9/17.
 */

public class HNModeHelpScreenActivity extends AppCompatActivity{
    int[] mLayouts = {
            R.layout.helpscreen_layout
    };

    int[] mDrawables = {
            R.drawable.hn,
            R.drawable.hnmodestep2,
            R.drawable.gr_configure_xxxx_2
    };

    int[] mTexts = {
            R.string.hnmodestep1string,
            R.string.hnmodestep2string,
            R.string.hnmodestep3string
    };
    private ViewPager mViewPager;
    private CircleIndicator mCircleIndicator;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hnmode_helpscreen_activity);
        TextView backButton= (TextView) findViewById(R.id.back);
        mViewPager = (ViewPager)findViewById(R.id.viewpager);
        mCircleIndicator = (CircleIndicator)findViewById(R.id.viewpagerindicator);
        HelpScreenStepsPagerAdapter helpScreenStepsPagerAdapter = new HelpScreenStepsPagerAdapter();
        mViewPager.setAdapter(helpScreenStepsPagerAdapter);
        mCircleIndicator.setViewPager(mViewPager);
        backButton.setOnClickListener(

                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        {
                            onBackPressed();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        finish();
        /*if(mViewPager!=null && mViewPager.getCurrentItem()==0) {
            finish();
        }else {
            mViewPager.setCurrentItem(0);
        }*/
    }

    public class HelpScreenStepsPagerAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;
        Button mFinishButton;
        TextView mText;
        TextView mSubText;
        ImageView mConfigureImage;

        public HelpScreenStepsPagerAdapter() {
            mLayoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public int getCount() {
            return mTexts.length;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.helpscreen_layout, container, false);
            mFinishButton = (Button)itemView.findViewById(R.id.finishButton);
            mText = (TextView)itemView.findViewById(R.id.step_text);
            mSubText = (TextView)itemView.findViewById(R.id.image_subtext);
            mConfigureImage = (ImageView)itemView.findViewById(R.id.configure_image);
            // Below 2 methods are ugliest way to do the job, i know. should have gone with
            // the library or custom view.
            if (position == (mTexts.length - 1)) {
                mFinishButton.setVisibility(View.VISIBLE);
            }
            mFinishButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            String text = getResources().getString(R.string.tutorial_step)+" "+ (position+1);
            mText.setText(text);
            mSubText.setText(mTexts[position]);
            mConfigureImage.setImageResource(mDrawables[position]);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}
