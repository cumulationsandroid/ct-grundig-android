package com.cumulations.grundig.StaticInstructions;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cumulations.grundig.DeviceDiscoveryActivity;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.nowplaying.CumulationsNowPlayingActivity;
import com.cumulations.grundig.util.LibreLogger;

public class spotifyInstructions extends DeviceDiscoveryActivity implements View.OnClickListener, LibreDeviceInteractionListner {
    private TextView m_back;
    private TextView learnMore , openSpotify;
    private String currentIpAddress;
    private boolean isLaunchSpotifyButtonClicked;
    ScanningHandler mScanHandler = ScanningHandler.getInstance();
    private SceneObject currentSceneObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotify_instructions);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            currentIpAddress = extras.getString("current_source");
        }
        if (currentIpAddress != null) {
            currentSceneObject = mScanHandler.getSceneObjectFromCentralRepo(currentIpAddress); // ActiveSceneAdapter.mMasterSpecificSlaveAndFreeDeviceMap.get(currentIpAddress);
        }
        m_back = (TextView) findViewById(R.id.back);
        m_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        openSpotify = (TextView)findViewById(R.id.openSpotify);
        learnMore = (TextView)findViewById(R.id.learnMore);
        openSpotify.setOnClickListener(this);
        learnMore.setText(Html.fromHtml("<a href= https://spotify.com/connect > Learn More </a>"));
        learnMore.setMovementMethod(LinkMovementMethod.getInstance());
        registerForDeviceEvents(this);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.openSpotify:
                isLaunchSpotifyButtonClicked = true;
                String appPackageName = "com.spotify.music";
                launchTheApp(appPackageName);
                break;
        }
    }

    public void launchTheApp(String appPackageName) {

        Intent intent = getPackageManager().getLaunchIntentForPackage(appPackageName);
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            redirectingToPlayStore(intent, appPackageName);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        LUCIControl luciControl = new LUCIControl(currentIpAddress);
                /*Sending asynchronous registration*/
        luciControl.sendAsynchronousCommand();
        luciControl.SendCommand(50, null, 1);
    }

    public void redirectingToPlayStore(Intent intent, String appPackageName) {

        try {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + appPackageName));
            startActivity(intent);

        } catch (android.content.ActivityNotFoundException anfe) {

            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName));
            startActivity(intent);

        }

    }

    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

    }

    @Override
    public void deviceGotRemoved(String ipaddress) {

    }

    @Override
    public void messageRecieved(NettyData dataRecived) {
        LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());
        String ipaddressRecieved = dataRecived.getRemotedeviceIp();
        LibreLogger.d(this, "Message recieved for ipaddress " + ipaddressRecieved + "command is " + packet.getCommand());

        String message = new String(packet.getpayload());

        if (currentIpAddress.equalsIgnoreCase(ipaddressRecieved)) {
            switch (packet.getCommand()) {

                case 50:
                    if (message.equalsIgnoreCase ("" + Constants.SPOTIFY_SOURCE) && isLaunchSpotifyButtonClicked) {
                        Intent intent = new Intent(this, CumulationsNowPlayingActivity.class);
                        intent.putExtra("current_ipaddress", currentIpAddress);
                        startActivity(intent);
                    }
                    break;
            }
        }
    }
}
