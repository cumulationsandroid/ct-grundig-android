package com.cumulations.grundig.StaticInstructions;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cumulations.grundig.R;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Anirudh Uppunda on 16/10/17.
 */

public class SpotifyInstructionsActivity extends AppCompatActivity {
    int[] mLayouts = {
            R.layout.helpscreen_layout
    };

    int[] mDrawables = {
            R.drawable.spotify_tutorial_1,
            R.drawable.tutorial_2_spotify_2x,
            R.drawable.spotify_tutorial_3
    };

    int[] mTexts = {
            R.string.spotifystep1string,
            R.string.spotifystep2string,
            R.string.spotifystep3string
    };

    private ViewPager mViewPager;
    private CircleIndicator mCircleIndicator;
    private TextView skip;
    private TextView back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotify_helpscreen_activity);
        skip = (TextView) findViewById(R.id.back);
        back = (TextView) findViewById(R.id.backbtn);
        mViewPager = (ViewPager)findViewById(R.id.viewpager);
        mCircleIndicator = (CircleIndicator)findViewById(R.id.viewpagerindicator);

        skip.setSelected(true);     back.setSelected(true);     findViewById(R.id.scene_title).setSelected(true);
        HelpScreenStepsPagerAdapter helpScreenStepsPagerAdapter = new HelpScreenStepsPagerAdapter();
        mViewPager.setAdapter(helpScreenStepsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                if (position == 2) {
//                    skip.setText(getResources().getString(R.string.openSpotify));
//                } else {
                    skip.setText(getResources().getString(R.string.skip));
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mCircleIndicator.setViewPager(mViewPager);
        skip.setOnClickListener(

                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        {
                            skipTutorial();
                        }
                    }
                });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void skipTutorial() {
        /*if(mViewPager!=null && mViewPager.getCurrentItem() < 2) {
            mViewPager.setCurrentItem(2);
        } else {*/
            String appPackageName = "com.spotify.music";
            launchTheApp(appPackageName);
        //}
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public class HelpScreenStepsPagerAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;
        Button mFinishButton;
        TextView mText;
        TextView mSubText;
        ImageView mConfigureImage;

        public HelpScreenStepsPagerAdapter() {
            mLayoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public int getCount() {
            return mTexts.length;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.helpscreen_layout, container, false);
            mFinishButton = (Button)itemView.findViewById(R.id.finishButton);
            mText = (TextView)itemView.findViewById(R.id.step_text);
            mSubText = (TextView)itemView.findViewById(R.id.image_subtext);
            mConfigureImage = (ImageView)itemView.findViewById(R.id.configure_image);
            if (position == (mTexts.length - 1)) {
                mFinishButton.setVisibility(View.VISIBLE);
            }
            mFinishButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String appPackageName = "com.spotify.music";
                    launchTheApp(appPackageName);
                }
            });
            String text = getResources().getString(R.string.tutorial_step)+" "+ (position+1);
            mText.setText(text);
            mSubText.setText(mTexts[position]);
            mConfigureImage.setImageResource(mDrawables[position]);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    public void launchTheApp(String appPackageName) {

        Intent intent = getPackageManager().getLaunchIntentForPackage(appPackageName);
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            redirectingToPlayStore(intent, appPackageName);
        }

    }


    public void redirectingToPlayStore(Intent intent, String appPackageName) {

        try {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + appPackageName));
            startActivity(intent);

        } catch (android.content.ActivityNotFoundException anfe) {

            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName));
            startActivity(intent);

        }

    }


}
