package com.cumulations.grundig;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.ManageDevice.CreateNewScene;
import com.cumulations.grundig.Network.NewSacActivity;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanThread;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;
import com.github.florent37.viewtooltip.ViewTooltip;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by praveena on 7/24/15.
 */
public class PlayNewActivity extends DeviceDiscoveryActivity implements View.OnClickListener, LibreDeviceInteractionListner {

    ProgressDialog mProgressDialog;
    private ScanningHandler mScanHandler = ScanningHandler.getInstance();
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private TextView mAppVersion;
    private ViewTooltip.TooltipView toolTipView;
    private TextView dummyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cumulations_activity_playnew);
        // No devices found.
        if(mScanHandler.getFreeDeviceList().size() <= 0){
            LibreLogger.d(this,"There are no devices found. Hence navigating to Configure activity"+mScanHandler.getFreeDeviceList().size());
            startActivity(new Intent(PlayNewActivity.this,ConfigureActivity.class));
            finish();
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        dummyText = (TextView)findViewById(R.id.dummy);
        setSupportActionBar(toolbar);
        initNavigationDrawer();

        /*findViewById(R.id.favorites_icon).setOnClickListener(this);
        findViewById(R.id.playnew_icon).setOnClickListener(this);
        findViewById(R.id.config_item).setOnClickListener(this);*/

        findViewById(R.id.play_new_button).setOnClickListener(this);


    }

    Handler refreshZonesHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (ScanningHandler.getInstance().centralSceneObjectRepo.size() > 0) {
                refreshZonesHandler.removeCallbacksAndMessages(null);
                Intent newIntent = new Intent(PlayNewActivity.this, ActiveScenesListActivity.class);
                startActivity(newIntent);
                finish();
            }
            refreshZonesHandler.sendEmptyMessageDelayed(1, 3000);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerForDeviceEvents(this);
        /*Karuna For Testing ... */
        setTheFooterIcons();
        /*Karuna For Testing ... */
        /*refreshDevices();*/

        setTheFooterIcons();

        if (ScanningHandler.getInstance().centralSceneObjectRepo.size() > 0) {
            refreshZonesHandler.removeCallbacksAndMessages(null);
            Intent newIntent = new Intent(PlayNewActivity.this, ActiveScenesListActivity.class);
            startActivity(newIntent);
            finish();
        } else if (refreshZonesHandler!=null)
            refreshZonesHandler.sendEmptyMessageDelayed(1, 3000);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                View dummyToolTipView = findViewById(R.id.dummy);
                showViewToolTip(dummyToolTipView,getString(R.string.click_to_open_menu_help), ViewTooltip.Position.BOTTOM, true);
            }
        },3500);*/

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                View playNewBtn = findViewById(R.id.play_new_button);
                showViewToolTip(playNewBtn,getString(R.string.add_one_more_zone), ViewTooltip.Position.TOP, true);

            }
        });

    }

    public String getVersion(Context context) {
        String Version = getString(R.string.title_activity_welcome);
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pInfo != null)
            Version = pInfo.versionName;

        return Version;
    }

    public void initNavigationDrawer() {

        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation_view);
        // 7th item is the app version in menu item
        mAppVersion = (TextView) navigationView.getMenu().getItem(8).getActionView().findViewById(R.id.appversion);
        try {
            mAppVersion.setText(getString(R.string.version)+" : "+getVersion(getApplicationContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*Enabling marquee for Navigation Menu items*/
        TextView addNewSpeakerText = (TextView) navigationView.getMenu().getItem(1).getActionView().findViewById(R.id.marqueeTitle);
        addNewSpeakerText.setSelected(true);

        TextView createNewZoneTv = (TextView) navigationView.getMenu().getItem(2).getActionView().findViewById(R.id.marqueeTitle);
        createNewZoneTv.setText(getString(R.string.create_new_zone));
        createNewZoneTv.setSelected(true);

        TextView whatIsHnModeTv = (TextView) navigationView.getMenu().getItem(4).getActionView().findViewById(R.id.marqueeTitle);
        whatIsHnModeTv.setText(getString(R.string.what_is_hn_mode));
        whatIsHnModeTv.setPadding(100,0,0,0);
        whatIsHnModeTv.setSelected(true);

        TextView whatIsSAModeTv = (TextView) navigationView.getMenu().getItem(5).getActionView().findViewById(R.id.marqueeTitle);
        whatIsSAModeTv.setText(getString(R.string.what_is_sa_mode));
        whatIsSAModeTv.setPadding(100,0,0,0);
        whatIsSAModeTv.setSelected(true);

        TextView howToCreateNewZoneTv = (TextView) navigationView.getMenu().getItem(6).getActionView().findViewById(R.id.marqueeTitle);
        howToCreateNewZoneTv.setText(getString(R.string.how_to_create_master));
        howToCreateNewZoneTv.setPadding(100,0,0,0);
        howToCreateNewZoneTv.setSelected(true);

        TextView howToAddNewSpeakerTv = (TextView) navigationView.getMenu().getItem(7).getActionView().findViewById(R.id.marqueeTitle);
        howToAddNewSpeakerTv.setText(getString(R.string.how_to_add_new_speaker));
        howToAddNewSpeakerTv.setPadding(100,0,0,0);
        howToAddNewSpeakerTv.setSelected(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id){
                    case R.id.home:
                        //Toast.makeText(getApplicationContext(),"Home",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PlayNewActivity.this, AddNewSpeakerToWifiHelpScreenActivity.class);
                        startActivity(intent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpSAMode:
                        Intent helpSaModeIntent = new Intent(PlayNewActivity.this, SAModeHelpScreenActivity.class);
                        startActivity(helpSaModeIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpHNMode:
                        Intent helpHnModeIntent = new Intent(PlayNewActivity.this, HNModeHelpScreenActivity.class);
                        startActivity(helpHnModeIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpCreateMaster:
                        Intent helpCreateMasterIntent = new Intent(PlayNewActivity.this, CreateMasterHelpScreenActivity.class);
                        startActivity(helpCreateMasterIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.addNewSpeaker:
                        Intent addNewSpeakerIntent = new Intent(PlayNewActivity.this, AddNewSpeakerHelpScreenActivity.class);
                        startActivity(addNewSpeakerIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.createNewZone:
                        Intent i = new Intent(PlayNewActivity.this, CreateNewScene.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.logout:
                        //finish();

                }
                return true;
            }
        });
        View header = navigationView.getHeaderView(0);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){

            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    public void refreshDevices(){
        LibreLogger.d(this,"RefreshDevices Without clearing");
        final LibreApplication application = (LibreApplication) getApplication();
        application.getScanThread().clearNodes();
        application.getScanThread().UpdateNodes();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();
            }
        }, 150);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 650);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1150);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1650);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 2150);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                application.getScanThread().UpdateNodes();
            }
        }, 5650);
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (toolTipView != null) {
            toolTipView.remove();
        }
        unRegisterForDeviceEvents();
    }

    @Override
    public void onBackPressed() {
        ScanningHandler mScanHandler = ScanningHandler.getInstance();

        if (mScanHandler.centralSceneObjectRepo.size()<1 || LSSDPNodeDB.getInstance().isAnyMasterIsAvailableInNetwork())
        {
            Intent intentx = new Intent(Intent.ACTION_MAIN);
            intentx.addCategory(Intent.CATEGORY_HOME);
            intentx.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
            startActivity(intentx);
            finish();
            System.exit(0);

        }else {
            super.onBackPressed();
            finish();
        }
   /*     Intent in = new Intent(PlayNewActivity.this, DMRDeviceListenerForegroundService.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(in);

        finish();
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (refreshZonesHandler != null)
            refreshZonesHandler.removeMessages(1);
        releaseAllHandler.removeCallbacksAndMessages(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.favorites_icon:


                if (LSSDPNodeDB.isCastDevicesExistsInTheNetwork()){

              /*      SharedPreferences sharedPreferences = getApplicationContext()
                            .getSharedPreferences(Constants.SHOWN_GOOGLE_TOS, Context.MODE_PRIVATE);
                    String shownGoogle = sharedPreferences.getString(Constants.SHOWN_GOOGLE_TOS, null);*/

                    // now TOS is recieved in msg box 224, so no need to check shared pref for this dialog

                /*    String shownGoogle = "Yes";
                    if (shownGoogle == null) {
                        showGoogleTosDialog(PlayNewActivity.this);
                    }
                    else {
                        Intent intent=new Intent(PlayNewActivity.this,GcastSources.class);
                        startActivity(intent);
                    }*/

                    //when play cast is clicked, open gCast app
                    openGHomeDialog();
                }
                else {
                    Toast.makeText(this, getString(R.string.favourits_not_implemented), Toast.LENGTH_LONG).show();
                }

                break;


            case R.id.playnew_icon:
                //Toast.makeText(this, "Play New", Toast.LENGTH_SHORT).show();
                Intent newIntent = new Intent(PlayNewActivity.this, CreateNewScene.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);
                finish();
                break;

            case R.id.config_item:
                //Toast.makeText(this, "Config", Toast.LENGTH_SHORT).show();
                Intent newIntent2 = new Intent(PlayNewActivity.this, NewSacActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent2);
                finish();
                break;

            case R.id.play_new_button:
                refreshZonesHandler.removeCallbacksAndMessages(null);
               // Toast.makeText(this, "Play New", Toast.LENGTH_SHORT).show();
                Intent newIntent1 = new Intent(PlayNewActivity.this, CreateNewScene.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                newIntent1.putExtra("selectspeakerscene", "true");
                startActivity(newIntent1);
                /*Don't kill first time to keep in stack*/
                //finish();
                break;
        }
    }


    Handler releaseAllHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == ActiveScenesListActivity.RELEASE_ALL_STARTED) {
                showLoader();
            }

            if (msg.what == ActiveScenesListActivity.RELEASE_ALL_SUCCESS) {
                closeLoader();
                releaseAllHandler.removeMessages(ActiveScenesListActivity.RELEASE_ALL_TIMEOUT);
                         /*making sure to call CreateNewScene activity*/
                Intent intent = new Intent(PlayNewActivity.this, CreateNewScene.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }

            if (msg.what == ActiveScenesListActivity.RELEASE_ALL_TIMEOUT) {
                closeLoader();
                         /*making sure to call CreateNewScene activity*/
                Intent intent = new Intent(PlayNewActivity.this, CreateNewScene.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        }
    };


    private void showLoader() {

        if (mProgressDialog == null) {
            Log.e("LUCIControl", "Null");
            if (!(PlayNewActivity.this.isFinishing())) {
                mProgressDialog = ProgressDialog.show(PlayNewActivity.this, getString(R.string.notice), getString(R.string.pleaseWait), true, true, null);
            }
        }
        mProgressDialog.setCancelable(false);

        if (!mProgressDialog.isShowing()) {
            Log.e("LUCIControl", "is Showing");
            if (!(PlayNewActivity.this.isFinishing())) {
                mProgressDialog = ProgressDialog.show(PlayNewActivity.this, getString(R.string.notice), getString(R.string.pleaseWait), true, true, null);
            }
            mProgressDialog.show();

        }

    }

    private void closeLoader() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            Log.e("LuciControl", "progress Dialog Closed");
            mProgressDialog.setCancelable(true);
            mProgressDialog.dismiss();
            mProgressDialog.cancel();
        }
    }

  /*This function gets Called When Drop All Pressed*/

    private void dropAllDevices(){
        ScanningHandler mScanningHandler = ScanningHandler.getInstance();
        ConcurrentHashMap<String, SceneObject> centralSceneObjectRepo = mScanningHandler.getSceneObjectFromCentralRepo();

            /*which means no master present hence all devices are free so need to do anything*/
        if (centralSceneObjectRepo == null || centralSceneObjectRepo.size() == 0) {
            LibreLogger.d(this, "No master present");
            return;
        }

        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
        LSSDPNodes mNodeToSendDropAll = mNodeDB.GetDB().get(0);

        LUCIControl mLuciControlToSendDropAll = new LUCIControl(mNodeToSendDropAll.getIP());
        mLuciControlToSendDropAll.SendCommand(MIDCONST.MID_JOIN_OR_DROP,LUCIMESSAGES.DROP_ALL,LSSDPCONST.LUCI_SET);

        /*clearing central repository*/
        mScanningHandler.clearSceneObjectsFromCentralRepo();
    }

    /* This function gets called when release all pressed*/
    private void releaseAllDevicesDialogue(int size) {

        String numberOfDevices = size + " ";
        if (!PlayNewActivity.this.isFinishing()) {


            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    PlayNewActivity.this);
            alertDialogBuilder.setTitle(getString(R.string.doYouWantToFree));
            alertDialogBuilder
                    .setMessage(numberOfDevices + getString(R.string.devicesText))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            /**
                             * sending free to all devices including DB
                             */

                            dialog.dismiss();

                            releaseAllHandler.sendEmptyMessage(ActiveScenesListActivity.RELEASE_ALL_STARTED);

                            /* Drop All Devices */
                            dropAllDevices();
                         /*this is synchronous function*//*
                            releaseAllMasterandSlave();*/
               /*sometime we have slave without master this will handle boundary condition */
                         /*this is synchronous function*/
                            freeAllDevicesFromDB();


                            releaseAllHandler.sendEmptyMessage(ActiveScenesListActivity.RELEASE_ALL_TIMEOUT);

                            ScanThread.getInstance().UpdateNodes();


                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*//noinspection SimplifiableIfStatement
        if (id == R.id.release_all) {
            try {

                ArrayList<LSSDPNodes> nodesList = LSSDPNodeDB.getInstance().GetDB();
                String[] mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);

                if (nodesList == null || mMasterIpKeySet.length == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.NO_DEVICES_TO_DROP),Toast.LENGTH_SHORT).show();
                    return super.onOptionsItemSelected(item);
                }

                releaseAllDevicesDialogue(nodesList.size());

                return true;
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, getString(R.string.some_error_occured_while_freeing), Toast.LENGTH_SHORT).show();
            }
        }
*/
        return super.onOptionsItemSelected(item);
    }


    /*Releasing all devices*/
    private boolean releaseAllMasterandSlave() {

        ScanningHandler mScanningHandler = ScanningHandler.getInstance();
        ConcurrentHashMap<String, SceneObject> centralSceneObjectRepo = mScanningHandler.getSceneObjectFromCentralRepo();

        /*which means no master present hence all devices are free so need to do anything*/
        if (centralSceneObjectRepo == null || centralSceneObjectRepo.size() == 0) {
            LibreLogger.d(this, "No master present");
            return false;
        }

        for (String masterIPAddress : centralSceneObjectRepo.keySet()) {
            ArrayList<LSSDPNodes> lssdpNodesArrayList = mScanningHandler
                    .getSlaveListForMasterIp(masterIPAddress, mScanningHandler.getconnectedSSIDname(getApplicationContext()));

            if (lssdpNodesArrayList != null && lssdpNodesArrayList.size() != 0) {
                for (LSSDPNodes mSlaves : lssdpNodesArrayList) {
                /*freeing slave for particular master*/
                    LUCIControl luciControl = new LUCIControl(mSlaves.getIP());
                    luciControl.sendAsynchronousCommand();
                    luciControl.SendCommand(MIDCONST.MID_DDMS, LUCIMESSAGES.SETFREE, LSSDPCONST.LUCI_SET);

                    try {
                        Thread.sleep(150);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            /*freeing master*/
            LUCIControl luciControl = new LUCIControl(masterIPAddress);
            luciControl.sendAsynchronousCommand();
            luciControl.SendCommand(MIDCONST.MID_DDMS, LUCIMESSAGES.SETFREE, LSSDPCONST.LUCI_SET);
        }
        /*clearing central repository*/
        mScanningHandler.clearSceneObjectsFromCentralRepo();
        return true;
    }

    /*freeing all devices from DB and clearing it*/
    private void freeAllDevicesFromDB() {

        ArrayList<LSSDPNodes> nodesList = LSSDPNodeDB.getInstance().GetDB();
        if (nodesList != null && nodesList.size() > 0) {

            for (LSSDPNodes nodes : nodesList) {
                LUCIControl luciControl = new LUCIControl(nodes.getIP());
                    /*send this command only when you bother about 103*/
                luciControl.sendAsynchronousCommand();
                luciControl.SendCommand(MIDCONST.MID_DDMS, LUCIMESSAGES.SETFREE, LSSDPCONST.LUCI_SET);
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        LSSDPNodeDB.getInstance().clearDB();

    }

    public void sendMesearchUpdateNodes() {

        final LibreApplication application = (LibreApplication) getApplication();
        application.getScanThread().UpdateNodes();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();
            }
        }, 500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1000);

    }


    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

        if (refreshZonesHandler != null)
            refreshZonesHandler.removeMessages(1);
        if (node.getDeviceState().contains("M")) {
            if (!mScanHandler.isIpAvailableInCentralSceneRepo(node.getIP())) {
                SceneObject sceneObjec = new SceneObject(" ", node.getFriendlyname(), 0, node.getIP());
                mScanHandler.putSceneObjectToCentralRepo(node.getIP(), sceneObjec);
            }
            updateSceneObjectAndLaunchActiveScene(node.getIP());
        }


    }

    @Override
    public void deviceGotRemoved(String ipaddress) {

    }

    @Override
    public void messageRecieved(NettyData packet) {

        Log.e("PlayNewActivity", "MessageReceived" + new String(packet.getMessage()));

        LUCIPacket mLucipacket = new LUCIPacket(packet.getMessage());
        if (mLucipacket.getCommand() == 103) {
            String msg = new String(packet.getMessage());
            /*which means master is present now so prompting to ActiveScenesListActivity*/

            if (msg.contains("MASTER")) {
                updateSceneObjectAndLaunchActiveScene(packet.getRemotedeviceIp());
            }


        }

    }
    /*this method will get called when we are getting master 103*/
    private void updateSceneObjectAndLaunchActiveScene(String ipaddress) {

        LSSDPNodes mMasterNode = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);

            if (mMasterNode!=null) {

                if (!mScanHandler.isIpAvailableInCentralSceneRepo(ipaddress)) {
                    SceneObject mMastersceneObjec = new SceneObject(" ", mMasterNode.getFriendlyname(), 0, mMasterNode.getIP());
                    mScanHandler.putSceneObjectToCentralRepo(ipaddress, mMastersceneObjec);
                }
                if(LibreApplication.mCleanUpIsDoneButNotRestarted==false) {
                    Intent intent = new Intent(PlayNewActivity.this, ActiveScenesListActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }

        }

    public void setTheFooterIcons() {

        if( LSSDPNodeDB.isCastDevicesExistsInTheNetwork()) {

            TextView icon = (TextView) findViewById(R.id.favorites_icon);
            Drawable top = getResources().getDrawable(R.mipmap.ic_cast_black_24dp);
            icon.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
            icon.setText(R.string.favorite_button_playcast);
        }else{
            TextView icon = (TextView) findViewById(R.id.favorites_icon);
         /*   Drawable top = getResources().getDrawable(R.mipmap.ic_favorites);
            icon.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
            icon.setText(R.string.favorite_button);*/
            icon.setVisibility(View.GONE);
        }
    }

    public void showViewToolTip(View view, String text, ViewTooltip.Position position, final boolean shouldRepeat) {
        SharedPreferences sharedPreferences = getSharedPreferences("apppreference", MODE_PRIVATE);

        if (view ==null)
        {
            //Toast.makeText(this,"Hello", Toast.LENGTH_LONG).show();
            return;
        }

        if (!sharedPreferences.getBoolean(PlayNewActivity.class.getSimpleName(),false)) {
            sharedPreferences.edit().putBoolean(PlayNewActivity.class.getSimpleName(),true).apply();

            /*if (!shouldRepeat) {
                ViewTooltip
                        .on(view)
                        .autoHide(false,1000)
                        .clickToHide(true)
                        .align(ViewTooltip.ALIGN.START)
                        .position(ViewTooltip.Position.RIGHT)
                        .text(text)
                        .textColor(Color.WHITE)
                        .color(getResources().getColor(R.color.blue))
                        .corner(10)
                        .show();
            } else {*/
            toolTipView = ViewTooltip
                    .on(view)
                    .autoHide(true,3000)
                    .clickToHide(true)
                    .align(ViewTooltip.ALIGN.CENTER)
                    .position(position)
                    .text(text)
                    .textColor(getResources().getColor(R.color.viewToolTipTextColor))
                    .color(getResources().getColor(R.color.viewToolTipColor))
                    .corner(10)
                    .onHide(new ViewTooltip.ListenerHide() {
                        @Override
                        public void onHide(View view) {
                            if (shouldRepeat) {
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        View playNewBtn = findViewById(R.id.play_new_button);
                                        showViewToolTip(playNewBtn, getString(R.string.add_one_more_zone), ViewTooltip.Position.TOP, false);

                                    }
                                });
                            }
                        }
                    })
                    .show();


        }

        return;
    }

}