package com.cumulations.grundig.SourceOptions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cumulations.grundig.DeviceDiscoveryFragment;
import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.R;
import com.cumulations.grundig.RemoteSourcesList;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;
import com.github.johnpersano.supertoasts.SuperToast;
//import com.squareup.leakcanary.RefWatcher;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cumulations on 25/7/17.
 */

public class CumulationsLocalSourcesFragment extends DeviceDiscoveryFragment implements LibreDeviceInteractionListner {

    public static CumulationsLocalSourcesFragment fragment;
    private RecyclerView mRecyclerView;
    private CumulationsLocalSourcesRecyclerAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private String currentIpAddress;
    private boolean btStatus;
    private boolean AuxStatus;

    private static final String TAG_CMD_ID = "CMD ID";
    private static final String TAG_WINDOW_CONTENT = "Window CONTENTS";
    private static final String TAG_BROWSER = "Browser";
    private final int CREDENTIAL_USER_NAME = 31;
    private final int CREDENTIAL_PASSWORD = 32;
    private final int CREDENTIAL_TIMEOUT = 330;
    private final int TIMEOUT = 3000;

    final int NETWORK_TIMEOUT = 101;
    private final int ACTION_INITIATED = 12345;
    public static final String GET_HOME = "GETUI:HOME";
    public static int current_source_index_selected = -1;
    public static boolean shouldShowCasting = false;


    String[] localSources;
    private ProgressDialog m_progressDlg;
    Fragment mFragment;
    private ScanningHandler scanningHandler = ScanningHandler.getInstance();

    public static CumulationsLocalSourcesFragment newInstance(String currentIpAddress, boolean btStatus, boolean AuxStatus) {


        Bundle args = new Bundle();
        args.putString("IPADDRESS", currentIpAddress);
        args.putBoolean("btstatus", btStatus);
        args.putBoolean("auxstatus", AuxStatus);
        fragment = new CumulationsLocalSourcesFragment();
        fragment.setArguments(args);


        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (scanningHandler.getconnectedSSIDname(getActivity()) == ScanningHandler.SA_MODE) {
            localSources = new String[]{getString(R.string.local_mydevice), getString(R.string.local_bluetooth), getString(R.string.local_USB), getString(R.string.local_auxin)};
        } else {
            localSources = new String[]{getString(R.string.local_mydevice), getString(R.string.local_bluetooth), getString(R.string.local_USB), getString(R.string.local_auxin)};
        }

        mFragment = fragment;
        currentIpAddress = getArguments().getString("IPADDRESS");
        btStatus = getArguments().getBoolean("btstatus");
        AuxStatus = getArguments().getBoolean("auxstatus");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cumulations_local_sources, container, false);
        mRecyclerView = (RecyclerView)(view.findViewById(R.id.recycler_view));
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new CumulationsLocalSourcesRecyclerAdapter(localSources, getActivity(), currentIpAddress, btStatus, AuxStatus, mFragment);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration divider = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.cumulations_recycler_divider));
        mRecyclerView.addItemDecoration(divider);
        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment = null;
//        RefWatcher refWatcher = LibreApplication.getRefWatcher(getActivity());
//        refWatcher.watch(this);
    }

    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

    }

    @Override
    public void deviceGotRemoved(String ipaddress) {

    }

    @Override
    public void messageRecieved(NettyData dataRecived) {
        byte[] buffer = dataRecived.getMessage();
        String ipaddressRecieved = dataRecived.getRemotedeviceIp();

        LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());
        LibreLogger.d(this, "Message recieved for ipaddress " + ipaddressRecieved + "command is " + packet.getCommand());

        if (currentIpAddress.equalsIgnoreCase(ipaddressRecieved)) {
            switch (packet.getCommand()) {
                case 42: {

                    String message = new String(packet.getpayload());
                    LibreLogger.d(this, " message 42 recieved  " + message);
                    try {
                        parseJsonAndReflectInUI(message);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        LibreLogger.d(this, " Json exception ");

                    }
                }
                break;

                case 208: {
                    /* This code is crashing for array out of index exception and hence will be handled with a try and catch -Praveen*/
                    try {
                        String messages = new String(packet.getpayload());
                        LibreLogger.d(this, " got deezer " + messages);
                        String credentials[] = messages.split(":");

                        if (credentials.length == 0)
                            return;

                        String userName = "";
                        String userPassword = "";
                        String tidalUserName = "";
                        String tidalUserPassword = "";

                        /*Added for device state*/
                        Message message = Message.obtain();


                        if (credentials[0].contains("DeezerUserName")) {
                            userName = credentials[1];

                            message.obj = userName;
                            message.what = CREDENTIAL_USER_NAME;
                            credentialHandler.sendMessage(message);
                            //mOnOnlineSourcesMessageRecieved.sendUsername(userName);

                        }
                        if (credentials[0].contains("DeezerUserPassword")) {
                            userPassword = credentials[1];

                            message.obj = userPassword;
                            message.what = CREDENTIAL_PASSWORD;
                            credentialHandler.sendMessage(message);

                            //mOnOnlineSourcesMessageRecieved.sendPassword(userPassword);

                        }
                        if (credentials[0].contains("TidalUserName")) {
                            tidalUserName = credentials[1];

                            message.obj = tidalUserName;
                            message.what = CREDENTIAL_USER_NAME;
                            credentialHandler.sendMessage(message);


                        }
                        if (credentials[0].contains("TidalUserPassword")) {
                            tidalUserPassword = credentials[1];

                            message.obj = tidalUserPassword;
                            message.what = CREDENTIAL_PASSWORD;
                            credentialHandler.sendMessage(message);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        LibreLogger.d(this, "Credentials not read! Error!! Khajan Bhai!");
                    }
                }
                break;
            }
        }
    }



    private void parseJsonAndReflectInUI(String jsonStr) throws JSONException {

        LibreLogger.d(this, "Json Recieved from remote device " + jsonStr);
        if (jsonStr != null) {

            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeLoader();
                    }
                });

                if (credentialHandler.hasMessages(NETWORK_TIMEOUT))
                    credentialHandler.removeMessages(NETWORK_TIMEOUT);

                JSONObject root = new JSONObject(jsonStr);
                int cmd_id = root.getInt(TAG_CMD_ID);
                JSONObject window = root.getJSONObject(TAG_WINDOW_CONTENT);

                LibreLogger.d(this, "Command Id" + cmd_id);

                if (cmd_id == 1) {
                    /*
                      */
                    String Browser = window.getString(TAG_BROWSER);
                    /*if (Browser.equalsIgnoreCase("HOME")) {

                        *//* Now we have succseefully got the stack intialiized to home *//*
                        credentialHandler.removeMessages(NETWORK_TIMEOUT);
                        unRegisterForDeviceEvents();
                        Intent intent = new Intent(getContext(), RemoteSourcesList.class);
                        intent.putExtra("current_ipaddress", currentIpAddress);
                        intent.putExtra("current_source_index_selected", current_source_index_selected);
                        LibreLogger.d(this, "removing handler message");
                        startActivity(intent);
                        getActivity().finish();
                    }*/
                }
            } catch (Exception e) {

            }
        }
    }



    /*this method is to show error in whole application*/
    public void showErrorMessage(final LibreError message) {
        try {

            if (LibreApplication.hideErrorMessage)
                return;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    LibreLogger.d(this, "Showing the supertaloast " + System.currentTimeMillis());
                    SuperToast superToast = new SuperToast(getContext());

                    if (message != null && message.getErrorMessage().contains("is no longer available")) {
                        LibreLogger.d(this, "Device go removed showing error in Device Discovery");
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                    }
                    if(message.getmTimeout()==0) {//TimeoutDefault
                        superToast.setDuration(SuperToast.Duration.LONG);
                    }else{
                        superToast.setDuration(SuperToast.Duration.VERY_SHORT);
                    }
                    superToast.setText("" + message);
                    superToast.setAnimations(SuperToast.Animations.FLYIN);
                    superToast.setIcon(SuperToast.Icon.Dark.INFO, SuperToast.IconPosition.LEFT);
                    superToast.show();
                    if (message != null && message.getErrorMessage().contains(""))
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showLoader() {

        if (getActivity().isFinishing())
            return;

        if (m_progressDlg == null) {
            m_progressDlg = new ProgressDialog(getActivity());
        }
        m_progressDlg.setMessage(getString(R.string.loading));
        m_progressDlg.setCancelable(false);
           /* m_progressDlg.setButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    m_progressDlg.cancel();
                }
            });*/
//            m_progressDlg.show();


        if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }
    }

    private void closeLoader() {

        if (m_progressDlg != null) {

            if (m_progressDlg.isShowing() == true) {
                m_progressDlg.dismiss();
            }

        }

    }

    Handler credentialHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == NETWORK_TIMEOUT) {
                LibreLogger.d(this, "recieved handler msg");
                closeLoader();

                /*showing error to user*/
                LibreError error = new LibreError(currentIpAddress, Constants.INTERNET_ITEM_SELECTED_TIMEOUT_MESSAGE);
                showErrorMessage(error);
//                Toast.makeText(getApplicationContext(), Constants.LOADING_TIMEOUT_REBOOT, Toast.LENGTH_SHORT).show();
            }

            if (msg.what == ACTION_INITIATED) {
                showLoader();
            }


            if (msg.what == CREDENTIAL_USER_NAME) {
                //userName = (String) msg.obj;
            }

            if (msg.what == CREDENTIAL_PASSWORD) {
                //userPassword = (String) msg.obj;
            }

            if (msg.what == CREDENTIAL_TIMEOUT) {

                if (!(getActivity().isFinishing())) {

                }
            }
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        credentialHandler.removeCallbacksAndMessages(null);
    }
}
