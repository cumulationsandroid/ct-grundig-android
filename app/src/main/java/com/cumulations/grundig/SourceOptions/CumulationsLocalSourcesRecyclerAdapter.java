package com.cumulations.grundig.SourceOptions;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.app.dlna.dmc.LocalDMSActivity;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.Sources;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.github.johnpersano.supertoasts.SuperToast;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by cumulations on 25/7/17.
 */

public class CumulationsLocalSourcesRecyclerAdapter extends RecyclerView.Adapter<CumulationsLocalSourcesRecyclerAdapter.MyViewHolder> {

    private final ScanningHandler scanningHandler = ScanningHandler.getInstance();
    private boolean saMode;
    private String[] localSources;
    private int[] images = {R.drawable.localcontent, /*R.drawable.favorite, R.drawable.wifi,*/ R.drawable.bluetooth, R.drawable.usb, /*R.drawable.sdcard,*/ R.drawable._aux};
    private Context context;
    private String currentIpAddress;

    final int NETWORK_TIMEOUT = 101;
    final int AUX_BT_TIMEOUT = 0x2;
    private final int ACTION_INITIATED = 12345;
    private final int BT_AUX_INITIATED = 12345;
    private ProgressDialog m_progressDlg;

    public static final String GET_HOME = "GETUI:HOME";
    public static final String BLUETOOTH_OFF = "OFF";
    public static final String BLUETOOTH_ON = "ON";

    public static boolean isBluetoothChecked = false;
    public static boolean isAuxChecked = false;
    public static CompoundButton.OnCheckedChangeListener listener;
    public static SwitchCompat switchButtonBluetooth;
    public static SwitchCompat switchButtonAux;
    public static List<SwitchCompat> switchBluetoothList = new ArrayList<SwitchCompat>();
    public static List<SwitchCompat> switchAuxList = new ArrayList<SwitchCompat>();
    public CumulationsLocalSourcesFragment mFragment;
    private SwitchCompat bluetoothReference;

    public CumulationsLocalSourcesRecyclerAdapter(String[] localSources, Context context, String currentIpAddress, boolean btStatus, boolean AuxStatus, Fragment fragment) {
        this.localSources = localSources;
        this.context = context;
        this.currentIpAddress = currentIpAddress;
        if (scanningHandler.getconnectedSSIDname(context) == ScanningHandler.SA_MODE) {
            saMode = true;
        }
        mFragment = (CumulationsLocalSourcesFragment)fragment;
        switchBluetoothList = new ArrayList<SwitchCompat>();
        switchAuxList = new ArrayList<SwitchCompat>();
    }

    public ViewTooltip.TooltipView showBluetoothToolTip() {
        return showViewToolTip(bluetoothReference, context.getString(R.string.bluetooth_tip).replace("\n", "<br/>"), ViewTooltip.Position.BOTTOM, false);
    }

    public ViewTooltip.TooltipView showViewToolTip(View view, String text, ViewTooltip.Position position, final boolean shouldRepeat) {
        ViewTooltip.TooltipView toolTipView = null;
        SharedPreferences sharedPreferences = context.getSharedPreferences("appreference", MODE_PRIVATE);

        if (view ==null)
        {
            //Toast.makeText(context,"Hello", Toast.LENGTH_LONG).show();
            return null;
        }

        if (sharedPreferences.getBoolean(CumulationsLocalSourcesRecyclerAdapter.class.getSimpleName(),false) == false) {
            sharedPreferences.edit().putBoolean(CumulationsLocalSourcesRecyclerAdapter.class.getSimpleName(),true).commit();

            toolTipView = ViewTooltip
                    .on(view)
                    .autoHide(true, 5000)
                    .clickToHide(true)
                    .align(ViewTooltip.ALIGN.CENTER)
                    .position(position)
                    .text(text)
                    .textColor(context.getResources().getColor(R.color.viewToolTipTextColor))
                    .color(context.getResources().getColor(R.color.viewToolTipColor))
                    .corner(10)
                    .show();
        }

        return toolTipView;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        SwitchCompat switchButtonBluetooth;
        SwitchCompat switchButtonAux;
        RelativeLayout linearLayout;
        CardView parentView;
        public MyViewHolder(View itemView) {
            super(itemView);
            parentView = (CardView) itemView.findViewById(R.id.card_view);
            image = (ImageView)itemView.findViewById(R.id.image);
            name = (TextView)itemView.findViewById(R.id.name);
            switchButtonBluetooth = (SwitchCompat) itemView.findViewById(R.id.switchButtonBluetooth);
            switchButtonAux = (SwitchCompat)itemView.findViewById(R.id.switchButtonAux);
            linearLayout = (RelativeLayout)itemView.findViewById(R.id.linearLayout);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cumulations_local_sources_layout, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, final int position) {

        ImageView image = myViewHolder.image;
        TextView name = myViewHolder.name;
        switchButtonBluetooth = myViewHolder.switchButtonBluetooth;
        switchButtonAux = myViewHolder.switchButtonAux;
        switchBluetoothList.add(switchButtonBluetooth);
        switchAuxList.add(switchButtonAux);

        if (!isThisSourceAvailable(context.getString(R.string.local_USB))
                && localSources[position].contains(context.getString(R.string.local_USB))) {
            myViewHolder.parentView.setVisibility(View.GONE);
            myViewHolder.linearLayout.setVisibility(View.GONE);
            return;
        }

        if (/*position == 3*/localSources[position].contains(context.getString(R.string.local_bluetooth))) {
            bluetoothReference = switchButtonBluetooth;
            switchButtonAux.setVisibility(View.GONE);
            switchButtonBluetooth.setVisibility(View.VISIBLE);

            switchButtonBluetooth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 final LUCIControl luciControl = new LUCIControl(currentIpAddress);
                 luciControl.sendAsynchronousCommand();
                 credentialHandler.sendEmptyMessageDelayed(AUX_BT_TIMEOUT, 5000);
                if (buttonView.isPressed()) {
                    if (isChecked) {
                        isBluetoothChecked = true;
//                    showLoaderAndAskSource(getString(R.string.BtOnAlert));
                        Message msg = new Message();
                        msg.what = BT_AUX_INITIATED;
                        Bundle b = new Bundle();
                        b.putString("MessageText", context.getString(R.string.BtOnAlert));
                        msg.setData(b);
                        credentialHandler.sendMessage(msg);

                        Log.d("BLUETOOTH", "Bluetooth status if: " + isBluetoothChecked);
                        Log.d("BLUETOOTH", "aux status: " + isAuxChecked);
                        if (isAuxChecked) {
                            luciControl.SendCommand(MIDCONST.MID_AUX_STOP, null, LSSDPCONST.LUCI_SET);
                            isAuxChecked = false;
                            switchAuxList.get(3).setTag(3);
                            //switchButton.setOnCheckedChangeListener(null);
                            switchAuxList.get(3).setChecked(false);
                            //switchButton.setOnCheckedChangeListener(this);
                        }
                        // sleep when BT to AUX switch
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                luciControl.SendCommand(MIDCONST.MID_BLUETOOTH, BLUETOOTH_ON, LSSDPCONST.LUCI_SET);
                            }
                        }, 1000);

                    } else {
                       /* Setting the source to default */
//                    showLoaderAndAskSource(getString(R.string.BtOffAlert));

                        isBluetoothChecked = false;

                        Log.d("BLUETOOTH", "Bluetooth status else: " + isBluetoothChecked);
                        Log.d("BLUETOOTH", "aux status: " + isAuxChecked);
                        Message msg = new Message();
                        msg.what = BT_AUX_INITIATED;
                        Bundle b = new Bundle();
                        b.putString("MessageText", context.getString(R.string.BtOffAlert));
                        msg.setData(b);
                        credentialHandler.sendMessage(msg);
                        luciControl.SendCommand(MIDCONST.MID_BLUETOOTH, BLUETOOTH_OFF, LSSDPCONST.LUCI_SET);
                    }
                }
             }
         });
        } else if (/*position == 6*/localSources[position].contains(context.getString(R.string.local_auxin))) {
            switchButtonAux.setVisibility(View.VISIBLE);
            switchButtonAux.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    final LUCIControl luciControl = new LUCIControl(currentIpAddress);
                    luciControl.sendAsynchronousCommand();
                    credentialHandler.sendEmptyMessageDelayed(AUX_BT_TIMEOUT, 5000);
                    if (buttonView.isPressed()) {
                        if (isChecked) {
                            isAuxChecked = true;
                            Message msg = new Message();
                            msg.what = BT_AUX_INITIATED;
                            Bundle b = new Bundle();
                            b.putString("MessageText", context.getString(R.string.AuxOnAlert));
                            msg.setData(b);
                            credentialHandler.sendMessage(msg);
                            Log.d("BLUETOOTH", "Bluetooth status: " + isBluetoothChecked);
                            Log.d("BLUETOOTH", "aux status if: " + isAuxChecked);
                            if (isBluetoothChecked) {
                                luciControl.SendCommand(MIDCONST.MID_BLUETOOTH, BLUETOOTH_OFF, LSSDPCONST.LUCI_SET);
                                isBluetoothChecked = false;
                                switchBluetoothList.get(1).setTag(1);
                                switchBluetoothList.get(1).setChecked(false);
                            }

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    luciControl.SendCommand(MIDCONST.MID_AUX_START, null, LSSDPCONST.LUCI_SET);
                                }
                            }, 1000);
                        } else {
                            isAuxChecked = false;

                            Log.d("BLUETOOTH", "Bluetooth status: " + isBluetoothChecked);
                            Log.d("BLUETOOTH", "aux status else: " + isAuxChecked);
                            Message msg = new Message();
                            msg.what = BT_AUX_INITIATED;
                            Bundle b = new Bundle();
                            b.putString("MessageText", context.getString(R.string.AuxOffAlert));
                            msg.setData(b);
                            credentialHandler.sendMessage(msg);
                            luciControl.SendCommand(MIDCONST.MID_AUX_STOP, null, LSSDPCONST.LUCI_SET);

                        }
                    }
                }
            });
        }
        final RelativeLayout linearLayout = myViewHolder.linearLayout;
        image.setImageResource(images[position]);
        name.setText(localSources[position]);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Local content
                if (/*position == 0*/localSources[position].contains(context.getString(R.string.local_mydevice))) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                            Toast.makeText(context, context.getString(R.string.locationEnable), Toast.LENGTH_SHORT).show();
                            ((CumulationsSourcesOptionActivity)context).checkStoragePermission();
                            return;
                        }
                    }
                    //showLoader();
                    credentialHandler.removeCallbacksAndMessages(null);
                    Intent localIntent = new Intent(context, LocalDMSActivity.class);
                    localIntent.putExtra("isLocalDeviceSelected", true);
                    localIntent.putExtra("current_ipaddress", currentIpAddress);
                    localIntent.putExtra("source", "localcontent");
                    localIntent.putExtra(Constants.FROM_ACTIVITY,CumulationsSourcesOptionActivity.class.getSimpleName());
                    context.startActivity(localIntent);
                    ((Activity)context).finish();

                } else if (/*position == 4*/localSources[position].contains(context.getString(R.string.local_USB))) { // USB
                    CumulationsSourcesOptionActivity.current_source_index_selected = 3;
                /*Reset the UI to Home ,, will wait for the confirmation of home command completion and then start the required activity*/
                    LUCIControl luciControl = new LUCIControl(currentIpAddress);
                    luciControl.sendAsynchronousCommand();
                    luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
                    ///////////// timeout for dialog - showLoader() ///////////////////
                    ((CumulationsSourcesOptionActivity)context).credentialHandler.sendEmptyMessageDelayed(NETWORK_TIMEOUT, Constants.ITEM_CLICKED_TIMEOUT);
                    showLoader();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return localSources.length;
    }



    Handler credentialHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == ACTION_INITIATED) {
                showLoader();
            }

            if (msg.what == BT_AUX_INITIATED) {
                String showMessage = msg.getData().getString("MessageText");
                showLoaderAndAskSource(showMessage);
            }
            if (msg.what == AUX_BT_TIMEOUT) {
                closeLoader();
                if (isBluetoothChecked) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showBluetoothToolTip();
                        }
                    });
                }

            }
        }
    };

    private void showLoaderAndAskSource(String source) {
        if (((Activity)context).isFinishing())
            return;

        if (m_progressDlg == null) {
            m_progressDlg = new ProgressDialog(context);
            m_progressDlg.setTitle("Please wait");
            m_progressDlg.setIndeterminate(true);
        }

        m_progressDlg.setMessage(source);
        if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }
    }

    private void showLoader() {

        if (((Activity)context).isFinishing())
            return;

        if (m_progressDlg == null) {
            m_progressDlg = new ProgressDialog(context);
        }
        m_progressDlg.setMessage(context.getString(R.string.loading));
        m_progressDlg.setCancelable(false);
           /* m_progressDlg.setButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    m_progressDlg.cancel();
                }
            });*/
//            m_progressDlg.show();


        if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }
    }

    private void closeLoader() {

        if (m_progressDlg != null) {

            if (m_progressDlg.isShowing() == true) {
                try {
                    m_progressDlg.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

    }

    /*this method is to show error in whole application*/
    public void showErrorMessage(final LibreError message) {
        try {

            if (LibreApplication.hideErrorMessage)
                return;

            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    LibreLogger.d(this, "Showing the supertaloast " + System.currentTimeMillis());
                    SuperToast superToast = new SuperToast(context);

                    if (message != null && message.getErrorMessage().contains("is no longer available")) {
                        LibreLogger.d(this, "Device go removed showing error in Device Discovery");
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                    }
                    if(message.getmTimeout()==0) {//TimeoutDefault
                        superToast.setDuration(SuperToast.Duration.LONG);
                    }else{
                        superToast.setDuration(SuperToast.Duration.VERY_SHORT);
                    }
                    superToast.setText("" + message);
                    superToast.setAnimations(SuperToast.Animations.FLYIN);
                    superToast.setIcon(SuperToast.Icon.Dark.INFO, SuperToast.IconPosition.LEFT);
                    superToast.show();
                    if (message != null && message.getErrorMessage().contains(""))
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isThisSourceAvailable(String src){
        LSSDPNodes theNodeBasedOnTheIpAddress = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(currentIpAddress);
        if (theNodeBasedOnTheIpAddress == null || theNodeBasedOnTheIpAddress.getmDeviceCap() == null || theNodeBasedOnTheIpAddress.getmDeviceCap().getmSource() == null) {
            return false;
        }

        Sources mSourceForThisDevice = theNodeBasedOnTheIpAddress.getmDeviceCap().getmSource();
        if (mSourceForThisDevice == null)
            return false;
        if (src.contains(context.getString(R.string.local_USB)) && mSourceForThisDevice.isUsb()){
            return true;
        }

        return false;
    }


}
