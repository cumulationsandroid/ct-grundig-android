package com.cumulations.grundig.SourceOptions;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.R;
//import com.squareup.leakcanary.RefWatcher;

/**
 * Created by cumulations on 7/8/17.
 */

public class DeezerLoginDialogFragment extends DialogFragment {

    int style = DialogFragment.STYLE_NO_TITLE;

    private EditText mUserName;
    private EditText mPassword;
    public Button mLogin;
    private int position;
    private ImageView mLoginLogo;
    private TextView mLoginText;
    public CumulationsOnlineSourcesFragment mCumulationsOnlineSourcesFragment;
    private DeezerTidalLoginActivityListener deezerTidalLoginActivityListener;

    public static DeezerLoginDialogFragment newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt("position", position);
        DeezerLoginDialogFragment fragment = new DeezerLoginDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public DeezerLoginDialogFragment() {

    }

    public void setCumulationsOnlineSourcesFragment(CumulationsOnlineSourcesFragment cumulationsOnlineSourcesFragment) {
        this.mCumulationsOnlineSourcesFragment = cumulationsOnlineSourcesFragment;
    }

    public void setUserName(String username) {
        this.mUserName.setText(username);
    }

    public void setPassword(String password) {
        this.mPassword.setText(password);
    }

    public Button getLoginButton() {
        return mLogin;
    }

    public EditText getUserName() {
        return mUserName;
    }

    public EditText getPassword() {
        return mPassword;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("position");
        setStyle(style, R.style.MyTheme_DeezerDialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.cumulations_deezer_login_layout, container);
        //getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mLoginLogo = (ImageView)v.findViewById(R.id.loginLogo);
        mLoginText = (TextView)v.findViewById(R.id.deezerloginText);
        if (position == 6) {
            mLoginLogo.setImageResource(R.drawable.tidal_circle);
            mLoginText.setText(getString(R.string.tidal_login));
        } else {
            mLoginLogo.setImageResource(R.drawable.deezer_logo_circle);
            mLoginText.setText(getString(R.string.deezer_login));
        }
        mUserName = (EditText)v.findViewById(R.id.username);
        mPassword = (EditText)v.findViewById(R.id.password);
        mLogin = (Button)v.findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCumulationsOnlineSourcesFragment != null)
                    mCumulationsOnlineSourcesFragment.deezerLogin(position);
                if (deezerTidalLoginActivityListener != null)
                    deezerTidalLoginActivityListener.deezerTidalLogin(position);
            }
        });
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        RefWatcher refWatcher = LibreApplication.getRefWatcher(getActivity());
//        refWatcher.watch(this);
    }

    public void setDeezerTidalLoginActivityListener(DeezerTidalLoginActivityListener deezerTidalLoginActivityListener) {
        this.deezerTidalLoginActivityListener = deezerTidalLoginActivityListener;
    }

    public void removeDeezerTidalLoginActivityListener(){
        deezerTidalLoginActivityListener = null;
    }
}
