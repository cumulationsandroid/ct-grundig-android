package com.cumulations.grundig.SourceOptions;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.ActiveScenesListActivity;
import com.cumulations.grundig.DeviceDiscoveryFragment;
import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.R;
import com.cumulations.grundig.RemoteSourcesList;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.nowplaying.CumulationsNowPlayingActivity;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.ShowLoader;
import com.cumulations.grundig.util.Sources;
import com.github.johnpersano.supertoasts.SuperToast;
//import com.squareup.leakcanary.RefWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by cumulations on 25/7/17.
 */

public class CumulationsOnlineSourcesFragment extends DeviceDiscoveryFragment implements LibreDeviceInteractionListner {

    public static CumulationsOnlineSourcesFragment fragment;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private String currentIpAddress;
    private ArrayList<String> musicSourcesList = new ArrayList<String>();
    private CumulationsOnlineSourcesRecyclerAdapter mAdapter;

    private static String deezer = "Deezer";
    private static String tidal = "TIDAL";
    //    private static String favourite = "Favourites";
    private static String vtuner = "vTuner";
    private static String spotify = "Spotify";
    private static String tuneIn = "TuneIn";
    private static String qmusic = "QQ Music";
    private static String networkDevicePlayers = "Network Device Players";
    private static String favourites = "Favorites";
    private ProgressDialog m_progressDlg;

    private static final String TAG_CMD_ID = "CMD ID";
    private static final String TAG_WINDOW_CONTENT = "Window CONTENTS";
    private static final String TAG_BROWSER = "Browser";
    private final int CREDENTIAL_USER_NAME = 31;
    private final int CREDENTIAL_PASSWORD = 32;
    private final int CREDENTIAL_TIMEOUT = 330;
    private final int TIMEOUT = 3000;

    final int NETWORK_TIMEOUT = 101;
    private final int ACTION_INITIATED = 12345;
    public static final String GET_HOME = "GETUI:HOME";
    public static int current_source_index_selected = -1;
    public static boolean shouldShowCasting = false;
    public ImageView loadingBar;
    public TextView loadingText;
    Fragment mFragment;


    private String userName = "";
    private String userPassword = "";
    private DeezerLoginDialogFragment deezerLoginDialogFragment;
    ScanningHandler mScanHandler = ScanningHandler.getInstance();
    private SceneObject currentSceneObject;

    interface tabChangeListener {
        public void onTabChange(boolean add);
    }
    public static tabChangeListener mTabChangeListener;

    interface onOnlineSourcesMessageRecieved {
        void sendUsername(String username);
        void sendPassword(String password);
    }

    onOnlineSourcesMessageRecieved mOnOnlineSourcesMessageRecieved;


    public static CumulationsOnlineSourcesFragment newInstance(String currentIpAddress) {
        Bundle args = new Bundle();
        args.putString("IPADDRESS", currentIpAddress);
        fragment = new CumulationsOnlineSourcesFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentIpAddress = getArguments().getString("IPADDRESS");
        if (currentIpAddress != null) {
            currentSceneObject = mScanHandler.getSceneObjectFromCentralRepo(currentIpAddress); // ActiveSceneAdapter.mMasterSpecificSlaveAndFreeDeviceMap.get(currentIpAddress);
        }
        mFragment = fragment;
        registerForDeviceEvents(this);
    }



    public String getconnectedSSIDname() {
        WifiManager wifiManager;
        wifiManager = (WifiManager) getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        Log.d("CreateNewScene", "getconnectedSSIDname wifiInfo = " + wifiInfo.toString());
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }
        Log.d("SacListActivity", "Connected SSID" + ssid);
        return ssid;
    }


    public static void setTabChangeListener(tabChangeListener tabChangeListener) {
        mTabChangeListener = tabChangeListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cumulations_online_sources, container, false);
        loadingBar = (ImageView)view.findViewById(R.id.loading);
        loadingText = (TextView)view.findViewById(R.id.loadingText);
        mRecyclerView = (RecyclerView)(view.findViewById(R.id.recycler_view));
        mLayoutManager = new LinearLayoutManager(getActivity());
        //mAdapter = new CumulationsOnlineSourcesRecyclerAdapter(new ArrayList<String>(), getActivity(), currentIpAddress);
        //mRecyclerView.setAdapter(mAdapter);
        //mRecyclerView.setLayoutManager(mLayoutManager);

        LSSDPNodeDB lssdpNodeDB1 = LSSDPNodeDB.getInstance();
        LSSDPNodes theNodeBasedOnTheIpAddress1 = lssdpNodeDB1.getTheNodeBasedOnTheIpAddress(currentIpAddress);
        if (getconnectedSSIDname().contains(Constants.DDMS_SSID) || ( theNodeBasedOnTheIpAddress1 != null &&
                theNodeBasedOnTheIpAddress1.getNetworkMode().contains("P2P") == true)) {
            mAdapter = new CumulationsOnlineSourcesRecyclerAdapter(musicSourcesList, getActivity(), currentIpAddress, mFragment );
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(mLayoutManager);


        } else {
            mAdapter = new CumulationsOnlineSourcesRecyclerAdapter(musicSourcesList, getActivity(), currentIpAddress, mFragment);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(mLayoutManager);
        }

        DividerItemDecoration divider = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.cumulations_recycler_divider));
        mRecyclerView.addItemDecoration(divider);

        LSSDPNodeDB lssdpNodeDB = LSSDPNodeDB.getInstance();
        LSSDPNodes theNodeBasedOnTheIpAddress = lssdpNodeDB.getTheNodeBasedOnTheIpAddress(currentIpAddress);

        if (theNodeBasedOnTheIpAddress != null && theNodeBasedOnTheIpAddress.getgCastVerision() != null
                &&     theNodeBasedOnTheIpAddress1.getNetworkMode().contains("P2P") == false) {
            //final LinearLayout castServicesButton = (LinearLayout) headerlayout.findViewById(R.id.chromecast_services);
            //castServicesButton.setVisibility(View.VISIBLE);
            //mTabChangeListener.onTabChange(true);
            shouldShowCasting = true;

            try {
                musicSourcesList.clear();
                musicSourcesList.add(spotify);
                /*mediaserverList.add(tidal);
                mediaserverList.add(vtuner);
                mediaserverList.add(qmusic);*/

                mAdapter.setOnlineSources(musicSourcesList);
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            //final LinearLayout castServicesButton = (LinearLayout) headerlayout.findViewById(R.id.chromecast_services);
            //castServicesButton.setVisibility(View.GONE);
            //mTabChangeListener.onTabChange(false);
            shouldShowCasting = false;
            try {
                musicSourcesList.clear();
                musicSourcesList.add(spotify);
                musicSourcesList.add(deezer);
                musicSourcesList.add(tidal);
                musicSourcesList.add(vtuner);
                musicSourcesList.add(tuneIn);
                musicSourcesList.add(qmusic);
                musicSourcesList.add(/*favourites*/getString(R.string.local_favorites));
                musicSourcesList.add(/*networkDevicePlayers*/getString(R.string.local_networkdevicePlayer));
                mAdapter.setOnlineSources(musicSourcesList);
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        EnableOrDisableViews();
        return view;
    }


    private void EnableOrDisableViews() {
        LSSDPNodes theNodeBasedOnTheIpAddress = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(currentIpAddress);


        if (theNodeBasedOnTheIpAddress == null || theNodeBasedOnTheIpAddress.getmDeviceCap() == null || theNodeBasedOnTheIpAddress.getmDeviceCap().getmSource() == null) {
            return;
        }

        Sources mSourceForThisDevice = theNodeBasedOnTheIpAddress.getmDeviceCap().getmSource();
        boolean isAnyoneEnabled=false;
        {

            if (mSourceForThisDevice != null) {
                if (mSourceForThisDevice.isDmr()) {
                    isAnyoneEnabled = true;
                }
                if (mSourceForThisDevice.isDmp()) {
                    isAnyoneEnabled = true;
                }
                musicSourcesList.clear();

                if (mSourceForThisDevice.isSDcard() && !theNodeBasedOnTheIpAddress.getmDeviceCap().
                        getmDeviceCap().equalsIgnoreCase(Constants.LS9)) {
                    isAnyoneEnabled = true;
                }

                if (!theNodeBasedOnTheIpAddress.getNetworkMode().contains("P2P")) {
                    if (mSourceForThisDevice.isvTuner()) {
                        musicSourcesList.add(vtuner);
                    }

                    if (mSourceForThisDevice.isTuneIn()) {
                        musicSourcesList.add(tuneIn);
                    }

                    if (mSourceForThisDevice.isSpotify()) {
                        musicSourcesList.add(spotify);
                    }

                    if (mSourceForThisDevice.isDeezer()) {
                        musicSourcesList.add(deezer);
                    }

                    if (mSourceForThisDevice.isTidal()) {
                        musicSourcesList.add(tidal);
                    }

                    if (mSourceForThisDevice.isFavourites()) {
//                        isAnyoneEnabled = true;
                        musicSourcesList.add(/*favourites*/getString(R.string.local_favorites));
                    }

                    if (mSourceForThisDevice.isDmp()){
                        musicSourcesList.add(/*networkDevicePlayers*/getString(R.string.local_networkdevicePlayer));
                    }

                }

                if (mSourceForThisDevice.isAuxIn()) {
                    isAnyoneEnabled = true;
                }
                if (mSourceForThisDevice.isBluetooth()) {
                    isAnyoneEnabled = true;
                }

                if (theNodeBasedOnTheIpAddress.getmDeviceCap().
                        getmDeviceCap().equalsIgnoreCase(Constants.LS9)
                        && mSourceForThisDevice.isGoogleCast()
                        && !theNodeBasedOnTheIpAddress.getNetworkMode().contains("P2P")) {
                    //final LinearLayout castServicesButton = (LinearLayout) headerlayout.findViewById(R.id.chromecast_services);
                    //castServicesButton.setVisibility(View.VISIBLE);
                    //mTabChangeListener.onTabChange(true);
                    shouldShowCasting = true;
                    //sdcard.setVisibility(View.GONE);
                    try {
                        musicSourcesList.clear();
                        isAnyoneEnabled = true;
                        if (mSourceForThisDevice.isSpotify())
                            musicSourcesList.add(spotify);
                        if (mSourceForThisDevice.isFavourites()) {
                            musicSourcesList.add(/*favourites*/getString(R.string.local_favorites));
                        }

                        if (mSourceForThisDevice.isDmp()){
                            musicSourcesList.add(/*networkDevicePlayers*/getString(R.string.local_networkdevicePlayer));
                        }

                        mAdapter.setOnlineSources(musicSourcesList);
                        mAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!isAnyoneEnabled) {
                    showAlertDialog();
                }
                mAdapter.setOnlineSources(musicSourcesList);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    AlertDialog appsourcealertDialog;;
    private void showAlertDialog() {

        {

            if (!getActivity().isFinishing()) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        getActivity());

                // set title
                alertDialogBuilder.setTitle("Alert");

                // set dialog message
                alertDialogBuilder
                        .setMessage(getResources().getString(R.string.appsourcelistnotSet))
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (getActivity().getIntent().hasExtra(Constants.FROM_ACTIVITY)) {
                                    Intent intent = new Intent(getActivity(), CumulationsNowPlayingActivity.class);
                                    intent.putExtra("current_ipaddress", currentIpAddress);
                                    startActivity(intent);
                                    getActivity().finish();
                                } else {
                                    Intent intent = new Intent(getActivity(), ActiveScenesListActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                                appsourcealertDialog = null;

                            }
                        });

                // create alert dialog
                if (appsourcealertDialog == null)
                    appsourcealertDialog = alertDialogBuilder.create();
                /*alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);*/
                // show it

                appsourcealertDialog.show();
            }
        }
    }


    public void setUsernameAndPassword(onOnlineSourcesMessageRecieved messageRecieved) {
        mOnOnlineSourcesMessageRecieved = messageRecieved;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment = null;
//        RefWatcher refWatcher = LibreApplication.getRefWatcher(getActivity());
//        refWatcher.watch(this);
    }

    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

    }

    @Override
    public void deviceGotRemoved(String ipaddress) {

    }

    @Override
    public void messageRecieved(NettyData dataRecived) {
        byte[] buffer = dataRecived.getMessage();
        String ipaddressRecieved = dataRecived.getRemotedeviceIp();

        LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());
        LibreLogger.d(this, "Message recieved for ipaddress " + ipaddressRecieved + "command is " + packet.getCommand());

        if (currentIpAddress.equalsIgnoreCase(ipaddressRecieved)) {
            switch (packet.getCommand()) {
                case 42: {

                    String message = new String(packet.getpayload());
                    LibreLogger.d(this, " message 42 recieved  " + message);
                    try {
                        parseJsonAndReflectInUI(message);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        LibreLogger.d(this, " Json exception ");

                    }

                }
                break;
            }
        }
    }



    private void parseJsonAndReflectInUI(String jsonStr) throws JSONException {

        LibreLogger.d(this, "Json Recieved from remote device " + jsonStr);
        if (jsonStr != null) {

            try {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeLoader();
                    }
                });
                if (credentialHandler.hasMessages(NETWORK_TIMEOUT))
                    credentialHandler.removeMessages(NETWORK_TIMEOUT);

                JSONObject root = new JSONObject(jsonStr);
                int cmd_id = root.getInt(TAG_CMD_ID);
                JSONObject window = root.getJSONObject(TAG_WINDOW_CONTENT);

                LibreLogger.d(this, "Command Id" + cmd_id);

                if (cmd_id == 1) {
                    /*
                      */
                    String Browser = window.getString(TAG_BROWSER);
                    /*if (Browser.equalsIgnoreCase("HOME")) {

                        *//* Now we have successfully got the stack intialiized to home *//*
                        credentialHandler.removeMessages(NETWORK_TIMEOUT);
                        unRegisterForDeviceEvents();
                        Intent intent = new Intent(getContext(), RemoteSourcesList.class);
                        intent.putExtra("current_ipaddress", currentIpAddress);
                        intent.putExtra("current_source_index_selected", current_source_index_selected);
                        LibreLogger.d(this, "removing handler message");
                        startActivity(intent);
                        getActivity().finish();
                    }*/
                }
            } catch (Exception e) {

            }
        }
    }



    /*this method is to show error in whole application*/
    public void showErrorMessage(final LibreError message) {
        try {

            if (LibreApplication.hideErrorMessage)
                return;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    LibreLogger.d(this, "Showing the supertaloast " + System.currentTimeMillis());
                    SuperToast superToast = new SuperToast(getContext());

                    if (message != null && message.getErrorMessage().contains("is no longer available")) {
                        LibreLogger.d(this, "Device go removed showing error in Device Discovery");
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                    }
                    if(message.getmTimeout()==0) {//TimeoutDefault
                        superToast.setDuration(SuperToast.Duration.LONG);
                    }else{
                        superToast.setDuration(SuperToast.Duration.VERY_SHORT);
                    }
                    superToast.setText("" + message);
                    superToast.setAnimations(SuperToast.Animations.FLYIN);
                    superToast.setIcon(SuperToast.Icon.Dark.INFO, SuperToast.IconPosition.LEFT);
                    superToast.show();
                    if (message != null && message.getErrorMessage().contains(""))
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showLoaderAndAskSource(String source) {
        if (getActivity().isFinishing())
            return;

        ShowLoader.showLoader(loadingBar, loadingText, getActivity());
       /* if (m_progressDlg == null) {
            m_progressDlg = new ProgressDialog(getActivity());
            m_progressDlg.setTitle("Please wait");
            m_progressDlg.setIndeterminate(true);
        }

        m_progressDlg.setMessage(source);
        if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }*/
        //asking source
        /*LUCIControl luciControl = new LUCIControl(current_ipaddress);
        luciControl.SendCommand(50, null, LSSDPCONST.LUCI_GET);*/
    }
    private void showLoader() {

        ShowLoader.showLoader(loadingBar, loadingText, getActivity());

        if (getActivity().isFinishing())
            return;

        /*if (m_progressDlg == null) {
            m_progressDlg = new ProgressDialog(getActivity());
        }
        m_progressDlg.setMessage(getString(R.string.loading));
        m_progressDlg.setCancelable(false);*/
           /* m_progressDlg.setButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    m_progressDlg.cancel();
                }
            });*/
//            m_progressDlg.show();


        /*if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }*/
    }

    private void closeLoader() {
        loadingBar.clearAnimation();
        loadingBar.setVisibility(View.GONE);
        loadingText.setVisibility(View.GONE);
        /*if (m_progressDlg != null) {

            if (m_progressDlg.isShowing() == true) {
                m_progressDlg.dismiss();
            }

        }*/

    }
    
    
    public void deezerLogin(int position) {
        if (TextUtils.isEmpty(deezerLoginDialogFragment.getUserName().getText().toString())) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.enterUsername), Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(deezerLoginDialogFragment.getPassword().getText().toString())) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.enterPassword), Toast.LENGTH_SHORT).show();
            return;
        }

        //  m_progressDlg = ProgressDialog.show(SourcesOptionActivity.this, "Notice", "Loading...", true, true, null);
        showLoader();

        LUCIControl luciControl = new LUCIControl(currentIpAddress);
        if (position == 5) {
            luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_USERNAME + deezerLoginDialogFragment.getUserName().getText().toString(),
                    LSSDPCONST.LUCI_SET);
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_PASSWORD + deezerLoginDialogFragment.getPassword().getText().toString(),
                    LSSDPCONST.LUCI_SET);
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
        } else if (position == 6) {
                    luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_USERNAME + deezerLoginDialogFragment.getUserName().toString(),
                            LSSDPCONST.LUCI_SET);
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                    }
                    luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_PASSWORD + deezerLoginDialogFragment.getPassword().toString(),
                            LSSDPCONST.LUCI_SET);
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                    }
                    luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);

        }


        deezerLoginDialogFragment.dismiss();
    }


    /*this dialog will open when we click on deezer or tidal*/
    private void sourceAuthDialogue(final int position) {
        // custom dialog

        FragmentManager fm = (getActivity()).getSupportFragmentManager();
        deezerLoginDialogFragment = DeezerLoginDialogFragment.newInstance(position);
        deezerLoginDialogFragment.setCumulationsOnlineSourcesFragment((CumulationsOnlineSourcesFragment)mFragment);
        deezerLoginDialogFragment.show(fm, "deezerdialog");

        if (((Activity)getActivity()).isFinishing())
            return;


        // set the custom dialog components - text, image and button
        /*final EditText userName = (EditText) dialog.findViewById(R.id.user_name);
        final EditText userPassword = (EditText) dialog.findViewById(R.id.user_password);*/


        /*userName.setText(this.userName);
        userPassword.setText(this.userPassword);*/

        /*deezerLoginDialogFragment.setUserName(this.userName);
        deezerLoginDialogFragment.setPassword(this.userPassword);

        *//*Button submitButton = (Button) dialog.findViewById(R.id.deezer_ok_button);*//*
        // if button is clicked, close the custom dialog
        deezerLoginDialogFragment.getLoginButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(deezerLoginDialogFragment.getUserName().getText().toString())) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.enterUsername), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(deezerLoginDialogFragment.getPassword().getText().toString())) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.enterPassword), Toast.LENGTH_SHORT).show();
                    return;
                }

                //  m_progressDlg = ProgressDialog.show(SourcesOptionActivity.this, "Notice", "Loading...", true, true, null);
                showLoader();

                LUCIControl luciControl = new LUCIControl(currentIpAddress);
                if (position == 5) {
                    luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_USERNAME + deezerLoginDialogFragment.getUserName().getText().toString(),
                            LSSDPCONST.LUCI_SET);
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                    }
                    luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_PASSWORD + deezerLoginDialogFragment.getPassword().getText().toString(),
                            LSSDPCONST.LUCI_SET);
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                    }
                    luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
                } else if (position == 6) {
                    *//*luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_USERNAME + userName.getText().toString(),
                            LSSDPCONST.LUCI_SET);
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                    }
                    luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_PASSWORD + userPassword.getText().toString(),
                            LSSDPCONST.LUCI_SET);
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                    }
                    luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);*//*

                }


                deezerLoginDialogFragment.dismiss();
            }
        });*/
    }


    Handler credentialHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == NETWORK_TIMEOUT) {
                LibreLogger.d(this, "recieved handler msg");
                closeLoader();

                /*showing error to user*/
                LibreError error = new LibreError(currentIpAddress, Constants.INTERNET_ITEM_SELECTED_TIMEOUT_MESSAGE);
                showErrorMessage(error);
//                Toast.makeText(getApplicationContext(), Constants.LOADING_TIMEOUT_REBOOT, Toast.LENGTH_SHORT).show();
            }

            if (msg.what == ACTION_INITIATED) {
                showLoader();
            }


            /*if (msg.what == CREDENTIAL_USER_NAME) {
                userName = (String) msg.obj;
            }

            if (msg.what == CREDENTIAL_PASSWORD) {
                userPassword = (String) msg.obj;
            }

            if (msg.what == CREDENTIAL_TIMEOUT) {



                if (getActivity() != null && !(getActivity().isFinishing())) {
                    try {
                        if ((userName == null || userName.isEmpty()) && (userPassword == null || userPassword.isEmpty())) {
                            closeLoader();
                            sourceAuthDialogue(current_source_index_selected);
                        } else {
                            LUCIControl luciControl = new LUCIControl(currentIpAddress);
                            if (current_source_index_selected == 5) {
                                *//*deezer*//*
                                luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_USERNAME + userName,
                                        LSSDPCONST.LUCI_SET);
                                try {
                                    Thread.sleep(100);
                                } catch (Exception e) {
                                }
                                luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_PASSWORD + userPassword,
                                        LSSDPCONST.LUCI_SET);
                                try {
                                    Thread.sleep(100);
                                } catch (Exception e) {
                                }
                                luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
                            } else if (current_source_index_selected == 6) {
                                *//*tidal*//*
                                luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_USERNAME + userName,
                                        LSSDPCONST.LUCI_SET);
                                try {
                                    Thread.sleep(100);
                                } catch (Exception e) {
                                }
                                luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_PASSWORD + userPassword,
                                        LSSDPCONST.LUCI_SET);
                                try {
                                    Thread.sleep(100);
                                } catch (Exception e) {
                                }
                                luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);

                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }*/
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (shouldShowCasting) {
            //mTabChangeListener.onTabChange(true);
        } else {
            //mTabChangeListener.onTabChange(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        credentialHandler.removeCallbacksAndMessages(null);
    }
}
