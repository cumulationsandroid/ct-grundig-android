package com.cumulations.grundig.SourceOptions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.LibreWebViewActivity;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;

import java.util.ArrayList;

/**
 * Created by cumulations on 28/7/17.
 */

public class CumulationsCastingSourcesRecyclerAdapter extends RecyclerView.Adapter<CumulationsCastingSourcesRecyclerAdapter.MyViewHolder> {

    private ArrayList<String> castingSources;
    private Context context;
    private String currentIpAddress;
    AlertDialog launchGcastAppAlert;
    private ScanningHandler mScanHandler = ScanningHandler.getInstance();


    public void setCastingSources(ArrayList<String> sources) {
        this.castingSources = sources;
    }


    public CumulationsCastingSourcesRecyclerAdapter(ArrayList<String> castingSources, Context context, String currentIpAddress) {
        this.castingSources = castingSources;
        this.context = context;
        this.currentIpAddress = currentIpAddress;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        RelativeLayout linearLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (ImageView)itemView.findViewById(R.id.image);
            name = (TextView)itemView.findViewById(R.id.name);
            linearLayout = (RelativeLayout)itemView.findViewById(R.id.linearLayout);
        }
    }

    @Override
    public CumulationsCastingSourcesRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cumulations_casting_sources_layout, parent, false);


        CumulationsCastingSourcesRecyclerAdapter.MyViewHolder myViewHolder = new CumulationsCastingSourcesRecyclerAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(CumulationsCastingSourcesRecyclerAdapter.MyViewHolder myViewHolder, final int position) {
        final ImageView image = myViewHolder.image;
        TextView name = myViewHolder.name;
        RelativeLayout linearLayout = myViewHolder.linearLayout;
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String packageName;
                switch (position) {
                    case 0:
                        packageName = "com.spotify.music";
                        break;
                    case 1:
                        packageName = "deezer.android.app";
                        break;
                    case 2:
                        packageName = "com.google.android.music";
                        break;
                    case 3:
                        packageName = "com.pandora.android";
                        break;
                    case 4:
                        packageName = "tunein.player";
                        break;
                    case 5:
                        packageName = context.getString(R.string.googleCastEnabledApps);
                        break;
                    default:
                        packageName = "com.spotify.music";
                        break;
                }
                launchAppOnClick(packageName);
            }
        });
        name.setText(castingSources.get(position));

        if (position == 5) {
            image.setVisibility(View.GONE);
            /*name.setText(Html.fromHtml("<a href=\"http://g.co/cast/audioapps\">Google Cast-enabled apps</a>"
                    *//*context.getResources().getString(R.string.Google_cast_enabled_apps_hyperlink)*//*));
            name.setMovementMethod(LinkMovementMethod.getInstance());*/

            name.setText(getGoogleCastEnabledAppsSpannableString());
            name.setAutoLinkMask(-1);
            name.setMovementMethod(LinkMovementMethod.getInstance());
            Linkify.addLinks(getGoogleCastEnabledAppsSpannableString(),Linkify.WEB_URLS);
        }

        if ("spotify".equalsIgnoreCase(castingSources.get(position))) {
            image.setImageResource(R.drawable.spotify_logo_without_text_svg);
        } else if ("deezer".equalsIgnoreCase(castingSources.get(position))) {
            image.setImageResource(R.drawable.deezer_logo_circle);
        } else if ("google play".equalsIgnoreCase(castingSources.get(position))) {
            image.setImageResource(R.drawable.googlemusic);
        } else if ("pandora".equalsIgnoreCase(castingSources.get(position))) {
            image.setImageResource(R.drawable.pandora);
        } else if ("tunein".equalsIgnoreCase(castingSources.get(position))) {
            image.setImageResource(R.drawable.tunein);
        } else if ("Google Cast-enabled apps".equalsIgnoreCase(castingSources.get(position))){
            image.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return castingSources.size();
    }


    private void launchAppOnClick(String packageName) {
        if (isTOSAvailable(currentIpAddress)){
            if (packageName.equals(context.getString(R.string.googleCastEnabledApps))){
                Intent intent = new Intent(context, LibreWebViewActivity.class);
                intent.putExtra("url", "http://g.co/cast/audioapps");
                intent.putExtra("title", "Sources");
                context.startActivity(intent);
            } else {
                launchTheApp(packageName);
            }
        } else {
            openGHomeDialog();
        }
    }


    public void launchTheApp(String appPackageName) {

        /* Special check for the sources optin */
        LSSDPNodes masterNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(currentIpAddress);
        int mode = 1;

        if (masterNode.getNetworkMode().contains("WLAN") == true)
            mode = 0;
        ArrayList<LSSDPNodes> mSlaveList = mScanHandler.getSlaveListForMasterIp(currentIpAddress, mode);
        if (mSlaveList != null && mSlaveList.size() > 0) {
            if (LSSDPNodeDB.isCastDevicesExistsInTheNetwork())
                Toast.makeText(context, context.getString(R.string.castGroupToast), Toast.LENGTH_LONG).show();
        }


        Intent intent = context.getPackageManager().getLaunchIntentForPackage(appPackageName);


        if (intent != null) {

/*
    commenting for avoidig forcefull DMR playback
            try {
                LibreLogger.d(this, "Going to Remove the DMR Playback");
                RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(current_ipaddress);
                    */
/* For the New DMR Implementation , whenever a Source Switching is happening
                        * we are Stopping the Playback *//*

                if (renderingDevice != null) {
                    String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                    PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                    playbackHelper.StopPlayback();
                    playbackHelper = null;
                    LibreApplication.PLAYBACK_HELPER_MAP.remove(renderingUDN);
                }
            } catch (Exception e) {
                LibreLogger.d(this, "Removing the DMR Playback is Exception");
            }
*/


            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        } else {

            redirectingToPlayStore(intent, appPackageName);

        }

    }

    public void openGHomeDialog() {
        if (!((Activity)context).isFinishing()) {
            launchGcastAppAlert = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            String Message = context.getResources().getString(R.string.openGHome);
            builder.setMessage(Message)
                    .setCancelable(false)
                    .setPositiveButton("Launch", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            launchGcastAppAlert.dismiss();
                            launchTheApp("com.google.android.apps.chromecast.app");


                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    launchGcastAppAlert.dismiss();
                }
            });

            if (launchGcastAppAlert == null) {
                launchGcastAppAlert = builder.show();
                TextView messageView = (TextView)launchGcastAppAlert.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }

            launchGcastAppAlert.show();

        }
    }


    private boolean isTOSAvailable(String ipAddress){
        LSSDPNodeDB mLssdpNodeDb = LSSDPNodeDB.getInstance();
        LSSDPNodes mNode = mLssdpNodeDb.getTheNodeBasedOnTheIpAddress(ipAddress);
        if (mNode!=null && !mNode.getTOSValue().equals("14")){
            return true;
        }
        return false;
    }

    public void redirectingToPlayStore(Intent intent, String appPackageName) {

        try {

            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + appPackageName));
            context.startActivity(intent);

        } catch (android.content.ActivityNotFoundException anfe) {

            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName));
            context.startActivity(intent);

        }

    }

    private SpannableString getGoogleCastEnabledAppsSpannableString(){
        String targetString = context.getString(R.string.googleCastEnabledApps);
        SpannableString spannableString = new SpannableString(targetString);
        ClickableSpan googleEnabledAppsClicked = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent=new Intent(context,LibreWebViewActivity.class);
                intent.putExtra("url", "http://g.co/cast/audioapps");
                context.startActivity(intent);
            }
        };

        spannableString.setSpan(googleEnabledAppsClicked,0,targetString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.blue)), 0,targetString.length()-1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

}
