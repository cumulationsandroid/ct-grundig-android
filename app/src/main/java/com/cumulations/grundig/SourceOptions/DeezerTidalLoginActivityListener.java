package com.cumulations.grundig.SourceOptions;

/**
 * Created by Amit Tumkur on 20-12-2017.
 */

public interface DeezerTidalLoginActivityListener {
    void deezerTidalLogin(int currentSrcIndexSelected);
}
