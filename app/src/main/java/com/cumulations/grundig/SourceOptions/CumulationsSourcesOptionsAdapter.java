package com.cumulations.grundig.SourceOptions;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;

import java.util.ArrayList;

/**
 * Created by cumulations on 25/7/17.
 */

public class CumulationsSourcesOptionsAdapter extends FragmentPagerAdapter {

    private ArrayList<String> tabTitles = new ArrayList<String>(){{
       // add("Local Sources");
        //adc(context.getString(R.string.musciSources_online));
        //add("Cast sources");
    }};
    private String currentIpAddress;
    private boolean btStatus;
    private boolean AuxStatus;
    private TabLayout tabs;
    ScanningHandler scanningHandler = ScanningHandler.getInstance();
    public CumulationsSourcesOptionsAdapter(FragmentManager fm, String currentIpAddress, boolean btStatus, boolean AuxStatus, Context context) {
        super(fm);
        this.currentIpAddress = currentIpAddress;
        this.btStatus = btStatus;
        this.AuxStatus = AuxStatus;

        tabTitles.add(context.getString(R.string.musicsources_local));
        LSSDPNodes device= LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(currentIpAddress);
        if (scanningHandler.getconnectedSSIDname(context) != ScanningHandler.SA_MODE) {
            tabTitles.add(context.getString(R.string.music_sources_pnline));
            if (device != null && device.getgCastVerision() != null) {
                tabTitles.add(context.getString(R.string.musciSources_cast));
            }
        }

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return CumulationsLocalSourcesFragment.newInstance(currentIpAddress, btStatus, AuxStatus);
            case 1:
                return CumulationsOnlineSourcesFragment.newInstance(currentIpAddress);
            case 2:
                return CumulationsCastingSourcesFragment.newInstance(currentIpAddress);
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabTitles.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
