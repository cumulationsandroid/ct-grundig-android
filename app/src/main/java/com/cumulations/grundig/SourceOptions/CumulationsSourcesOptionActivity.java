package com.cumulations.grundig.SourceOptions;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.ActiveScenesListActivity;
import com.cumulations.grundig.DeviceDiscoveryActivity;
import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.R;
import com.cumulations.grundig.RemoteSourcesList;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.nowplaying.CumulationsNowPlayingActivity;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.ShowLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CumulationsSourcesOptionActivity extends DeviceDiscoveryActivity implements LibreDeviceInteractionListner,
        DeezerTidalLoginActivityListener{


    private CumulationsCustomTabLayout mTabLayout;
    private ViewPager mViewPager;


    private ArrayList<String> listDataHeader;
    private ListView mListView;
    private static final String TAG_CMD_ID = "CMD ID";
    private static final String TAG_WINDOW_CONTENT = "Window CONTENTS";
    private static final String TAG_BROWSER = "Browser";
    public static final String GET_HOME = "GETUI:HOME";
    public static final String GET_BROWSE = "GETUI:BROWSER";
    public static final String GET_DEEZER_USERNAME = "DeezerUserName";
    public static final String BLUETOOTH_OFF = "OFF";
    public static final String BLUETOOTH_ON = "ON";

    final int NETWORK_TIMEOUT = 101;
    final int AUX_BT_TIMEOUT = 0x2;
    private final int ACTION_INITIATED = 12345;
    private final int BT_AUX_INITIATED = 12345;
    private final int CREDENTIAL_SUCCESSFULL = 30;
    private final int CREDENTIAL_USER_NAME = 31;
    private final int CREDENTIAL_PASSWORD = 32;
    private final int CREDENTIAL_TIMEOUT = 330;
    private final int TIMEOUT = 3000;

    private String userName = "";
    private String userPassword = "";


    ScanningHandler mScanHandler = ScanningHandler.getInstance();


    private static String deezer = "Deezer";
    private static String tidal = "TIDAL";
    //    private static String favourite = "Favourites";
    private static String vtuner = "vTuner";
    private static String spotify = "Spotify";
    private static String tuneIn = "TuneIn";
    private static String qmusic = "QQ Music";

    private String current_ipaddress;
    private String current_source;
    private ProgressDialog m_progressDlg;
    public static int current_source_index_selected = -1;
    private TextView backText;
    private TabLayout tabs;

    View headerlayout;
    View footerlayout;
    final static List<String> mediaserverList = new ArrayList<String>();

    /*Decalartion in Globally */
    TextView doneBut;
    LinearLayout localId;
    LinearLayout networkid;
    LinearLayout usbid;
    LinearLayout sdcard;
    LinearLayout favouritesButton;
    LinearLayout auxLayout;
    LinearLayout btLayout;

    private boolean btStatus = true;
    private boolean AuxStatus = true;
    private ImageView loadingBar;
    private TextView loadingText;
    private AppBarLayout appBarLayout;
    private CumulationsSourcesOptionsAdapter cumulationsSourcesOptionsAdapter;

    private DeezerLoginDialogFragment deezerLoginDialogFragment;
    /*mic changes*/
    private static final int REQUEST_READ_STORAGE_PERMISSION = 231;
    private boolean permissionGranted = false;
    private String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cumulations_activity_sources_option);
        appBarLayout = (AppBarLayout)findViewById(R.id.id_appbar);
        tabs = (CumulationsCustomTabLayout)findViewById(R.id.tabs);
        loadingBar = (ImageView)findViewById(R.id.loading);
        loadingText = (TextView)findViewById(R.id.loadingText);
        backText = (TextView)findViewById(R.id.back);
        backText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        current_ipaddress = getIntent().getStringExtra("current_ipaddress");
        current_source = getIntent().getStringExtra("current_source");
        registerForDeviceEvents(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        mViewPager = (ViewPager)findViewById(R.id.viewpager);
        setupViewPager(mViewPager);
        setUpTabLayout();
    }



    @Override
    protected void onResume() {
        super.onResume();
        readTheCurrentBluetoothStatus();
        readTheCurrentAuxStatus();
        /*Handle when my device clicked*/
//        checkStoragePermission();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        checkSwitchStatus();
    }

    public void checkStoragePermission(){
        /*Check if it's Marshamellow*/
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            /*Check if granted*/
            if (!isStoragePermissionGranted()){
                /*Check if forever denied*/
                if (!isStoragePermissionForeverDenied()) {
                    /*This will become true only when user denies for first time*/
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_READ_STORAGE_PERMISSION);
                    } else {
                        /*Ask permission if first time asking*/
                        if (!isFirstTimeStoragePermissionAsked()) {
                            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    REQUEST_READ_STORAGE_PERMISSION);
                        } else {
                            /*Both first time permission has been asked and user selected never ask again with deny
                            * causing shouldShowRequestPermissionRationale to be false*/
                            LibreApplication.mStoragePermissionGranted = false;
                            getSharedPreferences(Constants.FOREVER_DENIED_PERMISSIONS,MODE_PRIVATE)
                                    .edit()
                                    .putBoolean(Constants.READ_STORAGE_PERMISSION,true)
                                    .apply();
                            showAlertDialogForStoragePermissionRequired();
                        }
                    }
                } else {
                    showAlertDialogForStoragePermissionRequired();
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isStoragePermissionGranted(){
        return (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean isStoragePermissionForeverDenied(){
        return getSharedPreferences(Constants.FOREVER_DENIED_PERMISSIONS,MODE_PRIVATE)
                .getBoolean(Constants.READ_STORAGE_PERMISSION,false);
    }

    private void checkSwitchStatus() {
        if (current_source != null) {
            int currentSource = Integer.valueOf(current_source);
            SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo().get(current_ipaddress);

			/* Karuna , if Zone is playing in BT/AUX and We Released the Zone
            and we creating the same Guy as a Master then Aux should not Switch ON as a Default*/
            if (currentSource == 14 && sceneObject != null && sceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED) {
                //auxbutton.setChecked(true, false);
                Log.d("AUXSTATE", "--" + sceneObject.getPlaystatus());
                AuxStatus = false;
                Log.d("BLUETOOTH", "01");
                CumulationsLocalSourcesRecyclerAdapter.isAuxChecked = false;
                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setTag(3);
                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setChecked(false);
                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                //auxbutton.setChecked(false, false);
            }
            /* Karuna , if Zone is playing in BT/AUX and We Released the Zone
            and we creating the same Guy as a Master then Aux should not Switch ON as a Default*/

            if (currentSource == 19 && sceneObject != null &&
                    (sceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED
                            || sceneObject.getPlaystatus() == SceneObject.CURRENTLY_NOTPLAYING)) {
                //bluetooth.setChecked(true, false);
                Log.d("BLUETOOTH", "00");
                CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked = false;
                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setTag(1);
                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(false);
                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                Log.d("BTSTATE", "--" + sceneObject.getPlaystatus());
                btStatus = false;
                //bluetooth.setChecked(false, false);
            }
        }
    }

    private void setupViewPager(final ViewPager viewPager) {
       /* CumulationsOnlineSourcesFragment.mTabChangeListener = new CumulationsOnlineSourcesFragment.tabChangeListener() {
            @Override
            public void onTabChange(boolean add) {
                if (add) {
                    mTabLayout.addTab(mTabLayout.newTab().setText("Casting sources"));
                    cumulationsSourcesOptionsAdapter.addTabPage("Casting sources");
                } else {
                    cumulationsSourcesOptionsAdapter.removeTabPage(2);
                }
            }
        };*/
        cumulationsSourcesOptionsAdapter = new CumulationsSourcesOptionsAdapter(getSupportFragmentManager(), current_ipaddress, btStatus, AuxStatus,this);
        if (cumulationsSourcesOptionsAdapter.getCount() == 1) {
            appBarLayout.setLayoutParams(new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100));
        }
        viewPager.setAdapter(cumulationsSourcesOptionsAdapter);
    }

    private void setUpTabLayout() {
        mTabLayout = (CumulationsCustomTabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
    }




    private void showLoaderAndAskSource(String source) {
        if (CumulationsSourcesOptionActivity.this.isFinishing())
            return;

        ShowLoader.showLoader(loadingBar, loadingText, this);
        /*if (m_progressDlg == null) {
            m_progressDlg = new ProgressDialog(CumulationsSourcesOptionActivity.this);
            m_progressDlg.setTitle("Please wait");
            m_progressDlg.setIndeterminate(true);
        }

        m_progressDlg.setMessage(source);
        if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }*/
        //asking source
        /*LUCIControl luciControl = new LUCIControl(current_ipaddress);
        luciControl.SendCommand(50, null, LSSDPCONST.LUCI_GET);*/
    }
    private void showLoader() {

        if (CumulationsSourcesOptionActivity.this.isFinishing())
            return;

        ShowLoader.showLoader(loadingBar, loadingText, this);

        /*if (m_progressDlg == null) {
            m_progressDlg = new ProgressDialog(CumulationsSourcesOptionActivity.this);
        }
        m_progressDlg.setMessage(getString(R.string.loading));
        m_progressDlg.setCancelable(false);
           *//* m_progressDlg.setButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    m_progressDlg.cancel();
                }
            });*//*
//            m_progressDlg.show();


        if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }*/
    }

    private void closeLoader() {
        loadingBar.clearAnimation();
        loadingBar.setVisibility(View.GONE);
        loadingText.setVisibility(View.GONE);
        /*if (m_progressDlg != null) {

            if (m_progressDlg.isShowing() == true) {
                m_progressDlg.dismiss();
            }

        }
*/
    }


    Handler credentialHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == NETWORK_TIMEOUT) {
                LibreLogger.d(this, "recieved handler msg");
                closeLoader();

                /*showing error to user*/
                LibreError error = new LibreError(current_ipaddress, Constants.INTERNET_ITEM_SELECTED_TIMEOUT_MESSAGE);
                showErrorMessage(error);
//                Toast.makeText(getApplicationContext(), Constants.LOADING_TIMEOUT_REBOOT, Toast.LENGTH_SHORT).show();
            }

            if (msg.what == ACTION_INITIATED) {
                showLoader();
            }


            if (msg.what == CREDENTIAL_USER_NAME) {
                userName = (String) msg.obj;
            }

            if (msg.what == CREDENTIAL_PASSWORD) {
                userPassword = (String) msg.obj;
            }

            if (msg.what == CREDENTIAL_TIMEOUT) {



                if (!(CumulationsSourcesOptionActivity.this.isFinishing())) {
                    try {
                        if ((userName == null || userName.isEmpty()) && (userPassword == null || userPassword.isEmpty())) {
                            closeLoader();
                            sourceAuthDialogue(current_source_index_selected);
                        } else {
                            LUCIControl luciControl = new LUCIControl(current_ipaddress);
                            if (current_source_index_selected == 5) {
                                /*deezer*/
                                luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_USERNAME + userName,
                                        LSSDPCONST.LUCI_SET);
                                try {
                                    Thread.sleep(100);
                                } catch (Exception e) {
                                }
                                luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_PASSWORD + userPassword,
                                        LSSDPCONST.LUCI_SET);
                                try {
                                    Thread.sleep(100);
                                } catch (Exception e) {
                                }
                                luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
                            } else if (current_source_index_selected == 6) {
                                /*tidal*/
                                luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_USERNAME + userName,
                                        LSSDPCONST.LUCI_SET);
                                try {
                                    Thread.sleep(100);
                                } catch (Exception e) {
                                }
                                luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_PASSWORD + userPassword,
                                        LSSDPCONST.LUCI_SET);
                                try {
                                    Thread.sleep(100);
                                } catch (Exception e) {
                                }
                                luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);

                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (msg.what == BT_AUX_INITIATED) {
                String showMessage = msg.getData().getString("MessageText");
                showLoaderAndAskSource(showMessage);
            }
            if (msg.what == AUX_BT_TIMEOUT) {
                closeLoader();
            }
        }
    };




    private void readTheCurrentBluetoothStatus() {
        LUCIControl luciControl = new LUCIControl(current_ipaddress);
        luciControl.sendAsynchronousCommand();
        luciControl.SendCommand(MIDCONST.MID_BLUETOOTH, null, LSSDPCONST.LUCI_GET);
    }


    private void readTheCurrentAuxStatus() {
        LUCIControl luciControl = new LUCIControl(current_ipaddress);
        luciControl.SendCommand(50, null, LSSDPCONST.LUCI_GET);
        ///luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
    }


    private void parseJsonAndReflectInUI(String jsonStr) throws JSONException {

        LibreLogger.d(this, "Json Recieved from remote device " + jsonStr);
        if (jsonStr != null) {

            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeLoader();
                    }
                });
                if (credentialHandler.hasMessages(NETWORK_TIMEOUT))
                    credentialHandler.removeMessages(NETWORK_TIMEOUT);

                JSONObject root = new JSONObject(jsonStr);
                int cmd_id = root.getInt(TAG_CMD_ID);
                JSONObject window = root.getJSONObject(TAG_WINDOW_CONTENT);

                LibreLogger.d(this, "Command Id" + cmd_id);

                if (cmd_id == 1) {
                    /*
                      */
                    String Browser = window.getString(TAG_BROWSER);
                    if (Browser.equalsIgnoreCase("HOME")) {

                        /* Now we have succseefully got the stack intialiized to home */
                        credentialHandler.removeMessages(NETWORK_TIMEOUT);
                        unRegisterForDeviceEvents();
                        Intent intent = new Intent(CumulationsSourcesOptionActivity.this, RemoteSourcesList.class);
                        intent.putExtra("current_ipaddress", current_ipaddress);
                        intent.putExtra("current_source_index_selected", current_source_index_selected);
                        LibreLogger.d(this, "removing handler message");
                        startActivity(intent);
                        finish();
                    }
                }
            } catch (Exception e) {

            }
        }
    }


    /*this dialog will open when we click on deezer or tidal*/
    private void sourceAuthDialogue(final int position) {
        // custom dialog

        if (CumulationsSourcesOptionActivity.this.isFinishing())
            return;
        deezerLoginDialogFragment = DeezerLoginDialogFragment.newInstance(position);
        deezerLoginDialogFragment.setDeezerTidalLoginActivityListener(this);
        deezerLoginDialogFragment.show(getSupportFragmentManager(), "deezerdialog");
        closeLoader();
    }


    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

    }

    @Override
    public void deviceGotRemoved(String ipaddress) {

    }

    @Override
    public void messageRecieved(NettyData dataRecived) {

        byte[] buffer = dataRecived.getMessage();
        String ipaddressRecieved = dataRecived.getRemotedeviceIp();

        LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());

        LibreLogger.d(this, "Message recieved for ipaddress " + ipaddressRecieved + "command is " + packet.getCommand());

        if (current_ipaddress.equalsIgnoreCase(ipaddressRecieved)) {
            switch (packet.getCommand()) {

                case 42: {

                    String message = new String(packet.getpayload());
                    LibreLogger.d(this, " message 42 recieved  " + message);
                    try {
                        parseJsonAndReflectInUI(message);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        LibreLogger.d(this, " Json exception ");

                    }
                }
                break;

                /* This indicates the bluetooth status*/
                case 209: {


                    String message = new String(packet.getpayload());
                    LibreLogger.d(this, " message 209 is recieved  " + message);
                   /* if (BLUETOOTH_ON.equalsIgnoreCase(message)) {
                        bluetooth.setChecked(true, false);
                    } else {
                        bluetooth.setChecked(false, false);
                    }
*/
                }
                break;
                case 51: {

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo().get(current_ipaddress);
                        sceneObject.setPlaystatus(duration);


                        if (mScanHandler.isIpAvailableInCentralSceneRepo(current_ipaddress)) {
                            mScanHandler.putSceneObjectToCentralRepo(current_ipaddress, sceneObject);
                        }

                        if (sceneObject.getCurrentSource() == 14) {
                            if (CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked) {
                                Log.d("BLUETOOTH", "bluetooth 50");
                                CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked = false;
                                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setTag(1);
                                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(false);
                                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                            }
                            if (sceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
                                Log.d("BLUETOOTH", "02");
                                CumulationsLocalSourcesRecyclerAdapter.isAuxChecked = true;
                                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setTag(3);
                                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setChecked(true);
                            }
                        } else if (sceneObject.getCurrentSource() == 19) {
                            if (CumulationsLocalSourcesRecyclerAdapter.isAuxChecked) {
                                Log.d("BLUETOOTH", "bluetooth 98");
                                CumulationsLocalSourcesRecyclerAdapter.isAuxChecked = false;
                                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setTag(3);
                                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setChecked(false);
                                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                            }
                            if (sceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
                                Log.d("BLUETOOTH", "bluetooth 99");
                                CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked = true;
                                Log.d("BLUETOOTH", "TAG IS " + CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).getTag());
                                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setTag(1);
                                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(true);//true
                                //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                            }
                        } else {
                            CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked = false;
                            CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setTag(1);
                            //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                            CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(false);
                            //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                            Log.d("BLUETOOTH", "bluetooth 51");
                            CumulationsLocalSourcesRecyclerAdapter.isAuxChecked = false;
                            CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setTag(3);
                            //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                            CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setChecked(false);
                            //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                            //auxbutton.setChecked(false, false);
                            //bluetooth.setChecked(false, false);
                        }
                        LibreLogger.d(this, "Recieved the playstate to be" + sceneObject.getPlaystatus());
                    } catch (Exception e) {

                    }

                }
                break;

                case 50: {
                    String message = new String(packet.getpayload());
                    // Toast.makeText(getApplicationContext(),"Message 50 is Received"+message,Toast.LENGTH_SHORT).show();
                    LibreLogger.d(this, " message 50 is recieved  " + message);
                    if (message.contains("14")) {
                        credentialHandler.removeMessages(AUX_BT_TIMEOUT);
                        credentialHandler.sendEmptyMessage(AUX_BT_TIMEOUT);

                        SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo().get(current_ipaddress);
                        if (sceneObject != null) {
                            sceneObject.setCurrentSource(14);
                            mScanHandler.getSceneObjectFromCentralRepo().put(current_ipaddress, sceneObject);
                        }
                        /* Karunakaran : CrashAnalytics crash fix
                        if (sceneObject != null && sceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED || sceneObject.getPlaystatus() == SceneObject.CURRENTLY_NOTPLAYING) {
                            //auxbutton.setChecked(false,false);
                            return;
                        }*/
                        if (CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList == null ||
                                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.size() == 0)
                            return;
                        if (CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked) {
                            Log.d("BLUETOOTH", "bluetooth 52");
                            CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked = false;
                            CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setTag(1);
                            //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                            CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(false);
                            //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                            //bluetooth.setChecked(false, false);
                            //auxbutton.setChecked(true, false);
                        }
                        Log.d("BLUETOOTH", "04");

                        if (CumulationsLocalSourcesRecyclerAdapter.switchAuxList == null ||
                                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.size() == 0)
                            return;
                        CumulationsLocalSourcesRecyclerAdapter.isAuxChecked = true;
                        CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setTag(3);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                        CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setChecked(true);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                    } else if (message.contains("19")) {
                        /*removing timeout and closing loader*/
                        credentialHandler.removeMessages(AUX_BT_TIMEOUT);
                        credentialHandler.sendEmptyMessage(AUX_BT_TIMEOUT);

                        SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo().get(current_ipaddress);
                        if (sceneObject != null) {
                            sceneObject.setCurrentSource(19);
                            mScanHandler.getSceneObjectFromCentralRepo().put(current_ipaddress, sceneObject);
                        }
                     /* This code is of No  Use
                     * *//* Karunakaran : CrashAnalytics crash fix *//*
                        if (sceneObject != null && (sceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED || sceneObject.getPlaystatus() == SceneObject.CURRENTLY_NOTPLAYING)) {
                            //bluetooth.setChecked(false,false);
                            return;
                        }*/
                        if (CumulationsLocalSourcesRecyclerAdapter.switchAuxList == null ||
                                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.size() == 0)
                            return;
                        if (CumulationsLocalSourcesRecyclerAdapter.isAuxChecked) {
                            Log.d("BLUETOOTH", "05");
                            CumulationsLocalSourcesRecyclerAdapter.isAuxChecked = false;
                            CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setTag(3);
                            //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                            CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setChecked(false);
                            //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                            //auxbutton.setChecked(false, false);
                        }
                        Log.d("BLUETOOTH", "06");
                        if (CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList == null ||
                                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.size() == 0)
                            return;
                        CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked = true;
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                        //CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(true);
                        CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setTag(1);
                        CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(true);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                        //bluetooth.setChecked(true, false);
                    } else if (message.contains("0") || message.contains("NO_SOURCE")) {
                        LibreLogger.d(this, " No Source received hence closing dialog");
                        /**Closing loader after 1.5 second*/
                        credentialHandler.removeMessages(AUX_BT_TIMEOUT);
                        credentialHandler.sendEmptyMessageDelayed(AUX_BT_TIMEOUT, 1500);

                        CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked = false;
                        if (CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList == null ||
                                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.size() == 0)
                            return;
                        CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setTag(1);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                        CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(false);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                        Log.d("BLUETOOTH", "bluetooth 53");

                        CumulationsLocalSourcesRecyclerAdapter.isAuxChecked = false;
                        if (CumulationsLocalSourcesRecyclerAdapter.switchAuxList == null ||
                                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.size() == 0)
                            return;
                        CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setTag(3);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                        CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setChecked(false);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                        //bluetooth.setChecked(false, false);
                        //auxbutton.setChecked(false, false);
                    } else {
                        SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo().get(current_ipaddress);
                        if (sceneObject != null)
                            sceneObject.setCurrentSource(-1);
                        CumulationsLocalSourcesRecyclerAdapter.isBluetoothChecked = false;
                        if (CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList == null ||
                                CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.size() == 0)
                            return;
                        CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setTag(1);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                        CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList.get(1).setChecked(false);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                        Log.d("BLUETOOTH", "bluetooth 54");

                        CumulationsLocalSourcesRecyclerAdapter.isAuxChecked = false;
                        if (CumulationsLocalSourcesRecyclerAdapter.switchAuxList == null ||
                                CumulationsLocalSourcesRecyclerAdapter.switchAuxList.size() == 0)
                            return;
                        CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setTag(3);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(null);
                        CumulationsLocalSourcesRecyclerAdapter.switchAuxList.get(3).setChecked(false);
                        //CumulationsLocalSourcesRecyclerAdapter.switchButton.setOnCheckedChangeListener(CumulationsLocalSourcesRecyclerAdapter.listener);
                        /*auxbutton.setChecked(false, false);
                        bluetooth.setChecked(false, false);*/
                    }
                }
                break;
                case 208: {


                    /* This code is crashing for array out of index exception and hence will be handled with a try and catch -Praveen*/

                    try {
                        String messages = new String(packet.getpayload());
                        LibreLogger.d(this, " got deezer " + messages);
                        String credentials[] = messages.split(":");

                        if (credentials.length == 0)
                            return;

                        String deezerUserName = "";
                        String deezerUserPassword = "";
                        String tidalUserName = "";
                        String tidalUserPassword = "";

                        /*Added for device state*/
                        Message message = Message.obtain();


                        if (credentials[0].contains("DeezerUserName")) {
                            deezerUserName = credentials[1];

                            message.obj = deezerUserName;
                            message.what = CREDENTIAL_USER_NAME;
                            credentialHandler.sendMessage(message);
                            //mOnOnlineSourcesMessageRecieved.sendUsername(userName);

                        }
                        if (credentials[0].contains("DeezerUserPassword")) {
                            deezerUserPassword = credentials[1];

                            message.obj = deezerUserPassword;
                            message.what = CREDENTIAL_PASSWORD;
                            credentialHandler.sendMessage(message);

                            //mOnOnlineSourcesMessageRecieved.sendPassword(userPassword);

                        }
                        if (credentials[0].contains("TidalUserName")) {
                            tidalUserName = credentials[1];

                            message.obj = tidalUserName;
                            message.what = CREDENTIAL_USER_NAME;
                            credentialHandler.sendMessage(message);


                        }
                        if (credentials[0].contains("TidalUserPassword")) {
                            tidalUserPassword = credentials[1];

                            message.obj = tidalUserPassword;
                            message.what = CREDENTIAL_PASSWORD;
                            credentialHandler.sendMessage(message);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        LibreLogger.d(this, "Credentials not read! Error!! Khajan Bhai!");
                    }
                }
                break;


            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CumulationsLocalSourcesRecyclerAdapter.switchButtonBluetooth = null;
        CumulationsLocalSourcesRecyclerAdapter.switchButtonAux = null;
        CumulationsLocalSourcesRecyclerAdapter.switchBluetoothList = null;
        CumulationsLocalSourcesRecyclerAdapter.switchAuxList = null;
    }

        @Override
    public void onBackPressed() {
        super.onBackPressed();
            LibreApplication.isConvertViewClicked = false;

            if (getIntent().hasExtra(Constants.FROM_ACTIVITY)) {
                Intent intent = new Intent(CumulationsSourcesOptionActivity.this, CumulationsNowPlayingActivity.class);
                intent.putExtra("current_ipaddress", current_ipaddress);
                startActivity(intent);
            } else {
                Intent intent = new Intent(CumulationsSourcesOptionActivity.this, ActiveScenesListActivity.class);
                startActivity(intent);
            }
        finish();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    protected void onStop() {
        super.onStop();
        credentialHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void deezerTidalLogin(int currentSrcIndexSelected) {
        if (TextUtils.isEmpty(deezerLoginDialogFragment.getUserName().getText().toString())) {
            Toast.makeText(this, getString(R.string.enterUsername), Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(deezerLoginDialogFragment.getPassword().getText().toString())) {
            Toast.makeText(this,getString(R.string.enterPassword), Toast.LENGTH_SHORT).show();
            return;
        }

        //  m_progressDlg = ProgressDialog.show(SourcesOptionActivity.this, "Notice", "Loading...", true, true, null);
        showLoader();

        LUCIControl luciControl = new LUCIControl(current_ipaddress);
        if (currentSrcIndexSelected == 5) {
            luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_USERNAME + deezerLoginDialogFragment.getUserName().getText().toString(),
                    LSSDPCONST.LUCI_SET);
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.DEEZER_PASSWORD + deezerLoginDialogFragment.getPassword().getText().toString(),
                    LSSDPCONST.LUCI_SET);
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
        } else if (currentSrcIndexSelected == 6) {
            luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_USERNAME + deezerLoginDialogFragment.getUserName().toString(),
                    LSSDPCONST.LUCI_SET);
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            luciControl.SendCommand(MIDCONST.MID_DEEZER, LUCIMESSAGES.TIDAL_PASSWORD + deezerLoginDialogFragment.getPassword().toString(),
                    LSSDPCONST.LUCI_SET);
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);

        }


        deezerLoginDialogFragment.dismiss();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_STORAGE_PERMISSION: {
                if (grantResults.length >= 1) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                        permissionGranted = false;
                        LibreApplication.mStoragePermissionGranted = false;
                        getSharedPreferences(Constants.FIRST_TIME_PERMISSIONS_DENIED,MODE_PRIVATE)
                                .edit()
                                .putBoolean(Constants.READ_STORAGE_PERMISSION,true)
                                .apply();
                    } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        permissionGranted = true;
                        LibreApplication.mStoragePermissionGranted = true;
                        getSharedPreferences(Constants.FOREVER_DENIED_PERMISSIONS,MODE_PRIVATE)
                                .edit()
                                .putBoolean(Constants.READ_STORAGE_PERMISSION,false)
                                .apply();
                    }
                }

            }
        }
    }

    private void showAlertDialogForStoragePermissionRequired() {
        AlertDialog.Builder requestPermission = new AlertDialog.Builder(CumulationsSourcesOptionActivity.this);
        requestPermission.setTitle(getString(R.string.permitNotAvailable))
                .setMessage(getString(R.string.enableStoragePermit))
                .setPositiveButton(getString(R.string.gotoSettings), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //navigate to settings
                        alert.dismiss();
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alert.dismiss();
                        getSharedPreferences(Constants.FOREVER_DENIED_PERMISSIONS,MODE_PRIVATE)
                                .edit().putBoolean(Constants.READ_STORAGE_PERMISSION,true).apply();
//                        finish();
                    }
                })
                .setCancelable(false);
        if (alert == null) {
            alert = requestPermission.create();
        }
        if (alert != null && !alert.isShowing())
            alert.show();
    }

    private boolean isFirstTimeStoragePermissionAsked() {
        return getSharedPreferences(Constants.FIRST_TIME_PERMISSIONS_DENIED,MODE_PRIVATE)
                .getBoolean(Constants.READ_STORAGE_PERMISSION,false);
    }
}
