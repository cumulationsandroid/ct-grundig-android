package com.cumulations.grundig.SourceOptions;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.lang.reflect.Field;

/**
 * Created by cumulations on 19/8/17.
 */

public class CumulationsCustomTabLayout extends TabLayout {

    public CumulationsCustomTabLayout(Context context) {
        super(context);
    }

    public CumulationsCustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CumulationsCustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        try {
            if (getTabCount() == 0)
                return;

            Field field = TabLayout.class.getDeclaredField("mScrollableTabMinWidth");
            field.setAccessible(true);
            float individaulTabWidth=(getMeasuredWidth() / (float) getTabCount());
            field.set(this, (int) individaulTabWidth);
            if (getTabCount() >= 2) {
                setTabMode(MODE_SCROLLABLE);
            } else {
               // setVisibility(TabLayout.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
