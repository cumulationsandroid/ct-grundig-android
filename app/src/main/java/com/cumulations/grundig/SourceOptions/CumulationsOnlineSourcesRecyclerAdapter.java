package com.cumulations.grundig.SourceOptions;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.StaticInstructions.SpotifyInstructionsActivity;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.ShowLoader;
import com.github.johnpersano.supertoasts.SuperToast;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by cumulations on 28/7/17.
 */

public class CumulationsOnlineSourcesRecyclerAdapter extends RecyclerView.Adapter<CumulationsOnlineSourcesRecyclerAdapter.MyViewHolder> {

    private ArrayList<String> onlineSources;
    private int spotify = R.drawable.spotify_logo_without_text_svg;
    private int deezer = R.drawable.deezer_logo_circle;
    private int tidal = R.drawable.tidal;
    private int tunein = R.drawable.tunein;
    private int vtuner = R.drawable.vtuner;
    private int favourites = R.drawable.ic_favorite_white_36dp;
    private int networkDevicesPlayers = R.drawable.ic_wifi_white_36dp;
    final int NETWORK_TIMEOUT = 101;
    final int AUX_BT_TIMEOUT = 0x2;
    private final int ACTION_INITIATED = 12345;
    private final int BT_AUX_INITIATED = 12345;
    private final int CREDENTIAL_SUCCESSFULL = 30;
    private final int CREDENTIAL_USER_NAME = 31;
    private final int CREDENTIAL_PASSWORD = 32;
    private final int CREDENTIAL_TIMEOUT = 330;
    private final int TIMEOUT = 3000;

    private RelativeLayout mRelativeLayout;

    //private int current_source_index_selected = -1;
    private ProgressDialog m_progressDlg;
    private String userName = "";
    private String userPassword = "";
    public static final String GET_HOME = "GETUI:HOME";
    private Context context;
    private String currentIpAddress;
    private CumulationsOnlineSourcesFragment mCumulationsOnlineSourcesFragment;


    public void setOnlineSources(ArrayList<String> sources) {
        this.onlineSources = sources;
    }

    private void showLoaderAndAskSource(String source) {
        if (((Activity)context).isFinishing())
            return;

        if (m_progressDlg == null) {
            m_progressDlg = new ProgressDialog(context);
            m_progressDlg.setTitle("Please wait");
            m_progressDlg.setIndeterminate(true);
        }

        m_progressDlg.setMessage(source);
        if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }
        //asking source
        /*LUCIControl luciControl = new LUCIControl(current_ipaddress);
        luciControl.SendCommand(50, null, LSSDPCONST.LUCI_GET);*/
    }

    private void showLoader() {

        if (((Activity)context).isFinishing())
            return;
        ShowLoader.showLoader(mCumulationsOnlineSourcesFragment.loadingBar, mCumulationsOnlineSourcesFragment.loadingText, context);
    }

    private void closeLoader() {
        mCumulationsOnlineSourcesFragment.loadingBar.clearAnimation();
        mCumulationsOnlineSourcesFragment.loadingBar.setVisibility(View.GONE);
        mCumulationsOnlineSourcesFragment.loadingText.setVisibility(View.GONE);
    }

    /*this method is to show error in whole application*/
    public void showErrorMessage(final LibreError message) {
        try {

            if (LibreApplication.hideErrorMessage)
                return;

            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    LibreLogger.d(this, "Showing the supertaloast " + System.currentTimeMillis());
                    SuperToast superToast = new SuperToast(context);

                    if (message != null && message.getErrorMessage().contains("is no longer available")) {
                        LibreLogger.d(this, "Device go removed showing error in Device Discovery");
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                    }
                    if(message.getmTimeout()==0) {//TimeoutDefault
                        superToast.setDuration(SuperToast.Duration.LONG);
                    }else{
                        superToast.setDuration(SuperToast.Duration.VERY_SHORT);
                    }
                    superToast.setText("" + message);
                    superToast.setAnimations(SuperToast.Animations.FLYIN);
                    superToast.setIcon(SuperToast.Icon.Dark.INFO, SuperToast.IconPosition.LEFT);
                    superToast.show();
                    if (message != null && message.getErrorMessage().contains(""))
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getconnectedSSIDname() {
        WifiManager wifiManager;
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        Log.d("CreateNewScene", "getconnectedSSIDname wifiInfo = " + wifiInfo.toString());
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }
        Log.d("SacListActivity", "Connected SSID" + ssid);
        return ssid;
    }


    public CumulationsOnlineSourcesRecyclerAdapter(ArrayList<String> onlineSources, Context context, String currentIpAddress, Fragment fragment) {
        this.onlineSources = onlineSources;
        this.context = context;
        this.currentIpAddress = currentIpAddress;
        this.mCumulationsOnlineSourcesFragment = (CumulationsOnlineSourcesFragment) fragment;
        if (fragment!=null) {
            ((CumulationsOnlineSourcesFragment) fragment).setUsernameAndPassword(new CumulationsOnlineSourcesFragment.onOnlineSourcesMessageRecieved() {
                @Override
                public void sendUsername(String name) {
                    userName = name;
                }

                @Override
                public void sendPassword(String password) {
                    userPassword = password;
                }
            });
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        TextView name;
        RelativeLayout relativeLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            image = (CircleImageView) itemView.findViewById(R.id.image);
            name = (TextView)itemView.findViewById(R.id.name);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
        }
    }

    @Override
    public CumulationsOnlineSourcesRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cumulations_online_sources_layout, parent, false);


        CumulationsOnlineSourcesRecyclerAdapter.MyViewHolder myViewHolder = new CumulationsOnlineSourcesRecyclerAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(CumulationsOnlineSourcesRecyclerAdapter.MyViewHolder myViewHolder, final int position) {
        ImageView image = myViewHolder.image;
        TextView name = myViewHolder.name;
        RelativeLayout relativeLayout = myViewHolder.relativeLayout;
        name.setText(onlineSources.get(position));
        if ("spotify".equalsIgnoreCase(onlineSources.get(position))) {
            image.setImageResource(spotify);
        } else if ("deezer".equalsIgnoreCase(onlineSources.get(position))) {
            image.setImageResource(deezer);
        } else if ("tidal".equalsIgnoreCase(onlineSources.get(position))) {
            image.setImageResource(tidal);
        } else if ("tunein".equalsIgnoreCase(onlineSources.get(position))) {
            image.setImageResource(tunein);
        } else if ("vtuner".equalsIgnoreCase(onlineSources.get(position))) {
            image.setImageResource(vtuner);
        } else if (context.getString(R.string.local_favorites).equalsIgnoreCase(onlineSources.get(position))) {
            image.setImageResource(favourites);
        } else if (context.getString(R.string.local_networkdevicePlayer).equalsIgnoreCase(onlineSources.get(position))) {
            image.setImageResource(networkDevicesPlayers);
        }

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("deezer".equalsIgnoreCase(onlineSources.get(position))) {
                    showLoader();
                    CumulationsSourcesOptionActivity.current_source_index_selected = 5;
//                    mCumulationsOnlineSourcesFragment.credentialHandler.sendEmptyMessageDelayed(CREDENTIAL_TIMEOUT, TIMEOUT);
                    ((CumulationsSourcesOptionActivity)context).credentialHandler.sendEmptyMessageDelayed(CREDENTIAL_TIMEOUT, TIMEOUT);

                    if (!getconnectedSSIDname().contains(Constants.DDMS_SSID)) {

//                        Toast.makeText(getApplicationContext(), "No Internet Connection ,in SA Mode", Toast.LENGTH_SHORT).show();
                        LUCIControl luciControl = new LUCIControl(currentIpAddress);
                        luciControl.sendAsynchronousCommand();
                        luciControl.SendCommand(208, "READ_DeezerUserName", LSSDPCONST.LUCI_SET);
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {

                        }
                        luciControl.SendCommand(208, "READ_DeezerUserPassword", LSSDPCONST.LUCI_SET);
                    }

                } else if ("tidal".equalsIgnoreCase(onlineSources.get(position))) {
                    showLoader();
                    ((CumulationsSourcesOptionActivity)context).credentialHandler.sendEmptyMessageDelayed(CREDENTIAL_TIMEOUT, TIMEOUT);
                    CumulationsSourcesOptionActivity.current_source_index_selected = 6;
//                    mCumulationsOnlineSourcesFragment.credentialHandler.sendEmptyMessageDelayed(CREDENTIAL_TIMEOUT, TIMEOUT);
                    if (!getconnectedSSIDname().contains(Constants.DDMS_SSID)) {
                        LUCIControl luciControl = new LUCIControl(currentIpAddress);
                        luciControl.sendAsynchronousCommand();
                        luciControl.SendCommand(208, "READ_TidalUserName", LSSDPCONST.LUCI_SET);
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {

                        }
                        luciControl.SendCommand(208, "READ_TidalUserPassword", LSSDPCONST.LUCI_SET);
                    }
                } else if ("spotify".equalsIgnoreCase(onlineSources.get(position))) {
                    if (((CumulationsSourcesOptionActivity)context).isFinishing())
                        return;

                    Intent spotifyIntent = new Intent(context, SpotifyInstructionsActivity.class);
                    spotifyIntent.putExtra("current_ipaddress", currentIpAddress);
                    spotifyIntent.putExtra("current_source", "" + currentIpAddress);

                    context.startActivity(spotifyIntent);
                } else if ("vtuner".equalsIgnoreCase(onlineSources.get(position))) {
                    CumulationsSourcesOptionActivity.current_source_index_selected = 1;
                /*Reset the UI to Home ,, will wait for the confirmation of home command completion and then start the required activity*/
                    LUCIControl luciControl = new LUCIControl(currentIpAddress);
                    luciControl.sendAsynchronousCommand();
                    luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);

                    ///////////// timeout for dialog - showLoader() ///////////////////
                    mCumulationsOnlineSourcesFragment.credentialHandler.sendEmptyMessageDelayed(NETWORK_TIMEOUT, Constants.ITEM_CLICKED_TIMEOUT);
                    showLoader();

                } else if ("tunein".equalsIgnoreCase(onlineSources.get(position))) {
//                    CumulationsOnlineSourcesFragment.current_source_index_selected = 2;
                    CumulationsSourcesOptionActivity.current_source_index_selected = 2;
                /*Reset the UI to Home ,, will wait for the confirmation of home command completion and then start the required activity*/
                    LUCIControl luciControl = new LUCIControl(currentIpAddress);
                    luciControl.sendAsynchronousCommand();
                    luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);

                    ///////////// timeout for dialog - showLoader() ///////////////////
                    mCumulationsOnlineSourcesFragment.credentialHandler.sendEmptyMessageDelayed(NETWORK_TIMEOUT, Constants.ITEM_CLICKED_TIMEOUT);
                    showLoader();

                } else if (onlineSources.get(position).contains(context.getString(R.string.local_favorites))){
                    showFavContents();
                } else if (onlineSources.get(position).contains(context.getString(R.string.local_networkdevicePlayer))){
                    CumulationsSourcesOptionActivity.current_source_index_selected = 0;
                    //Reset the UI to Home ,, will wait for the confirmation of home command completion and then start the required activity
                    LUCIControl luciControl = new LUCIControl(currentIpAddress);
                    luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
                    mCumulationsOnlineSourcesFragment.credentialHandler.sendEmptyMessageDelayed(NETWORK_TIMEOUT, Constants.ITEM_CLICKED_TIMEOUT);
                    LibreLogger.d(this, "sending handler msg");
                    showLoader();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return onlineSources.size();
    }

    private void showFavContents() {
        CumulationsSourcesOptionActivity.current_source_index_selected = 7;
        LUCIControl luciControl = new LUCIControl(currentIpAddress);
        luciControl.sendAsynchronousCommand();
        luciControl.SendCommand(MIDCONST.MID_REMOTE_UI, GET_HOME, LSSDPCONST.LUCI_SET);
        mCumulationsOnlineSourcesFragment.credentialHandler.sendEmptyMessageDelayed(NETWORK_TIMEOUT,Constants.ITEM_CLICKED_TIMEOUT);
        showLoader();
    }
}
