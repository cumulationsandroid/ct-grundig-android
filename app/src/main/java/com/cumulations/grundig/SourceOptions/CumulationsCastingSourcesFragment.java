package com.cumulations.grundig.SourceOptions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cumulations.grundig.R;

import java.util.ArrayList;

/**
 * Created by cumulations on 25/7/17.
 */

public class CumulationsCastingSourcesFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private CumulationsCastingSourcesRecyclerAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private String currentIpAddress;
    private boolean btStatus;
    private boolean AuxStatus;
    ArrayList<String> castingSources =new ArrayList<>();
    public static CumulationsCastingSourcesFragment newInstance(String currentIpAddress) {
        Bundle args = new Bundle();
        args.putString("IPADDRESS", currentIpAddress);
        CumulationsCastingSourcesFragment fragment = new CumulationsCastingSourcesFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentIpAddress = getArguments().getString("IPADDRESS");
        castingSources    = new ArrayList<String>(){{
            add("Spotify");
            add("Deezer");
            add("Google play");
            add("Pandora");
            add("Tunein");
            add("");
            //add(Html.fromHtml(getResources().getString(R.string.google_Cast_enabled_apps)).toString());
        }};

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cumulations_casting_sources, container, false);
        mRecyclerView = (RecyclerView)(view.findViewById(R.id.recycler_view));
        mLayoutManager = new LinearLayoutManager(getActivity());
        if (!CumulationsOnlineSourcesFragment.shouldShowCasting) {
            castingSources.clear();
        }
        mAdapter = new CumulationsCastingSourcesRecyclerAdapter(castingSources, getActivity(), currentIpAddress);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration divider = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.cumulations_recycler_divider));
        mRecyclerView.addItemDecoration(divider);
        return view;
    }
}
