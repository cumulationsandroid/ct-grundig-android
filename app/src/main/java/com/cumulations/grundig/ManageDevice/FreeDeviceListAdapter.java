package com.cumulations.grundig.ManageDevice;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cumulations.grundig.NewManageDevices.NewManageAdapter;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.util.LibreLogger;
import com.github.florent37.viewtooltip.ViewTooltip;

import java.util.ArrayList;
import java.util.Collection;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by karunakaran on 8/8/2015.
 */
public class FreeDeviceListAdapter extends ArrayAdapter<LSSDPNodes> {

    private Context mContext;
    private int id;
    public boolean isLS9Available;
    private boolean isFreeDeviceAvailable;
    private RelativeLayout relativeLayout;

    public static ArrayList <LSSDPNodes>FreeDeviceLSSDPNodeMap = new ArrayList<LSSDPNodes>();
    ScanningHandler mScanHandler = ScanningHandler.getInstance();
    public FreeDeviceListAdapter(Context context, int textViewResourceId)
    {

        super(context, textViewResourceId);
        mContext = context;
        id = textViewResourceId;


    }

    public int getFreeDeviceSize() {
        return FreeDeviceLSSDPNodeMap.size();
    }

    public boolean FreedeviceListAvailable(LSSDPNodes mNewNode){            
        for(LSSDPNodes mOldNode : FreeDeviceLSSDPNodeMap){
            if(mOldNode!=null && mNewNode != null && mOldNode.getUSN().equalsIgnoreCase(mNewNode.getUSN())){
                return true;
            }
        }
        return false;
    }

    public boolean isFreeDeviceAvailable(){
        return isFreeDeviceAvailable;
    }
    @Override
    public void add(LSSDPNodes object) {
        if(!FreedeviceListAvailable(object)) {
            isFreeDeviceAvailable=true;
            FreeDeviceLSSDPNodeMap.add(object);
        }
        super.add(object);
    }

    @Override
    public void addAll(Collection collection) {
        super.addAll(collection);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public void clear() {
        FreeDeviceLSSDPNodeMap.clear();
        super.clear();
    }

    @Override
    public int getCount() {

        isLS9Available= LSSDPNodeDB.isCastDevicesExistsInTheNetwork();
        isFreeDeviceAvailable =false;
        if(isLS9Available){
            if(LSSDPNodeDB.getInstance().GetDB().get(0).getNetworkMode().contains("P2P")){
                isLS9Available=false;
            }
        }
        if(isLS9Available)
            return FreeDeviceLSSDPNodeMap.size();
        else
        {
            if(FreeDeviceLSSDPNodeMap.size()>0)
                return FreeDeviceLSSDPNodeMap.size();
            else{
                // returning 1 because getView will be called only when getCount returns 0.
                // variable isFreeDeviceAvailable is used to later filter in getView method.
                isFreeDeviceAvailable =false;
                return 0;
            }

        }
    }

    @Override
    public void remove(LSSDPNodes object) {
        FreeDeviceLSSDPNodeMap.remove(object);
        super.remove(object);
    }

    @Override
    public View getView(int position, final View v, ViewGroup parent)
    {
        View mView = v ;
        if(mView == null){
            LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mView = vi.inflate(id, null);
        }
        ManageDeviceHandler mManage = ManageDeviceHandler.getInstance();
        String ipaddress = "";
        RelativeLayout mRelative = (RelativeLayout) mView.findViewById(R.id.list_item_layout);
        if (position == 0) {
            relativeLayout = mRelative;
        }
        TextView text = (TextView) mView.findViewById(R.id.list_item_textView);
        TextView mCastPlayingTv = (TextView) mView.findViewById(R.id.tvCastIsPlaying);
        //ImageView image = (ImageView) mView.findViewById(R.id.manage_device_speaker);
        ImageView castIcon = (ImageView) mView.findViewById(R.id.casting_icon);

        if(!isFreeDeviceAvailable) {
            LibreLogger.d(this, "device count is " + getCount());
            //image.setVisibility(View.VISIBLE);
            if (isLS9Available && position == FreeDeviceLSSDPNodeMap.size()) {
                text.setText(R.string.createCastGrp);
                //image.setImageResource(R.mipmap.ic_cast_white_24dp_2x);
            } else {
                if( FreeDeviceLSSDPNodeMap.get(position).getCurrentSource() == MIDCONST.GCAST_SOURCE
                        && FreeDeviceLSSDPNodeMap.get(position).getmPlayStatus() == SceneObject.CURRENTLY_PLAYING){
                    /*mView.setBackground(mContext.getResources().getDrawable(R.drawable.app_rounded_cast_playing));
                    mCastPlayingTv.setVisibility(View.VISIBLE);
                    mCastPlayingTv.setText(R.string.CastPlaying);*/
//                    mView.setBackground(mContext.getResources().getDrawable(R.drawable.app_rounded_corners_list_item_selected));
                    castIcon.setVisibility(View.VISIBLE);
                    text.setTypeface(null,Typeface.ITALIC);

                }else{
//                    mRelative.setBackgroundResource(0);
                    mCastPlayingTv.setVisibility(View.GONE);
                    castIcon.setVisibility(View.GONE);
                    text.setTypeface(null,Typeface.NORMAL);
                    if(position == mManage.selectedPosition) {
                        mView.setSelected(true);
//                        mView.setBackground(mContext.getResources().getDrawable(R.drawable.app_rounded_corners_list_item_selected));
                    }
                }
                text.setText(FreeDeviceLSSDPNodeMap.get(position).getFriendlyname());
                //image.setImageResource(R.mipmap.speaker_icon);
            }
        }else{
            //text.setText(R.string.noDeviceFound);
            //image.setVisibility(View.GONE);
        }
        return mView;
    }

    public ViewTooltip.TooltipView showToolTip() {
        return showViewToolTip(relativeLayout, mContext.getString(R.string.select_master_speaker), ViewTooltip.Position.BOTTOM, false);
    }

    public ViewTooltip.TooltipView showViewToolTip(View view, String text, ViewTooltip.Position position, final boolean shouldRepeat) {
        ViewTooltip.TooltipView toolTipView = null;
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("appreference", MODE_PRIVATE);

        if (view ==null)
        {
            //Toast.makeText(context,"Hello", Toast.LENGTH_LONG).show();
            return null;
        }

        if (sharedPreferences.getBoolean(FreeDeviceListAdapter.class.getSimpleName(),false) == false) {
            sharedPreferences.edit().putBoolean(FreeDeviceListAdapter.class.getSimpleName(),true).commit();

            toolTipView = ViewTooltip
                    .on(view)
                    .autoHide(true, 5000)
                    .clickToHide(true)
                    .align(ViewTooltip.ALIGN.CENTER)
                    .position(position)
                    .text(text)
                    .textColor(mContext.getResources().getColor(R.color.viewToolTipTextColor))
                    .color(mContext.getResources().getColor(R.color.viewToolTipColor))
                    .corner(10)
                    .show();
        }

        return toolTipView;
    }

}