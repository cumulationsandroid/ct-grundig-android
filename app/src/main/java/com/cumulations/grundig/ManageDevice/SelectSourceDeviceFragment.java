package com.cumulations.grundig.ManageDevice;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cumulations.grundig.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class SelectSourceDeviceFragment extends Fragment {

    public SelectSourceDeviceFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_source_device, container, false);
    }
}
