package com.cumulations.grundig;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cumulations.grundig.Network.PlayTestSoundForChromeCastDevice;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.ShowLoader;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Anirudh Uppunda on 26/9/17.
 */

public class AddNewSpeakerToWifiHelpScreenActivity extends AppCompatActivity {
    int[] mLayouts = {
            R.layout.helpscreen_layout
    };

    int[] mDrawables = {
            R.drawable.tutorial0,
            R.drawable.gr_configure_xxxx_image
    };

    int[] mTexts = {
            R.string.add_new_speaker_to_wifi_step1,
            R.string.tapOnTheSettingsButton
    };
    protected AlertDialog alert;
    private String mCurrentSsidItGotConnected;
    boolean isUserComingBackFromWiFiSettings;
    private ViewPager mViewPager;
    private CircleIndicator mCircleIndicator;
    private ImageView mLoading;
    private TextView mLoadingText;
    private HelpScreenStepsPagerAdapter helpScreenStepsPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnewspeaker_towifi__helpscreen_activity);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mLoadingText = (TextView)findViewById(R.id.loadingText);
        mLoading = (ImageView)findViewById(R.id.loading);
        mLoadingText.setText(getString(R.string.pleaseWait));
        TextView backButton= (TextView) findViewById(R.id.back);
        mViewPager = (ViewPager)findViewById(R.id.viewpager);
        mCircleIndicator = (CircleIndicator)findViewById(R.id.viewpagerindicator);


        findViewById(R.id.scene_title).setSelected(true);

        helpScreenStepsPagerAdapter = new HelpScreenStepsPagerAdapter();
        mViewPager.setAdapter(helpScreenStepsPagerAdapter);
        mCircleIndicator.setViewPager(mViewPager);
        backButton.setOnClickListener(

                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        {
                            onBackPressed();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCurrentSsidItGotConnected = getconnectedSSIDname(AddNewSpeakerToWifiHelpScreenActivity.this);
        if ( isUserComingBackFromWiFiSettings &&   !mCurrentSsidItGotConnected.contains(Constants.WAC_SSID)
                && !mCurrentSsidItGotConnected.endsWith(".d")
                && !LibreApplication.activeSSID.equalsIgnoreCase(mCurrentSsidItGotConnected)) {


            if (!AddNewSpeakerToWifiHelpScreenActivity.this.isFinishing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddNewSpeakerToWifiHelpScreenActivity.this);
                alert = null;
                builder.setMessage(getResources().getString(R.string.title_error_connection) + "( " + mCurrentSsidItGotConnected + ")" +
                        " \n" + getString(R.string.networkChangeRestartApp))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                mCurrentSsidItGotConnected = "";
                                restartApp(AddNewSpeakerToWifiHelpScreenActivity.this);
                                alert.dismiss();
                            }
                        });
                if (alert == null) {
                    alert = builder.show();
                    TextView messageView = (TextView) alert.findViewById(android.R.id.message);
                    messageView.setGravity(Gravity.CENTER);
                }

                alert.show();
            }
        }
        if(mViewPager!=null && mCurrentSsidItGotConnected.contains(Constants.WAC_SSID))
        {
            if( helpScreenStepsPagerAdapter.mFinishButton!=null)
                helpScreenStepsPagerAdapter.mFinishButton.setText(getString(R.string.next));

        }

    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        /*if(mViewPager!=null && mViewPager.getCurrentItem()==0) {
            finish();
        }else {
            mViewPager.setCurrentItem(0);
        }*/
        finish();
    }


    public String getconnectedSSIDname(Context mContext) {
        WifiManager wifiManager;
        wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        LibreLogger.d(this, "getconnectedSSIDname wifiInfo = " + wifiInfo.toString());
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }


        return ssid;
    }


    /* Created by Karuna, To fix the RestartApp Issue
    * * Till i HAave to Analyse more on this code */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void restartApp(Context context) {
        Log.d("NetworkChanged", "App is Restarting");
        //finish();
        /* Stopping ForeGRound Service Whenwe are Restarting the APP */
        Intent in = new Intent(AddNewSpeakerToWifiHelpScreenActivity.this, DMRDeviceListenerForegroundService.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(in);

        Intent mStartActivity = new Intent(context, SplashScreenActivity.class);
                /*sending to let user know that app is restarting*/
        mStartActivity.putExtra(SplashScreenActivity.APP_RESTARTING, true);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 200, mPendingIntent);

        /* * Finish this activity, and tries to finish all activities immediately below it
     * in the current task that have the same affinity.*/
        ActivityCompat.finishAffinity(this);
        /* Killing our Android App with The PID For the Safe Case */
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        //System.exit(0);

    }




    public class HelpScreenStepsPagerAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;
        Button mFinishButton;
        TextView mText;
        TextView mSubText;
        ImageView mConfigureImage;
        WebView configGifWebView;

        public HelpScreenStepsPagerAdapter() {
            mLayoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public int getCount() {
            return mTexts.length;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.helpscreen_layout, container, false);
            mFinishButton = (Button)itemView.findViewById(R.id.finishButton);
            modifyButtonLayoutAndSetText(mFinishButton);
            mText = (TextView)itemView.findViewById(R.id.step_text);
            mSubText = (TextView)itemView.findViewById(R.id.image_subtext);
            mConfigureImage = (ImageView)itemView.findViewById(R.id.configure_image);
            configGifWebView = (WebView) itemView.findViewById(R.id.gif_image_webview);
            if (position == (mTexts.length - 1)) {
                mFinishButton.setVisibility(View.VISIBLE);

            }
            mFinishButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mCurrentSsidItGotConnected.contains(Constants.WAC_SSID)) {
                        Intent newIntent=new Intent(AddNewSpeakerToWifiHelpScreenActivity.this, PlayTestSoundForChromeCastDevice.class);
                        startActivity(newIntent);
                        finish();
                    }
                    else{
                        showLoader(getString(R.string.action_settings));
                        isUserComingBackFromWiFiSettings = true;

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                closeLoader();
                            /* Giving User Access to Connect to Specific WAC_SSIDXXXX*/
                                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            }
                        }, 1000);
                    }
                }
            });
            String text = getResources().getString(R.string.tutorial_step)+" "+ (position+1);
            mText.setText(text);
//            mText.setText(R.string.tutorial_step + (position + 1));
            mSubText.setText(mTexts[position]);
            if (position == 0) {
                mConfigureImage.setVisibility(View.GONE);
                configGifWebView.setVisibility(View.VISIBLE);
                configGifWebView.setBackgroundColor(Color.TRANSPARENT); //for gif without background
                configGifWebView.loadUrl("file:///android_asset/htmls/hn_blink.html");
            } else {
                configGifWebView.setVisibility(View.GONE);
                mConfigureImage.setVisibility(View.VISIBLE);
                mConfigureImage.setImageResource(mDrawables[position]);
            }
            container.addView(itemView);
            return itemView;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    private void modifyButtonLayoutAndSetText(Button button) {
        if(mCurrentSsidItGotConnected!=null && mCurrentSsidItGotConnected.contains(Constants.WAC_SSID))
            button.setText(getString(R.string.next));
        else
            button.setText(getString(R.string.openSetting));
        button.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        button.setPadding(10, 8, 10, 8);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)button.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        button.setLayoutParams(params);
    }
    private void closeLoader() {
        mLoading.clearAnimation();
        mLoading.setVisibility(View.GONE);
        mLoadingText.setVisibility(View.GONE);
    }

    public void showLoader(final String msg) {
        ShowLoader.showLoader(mLoading, mLoadingText, AddNewSpeakerToWifiHelpScreenActivity.this);
    }
}
