package com.cumulations.grundig;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Anirudh Uppunda on 19/9/17.
 */

public class SAModeHelpScreenActivity extends AppCompatActivity {

    int[] mLayouts = {
            R.layout.helpscreen_layout
    };

    int[] mDrawables = {
            R.drawable.ellipse_4_copy,
            R.drawable.ellipse_4_copy,
            R.drawable.sa_mode_image_new,
            R.drawable.gr_configure_xxxx,
            R.drawable.samodestep4
    };

    int[] mTexts = {
            R.string.samodestep1string,
            R.string.sa_mode_step2_gif_string,
            R.string.samodestep3stringNew,
            R.string.samodestep3string,
            R.string.samodestep4string
    };
    private ViewPager mViewPager;
    private CircleIndicator mCircleIndicator;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_samode_helpscreen_activity);
        TextView backButton= (TextView) findViewById(R.id.back);
        mViewPager = (ViewPager)findViewById(R.id.viewpager);
        mCircleIndicator = (CircleIndicator)findViewById(R.id.viewpagerindicator);
        HelpScreenStepsPagerAdapter helpScreenStepsPagerAdapter = new HelpScreenStepsPagerAdapter();
        mViewPager.setAdapter(helpScreenStepsPagerAdapter);
        mCircleIndicator.setViewPager(mViewPager);
        backButton.setOnClickListener(

                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        {
                            onBackPressed();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        /*if(mViewPager!=null && mViewPager.getCurrentItem()==0) {
            finish();
        }else {
            mViewPager.setCurrentItem(0);
        }*/
        finish();
    }

    public class HelpScreenStepsPagerAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;
        Button mFinishButton;
        TextView mText;
        TextView mSubText;
        ImageView mConfigureImage;
        WebView gifWebView;

        public HelpScreenStepsPagerAdapter() {
            mLayoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public int getCount() {
            return mTexts.length;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.helpscreen_layout, container, false);
            mFinishButton = (Button)itemView.findViewById(R.id.finishButton);
            mText = (TextView)itemView.findViewById(R.id.step_text);
            mSubText = (TextView)itemView.findViewById(R.id.image_subtext);
            mConfigureImage = (ImageView)itemView.findViewById(R.id.configure_image);
            gifWebView = (WebView) itemView.findViewById(R.id.gif_image_webview);
            if (position == (mTexts.length - 1)) {
                mFinishButton.setVisibility(View.VISIBLE);
            }
            mFinishButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            String text = getResources().getString(R.string.tutorial_step)+" "+ (position+1);
            mText.setText(text);
            mSubText.setText(mTexts[position]);
            if (position == 1){
                gifWebView.setVisibility(View.VISIBLE);
                mConfigureImage.setVisibility(View.GONE);
                gifWebView.setBackgroundColor(Color.TRANSPARENT); //for gif without background
                gifWebView.loadUrl("file:///android_asset/htmls/sa_blink.html");
            } else {
                gifWebView.setVisibility(View.GONE);
                mConfigureImage.setVisibility(View.VISIBLE);
            }
            mConfigureImage.setImageResource(mDrawables[position]);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}
