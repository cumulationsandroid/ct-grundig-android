package com.cumulations.grundig;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SourceOptions.CumulationsSourcesOptionActivity;
import com.cumulations.grundig.app.dlna.dmc.LocalDMSActivity;
import com.cumulations.grundig.app.dlna.dmc.utility.PlaybackHelper;
import com.cumulations.grundig.app.dlna.dmc.utility.UpnpDeviceManager;
import com.cumulations.grundig.constants.CommandType;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.netty.BusProvider;
import com.cumulations.grundig.nowplaying.CumulationsNowPlayingActivity;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.PicassoTrustCertificates;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;

import org.fourthline.cling.model.meta.RemoteDevice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import static com.cumulations.grundig.LibreApplication.isConvertViewClicked;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.DEEZER_SOURCE;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.REPEAT_OFF;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.TIDAL_SOURCE;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.TUNEIN_SOURCE;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.USB_SOURCE;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.VTUNER_SOURCE;

/**
 * Created by karunakaran on 7/22/2015.
 */
public class ActiveSceneAdapter extends ArrayAdapter<SceneObject> {

    public static final String SCENE_MAP = "scene_map";
    public static final String CURRENT_IPADDRESS = "current_ipaddress";


    public static final int PREPARATION_INIT = 0x77;
    public static final java.lang.String SCENE_POSITION = "scene_position";


    public static LinkedHashMap<String, SceneObject> sceneMap = new LinkedHashMap<>();
    Context context;
    private boolean isVolumeEnabled = false;
    private HashMap<String, String> volumeHashMap = new HashMap<>();

    Handler activehandler;

    public ActiveSceneAdapter(Context context, int textViewResourceId,Handler handler) {

        super(context, textViewResourceId);
        this.context = context;
       activehandler=handler;
    }
    public static int getCountNumberofDevicesInAdapter(){
        try {

            return sceneMap.keySet().toArray().length;
        } catch (Exception e) {

            return 0;
        }
    }
    public boolean checkSceneobjectIsPresentinList(String mNodeIp) {
        return sceneMap.containsKey(mNodeIp);
    }

    @Override
    public int getCount() {

        try {

            return sceneMap.keySet().toArray().length;
        } catch (Exception e) {

            return 0;
        }
    }

    @Override
    public void add(SceneObject object) {


        /*if((object.getCurrentSource() == MIDCONST.GCAST_SOURCE)
            && (object.getPlaystatus() == SceneObject.CURRENTLY_STOPED)){

        }else*/{
            sceneMap.put(object.getIpAddress(), object);
            super.add(object);
        }
    }

    @Override
    public void addAll(Collection<? extends SceneObject> collection) {
        super.addAll(collection);

        Iterator<? extends SceneObject> iterator = collection.iterator();
        while (iterator.hasNext()) {
            SceneObject sceneObject = iterator.next();
            sceneMap.put(sceneObject.getIpAddress(), sceneObject);
        }
    }


    @Override
    public void insert(SceneObject object, int index) {
        super.insert(object, index);
    }

    @Override
    public void sort(Comparator<? super SceneObject> comparator) {
        super.sort(comparator);
    }

    @Override
    public void setNotifyOnChange(boolean notifyOnChange) {
        super.setNotifyOnChange(notifyOnChange);
    }

    @Override
    public void notifyDataSetChanged() {

        super.notifyDataSetChanged();
    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public SceneObject getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(SceneObject item) {
        return super.getPosition(item);
    }

    @Override
    public void setDropDownViewResource(int resource) {
        super.setDropDownViewResource(resource);
    }

    @Override
    public Filter getFilter() {
        return super.getFilter();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return super.getDropDownView(position, convertView, parent);
    }

    @Override
    public void clear() {
        sceneMap.clear();
        super.clear();
        sceneMap = new LinkedHashMap<>();
    }

    @Override
    public void remove(SceneObject object) {
        LibreLogger.d(this, "ActiveSceneList: Device has been removed");
        if(checkSceneobjectIsPresentinList(object.getIpAddress())) {
            sceneMap.remove(object.getIpAddress());
        }

        super.remove(object);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public class Holder {
        TextView SceneName, trackName, NumberOfDevice;
        ImageView albumArt;
        ImageButton mute;
        SeekBar volumebar;
        TextView volumePercentage;
        ImageButton play, next, previous;
        String ipaddress;
        ImageView mutebutton;
        ProgressBar preparationProgressBar;

        TextView sourceName;
        ImageView sourceImage;
        LinearLayout mLinearLayout;

        RelativeLayout mRelativeLayout;
        /* Added to handle the case where trackname is empty string"*/
        String currentTrackName = "-1";
        int previousSourceIndex;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Holder holder;
        final ScanningHandler mScanHandler = ScanningHandler.getInstance();
        // mScanHandler.updateSlaveToMasterHashMap();
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = View.inflate(context,R.layout.scenes_listview_layout, null);

            holder = new Holder();

            holder.albumArt = (ImageView) convertView.findViewById(R.id.albumart);
            holder.SceneName = (TextView) convertView.findViewById(R.id.name);
            holder.SceneName.setSelected(true);
            holder.trackName = (TextView) convertView.findViewById(R.id.trackName);
            holder.trackName.setSelected(true);
            holder.NumberOfDevice = (TextView) convertView.findViewById(R.id.numberOfDevice);
            holder.volumebar = (AppCompatSeekBar) convertView.findViewById(R.id.seekBar);
            holder.volumePercentage = (TextView)convertView.findViewById(R.id.volume_percentage);
            holder.play = (ImageButton) convertView.findViewById(R.id.media_btn_play);
            holder.previous = (ImageButton) convertView.findViewById(R.id.media_btn_previous);
            holder.next = (ImageButton) convertView.findViewById(R.id.media_btn_nxt);
            holder.mutebutton = (ImageView) convertView.findViewById(R.id.mute_button);

            holder.preparationProgressBar = (ProgressBar) convertView.findViewById(R.id.progress_loader);
            String ipaddress = (String) sceneMap.keySet().toArray()[position];
            holder.ipaddress = ipaddress;
            holder.sourceImage = (ImageView) convertView.findViewById(R.id.source_image);
            holder.sourceName = (TextView) convertView.findViewById(R.id.sourcename);
            holder.currentTrackName = "-1";
            holder.mRelativeLayout = (RelativeLayout)convertView.findViewById(R.id.main);
            holder.mLinearLayout = (LinearLayout)convertView.findViewById(R.id.playpauseandseekbarlayout);
            convertView.setTag(holder);


        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.mLinearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        holder.mRelativeLayout.setBackgroundColor(Color.parseColor("#ca000000"));

        /*if (position % 2 != 0) {
            holder.mRelativeLayout.setBackgroundColor(Color.parseColor("#000000"));
        } else {
            //holder.mRelativeLayout.setBackgroundResource(R.drawable.background);
            holder.mRelativeLayout.setBackgroundColor(Color.parseColor("#ca000000"));
            //holder.mRelativeLayout.setBackgroundResource(R.drawable.zonesettings_rectangle);
        }*/

        /* Resetting everytimne the adapter is called so that we can handle source switch between aux to others */
        holder.previous.setClickable(true);
        holder.next.setClickable(true);
        holder.mutebutton.setClickable(true);
        holder.mutebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Log.d("VOLUMEBAR", "clicked!!!");
                isVolumeEnabled = !isVolumeEnabled;
                if (isVolumeEnabled) {
                    holder.mutebutton.setBackgroundResource(R.drawable.ic_active_scenes_volume_up);
                    if (volumeHashMap.size() > 0) {
                        boolean isVolumeZero = false;
                        changeVolumeLogic(mScanHandler, holder, isVolumeZero);
                    }
                } else {
                    holder.mutebutton.setBackgroundResource(R.drawable.ic_active_scenes_volume_off);
                    volumeHashMap.put(holder.ipaddress, String.valueOf(holder.volumebar.getProgress()));
                    changeVolumeLogic(mScanHandler, holder, true);
                }*/
            }
        });


        holder.ipaddress = (String) sceneMap.keySet().toArray()[position];
        ;

        String numberOfDevices = mScanHandler.getNumberOfSlavesForMasterIp(holder.ipaddress, mScanHandler.getconnectedSSIDname(context)) + " "+
                context.getString(R.string.speakers);
        holder.NumberOfDevice.setText(numberOfDevices);
        /*LSSDPNodes mNode = mScanHandler.getLSSDPNodeFromCentralDB(holder.ipaddress);
        if (mNode != null) {
            LibreLogger.d("com.libre.Scanning", "Number of Devices For MasterIp: " + holder.ipaddress + "Device State " + mNode.getDeviceState());
            if (mNode.getDeviceState().contains("M")) {
                ArrayList<String> mSlaveList = mScanHandler.getSlaveListFromMasterIp(holder.ipaddress);
                LibreLogger.d("com.libre.Scanning", "Number of Devices For MasterIp: " + holder.ipaddress + "Number of Devices " + mSlaveList.size());
                int mNumberOfDevices = mSlaveList.size() + 1;
                holder.NumberOfDevice.setText("Speakers : " + mNumberOfDevices);
            }
        } else {
            LibreLogger.d("com.libre.Scanning", "Number of Devices For MasterIp: " + holder.ipaddress + "mNode is Null " + "null");
        }*/

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isConvertViewClicked) {
                    isConvertViewClicked = true;
                    if (sceneMap.size() > 0) {
                        ArrayList<String> arrayList = new ArrayList<>();

                        for (String key : sceneMap.keySet()) {
                            arrayList.add(key);
                        }

                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            ((ActiveScenesListActivity)context).getWindow().setSharedElementEnterTransition(enterTransition());
                            ((ActiveScenesListActivity)context).getWindow().setSharedElementReturnTransition(returnTransition());

                            ActivityOptionsCompat options = ActivityOptionsCompat.
                                    makeSceneTransitionAnimation((Activity) context, (View)holder.albumArt, "albumArt");
                            context.startActivity(new Intent(context, CumulationsNowPlayingActivity.class)
                                    .putExtra("shouldShowViewWithoutAnimation", false)
                                    .putExtra(CURRENT_IPADDRESS, holder.ipaddress)
                                    .putExtra(SCENE_MAP, arrayList)
                                    .putExtra(SCENE_POSITION, position)*//*
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)*//*, options.toBundle());
                        } else {*/
                            context.startActivity(new Intent(context, CumulationsNowPlayingActivity.class)
                                    .putExtra(CURRENT_IPADDRESS, holder.ipaddress)
                                    .putExtra(SCENE_MAP, arrayList)
                                    .putExtra(SCENE_POSITION, position)/*
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)*/);
//                            ((ActiveScenesListActivity)context).overridePendingTransition(R.anim.enter, R.anim.exit);
//                        }
                    }
                    else{
                        Log.d("ActiveSceneAdapter","isConvertViewClicked = "+isConvertViewClicked);
                        /**notifying adapter as sometime scene is present but not lssdp node*/
                        notifyDataSetChanged();
                    }
                }
            }
        });

        holder.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Next", "" + position);
                LSSDPNodes mNodeWeGotForControl = mScanHandler.getLSSDPNodeFromCentralDB(holder.ipaddress);
                if(mNodeWeGotForControl==null)
                    return;
                SceneObject scene = sceneMap.get(holder.ipaddress);
                if(scene!=null
                        && scene.getCurrentSource() == Constants.AUX_SOURCE
                        || scene.getCurrentSource() == Constants.GCAST_SOURCE
                        || scene.getCurrentSource() == Constants.VTUNER_SOURCE
                        || scene.getCurrentSource() == Constants.TUNEIN_SOURCE
                        || (scene.getCurrentSource() == Constants.BT_SOURCE
                        && ((mNodeWeGotForControl.getgCastVerision() == null
                        && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                        || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                        ||(mNodeWeGotForControl.getgCastVerision() != null
                        && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){
                    LibreError error = new LibreError("", context.getString(R.string.NEXT_PREVIOUS_NOT_ALLOWED),1);
                    BusProvider.getInstance().post(error);
                    return ;
                }

                doNextPrevious(true, scene);
            }
        });
        holder.previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("previous", "" + position);
                LSSDPNodes mNodeWeGotForControl = mScanHandler.getLSSDPNodeFromCentralDB(holder.ipaddress);
                if(mNodeWeGotForControl==null)
                    return;
                SceneObject scene = sceneMap.get(holder.ipaddress);
                if(scene!=null
                        && scene.getCurrentSource() == Constants.AUX_SOURCE
                        || scene.getCurrentSource() == Constants.GCAST_SOURCE
                        || scene.getCurrentSource() == Constants.VTUNER_SOURCE
                        || scene.getCurrentSource() == Constants.TUNEIN_SOURCE
                        || (scene.getCurrentSource() == Constants.BT_SOURCE
                        && ((mNodeWeGotForControl.getgCastVerision() == null
                        && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                        || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                        ||(mNodeWeGotForControl.getgCastVerision() != null
                        && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){
                    LibreError error = new LibreError("", context.getString(R.string.NEXT_PREVIOUS_NOT_ALLOWED),1);
                    BusProvider.getInstance().post(error);
                    return ;
                }
                doNextPrevious(false, scene);

            }
        });
        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LSSDPNodes mNodeWeGotForControl = mScanHandler.getLSSDPNodeFromCentralDB(holder.ipaddress);
                if(mNodeWeGotForControl==null)
                    return;
                SceneObject scene = sceneMap.get(holder.ipaddress);

                if (handleifSpeakerIsInNoSourceMode(scene)) {
                    return;
                }
                if(scene!=null
                        && scene.getCurrentSource() == Constants.AUX_SOURCE
                        || scene.getCurrentSource() == Constants.GCAST_SOURCE
                        || /*scene.getCurrentSource() == Constants.VTUNER_SOURCE
                        || scene.getCurrentSource() == Constants.TUNEIN_SOURCE
                        ||*/ (scene.getCurrentSource() == Constants.BT_SOURCE
                        && ((mNodeWeGotForControl.getgCastVerision() == null
                        && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                        || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                        ||(mNodeWeGotForControl.getgCastVerision() != null
                        && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){
                    LibreError error = new LibreError("", context.getString(R.string.PLAY_PAUSE_NOT_ALLOWED),1);//TimeoutValue !=0 means ,its VERYSHORT
                    BusProvider.getInstance().post(error);
                    return ;
                }


                if (scene!=null && scene.getCurrentSource() == 2) {
                    LibreLogger.d(this,"current source is DMR");
                    RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(scene.getIpAddress());
                    if (renderingDevice != null) {
                        String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                        PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);

                        if ( playbackHelper==null|| playbackHelper.getDmsHelper() == null||(scene!=null && false==scene.getPlayUrl().contains(LibreApplication.LOCAL_IP))) {
                            Toast.makeText(context, context.getString(R.string.no_active_playlist_select), Toast.LENGTH_SHORT).show();


                    /* In SA mode we need to go to local content while in HN mode we will go to sources option */

                            if (LibreApplication.activeSSID.equalsIgnoreCase(Constants.DDMS_SSID)) {
                                Intent localIntent = new Intent(context, LocalDMSActivity.class);
                                localIntent.putExtra("isLocalDeviceSelected", true);
                                localIntent.putExtra("current_ipaddress", scene.getIpAddress());
                                context.startActivity(localIntent);
                            } else {
                                Intent localIntent = new Intent(context, CumulationsSourcesOptionActivity.class);
                                localIntent.putExtra("current_ipaddress", scene.getIpAddress());
                                localIntent.putExtra("current_source", "" + scene.getCurrentSource());
                                context.startActivity(localIntent);
                            }
                            return;
                        }

                    }
                    else if (scene!=null&& scene.getPlayUrl()!=null && scene.getPlayUrl().contains(LibreApplication.LOCAL_IP)){
                        /* renderer is not found and hence there is no playlist also so open the sources option */


                                            /* In SA mode we need to go to local content while in HN mode we will go to sources option */

                        if (LibreApplication.activeSSID.equalsIgnoreCase(Constants.DDMS_SSID)) {
                            Intent localIntent = new Intent(context, LocalDMSActivity.class);
                            localIntent.putExtra("isLocalDeviceSelected", true);
                            localIntent.putExtra("current_ipaddress", scene.getIpAddress());
                            context.startActivity(localIntent);
                        } else {
                            Intent localIntent = new Intent(context, CumulationsSourcesOptionActivity.class);
                            localIntent.putExtra("current_ipaddress", scene.getIpAddress());
                            localIntent.putExtra("current_source", "" + scene.getCurrentSource());
                            context.startActivity(localIntent);
                        }
                        return;
                    }

                }



                LibreLogger.d(this,"current source is not DMR"+scene.getCurrentSource());




                if (scene!=null && scene.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
                    LUCIControl.SendCommandWithIp(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.PAUSE, CommandType.SET, holder.ipaddress);
                    scene.setPlaystatus(SceneObject.CURRENTLY_STOPED);
                    holder.play.setImageResource(R.drawable.activescenes_play);
                    holder.next.setImageResource(R.drawable.activescenes_forward);
                    holder.previous.setImageResource(R.drawable.activescenes_previous);
                    //holder.mutebutton.setImageResource(R.drawable.ic_active_scenes_volume_up);

                } else {
                    if(scene!=null && scene.getCurrentSource() == 19){ /* Change Done By Karuna, Because for BT Source there is no RESUME*/
                        LUCIControl.SendCommandWithIp(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.PLAY, CommandType.SET, holder.ipaddress);
                    }else {
                        LUCIControl.SendCommandWithIp(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.RESUME, CommandType.SET, holder.ipaddress);
                    }
                    scene.setPlaystatus(SceneObject.CURRENTLY_PLAYING);
                    holder.play.setImageResource(R.drawable.activescenes_pause);
                    holder.next.setImageResource(R.drawable.activescenes_forward);
                    holder.previous.setImageResource(R.drawable.activescenes_previous);
                    //holder.mutebutton.setImageResource(R.drawable.activescene_volume);


                }

            }
        });
        holder.volumebar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d("onProgresChanged" + progress, "" + position);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("onStartTracking", "" + position);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                LibreLogger.d("onStopTracking", "" + position);
                SceneObject scene = sceneMap.get(holder.ipaddress);
                holder.volumePercentage.setText(String.valueOf(seekBar.getProgress()));
                if (scene != null) {
                    if(scene.getCurrentSource() != Constants.AIRPLAY_SOURCE &&
                        scene.getCurrentSource() != Constants.SPOTIFY_SOURCE) {
                    LUCIControl.SendCommandWithIp(MIDCONST.ZONE_VOLUME, "" + seekBar.getProgress(), CommandType.SET, holder.ipaddress);
                    }else{
                        LUCIControl.SendCommandWithIp(MIDCONST.VOLUEM_CONTROL, "" + seekBar.getProgress(), CommandType.SET, holder.ipaddress);
                    }

                    //LUCIControl.SendCommandWithIp(MIDCONST.VOLUEM_CONTROL, "" + seekBar.getProgress(), CommandType.SET, holder.ipaddress);
                     SceneObject sceneObjectFromCentralRepo = mScanHandler.getSceneObjectFromCentralRepo(holder.ipaddress);
                    scene.setvolumeZoneInPercentage(seekBar.getProgress());
                    //scene.setVolumeValueInPercentage(seekBar.getProgress());
                    sceneMap.put(holder.ipaddress, scene);
                    if (sceneObjectFromCentralRepo != null) {
                        //  sceneObjectFromCentralRepo.setVolumeValueInPercentage(seekBar.getProgress());
                        sceneObjectFromCentralRepo.setvolumeZoneInPercentage(seekBar.getProgress());
                        mScanHandler.putSceneObjectToCentralRepo(holder.ipaddress, sceneObjectFromCentralRepo);
                    }
                }
            }
        });

        try {
            String ipaddress = (String) sceneMap.keySet().toArray()[position];
            SceneObject scene = sceneMap.get(ipaddress);

            /* Fix by KK , When Album art is not updating properly */
            if(holder.currentTrackName==null)
                holder.currentTrackName = "";

           /* if (scene.getCurrentSource() != 14) {*/ /* Karuna Commenting For the Single Function to Update the UI*/
            if (scene != null) {

                LibreLogger.d(this, "Scene Ipaddress " + ipaddress + "Scene Ipaddressss" + scene.getIpAddress());
                LibreLogger.d(this, "Scene Ipaddress " + scene.getSceneName());

                if (scene.getSceneName() != null && !scene.getSceneName().equalsIgnoreCase("NULL")) {
                    holder.SceneName.setText(scene.getSceneName());
                }

                if (scene.getTrackName() != null
                        && !scene.getTrackName().equalsIgnoreCase("NULL")
                        && !scene.getTrackName().isEmpty()) {
                    String trackname=scene.getTrackName();
                    /* This change is done to handle the case of deezer where the song name is appended by radio or skip enabled */
                    if (trackname!=null&&trackname.contains(Constants.DEZER_RADIO))
                        trackname=trackname.replace(Constants.DEZER_RADIO,"");

                    if (trackname!=null&&trackname.contains(Constants.DEZER_SONGSKIP))
                        trackname=trackname.replace(Constants.DEZER_SONGSKIP,"");
                    holder.trackName.setText(scene.getTrackName());
                } else if(scene.getAlbum_name()!=null
                        && !scene.getAlbum_name().isEmpty()
                        &&  !scene.getAlbum_name().equalsIgnoreCase("null")) {
                    holder.trackName.setText(scene.getAlbum_name());
                } else {
                    holder.trackName.setText("");
                }
                    /*this is to show loading dialog while we are preparing to play*/
                showPreparingDialog(scene, holder);


                /*album art handling*/
                String album_url = "";
                if (scene.getCurrentSource() != Constants.AUX_SOURCE
                        && scene.getCurrentSource() != Constants.BT_SOURCE
                        && scene.getCurrentSource() != Constants.GCAST_SOURCE) {
                    if (scene.getAlbum_art() != null
                            && !scene.getAlbum_art().isEmpty()
                            && scene.getAlbum_art().equalsIgnoreCase("coverart.jpg")) {
                        album_url = "http://" + scene.getIpAddress() + "/" + "coverart.jpg";

                        /* If Track Name is Different just Invalidate the Path
                        * And if we are resuming the Screen(Screen OFF and Screen ON) , it will not re-download it */
                        boolean mInvalidated = mInvalidateTheAlbumArt(scene, album_url);
                        LibreLogger.d(this, "Invalidated the URL " + album_url + " Status " + mInvalidated);
                        PicassoTrustCertificates.getInstance(context).load(album_url)
                                /*.memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)*/
                                .placeholder(R.mipmap.album_art)
                                .error(R.mipmap.album_art)
                                .into(holder.albumArt);
                    } else if (scene.getAlbum_art() == null || (scene.getAlbum_art().trim()).isEmpty()) {
                        holder.albumArt.setImageResource(R.mipmap.album_art);
                    } else {
                        album_url = scene.getAlbum_art();
                        boolean mInvalidated = mInvalidateTheAlbumArt(scene, album_url);
                        LibreLogger.d(this, "Invalidated the URL " + album_url + " Status " + mInvalidated);
                        if (!album_url.trim().isEmpty())
                            PicassoTrustCertificates.getInstance(context).load(album_url)
                                 /*   .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)*/
                                    .placeholder(R.mipmap.album_art)
                                    .error(R.mipmap.album_art)
                                    .into(holder.albumArt);
                    }
                } else {
                    PicassoTrustCertificates.getInstance(context).load(R.mipmap.album_art);
                }

//                     /* This change is done to ensure that the view is previously had aux on and now its swithced and hence image need to be handled*/
//                else if (holder.previousSourceIndex == 14 && scene.getCurrentSource() != 14 && scene.getCurrentSource() != 19) {
//                    String album_arturl = mScanHandler.getSceneObjectFromCentralRepo(ipaddress).getAlbum_art();
//                    if (album_arturl != null && album_arturl.contains("http://")) {
//                    } else {
//                        album_arturl = "http://" + ipaddress + "/" + "coverart.jpg";
//                    }
//                    Picasso.with(context).invalidate(album_arturl);
//
//                }

                if (scene.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
                    holder.play.setImageResource(R.drawable.activescenes_pause);
                    holder.next.setImageResource(R.drawable.activescenes_forward);
                    holder.previous.setImageResource(R.drawable.activescenes_previous);
                } else {
                    holder.play.setImageResource(R.drawable.activescenes_play);
                    holder.next.setImageResource(R.drawable.activescenes_forward);
                    holder.previous.setImageResource(R.drawable.activescenes_previous);
                }
                if (scene.getCurrentSource() == 18) {
                    seekBarNextPreviousView(holder, false);
                }

                if(scene.getCurrentSource()!=Constants.AIRPLAY_SOURCE &&
                        scene.getCurrentSource() != Constants.SPOTIFY_SOURCE) {
                    if (LibreApplication.ZONE_VOLUME_MAP.containsKey(ipaddress)) {
                        holder.volumebar.setProgress(LibreApplication.ZONE_VOLUME_MAP.get(ipaddress));
                        holder.volumePercentage.setText(String.valueOf(LibreApplication.ZONE_VOLUME_MAP.get(ipaddress)));
                        Log.d("VOLUME BAR", holder.volumePercentage.getText().toString());
                    } else {

                        LUCIControl control = new LUCIControl(ipaddress);
                        control.SendCommand(MIDCONST.ZONE_VOLUME, null, LSSDPCONST.LUCI_GET);
                        if (scene.getvolumeZoneInPercentage() >= 0) {
                            holder.volumebar.setProgress(scene.getvolumeZoneInPercentage());
                            holder.volumePercentage.setText(String.valueOf(scene.getvolumeZoneInPercentage()));
                            Log.d("VOLUME BAR", holder.volumePercentage.getText().toString());
                        }

                    }
                } else{/* For Airplay & spotify We havve to send and updating only 64*/
                    if (LibreApplication.INDIVIDUAL_VOLUME_MAP.containsKey(ipaddress)) {
                        holder.volumebar.setProgress(LibreApplication.INDIVIDUAL_VOLUME_MAP.get(ipaddress));
                        holder.volumePercentage.setText(String.valueOf(LibreApplication.INDIVIDUAL_VOLUME_MAP.get(ipaddress)));
                        Log.d("VOLUME BAR", holder.volumePercentage.getText().toString());
                    } else {

                        LUCIControl control = new LUCIControl(ipaddress);
                        control.SendCommand(MIDCONST.VOLUEM_CONTROL, null, LSSDPCONST.LUCI_GET);
                        if (scene.getVolumeValueInPercentage() >= 0) {
                            holder.volumebar.setProgress(scene.getVolumeValueInPercentage());
                            holder.volumePercentage.setText(String.valueOf(scene.getVolumeValueInPercentage()));
                            Log.d("VOLUME BAR", holder.volumePercentage.getText().toString());
                        }

                    }
                }
                playPauseNextPrevious(holder,scene);
                /* This line should always be here do not move this line above the ip-else loop where we check the current Source*/
                holder.previousSourceIndex = scene.getCurrentSource();

                setTheSourceIconFromCurrentSceneObject(scene, holder);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (position == getCount()) {
            notifyDataSetChanged();
        }
        doOrDisableViewsForGcast(holder, LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(holder.ipaddress));
        return convertView;

    }

    private void changeVolumeLogic(ScanningHandler mScanHandler, Holder holder, boolean isVolumeZero) {
        try {
            SceneObject scene = sceneMap.get(holder.ipaddress);
            if(scene.getCurrentSource() != Constants.AIRPLAY_SOURCE &&
                    scene.getCurrentSource() != Constants.SPOTIFY_SOURCE) {
                if (isVolumeZero) {
                    LUCIControl.SendCommandWithIp(MIDCONST.ZONE_VOLUME, "" + 0, CommandType.SET, holder.ipaddress);
                } else {
                    LUCIControl.SendCommandWithIp(MIDCONST.ZONE_VOLUME, "" + volumeHashMap.get(holder.ipaddress), CommandType.SET, holder.ipaddress);
                }
            }else{
                if (isVolumeZero) {
                    LUCIControl.SendCommandWithIp(MIDCONST.VOLUEM_CONTROL, "" + 0, CommandType.SET, holder.ipaddress);
                } else {
                    LUCIControl.SendCommandWithIp(MIDCONST.VOLUEM_CONTROL, "" + volumeHashMap.get(holder.ipaddress), CommandType.SET, holder.ipaddress);
                }
            }
            //LUCIControl.SendCommandWithIp(MIDCONST.VOLUEM_CONTROL, "" + seekBar.getProgress(), CommandType.SET, holder.ipaddress);
            SceneObject sceneObjectFromCentralRepo = mScanHandler.getSceneObjectFromCentralRepo(holder.ipaddress);

            if (scene != null) {
                if (isVolumeZero) {
                    scene.setvolumeZoneInPercentage(0);
                } else {
                    scene.setvolumeZoneInPercentage(Integer.valueOf(volumeHashMap.get(holder.ipaddress)));
                }
                //scene.setVolumeValueInPercentage(seekBar.getProgress());
                sceneMap.put(holder.ipaddress, scene);
                if (sceneObjectFromCentralRepo != null) {
                    //  sceneObjectFromCentralRepo.setVolumeValueInPercentage(seekBar.getProgress());
                    if (isVolumeZero) {
                        sceneObjectFromCentralRepo.setvolumeZoneInPercentage(0);
                    } else {
                        sceneObjectFromCentralRepo.setvolumeZoneInPercentage(Integer.valueOf(volumeHashMap.get(holder.ipaddress)));
                    }
                    mScanHandler.putSceneObjectToCentralRepo(holder.ipaddress, sceneObjectFromCentralRepo);
                }
            }
        } catch(NumberFormatException e) {
            e.printStackTrace();
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private android.transition.Transition enterTransition() {
        android.transition.ChangeBounds bounds = new android.transition.ChangeBounds();
        bounds.setDuration(2000);

        return bounds;
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private android.transition.Transition returnTransition() {
        android.transition.ChangeBounds bounds = new android.transition.ChangeBounds();
        bounds.setInterpolator(new DecelerateInterpolator());
        bounds.setDuration(2000);

        return bounds;
    }

    private void doOrDisableViewsForGcast(Holder holder, LSSDPNodes theNodeBasedOnTheIpAddress) {
        if(theNodeBasedOnTheIpAddress != null && theNodeBasedOnTheIpAddress.getCurrentSource()==MIDCONST.GCAST_SOURCE){
            Log.d("disableViews","gcast disabling, device = "+theNodeBasedOnTheIpAddress.getFriendlyname());
            holder.play.setImageResource(R.drawable.activescenes_play);
            holder.next.setImageResource(R.drawable.activescenes_forward);
            holder.previous.setImageResource(R.drawable.activescenes_previous);
            holder.albumArt.setImageResource(R.mipmap.album_art);
        }
    }

    private boolean mInvalidateTheAlbumArt(SceneObject scene,String album_url){
        if( !scene.getmPreviousTrackName().equalsIgnoreCase(scene.getTrackName())) {

            PicassoTrustCertificates.getInstance(context).invalidate(album_url);
            scene.setmPreviousTrackName(scene.getTrackName());
            SceneObject sceneObjectFromCentralRepo = ScanningHandler.getInstance().getSceneObjectFromCentralRepo(scene.getIpAddress());
            if (sceneObjectFromCentralRepo != null) {
                sceneObjectFromCentralRepo.setmPreviousTrackName(scene.getTrackName());
            }

            return true;
        }
        return false;
    }
    /*this dialog is to show small progress*/
    private void showPreparingDialog(SceneObject scene, Holder holder) {
        if (scene != null) {
            if (scene.getPreparingState() == SceneObject.PREPARING_STATE.PREPARING_INITIATED) {
                holder.preparationProgressBar.getIndeterminateDrawable().setColorFilter(0xFFE15D29, android.graphics.PorterDuff.Mode.MULTIPLY);
                holder.preparationProgressBar.setVisibility(View.VISIBLE);
                seekBarNextPreviousView(holder, false);
            } else if (scene.getPreparingState() == SceneObject.PREPARING_STATE.PREPARING_SUCCESS) {
                holder.preparationProgressBar.setVisibility(View.GONE);
                seekBarNextPreviousView(holder, true);
            } else if (scene.getPreparingState() == SceneObject.PREPARING_STATE.PREPARING_FAILED) {
                holder.preparationProgressBar.setVisibility(View.GONE);
                seekBarNextPreviousView(holder, true);
            }
        }
    }

    /* This Function For QQMusic To Disable the Seekbar Seek and Previous and Next*/

    /**
     * @TODO Khajan
     * value false means disabling views and vice-versa
     */
    /**/
    private void seekBarNextPreviousView(Holder mHolder, boolean value) {
        mHolder.previous.setClickable(value);
        mHolder.next.setClickable(value);
    }

    private void setTheSourceIconFromCurrentSceneObject(SceneObject currentSceneObject, Holder holder) {


        String source = "Local";
        enableViews(holder);
        Drawable img = context.getResources().getDrawable(
                R.mipmap.ic_sources);

        if (currentSceneObject.getCurrentSource()==0)
        {
            disableViews(currentSceneObject.getCurrentSource(),"",holder);
        }
        if (currentSceneObject.getCurrentSource() == 2) {
            img = context.getResources().getDrawable(
                    R.mipmap.dmr_icon);
            source = "DLNA";
        }
        if (currentSceneObject.getCurrentSource() == 3) {
            img = context.getResources().getDrawable(
                    R.mipmap.ic_sources);
            source = "DMP";
        }

        if (currentSceneObject.getCurrentSource() == 4) {
            img = context.getResources().getDrawable(
                    R.mipmap.spotify);
            source = "Spotify";
        }
        if (currentSceneObject.getCurrentSource() == USB_SOURCE) {
            img = context.getResources().getDrawable(
                    R.mipmap.usb);
            source = "Usb";
        }
        if (currentSceneObject.getCurrentSource() == 6) {
            img = context.getResources().getDrawable(
                    R.mipmap.sdcard);
            source = "SD Card";
        }
        if (currentSceneObject.getCurrentSource() == VTUNER_SOURCE) {
            img = context.getResources().getDrawable(
                    R.mipmap.vtuner_logo);

            source = "Vtuner";
            disableViews(currentSceneObject.getCurrentSource(),"",holder);
        }
        if (currentSceneObject.getCurrentSource() == TUNEIN_SOURCE) {
            img = context.getResources().getDrawable(
                    R.mipmap.tunein_logo1);

            source = "Tunein";
            disableViews(currentSceneObject.getCurrentSource(),"",holder);
        }
        if (currentSceneObject.getCurrentSource() == 14) { // Mail
            img = context.getResources().getDrawable(
                    R.mipmap.aux_in);
            source = "Aux In";
            disableViews(currentSceneObject.getCurrentSource(),context.getResources().getString(R.string.local_auxin),holder);
        }
        if (currentSceneObject.getCurrentSource() == 18) {
            img = context.getResources().getDrawable(
                    R.mipmap.qqmusic);
            source = "QPlay";
            seekBarNextPreviousView(holder, false);
            disableViews(currentSceneObject.getCurrentSource(), "", holder);
        }
        if (currentSceneObject.getCurrentSource() == 19) {
            img = context.getResources().getDrawable(
                    R.mipmap.bluetooth);
            source = "Bluetooth";
            if(currentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED) {
                disableViews(currentSceneObject.getCurrentSource(),context.getResources().getString(R.string.btOn),holder);
            }else if(currentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PAUSED)
            {
                disableViews(currentSceneObject.getCurrentSource(),context.getResources().getString(R.string.btOn),holder);
            }
            else {
                disableViews(currentSceneObject.getCurrentSource(),context.getResources().getString(R.string.btOn),holder);

            }
        }

        if (currentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE) {
            img = context.getResources().getDrawable(
                    R.mipmap.ic_cast_white_24dp_2x);
            source = "Casting";
                disableViews(currentSceneObject.getCurrentSource(),context.getResources().getString(R.string.casting),holder);


        }
        if (currentSceneObject.getCurrentSource() == DEEZER_SOURCE) {
            img = context.getResources().getDrawable(
                    R.mipmap.deezer_logo);
            source = "Deezer";
            disableViews(currentSceneObject.getCurrentSource(),"",holder);
        }
        if (currentSceneObject.getCurrentSource() == TIDAL_SOURCE) {
            img = context.getResources().getDrawable(
                    R.mipmap.tidal_logo1);
            source = "Tidal";
            disableViews(currentSceneObject.getCurrentSource(),currentSceneObject.getTrackName(),holder);

        }



        img.setBounds(0, 0, 15, 15);
        holder.sourceName.setText(source);
        holder.sourceImage.setImageDrawable(img);

        handleThePlayIconsForGrayoutOption(holder,currentSceneObject);

    }


    private void handleThePlayIconsForGrayoutOption(Holder holder,SceneObject sceneObject) {
        if (holder!=null) {
            if (holder.previous != null && holder.previous.isEnabled()) {
                holder.previous.setImageResource(R.drawable.activescenes_previous);
            } else if (holder.previous != null) {
                holder.previous.setImageResource(R.drawable.activescenes_previous);
            }


            if (holder.next != null && holder.next.isEnabled()) {
                holder.next.setImageResource(R.drawable.activescenes_forward);
            } else if (holder.next != null) {
                holder.next.setImageResource(R.drawable.activescenes_forward);
            }

            if (holder.play!=null&&holder.play.isEnabled()==false){

                holder.play.setImageResource(R.drawable.activescenes_play);

            }else if (sceneObject!=null &&sceneObject.getPlaystatus()==SceneObject.CURRENTLY_PLAYING){
                holder.play.setImageResource(R.drawable.activescenes_pause);
            }else {
                holder.play.setImageResource(R.drawable.activescenes_play);
            }

            playPauseNextPrevious(holder,sceneObject);

        }



    }
    public void playPauseNextPrevious(Holder holder,SceneObject sceneObject){
        LSSDPNodes mNodeWeGotForControl = ScanningHandler.getInstance().getLSSDPNodeFromCentralDB(holder.ipaddress);
        if (sceneObject!=null  && (/*sceneObject.getCurrentSource() == Constants.VTUNER_SOURCE
                || sceneObject.getCurrentSource() == Constants.TUNEIN_SOURCE
                || */((sceneObject.getCurrentSource() == Constants.BT_SOURCE
                && ((mNodeWeGotForControl.getgCastVerision() == null
                && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                ||(mNodeWeGotForControl.getgCastVerision() != null
                && (mNodeWeGotForControl.getBT_CONTROLLER() < 2)))))
                || sceneObject.getCurrentSource() == Constants.AUX_SOURCE
                || sceneObject.getCurrentSource() == 0)) {
            holder.play.setImageResource(R.drawable.activescenes_play);
            holder.next.setImageResource(R.drawable.activescenes_forward);
            holder.previous.setImageResource(R.drawable.activescenes_previous);
        }
    }
    public SceneObject getSceneObjectFromAdapter(String ip) {
        return sceneMap.get(ip);
    }


    private boolean handleifSpeakerIsInNoSourceMode(SceneObject sObject) {
        //Handling the No Source
        if(sObject!=null && sObject.getCurrentSource()==0||sObject.getCurrentSource()==17){

            Toast.makeText(context, context.getString(R.string.no_active_playlist_select), Toast.LENGTH_SHORT).show();

/* In SA mode we need to go to local content while in HN mode we will go to sources option */
            if (LibreApplication.activeSSID.contains(Constants.DDMS_SSID)){
                Intent localIntent = new Intent(context, LocalDMSActivity.class);
                localIntent.putExtra("isLocalDeviceSelected", true);
                localIntent.putExtra("current_ipaddress", sObject.getIpAddress());
                context.startActivity(localIntent);
            }else {
                Intent localIntent = new Intent(context, CumulationsSourcesOptionActivity.class);
                localIntent.putExtra("current_ipaddress", sObject.getIpAddress());
                localIntent.putExtra("current_source", "" + sObject.getCurrentSource());
                localIntent.putExtra(Constants.FROM_ACTIVITY, "NowPlayingFragment");
                context.startActivity(localIntent);
            }
            return true;
        }
        return false;
    }

    public boolean doNextPrevious(boolean isNextPressed, SceneObject object) {

        if (handleifSpeakerIsInNoSourceMode(object)) {
            return false;
        }
        /* DMR source callback*/
        if (object.getCurrentSource() == 2|| object.getCurrentSource()==0 || object.getCurrentSource()==12) {
            RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(object.getIpAddress());
            if (renderingDevice != null) {
                String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);


                if ( playbackHelper== null || playbackHelper.getDmsHelper() == null|| ( object!=null && false==object.getPlayUrl().contains(LibreApplication.LOCAL_IP))) {
                    Toast.makeText(context, context.getString(R.string.no_active_playlist_select), Toast.LENGTH_SHORT).show();

                                       /* In SA mode we need to go to local content while in HN mode we will go to sources option */
                    if (LibreApplication.activeSSID.contains(Constants.DDMS_SSID)){
                        Intent localIntent = new Intent(context, LocalDMSActivity.class);
                        localIntent.putExtra("isLocalDeviceSelected", true);
                        localIntent.putExtra("current_ipaddress", object.getIpAddress());
                        context.startActivity(localIntent);
                    }else {
                        Intent localIntent = new Intent(context, CumulationsSourcesOptionActivity.class);
                        localIntent.putExtra("current_ipaddress", object.getIpAddress());
                        localIntent.putExtra("current_source", "" + object.getCurrentSource());
                        context.startActivity(localIntent);
                    }


                    return false;
                }


                if ((object.getShuffleState() == 0) && (object.getRepeatState() == REPEAT_OFF)) {

                    if (isNextPressed) {
                        if (playbackHelper.isThisTheLastSong() || playbackHelper.isThisOnlySong()) {
                            Toast.makeText(context, context.getString(R.string.lastSongPlayed), Toast.LENGTH_LONG).show();
                            Message msg = new Message();
                            msg.what = Constants.PREPARATION_COMPLETED;
                            Bundle data = new Bundle();
                            data.putString("ipAddress", object.getIpAddress());
                            msg.setData(data);
                            activehandler.sendMessage(msg);
                            return false;
                        }
                    } else {
                        if (playbackHelper.isThisFirstSong() || playbackHelper.isThisOnlySong()) {
                            Toast.makeText(context, context.getString(R.string.onlyOneSong), Toast.LENGTH_LONG).show();
                            Message msg = new Message();
                            msg.what = Constants.PREPARATION_COMPLETED;
                            Bundle data = new Bundle();
                            data.putString("ipAddress", object.getIpAddress());
                            msg.setData(data);
                            activehandler.sendMessage(msg);
                            return false;
                        }
                    }
                }

                if (isNextPressed)
                    playbackHelper.playNextSong(1);
                else {
                 /* Setting the current seekbar progress -Start*/
                    float  duration = object.getCurrentPlaybackSeekPosition();
                    Log.d("Current Duration ", "Duration = " + duration / 1000);
                    float durationInSeeconds = duration/1000;
                    if(durationInSeeconds <5) {
                        playbackHelper.playNextSong(-1);
                    }else{
                        playbackHelper.playNextSong(0);
                    }
                }

                if(object.getCurrentSource()!=Constants.BT_SOURCE) {
                    Message msg = new Message();
                    msg.what = PREPARATION_INIT;
                    Bundle data = new Bundle();
                    data.putString("ipAddress", object.getIpAddress());
                    msg.setData(data);
                    activehandler.sendMessage(msg);
                }
                return true;


            } else
                return false;
        } else {

            LUCIControl control = new LUCIControl(object.getIpAddress());


            /* Chnage done to provide a message for the deezer next button click */

            if (object.getCurrentSource()==21){

                String trackname=object.getTrackName();

                if (isNextPressed){

                    if (trackname!=null&&trackname.contains(Constants.DEZER_SONGSKIP))
                    {
                        
                        Toast.makeText(context,"Activate Deezer Premium+ from your computer",Toast.LENGTH_LONG).show();
                        return false;
                    }

                }
            }


            if (isNextPressed)
                control.SendCommand(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.PLAY_NEXT, CommandType.SET);
            else
                control.SendCommand(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.PLAY_PREV, CommandType.SET);
            if(object.getCurrentSource()!=Constants.BT_SOURCE) {
                Message msg = new Message();
                msg.what = PREPARATION_INIT;
                Bundle data = new Bundle();
                data.putString("ipAddress", object.getIpAddress());
                msg.setData(data);
                activehandler.sendMessage(msg);
            }
            return true;
        }


    }
    public boolean doNextPrevious(boolean isNextPressed, SceneObject object,boolean mValue) {

        if (handleifSpeakerIsInNoSourceMode(object)) {
            return false;
        }
        /* DMR source callback*/
        if (object.getCurrentSource() == 2|| object.getCurrentSource()==0 || object.getCurrentSource()==12) {
            RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(object.getIpAddress());
            if (renderingDevice != null) {
                String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);


                if ( playbackHelper== null || playbackHelper.getDmsHelper() == null|| ( object!=null && false==object.getPlayUrl().contains(LibreApplication.LOCAL_IP))) {
                    if(mValue)
                        return true;
                    Toast.makeText(context, context.getString(R.string.no_active_playlist_select), Toast.LENGTH_SHORT).show();

                                       /* In SA mode we need to go to local content while in HN mode we will go to sources option */
                    if (LibreApplication.activeSSID.contains(Constants.DDMS_SSID)){
                        Intent localIntent = new Intent(context, LocalDMSActivity.class);
                        localIntent.putExtra("isLocalDeviceSelected", true);
                        localIntent.putExtra("current_ipaddress", object.getIpAddress());
                        context.startActivity(localIntent);
                    }else {
                        Intent localIntent = new Intent(context, CumulationsSourcesOptionActivity.class);
                        localIntent.putExtra("current_ipaddress", object.getIpAddress());
                        localIntent.putExtra("current_source", "" + object.getCurrentSource());
                        context.startActivity(localIntent);
                    }


                    return false;
                }


                if (isNextPressed)
                    playbackHelper.playNextSong(1);
                else {

                 /* Setting the current seekbar progress -Start*/
                    float  duration = object.getCurrentPlaybackSeekPosition();
                    Log.d("Current Duration ", "Duration = " + duration / 1000);
                    float durationInSeeconds = duration/1000;
                    if(durationInSeeconds <5) {
                        playbackHelper.playNextSong(-1);
                    }else{
                        playbackHelper.playNextSong(0);
                    }
                }

                if(object.getCurrentSource()!=Constants.BT_SOURCE) {
                    Message msg = new Message();
                    msg.what = PREPARATION_INIT;
                    Bundle data = new Bundle();
                    data.putString("ipAddress", object.getIpAddress());
                    msg.setData(data);
                    activehandler.sendMessage(msg);
                }
                return true;


            } else
                return false;
        } return  true;

    }
    public void enableViews(Holder mHolder) {

        mHolder.play.setClickable(true);
        mHolder.play.setEnabled(true);
        // //volumeBar.setClickable(false);
        // volumeBar.setEnabled(false);
        mHolder.mutebutton.setClickable(true);
        mHolder.mutebutton.setEnabled(true);
        mHolder.previous.setClickable(true);
        mHolder.previous.setEnabled(true);
        mHolder.next.setEnabled(true);
        mHolder.next.setClickable(true);
        mHolder.volumebar.setClickable(true);
        mHolder.volumebar.setEnabled(true);



        mHolder.play.setAlpha((float)1);
        mHolder.next.setAlpha((float)1);
        mHolder.previous.setAlpha((float) 1);

 }
    public void disableViews(int currentSrc,String message,Holder mHolder) {

        /* Dont have to call this function explicitly make use of setrceIco */

        switch (currentSrc) {


            case 0:
                //mHolder.play.setClickable(false);
                //mHolder.play.setEnabled(false);
                viewsWithLessOpacity(mHolder);
                break;

            case 12:
                //mHolder.play.setClickable(false);
                mHolder.play.setEnabled(false);
                break;

            case 8:
                //vtuner
                 {
                     mHolder.previous.setClickable(false);
                     mHolder.previous.setEnabled(false);
                     mHolder.next.setEnabled(false);
                     mHolder.next.setClickable(false);
                     mHolder.previous.setAlpha(.5f);
                     mHolder.next.setAlpha(.5f);
                     break;
                 }

            case 9:
                //tunein
            {
                /*setting seek to zero*/
                /* disable previous and next */
                mHolder.previous.setClickable(false);
                mHolder.previous.setEnabled(false);
                mHolder.next.setEnabled(false);
                mHolder.next.setClickable(false);
                mHolder.previous.setAlpha(.5f);
                mHolder.next.setAlpha(.5f);
//                mHolder.play.setClickable(false);
//                viewsWithLessOpacity(mHolder);
                break;

            }
            case 14:
                //Aux

            {

                /* disable previous and next */
                //mHolder.previous.setClickable(false);
                //mHolder.previous.setEnabled(false);
                //mHolder.next.setEnabled(false);
                //mHolder.next.setClickable(false);

                //mHolder.play.setClickable(false);
                //mHolder.play.setEnabled(false);
                mHolder.next.setImageResource(R.drawable.activescenes_forward);
                mHolder.previous.setImageResource(R.drawable.activescenes_previous);
                mHolder.play.setImageResource(R.drawable.activescenes_play);
                mHolder.trackName.setText(message);
                String album_url = "http://" + mHolder.ipaddress + "/" + "coverart1.jpg";
                PicassoTrustCertificates.getInstance(context).load(R.mipmap.album_art).memoryPolicy(MemoryPolicy.NO_STORE)
                        .placeholder(R.mipmap.album_art)
                        .into(mHolder.albumArt);
                break;

            }

            case 18: {   //QQ

                /* disable previous and next */
                mHolder.previous.setClickable(false);
                mHolder.previous.setEnabled(false);
                mHolder.next.setEnabled(false);
                mHolder.next.setClickable(false);


                break;
            }
            case 19:
                //Bluetooth
            {
                mHolder.trackName.setText(message);
                String album_url = "http://" + mHolder.ipaddress + "/" + "coverart1.jpg";

                PicassoTrustCertificates.getInstance(context).load(R.mipmap.album_art).memoryPolicy(MemoryPolicy.NO_STORE).placeholder(R.mipmap.album_art)
                        .into(mHolder.albumArt);

                final LSSDPNodes mNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(mHolder.ipaddress);
                if(mNode==null){
                    return;
                }
                LibreLogger.d(this,"BT controller value in sceneobject "+mNode.getBT_CONTROLLER());
            //    Toast.makeText(getContext(), "BT controller value in sceneobject " + currentSceneObject.getBT_CONTROLLER(), Toast.LENGTH_SHORT);
                if(mNode.getBT_CONTROLLER()!=1 || mNode.getBT_CONTROLLER()!=2 ||mNode.getBT_CONTROLLER()!=3 ) {
                    mHolder.next.setImageResource(R.drawable.activescenes_forward);
                    mHolder.previous.setImageResource(R.drawable.activescenes_previous);
                    mHolder.play.setImageResource(R.drawable.activescenes_play);
                }
                break;

            }
            case Constants.GCAST_SOURCE:{
                //gCast is Playing

                mHolder.play.setImageResource(R.drawable.activescenes_play);
                mHolder.next.setImageResource(R.drawable.activescenes_forward);
                mHolder.previous.setImageResource(R.drawable.activescenes_previous);
                mHolder.trackName.setText(message);
                mHolder.volumebar.setEnabled(false);
                mHolder.volumebar.setProgress(0);
                mHolder.volumebar.setClickable(false);

                break;
            }

            case DEEZER_SOURCE: {
                String trackname = message;
                if (message != null && message.contains(Constants.DEZER_RADIO))
                {   mHolder. previous.setEnabled(false);
                    mHolder.previous.setClickable(false);
                    mHolder.next.setEnabled(false);
                    mHolder.next.setClickable(false);
                    mHolder.previous.setAlpha(.5f);
                    mHolder.next.setAlpha(.5f);
                }
              /*  if (message!=null&& message.contains(Constants.DEZER_SONGSKIP)) {
                    mHolder.next.setEnabled(false);
                    mHolder.next.setClickable(false);
                }*/

                break;
            }

            case Constants.TIDAL_SOURCE:{
                mHolder. previous.setEnabled(false);
                mHolder.previous.setClickable(false);
                mHolder.next.setEnabled(false);
                mHolder.next.setClickable(false);
                mHolder.previous.setAlpha(.5f);
                mHolder.next.setAlpha(.5f);
                break;
            }
        }
    }

    public void viewsWithLessOpacity(Holder mHolder) {
        mHolder.play.setAlpha((float) 0.5);
        mHolder.next.setAlpha((float) 0.5);
        mHolder.previous.setAlpha((float) 0.5);
    }

}
