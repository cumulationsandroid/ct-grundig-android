package com.cumulations.grundig.nowplaying;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.transition.Transition;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.ActiveSceneAdapter;
import com.cumulations.grundig.ActiveScenesListActivity;
import com.cumulations.grundig.DeviceDiscoveryActivity;
import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.NewManageDevices.FreeSpeakersActivity;
import com.cumulations.grundig.NewManageDevices.NewManageDevices;
import com.cumulations.grundig.PlayNewActivity;
import com.cumulations.grundig.R;
import com.cumulations.grundig.RemoteSourcesList;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.SourceOptions.CumulationsSourcesOptionActivity;
import com.cumulations.grundig.TuneInRemoteSourcesList;
import com.cumulations.grundig.app.dlna.dmc.LocalDMSActivity;
import com.cumulations.grundig.app.dlna.dmc.processor.interfaces.DMRProcessor;
import com.cumulations.grundig.app.dlna.dmc.utility.DMRControlHelper;
import com.cumulations.grundig.app.dlna.dmc.utility.PlaybackHelper;
import com.cumulations.grundig.app.dlna.dmc.utility.UpnpDeviceManager;
import com.cumulations.grundig.constants.CommandType;
import com.cumulations.grundig.constants.DeviceMasterSlaveFreeConstants;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.BusProvider;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.PicassoTrustCertificates;
import com.cumulations.grundig.util.ShowLoader;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;

import org.fourthline.cling.model.ModelUtil;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.meta.Action;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.support.model.DIDLObject;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by cumulations on 29/6/17.
 */

public class CumulationsNowPlayingActivity extends DeviceDiscoveryActivity implements LibreDeviceInteractionListner,
        DMRProcessor.DMRProcessorListener, VolumeListener.OnChangeVolumeHardKeyListener, ZoneRenameDialogFragment.ZoneDialogListener {

    private Stack<DIDLObject> m_browseObjectStack = new Stack<DIDLObject>();

    private RelativeLayout mRelativeLayout;
    private TextView homeButton;
    private CircleImageView mRounderAlbumArt;
    private TextView mSongName;
    private TextView mArtistName;
    private ImageButton mVolume;
    private boolean mmSeekBarVisibility = false;
    private boolean mShuffleClicked = false;
    private boolean mRepeatClicked = false;
    private boolean volumeClicked = false;
    private AppCompatSeekBar mSeekBar;
    private AppCompatSeekBar mVolumeSeekBar;
    private TextView mVolumeText;
    private ImageButton mShuffle;
    private ImageButton mRepeat;
    private TextView mSongStartTime;
    private TextView mSongStopTime;
    private AppCompatSeekBar mAppCompatSeekBar;
    private ImageButton mPlaySong;
    private ImageButton mPreviousSong;
    private ImageButton mNextSong;
    private AppCompatImageButton mPlay;
    private AppCompatSpinner mZoneSpinner;
    private TextView mZoneName;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    private RelativeLayout volumeLayout;
    private ArrayList<String> zones = new ArrayList<>();//{"zone name 1", "zone name 2", "zone name 3"};
    private int vibrant;

    private AudioManager mAudioManager;

    private ArrayList<String> sceneAddressList = new ArrayList<>();
    private String mCurrentIpAddress;
    private SceneObject mCurrentSceneObject;
    private boolean isLocalDMRPlayback;

    public static final int REPEAT_OFF = 0;
    public static final int REPEAT_ONE = 1;
    public static final int REPEAT_ALL = 2;
    public static final int DMR_SOURCE = 2;
    private String currentTrackName = "-1";

    public static final int USB_SOURCE = 5;
    public static final int SD_CARD = 6;
    public static final int DEEZER_SOURCE = 21;
    public static final int FAV_SOURCE = 23;
    public static final int TIDAL_SOURCE = 22;
    public static final int VTUNER_SOURCE = 8;
    public static final int TUNEIN_SOURCE = 9;
    public static final int NETWORK_DEVICES = 3;
    public static final int ROON_SOURCE = 27;

    private boolean shouldShowViewsWithoutAnimation = true;
    public static boolean isPhoneCallbeingReceived;
    private int mCurrentNowPlayingScene;
    private Integer position;
    private ImageView mLoading;
    private TextView mLoadingText;

    ScanningHandler mScanHandler = ScanningHandler.getInstance();

    Handler showLoaderHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case Constants.PREPARATION_INITIATED:
                    //mProgressBar.setVisibility(View.VISIBLE);
                    /*disabling views while initiating */
                    preparingToPlay(false);
                    break;

                case Constants.PREPARATION_COMPLETED:
                    //mProgressBar.setVisibility(View.GONE);
                    /*enabling views while initiating */
                    preparingToPlay(true);
                    break;

                case Constants.PREPARATION_TIMEOUT_CONST:

                    /*enabling views while initiating */
                    preparingToPlay(true);

                      /*posting error to application here we are using bus because it doest extend DeviceDiscoverActivity*/
                  /*
                    Commenting the error dialog as it was getting triggered for the external app setting the AV transport url

                    LibreError error = new LibreError(currentIpAddress, getResources.getString(R.string.PREPARATION_TIMEOUT_HAPPENED));
                    BusProvider.getInstance().post(error);
                    */
                    //mProgressBar.setVisibility(View.GONE);
                    break;

            }


        }


    };
    private ProgressDialog mProgressDialog;
    private View menuItemViewAddSpeaker;
    private View menuItemView;
    private PhoneStateChangeListener pscl;
    private TelephonyManager tm;


    /*this method will take care of views while we are preparing to play*/
    private void preparingToPlay(boolean value) {

        if (!value) {
            //setTheSourceIconFromCurrentSceneObject();
        } else {
            mAppCompatSeekBar.setEnabled(value);
            mAppCompatSeekBar.setClickable(value);
            mPlaySong.setEnabled(value);
            mPlaySong.setClickable(value);
            mPreviousSong.setEnabled(value);
            mPreviousSong.setClickable(value);
            mNextSong.setEnabled(value);
            mNextSong.setClickable(value);
            mSeekBar.setEnabled(value);
            mSeekBar.setClickable(value);
        }
    }

    private void setupFragment(String currentIpAddress, boolean shouldShowViewsWithoutAnimation) {
        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        CumulationsNowPlayingFragment cumulationsNowPlayingFragment = new CumulationsNowPlayingFragment();
        ft.replace(R.id.container, cumulationsNowPlayingFragment, mCurrentIpAddress);
        // or ft.add(R.id.your_placeholder, new FooFragment());
        Bundle bundle = new Bundle();
        bundle.putBoolean("shouldShowViewsWithoutAnimation", shouldShowViewsWithoutAnimation);
        SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo(currentIpAddress);
        if (sceneObject != null) {
            bundle.putString("scene_IP", sceneObject.getIpAddress());
        }

        cumulationsNowPlayingFragment.setArguments(bundle);

        cumulationsNowPlayingFragment.setClass(new CumulationsNowPlayingFragment.getCurrentSceneObject() {
            @Override
            public void setCurrentSceneObject(SceneObject sObject) {
                CumulationsNowPlayingActivity.this.mCurrentSceneObject = sObject;
                if (CumulationsNowPlayingActivity.this.mCurrentSceneObject != null) {
                    mZoneName.setText(mCurrentSceneObject.getSceneName());
                }
            }
        });

        ft.commit();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cumulations_now_playing);
        Log.d("DENSITY", getResources().getDisplayMetrics().density + "");
        Log.d("ONCREATE", "ONCREATE CALLED");
        mLoading = (ImageView)findViewById(R.id.loading);
        mLoadingText = (TextView)findViewById(R.id.loadingText);
        mLoadingText.setText(getString(R.string.pleaseWait));
//        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        //mZoneSpinner = (AppCompatSpinner) findViewById(R.id.zone_spinner);
        mZoneName = (TextView)findViewById(R.id.zone_spinner);
        mZoneName.setSelected(true);
        homeButton = (TextView)findViewById(R.id.home);
        homeButton.setSelected(true);
        setClickListeners();
        if (getIntent().getExtras() != null) {
            shouldShowViewsWithoutAnimation  = getIntent().getBooleanExtra("shouldShowViewWithoutAnimation", true);
            try {
                position = getIntent().getIntExtra("scene_position", 0);
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }
        }
        populateSceneAddressList();
        mCurrentIpAddress = getIpAddress();
        setupFragment(mCurrentIpAddress, shouldShowViewsWithoutAnimation);

        //setupSpinner();
        setupToolbar();
        //initViews();
        //animateViews();
        /*if (shouldShowViewsWithoutAnimation) {
            //populateSceneAddressList();
            //mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
            mCurrentIpAddress = getIpAddress();
            //initViews();
            //setCurrentSceneObject();
            setupSpinner();
            setupToolbar();
        }*/

        /*if (getIntent().getExtras().getBoolean("fromspinner")) {
            populateSceneAddressList();
            //mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
            mCurrentIpAddress = getIpAddress();
            //initViews();
            setCurrentSceneObject();
            setupSpinner();
            setupToolbar();
        }*/

        //makeStatusBarTransparent();
        /*populateSceneAddressList();
        //mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
        mCurrentIpAddress = getIpAddress();
        //initViews();
        setCurrentSceneObject();
        setupSpinner();
        setupToolbar();*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        pscl = new PhoneStateChangeListener();
        tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(pscl, PhoneStateListener.LISTEN_CALL_STATE);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private void animateViews() {
        Transition sharedElementEnterTransition = getWindow().getSharedElementEnterTransition();
        sharedElementEnterTransition.addListener(new Transition.TransitionListener() {

            @Override
            public void onTransitionStart(Transition transition) {
                populateSceneAddressList();
                //mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
                mCurrentIpAddress = getIpAddress();
                //initViews();
                //setCurrentSceneObject();
                setupSpinner();
                setupToolbar();
                //setViews();
            }

            @Override
            public void onTransitionEnd(Transition transition) {
                /*showViews();
                Animation fadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_anim);
                anim.setRepeatCount(Animation.INFINITE);
                anim.setRepeatMode(Animation.RESTART);
                mAppCompatSeekBar.startAnimation(fadeInAnimation);
                mArtistName.startAnimation(fadeInAnimation);
                mSongName.startAnimation(fadeInAnimation);
                mSongStopTime.startAnimation(fadeInAnimation);
                mSongStartTime.startAnimation(fadeInAnimation);
                mPlaySong.startAnimation(anim);
                mPreviousSong.startAnimation(fadeInAnimation);
                mNextSong.startAnimation(fadeInAnimation);
                mShuffle.startAnimation(fadeInAnimation);
                mRepeat.startAnimation(fadeInAnimation);
                mVuMeterView.startAnimation(fadeInAnimation);
                mVolume.startAnimation(fadeInAnimation);
                mZoneSpinner.startAnimation(fadeInAnimation);
                mAppCompatSeekBar.startAnimation(fadeInAnimation);*/
            }

            @Override
            public void onTransitionCancel(Transition transition) {
                Log.d("TRANSITION", "Transition cancelled");
            }

            @Override
            public void onTransitionPause(Transition transition) {
                Log.d("TRANSITION", "Transition paused");
            }

            @Override
            public void onTransitionResume(Transition transition) {

            }
        });
    }


    private void makeStatusBarTransparent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }


    /*this method is to get UI and asynchronous call*/
    private void sendLuci() {

        LibreLogger.d(this, "NowPlaying: Sending the async command and 41 and 64 for ip address " + mCurrentIpAddress);
        LUCIControl luciControl = new LUCIControl(mCurrentIpAddress);
                /*Sending asynchronous registration*/
        luciControl.sendAsynchronousCommand();

        luciControl.SendCommand(41, "GETUI:PLAY", 2);
        luciControl.SendCommand(MIDCONST.ZONE_VOLUME, null, 1);
        luciControl.SendCommand(MIDCONST.VOLUEM_CONTROL, null, 1);
        /* Getting SCene Name To Update in UI */
        luciControl.SendCommand(MIDCONST.MID_SCENE_NAME, null, 1);
        // luciControl.SendCommand(64, null, 1);

        /* This is where we get the currentSource */
        luciControl.SendCommand(50, null, 1);

        luciControl.SendCommand(49, null, 1);

        //luciControl.SendCommand(222, "0:6", 1);
    }



    @Override
    protected void onResume() {
        super.onResume();
        registerForDeviceEvents(this);
        populateSceneAddressList();
        /*Praveena*
        /*this should not be commented -else source switching from DMR tro other wont work*/
        isLocalDMRPlayback = false;

        if (mCurrentSceneObject != null && mZoneName != null) {
            mZoneName.setText(mCurrentSceneObject.getSceneName());
        }
        checkIfTheZoneIsRemovedAndExit(mCurrentIpAddress);
    }


    private void checkIfTheZoneIsRemovedAndExit(String mCurrentIpAddress) {
        if(mCurrentIpAddress!=null &&ScanningHandler.getInstance().isIpAvailableInCentralSceneRepo(mCurrentIpAddress)==false){
            Toast.makeText(this,R.string.zone_got_removed,Toast.LENGTH_LONG).show();
            onBackPressed();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();
        tm.listen(pscl, PhoneStateListener.LISTEN_NONE);
        pscl = null;
        tm = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterForDeviceEvents();
    }

    /*private void showViews() {
        //mRelativeLayout.setVisibility(View.VISIBLE);
        mArtistName.setVisibility(View.VISIBLE);
        mSongName.setVisibility(View.VISIBLE);
        mSongStopTime.setVisibility(View.VISIBLE);
        mSongStartTime.setVisibility(View.VISIBLE);
        mPlaySong.setVisibility(View.VISIBLE);
        mPreviousSong.setVisibility(View.VISIBLE);
        mNextSong.setVisibility(View.VISIBLE);
        mShuffle.setVisibility(View.VISIBLE);
        mRepeat.setVisibility(View.VISIBLE);
        mVuMeterView.setVisibility(View.VISIBLE);
        mVolume.setVisibility(View.VISIBLE);
        //mZoneSpinner.setVisibility(View.VISIBLE);
        mZoneName.setVisibility(View.VISIBLE);
        mAppCompatSeekBar.setVisibility(View.VISIBLE);
    }

    private void initViews() {
        mRelativeLayout = (RelativeLayout)findViewById(R.id.now_paying_layout);
        //mPlay = (AppCompatImageButton)findViewById(R.id.play_song);
        homeButton = (TextView)findViewById(R.id.home);
        mRounderAlbumArt = (CircleImageView)findViewById(R.id.rounded_album_art);
        mProgressBar = (ProgressBar)findViewById(R.id.loading_progressbar);
        mArtistName = (TextView)findViewById(R.id.artist_name);
        mSongName = (TextView)findViewById(R.id.song_name);
        mSongName.setSelected(true);
        mSongStartTime = (TextView)findViewById(R.id.song_start_time);
        mSongStopTime = (TextView)findViewById(R.id.song_stop_time);
        mVolumeText = (TextView)findViewById(R.id.volumeText);
        mPlaySong = (ImageButton) findViewById(R.id.play_song);
        mPreviousSong = (ImageButton)findViewById(R.id.previous_song);
        mNextSong = (ImageButton)findViewById(R.id.next_song);
        mShuffle = (ImageButton) findViewById(R.id.shufflesong);
        mRepeat = (ImageButton) findViewById(R.id.repeatsong);
        mVuMeterView = (VuMeterView)findViewById(R.id.vumeter);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        mVolume = (ImageButton) findViewById(R.id.volume);
        mSeekBar = (AppCompatSeekBar) findViewById(R.id.volume_bar);
        //mZoneSpinner = (AppCompatSpinner) findViewById(R.id.zone_spinner);
        mZoneName = (TextView)findViewById(R.id.zone_spinner);
        mAppCompatSeekBar = (AppCompatSeekBar)findViewById(R.id.seekBar);
        //volumeLayout = (RelativeLayout)findViewById(R.id.relative_volume);
        setClickListeners();
    }*/

    private void setClickListeners() {
        homeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void setupSpinner() {
        CustomSpinnerAdapter zoneAdapter = new CustomSpinnerAdapter(this,R.layout.cumulations_spinner,zones);
        zoneAdapter.setDropDownViewResource(R.layout.cumulations_spinner_dropdown);
        mZoneSpinner.setAdapter(zoneAdapter);
        mZoneSpinner.setSelected(false);
        mZoneSpinner.setSelection(position,true);
        mZoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*Intent i = new Intent(CumulationsNowPlayingActivity.this, CumulationsNowPlayingActivity.class);
                i.putExtra("current_ipaddress", sceneAddressList.get(position));
                //i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.putExtra("shouldShowViewWithoutAnimation", false);
                i.putExtra("fromspinner", true);
                startActivity(i);*/
                //finish();
                setupFragment(sceneAddressList.get(position), true);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onDialogPositiveClick(String zoneName) {
        if (mCurrentSceneObject != null && mZoneName != null) {
            mZoneName.setText(zoneName);
        }
        hideKeyBoard();
    }

    private void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public class CustomSpinnerAdapter extends ArrayAdapter<String> {
        LayoutInflater inflater;
        List<String> spinnerItems;

        public CustomSpinnerAdapter(Context applicationContext, int resource, List<String> spinnerItems) {
            super(applicationContext, resource, spinnerItems);
            this.spinnerItems = spinnerItems;
            inflater = (LayoutInflater.from(applicationContext));
        }
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = inflater.inflate(R.layout.cumulations_spinner, null);
            TextView type = (TextView) view.findViewById(R.id.spinner_text);
            type.setText(spinnerItems.get(i));
            type.setSelected(true);
            return view;
        }
    }

    private void setupToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.nowplaying_toolbar);
        setSupportActionBar(mToolbar);
        /*mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        /*Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        mAppCompatSeekBar.startAnimation(fadeInAnimation);*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            LibreApplication.isConvertViewClicked = false;
//            supportFinishAfterTransition();
//        } else {
            LibreApplication.isConvertViewClicked = false;
            finish();
//            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
//        }
    }


    /*@Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }*/
/*

    @Override
    public void onClick(View view) {
        if (mCurrentSceneObject == null) {
            Toast.makeText(this, getString(R.string.Devicenotplaying), Toast.LENGTH_SHORT).show();
        } else {
            LUCIControl controlPlay = new LUCIControl(mCurrentSceneObject.getIpAddress());
            LSSDPNodes mNodeWeGotForControl = mScanHandler.getLSSDPNodeFromCentralDB(mCurrentIpAddress);
            if (mNodeWeGotForControl == null)
                return;
            if (mCurrentSceneObject.getCurrentSource() == DMR_SOURCE) {
                isLocalDMRPlayback = true;
            } else {
                isLocalDMRPlayback = false;
            }
            switch (view.getId()) {
                case R.id.play_song:
                    if(mCurrentSceneObject.getCurrentSource() == Constants.AUX_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.VTUNER_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.TUNEIN_SOURCE
                            || (mCurrentSceneObject.getCurrentSource() == Constants.BT_SOURCE
                            && ((mNodeWeGotForControl.getgCastVerision() == null
                            && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                            || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                            ||(mNodeWeGotForControl.getgCastVerision() != null
                            && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){

                        LibreError error = new LibreError("", getResources().getString(R.string.PLAY_PAUSE_NOT_ALLOWED), 1);
                        BusProvider.getInstance().post(error);
                        return;
                    }
                    if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {

                        if (doPlayPause(true) == true) {
                            mCurrentSceneObject.setPlaystatus(SceneObject.CURRENTLY_STOPED);
                            //mPlaySong.setText("{fa-pause}");
                            */
/*mPlaySong.setImageResource(R.mipmap.play_without_glow);
                            mNextSong.setImageResource(R.mipmap.next_without_glow);
                            mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*//*

                            mPlaySong.setImageResource(R.drawable.ic_play_circle_outline_white_64dp);
                            mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                            mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                            stopEqualizer();
                            //mutebutton.setImageResource(R.mipmap.mut_without_glow);
                        }
                    } else {

                        if (doPlayPause(false) == true) {

                            mCurrentSceneObject.setPlaystatus(SceneObject.CURRENTLY_PLAYING);
                            //mPlaySong.setText("{fa-play}");
                            */
/*mPlaySong.setImageResource(R.mipmap.pause_with_glow);
                            mNextSong.setImageResource(R.mipmap.next_with_glow);
                            mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*//*

                            mPlaySong.setImageResource(R.drawable.nowplaying_pause);
                            mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                            mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                            resumeEqualizer();
                            //mutebutton.setImageResource(R.mipmap.mut_with_glow);
                        }

                    }
                    break;

                case R.id.previous_song:
                    if(mCurrentSceneObject.getCurrentSource() == Constants.AUX_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.VTUNER_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.TUNEIN_SOURCE
                            || (mCurrentSceneObject.getCurrentSource() == Constants.BT_SOURCE
                            && ((mNodeWeGotForControl.getgCastVerision() == null
                            && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                            || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                            ||(mNodeWeGotForControl.getgCastVerision() != null
                            && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){
                        LibreError error = new LibreError("", getResources().getString(R.string.NEXT_PREVIOUS_NOT_ALLOWED),1);
                        BusProvider.getInstance().post(error);
                        return ;
                    }
                    if (mPreviousSong.isEnabled()) {
                        doNextPrevious(false);
                    }else{
                        LibreError error = new LibreError(mCurrentIpAddress,getString(R.string.requestTimeout));
                        BusProvider.getInstance().post(error);
                    }
                    break;
                case R.id.next_song:
                    if(mCurrentSceneObject.getCurrentSource() == Constants.AUX_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.VTUNER_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.TUNEIN_SOURCE
                            || (mCurrentSceneObject.getCurrentSource() == Constants.BT_SOURCE
                            && (mNodeWeGotForControl.getgCastVerision() == null
                            && (((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                            || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                            ||(mNodeWeGotForControl.getgCastVerision() != null
                            && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){
                        LibreError error = new LibreError("", getResources().getString(R.string.NEXT_PREVIOUS_NOT_ALLOWED),1);
                        BusProvider.getInstance().post(error);
                        return ;
                    }
                    if (mNextSong.isEnabled()) {
                        doNextPrevious(true);
                    }else{
                        LibreError error = new LibreError(mCurrentIpAddress, getString(R.string.requestTimeout));
                        BusProvider.getInstance().post(error);
                    }
                    break;

                case R.id.rounded_album_art:
                    break;
                case R.id.volume:
                    //TransitionManager.beginDelayedTransition(mRelativeLayout);
                    if (!volumeClicked)
                        mmSeekBarVisibility = !mmSeekBarVisibility;
                    if (mmSeekBarVisibility) {
                        if (!volumeClicked) {
                            volumeClicked = true;
                            //mVolume.setImageResource(R.drawable.cancwel);
                            //getResources().getDrawable(R.drawable.cumulations_circle_background).setColorFilter(Color.parseColor("#9E9E9E"), PorterDuff.Mode.SRC_IN);
                            mVolume.setImageResource(R.drawable.ic_close_white_24dp);
                            //mVolumeText.setBackgroundResource(R.drawable.cumulations_rounded_rectangle_for_text);
                            changeVolumeBackgroundColor(true, volumeClicked);
                            animatemSeekBar(380, true);
                        }
                    } else {
                        //mVolume.setImageResource(R.mipmap.mut_with_glow);
                        //mVolume.setImageResource(R.drawable.ic_volume_up_black_24dp);
                        if (!volumeClicked) {
                            volumeClicked = true;
                            animatemSeekBar(0, false);
                            //changeVolumeBackgroundColor(false);
                            mVolumeText.setVisibility(View.GONE);
                        }
                    }
                    break;

                case R.id.favourite_button:
                    LUCIControl favLuci = new LUCIControl(mCurrentSceneObject.getIpAddress());
                    if (mCurrentSceneObject.isFavourite()) {
                        favLuci.SendCommand(MIDCONST.MID_FAVOURITE, "GENERIC_FAV_DELETE", LSSDPCONST.LUCI_SET);
                    } else {
                        favLuci.SendCommand(MIDCONST.MID_FAVOURITE, "FAV_SAVE", LSSDPCONST.LUCI_SET);
                    }


//                    Toast.makeText(getActivity(), "Action completed", Toast.LENGTH_SHORT).show();

//                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Action completed", Snackbar.LENGTH_LONG)
//                            .setAction("Error", null)
//                            .show();

                    break;

                case R.id.shufflesong:

                    */
/*this is added to support shuffle and repeat option for Local Content*//*

                    if (isLocalDMRPlayback) {
                        RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentSceneObject.getIpAddress());
                        if (renderingDevice != null) {
                            String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                            PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                            if (playbackHelper != null) {
                                if (mCurrentSceneObject.getShuffleState() == 0) {
                                */
/*which means shuffle is off hence making it on*//*

                                    playbackHelper.setIsShuffleOn(true);
                                    mCurrentSceneObject.setShuffleState(1);
                                } else {
                                    */
/*which means shuffle is on hence making it off*//*

                                    mCurrentSceneObject.setShuffleState(0);
                                    playbackHelper.setIsShuffleOn(false);
                                }
                                */
/*inserting to central repo*//*

                                mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                                setViews();
                            }
                        }
                    } else {
                        LUCIControl luciControl = new LUCIControl(mCurrentSceneObject.getIpAddress());

                        if (mCurrentSceneObject.getShuffleState() == 0) {
                        */
/*which means shuffle is off*//*

                            luciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "SHUFFLE:ON", LSSDPCONST.LUCI_SET);

                        } else
                            luciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "SHUFFLE:OFF", LSSDPCONST.LUCI_SET);
                    }
                    break;


                case R.id.repeatsong:

                     */
/*this is added to support shuffle and repeat option for Local Content*//*

                    if (isLocalDMRPlayback) {
                        RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentSceneObject.getIpAddress());
                        if (renderingDevice != null) {
                            String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                            PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                            if (playbackHelper != null) {
                                if (mCurrentSceneObject.getRepeatState() == REPEAT_ALL) {
                                    playbackHelper.setRepeatState(REPEAT_OFF);
                                    mCurrentSceneObject.setRepeatState(REPEAT_OFF);
                                } else if (mCurrentSceneObject.getRepeatState() == REPEAT_OFF) {
                                    playbackHelper.setRepeatState(REPEAT_ONE);
                                    mCurrentSceneObject.setRepeatState(REPEAT_ONE);
                                } else if (mCurrentSceneObject.getRepeatState() == REPEAT_ONE) {
                                    playbackHelper.setRepeatState(REPEAT_ALL);
                                    mCurrentSceneObject.setRepeatState(REPEAT_ALL);
                                }
                                */
/*inserting to central repo*//*

                                mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                                setViews();
                            }
                        }
                    } else {
                        */
/**//*

                        LUCIControl shuffleLuciControl = new LUCIControl(mCurrentSceneObject.getIpAddress());
                        if (mCurrentSceneObject.getRepeatState() == REPEAT_ALL) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:OFF", LSSDPCONST.LUCI_SET);
                            mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp);
                            mCurrentSceneObject.setRepeatState(REPEAT_OFF);
                        }
                        //Not equal to spotify
                        else if (mCurrentSceneObject.getRepeatState() == REPEAT_OFF && mCurrentSceneObject.getCurrentSource() != Constants.SPOTIFY_SOURCE) {

                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ONE", LSSDPCONST.LUCI_SET);
                            mRepeat.setImageResource(R.drawable.ic_repeat_one);
                            mCurrentSceneObject.setRepeatState(REPEAT_ONE);
                        }

                        */
/* If the current source is spotify  *//*

                        else if (mCurrentSceneObject.getRepeatState() == REPEAT_OFF && mCurrentSceneObject.getCurrentSource() == Constants.SPOTIFY_SOURCE) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ALL", LSSDPCONST.LUCI_SET);
                            mRepeat.setImageResource(R.drawable.ic_repeatall);
                            mCurrentSceneObject.setRepeatState(REPEAT_ALL);
                        } else if (mCurrentSceneObject.getRepeatState() == REPEAT_ONE) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ALL", LSSDPCONST.LUCI_SET);
                            mRepeat.setImageResource(R.drawable.ic_repeatall);
                            mCurrentSceneObject.setRepeatState(REPEAT_ALL);
                        }
                        setViews();
                        */
/**//*
*/
/*
                        LUCIControl shuffleLuciControl = new LUCIControl(mCurrentSceneObject.getIpAddress());
                        if (mCurrentSceneObject.getRepeatState() == REPEAT_ALL) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:OFF", LSSDPCONST.LUCI_SET);
                        } else if (mCurrentSceneObject.getRepeatState() == REPEAT_OFF) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ONE", LSSDPCONST.LUCI_SET);
                        } else if (mCurrentSceneObject.getRepeatState() == REPEAT_ONE)
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ALL", LSSDPCONST.LUCI_SET);
*//*

                    }break;

            }
        }
    }
*/



    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        Fragment f = getSupportFragmentManager().findFragmentByTag(mCurrentIpAddress);
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    if (f != null) {
                        if (f.isVisible()) {
                            VolumeListener.OnChangeVolumeHardKeyListener volumeKeyListener = (CumulationsNowPlayingFragment)f;
                            volumeKeyListener.updateVolumeChangesFromHardwareKey(KeyEvent.KEYCODE_VOLUME_UP);
                        }
                    }
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    if (f != null) {
                        if (f.isVisible()) {
                            VolumeListener.OnChangeVolumeHardKeyListener volumeKeyListener = (CumulationsNowPlayingFragment)f;
                            volumeKeyListener.updateVolumeChangesFromHardwareKey(KeyEvent.KEYCODE_VOLUME_DOWN);
                        }
                    }
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }

    }


    private static class PhoneStateChangeListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    LibreLogger.d(this, "Phone is RINGING");
                    isPhoneCallbeingReceived = true;


                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    LibreLogger.d(this, "Phone is RINGING");
                    if (!isPhoneCallbeingReceived) {
                        // Start your new activity
                    } else {
                        // Cancel your old activity
                    }

                    // this should be the last piece of code before the break
                    isPhoneCallbeingReceived = true;
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    LibreLogger.d(this, "Phone is IDLE");
                    // this should be the last piece of code before the break
                    isPhoneCallbeingReceived = false;
                    break;
            }
        }
    }

    /*this method will get called whenever hardware volume is pressed*/
    @Override
    public void updateVolumeChangesFromHardwareKey(int keyCode) {


        /* Added to avoid user pressing the volume change when user is getting call*/
        if (CumulationsNowPlayingActivity.isPhoneCallbeingReceived)
            return;

        if (mCurrentSceneObject == null) {
            Log.d("VolumeHardKey", "Current Scene is--" + null);
            return;
        }
        if (mCurrentSceneObject!=null && mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE){
            LibreLogger.d(this,"source is cast. So, dont update volume");
            return;
        }

        Log.d("VolumeHardKey", "Device ip is--" + mCurrentSceneObject.getIpAddress());

        LUCIControl volumeControl = new LUCIControl(mCurrentSceneObject.getIpAddress());
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {

            if (mSeekBar.getProgress() > 85) {
                //  volumeControl.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + 100, CommandType.SET);
                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + 100, CommandType.SET);
            } else {
                //volumeControl.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + (volumeBar.getProgress() + 10), CommandType.SET);
                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + (mSeekBar.getProgress() + 3), CommandType.SET);
            }
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            if (mSeekBar.getProgress() < 15) {
                //volumeControl.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + 0, CommandType.SET);
                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + 0, CommandType.SET);
            } else {
                //volumeControl.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + (volumeBar.getProgress() - 10), CommandType.SET);
                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + (mSeekBar.getProgress() - 3), CommandType.SET);
            }


        }


    }


    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

    }

    @Override
    public void deviceGotRemoved(String ipaddress) {
        Log.d("DEVICEREMOVED", "Device removed");
        if (ipaddress.equals(mCurrentIpAddress)) {
            supportFinishAfterTransition();
        }
    }

    public void showLoader(String msg) {
        ShowLoader.showLoader(mLoading, mLoadingText, this);
    }

    public void closeLoader() {
        mLoading.clearAnimation();
        mLoading.setVisibility(View.GONE);
        mLoadingText.setVisibility(View.GONE);

    }

    Handler mNowPlayingActivityReleaseSceneHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Message failedMsg = new Message();
            failedMsg.what = DeviceMasterSlaveFreeConstants.LUCI_TIMEOUT_COMMAND;
            switch (msg.what) {

                case DeviceMasterSlaveFreeConstants.LUCI_SEND_FREE_COMMAND: {
                    mNowPlayingActivityReleaseSceneHandler.sendEmptyMessageDelayed(failedMsg.what, 35000);
                    showLoader("Releasing Scene");
                }
                break;

                case DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_FREE_COMMAND: {
                    mNowPlayingActivityReleaseSceneHandler.removeMessages(failedMsg.what);
                    Bundle b = new Bundle();
                    b = msg.getData();
                    String ipaddress = b.getString("ipAddress");
                    closeLoader();

                    LSSDPNodes node = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);
                    /* Crashed For Rashmi, So Fixed with Null Check
                     *  */
                    if(node==null) {
                        return;
                    }
                    LibreLogger.d(this, "Command 103 " + node.getFriendlyname() + node.getDeviceState());
                    /* if ((node != null) && node.getDeviceState().equals("M")) */
                    if ((node != null)  && sceneAddressList!=null && node.getIP().equalsIgnoreCase(sceneAddressList.get(mCurrentNowPlayingScene))) {
                        if (mScanHandler.isIpAvailableInCentralSceneRepo(ipaddress)) {
                            mScanHandler.removeSceneMapFromCentralRepo(ipaddress);
                        }
                      /*  LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();

                        boolean mMasterFound = false;
                        if (node != null)
                            UpdateLSSDPNodeList(node.getIP(), "Free");
                        for (LSSDPNodes mSlaveNode : m_ScanHandler.getSlaveListForMasterIp(node.getIP(),
                                m_ScanHandler.getconnectedSSIDname(getApplicationContext()))) {
                            UpdateLSSDPNodeList(mSlaveNode.getIP(), "Free");
                        }
                        m_ScanHandler.removeSceneMapFromCentralRepo(node.getIP());
                        for (LSSDPNodes mNode : mNodeDB.GetDB()) {
                            if (mNode.getDeviceState().contains("M") && !mNode.getIP().contains(ipaddress)) {

                                mMasterFound = true;
                            }
                            if (mMasterFound) {
                                break;
                            }
                        }*/
                        boolean mMasterFound = LSSDPNodeDB.getInstance().isAnyMasterIsAvailableInNetwork();
                        if (!mMasterFound) {
                            Intent intent = new Intent(CumulationsNowPlayingActivity.this, PlayNewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            StartLSSDPScanAfterRelease();
                            LibreApplication.isConvertViewClicked = false;
//                            ActivityCompat.finishAffinity(CumulationsNowPlayingActivity.this);
                            finish();
                        } else {
                            Intent intent = new Intent(CumulationsNowPlayingActivity.this, ActiveScenesListActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(intent);
                            StartLSSDPScanAfterRelease();
                            LibreApplication.isConvertViewClicked = false;
                            finish();

                        }
                    }
                }

                break;
                case DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_MASTER_COMMAND:
                    /*have to handle if we got 103 for any other Master*/
                    break;
                case DeviceMasterSlaveFreeConstants.LUCI_TIMEOUT_COMMAND: {
                    mNowPlayingActivityReleaseSceneHandler.removeMessages(failedMsg.what);

                    if (!(CumulationsNowPlayingActivity.this.isFinishing())) {
                        new AlertDialog.Builder(CumulationsNowPlayingActivity.this)
                                .setTitle(getString(R.string.deviceStateChanging))
                                .setMessage(getString(R.string.failed))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        closeLoader();
                                        dialog.cancel();
                                    }
                                }).setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }
                break;
                case DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_SLAVE_COMMAND:{
                    Bundle b = new Bundle();
                    b = msg.getData();
                    String ipaddress = b.getString("ipAddress");
                    LSSDPNodes node = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);
                    if(node==null)
                        return;

                    UpdateLSSDPNodeList(node.getIP(), "Slave");
                    LibreLogger.d(this, "Command 103 "+node.getFriendlyname() + "change to slave");
                }
            }
        }
    };


    public void StartLSSDPScanAfterRelease() {

        final LibreApplication application = (LibreApplication) getApplication();
        application.getScanThread().UpdateNodes();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();
            }
        }, 500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1000);

    }

    public void UpdateLSSDPNodeList(String ipaddress, String mDeviceState) {

        LSSDPNodes mToBeUpdateNode = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);
        //LSSDPNodes mMasterNode = m_ScanHandler.getLSSDPNodeFromCentralDB(mMasterIP);

        if (mToBeUpdateNode == null)
            return;

        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
        if (mDeviceState.equals("Master")) {
            mToBeUpdateNode.setDeviceState("M");
        } else if (mDeviceState.equals("Slave")) {
            mToBeUpdateNode.setDeviceState("S");
          /*  if(m_ScanHandler.getconnectedSSIDname(getApplicationContext())== m_ScanHandler.HN_MODE) {
                mToBeUpdateNode.setcSSID(mMasterNode.getcSSID());
            }else{
                mToBeUpdateNode.setZoneID(mMasterNode.getZoneID());
            }*/
        } else if (mDeviceState.equals("Free")) {
            mToBeUpdateNode.setDeviceState("F");
       /*     if(m_ScanHandler.getconnectedSSIDname(getApplicationContext())== m_ScanHandler.HN_MODE) {
                mToBeUpdateNode.setcSSID("");
            }else{
                mToBeUpdateNode.setZoneID("");
            }*/
        }
        mNodeDB.renewLSSDPNodeDataWithNewNode(mToBeUpdateNode);

    }


    @Override
    public void messageRecieved(NettyData dataRecived) {
        byte[] buffer = dataRecived.getMessage();
        String ipaddressRecieved = dataRecived.getRemotedeviceIp();

        LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());
        LibreLogger.d(this, "Message recieved for ipaddress " + ipaddressRecieved + "command is " + packet.getCommand());
        if (packet.getCommand() == 50) {
            String message = new String(packet.getpayload());
            //Toast.makeText(getApplicationContext(),"Got Source List" +message,Toast.LENGTH_SHORT).show();

        }
        if (packet.getCommand() == 103) {
            LibreLogger.d(this, "Command 103 " + new String(packet.getpayload()));
            Message successMsg = new Message();
            String message = new String(packet.getpayload());
            Bundle b = new Bundle();
            b.putString("ipAddress", dataRecived.getRemotedeviceIp());
            successMsg.setData(b);
            successMsg.obj = mScanHandler.getLSSDPNodeFromCentralDB(dataRecived.getRemotedeviceIp());
            if (message.contains("FREE")) {
                successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_FREE_COMMAND;
            } else if (message.contains("SLAVE")) {
                successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_SLAVE_COMMAND;
            } else if (message.contains("MASTER")) {
                successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_MASTER_COMMAND;
            }
            mNowPlayingActivityReleaseSceneHandler.sendMessage(successMsg);
        }

        if (packet.getCommand() == MIDCONST.MID_SCENE_NAME) {
            if (packet.getCommandType() == LSSDPCONST.LUCI_SET) {
                switch (packet.CommandStatus) {
                    case 0:
                        //Toast.makeText(this,"Zone rename successful",Toast.LENGTH_SHORT).show();
                        String msg = new String(packet.getpayload());
                        mZoneName.setText(msg);
                        mCurrentSceneObject.setSceneName(msg);
                        break;
                    case 2:
                        //Toast.makeText(this,"Zone rename failed, renaming not allowed when no slaves in the zone",Toast.LENGTH_SHORT).show();
                        mZoneName.setText(mCurrentSceneObject.getSceneName());
                        break;
                }
            }
        }

        /*LibreLogger.d(this, "Nowplaying: New message appeared for the device " + dataRecived.getRemotedeviceIp());
        if(dataRecived!=null &&
                (new LUCIPacket(dataRecived.getMessage()).getCommand() == MIDCONST.MID_DEVICE_STATE_ACK)){
        }
        if (mCurrentSceneObject != null && dataRecived.getRemotedeviceIp().equalsIgnoreCase(mCurrentIpAddress)) {
            LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());
            LibreLogger.d(this, "Packet is _" + packet.getCommand());

            switch (packet.getCommand()) {
              *//*  case MIDCONST.MID_DEVICE_STATE_ACK:
                    deviceCount.setText("" + mScanHandler.getNumberOfSlavesForMasterIp(currentIpAddress,
                            mScanHandler.getconnectedSSIDname(getActivity().getApplicationContext())));
                    break;*//*
                case MIDCONST.MID_SCENE_NAME: {
                    *//* This message box indicates the Scene Name*//**//*
;*//*                 *//* if Command Type 1 , then Scene Name information will be come in the same packet
if command type 2 and command status is 1 , then data will be empty., at that time we should not update the value .*//*
                    if(packet.getCommandStatus()==1
                            && packet.getCommandType()==2){
                        return;
                    }
                    String message = new String(packet.getpayload());
                    //int duration = Integer.parseInt(message);
                    try {
                        mCurrentSceneObject.setSceneName(message);// = duration/60000.0f;
                        LibreLogger.d(this, "Recieved the Scene Name to be " + mCurrentSceneObject.getSceneName());
                        if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                            mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                            //sceneName.setText(currentSceneObject.getSceneName());
                        }
                    } catch (Exception e) {

                    }
                }
                break;

                case 103:
                    if (packet.getCommand() == 103) {
                        LibreLogger.d(this, "Command 103 " + new String(packet.getpayload()));
                        Message successMsg = new Message();
                        String message = new String(packet.getpayload());
                        Bundle b = new Bundle();
                        b.putString("ipAddress", dataRecived.getRemotedeviceIp());
                        successMsg.setData(b);
                        successMsg.obj = mScanHandler.getLSSDPNodeFromCentralDB(dataRecived.getRemotedeviceIp());
                        if (message.contains("FREE")) {
                            successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_FREE_COMMAND;
                        } else if (message.contains("SLAVE")) {
                            successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_SLAVE_COMMAND;
                        } else if (message.contains("MASTER")) {
                            successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_MASTER_COMMAND;
                        }
                        mNowPlayingActivityReleaseSceneHandler.sendMessage(successMsg);
                    }
                    break;

                case 49: {
//                This message box indicates the current playing status of the scene, information like current seek position*//**//*
                    String message = new String(packet.getpayload());
                    if (!message.equals("")) {
                        long longDuration = Long.parseLong(message);

                        mCurrentSceneObject.setCurrentPlaybackSeekPosition(longDuration);

                        *//* Setting the current seekbar progress -Start*//*
                        float  duration = mCurrentSceneObject.getCurrentPlaybackSeekPosition();
                        mAppCompatSeekBar.setMax((int) mCurrentSceneObject.getTotalTimeOfTheTrack() / 1000);
                        Log.d("SEEK", "Duration = " + duration / 1000);
                        mAppCompatSeekBar.setProgress((int) duration / 1000);

                        DecimalFormat twoDForm = new DecimalFormat("#.##");
                        mSongStartTime.setText(convertMilisecondsToTimeString((int) duration / 1000));
                        mSongStopTime.setText(convertMilisecondsToTimeString(mCurrentSceneObject.getTotalTimeOfTheTrack() / 1000));
                        *//* Setting the current seekbar progress -END*//*

                        //setTheSourceIconFromCurrentSceneObject();


                     *//*   if (seekSongBar.isClickable()) {
                            seekSongBar.setProgress((int) duration / 1000);
                            DecimalFormat twoDForm = new DecimalFormat("#.##");
                            currentPlayPosition.setText(convertMilisecondsToTimeString(duration / 1000));
                            totalPlayPosition.setText(convertMilisecondsToTimeString(currentSceneObject.getTotalTimeOfTheTrack() / 1000));

                            Log.d("Bhargav SEEK", "Duration = " + duration / 1000);
                            Log.d("SEEK", "Duration = " + duration / 1000);
b
                        }*//*


                    }

                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                    LibreLogger.d(this, "Nowplaying: Recieved the current Seek position to be " + (int) mCurrentSceneObject.getCurrentPlaybackSeekPosition());
                    break;
                }

                case 50: {

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);

                        mCurrentSceneObject.setCurrentSource(duration);
                        if (mCurrentSceneObject.getCurrentSource()==14||mCurrentSceneObject.getCurrentSource()==19||mCurrentSceneObject.getCurrentSource()==0||mCurrentSceneObject.getCurrentSource()==12){
                            mRounderAlbumArt.setImageResource(R.mipmap.album_art);
                            blurImage(getResources().getDrawable(R.mipmap.album_art));
                            extractPaletteAndSetSeekbarProgressColor(getResources().getDrawable(R.mipmap.album_art));
                        }
                        LibreLogger.d(this, "Recieved the current source as  " + mCurrentSceneObject.getCurrentSource());
                        *//*if (duration == 14) {
                            disableViews("Aux Playing");
                        } else if (duration == 19) {
                            disableViews("BT Playing");
                        } else if (duration == 18) {
                            disableSeekBarNextPrevious();
                        } else {
                            enableViews();
                        }
*//*
                    } catch (Exception e) {

                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                    break;
                }
                case MIDCONST.ZONE_VOLUME: {

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        mCurrentSceneObject.setvolumeZoneInPercentage(duration);

                        if(mCurrentSceneObject.getCurrentSource()== Constants.AIRPLAY_SOURCE ||
                                mCurrentSceneObject.getCurrentSource()==Constants.SPOTIFY_SOURCE){
                            mSeekBar.setProgress(mCurrentSceneObject.getVolumeValueInPercentage());
                            mVolumeText.setText(String.valueOf(mCurrentSceneObject.getVolumeValueInPercentage()));
                        }else {
                            mSeekBar.setProgress(duration);
                            mVolumeText.setText(String.valueOf(duration));
                        }

                        //LibreLogger.d(this, "Recieved the current volume to be " + currentSceneObject.getvolumeZoneInPercentage());
                    } catch (Exception e) {

                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                }
                break;
                case MIDCONST.MID_FAVOURITE: {
                    String message = new String(packet.getpayload());
                    LibreLogger.d(this, "70 msg box" + "payload->" + message);
                    switch (message) {
                        case "GENERIC_FAV_SAVE_SUCCESS":
                            *//**fav success*//*
                            mCurrentSceneObject.setIsFavourite(true);
                            //favButton.setImageResource(R.drawable.ic_favorites_orange);
                            Toast.makeText(this, Constants.FAV_SUCCESSFUL, Toast.LENGTH_SHORT).show();
                            break;
                        case "GENERIC_FAV_DELETE_SUCCESS":
                            *//**fav deleted*//*
                            mCurrentSceneObject.setIsFavourite(false);
                            //favButton.setImageResource(R.drawable.ic_favorites_white);
                            Toast.makeText(this, getResources().getString(R.string.FAV_DELETED), Toast.LENGTH_SHORT).show();
                            break;
                        case "GENERIC_FAV_SAVE_FAILED":
                            Toast.makeText(this, getResources().getString(R.string.FAV_FAILED), Toast.LENGTH_SHORT).show();
                            break;
                        case "GENERIC_FAV_EXISTS":
                            Toast.makeText(this, getResources().getString(R.string.FAV_EXISTS), Toast.LENGTH_SHORT).show();
                            break;
                        case "GENERIC_FAV_VTUNER_DEL":
                            Toast.makeText(this, getResources().getString(R.string.VTUNER_FAV_DELETE), Toast.LENGTH_SHORT).show();
                            break;
                        default:
                    }
                }
                break;
                case 51: {
                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        mCurrentSceneObject.setPlaystatus(duration);
                        if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
                            if(mCurrentSceneObject.getCurrentSource()!=MIDCONST.GCAST_SOURCE) {
                                //mPlaySong.setText("{fa-pause}");
                                //mPlaySong.setImageResource(R.mipmap.pause_with_glow);
                                mPlaySong.setImageResource(R.drawable.ic_pause_circle_outline_white_64dp);
                                mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                                mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                                resumeEqualizer();
                                *//*mNextSong.setImageResource(R.mipmap.next_with_glow);
                                mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*//*
                            }
                            if(mCurrentSceneObject.getCurrentSource()!=19) {
                                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                                showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
                            }

                        }

                        else{
                            if(mCurrentSceneObject.getCurrentSource()!=MIDCONST.GCAST_SOURCE) {
                                //mPlaySong.setText("{fa-play}");
                                *//*mPlaySong.setImageResource(R.mipmap.play_with_gl);
                                mNextSong.setImageResource(R.mipmap.next_without_glow);
                                mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*//*
                                mPlaySong.setImageResource(R.drawable.ic_play_circle_outline_white_64dp);
                                mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                                mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                                stopEqualizer();

                            }
                            if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PAUSED) {
                            *//* this case happens only when the user has paused from the App so close the existing loader if any *//*
                                if(mCurrentSceneObject.getCurrentSource()!=19) {
                                    showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                                    showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
                                }

                            }
                            if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED&& mCurrentSceneObject.getCurrentSource()!=0) {

                                if(mCurrentSceneObject.getCurrentSource()!=19
                                        && mCurrentSceneObject.getCurrentSource()!= 15
                                        && mCurrentSceneObject.getCurrentSource()!= Constants.GCAST_SOURCE) {
                                    *//* When I am Stopping it , will change the album art to Default instead of Invalidate *//*
                                   *//* String default_album_url = "http://" + currentSceneObject.getIpAddress() + "/" + "coverart1.jpg";
                                    PicassoTrustCertificates.getInstance(getActivity()).load(default_album_url).
                                            placeholder(R.mipmap.album_art)
                                            .error(R.mipmap.album_art).into(albumArt);

    *//*
                                    showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_TIMEOUT_CONST, Constants.PREPARATION_TIMEOUT);
                                    showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_INITIATED);
                                }
                            }
                        }


*//*                            if(currentSceneObject.getCurrentSource() == 19){
                                disableViews("BT is Playing");
                            }else {
//                            if (isLoclaDMRPlayback) {
                            *//**//*device is playing now so closing loader*//**//*
                                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                                showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
//                            }

                            }
                            LibreLogger.d(this, "Play state is received");


                        } else if (currentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PAUSED) {
                            if(currentSceneObject.getCurrentSource() == 19){
                                disableViews("BT is Paused");
                            }
                            LibreLogger.d(this, "Pause state is received");
                        } else {
                            *//**//*which means stopped*//**//*
                            play.setImageResource(R.mipmap.play_with_gl);
                            next.setImageResource(R.mipmap.next_without_glow);
                            previous.setImageResource(R.mipmap.prev_without_glow);

                            if(currentSceneObject.getCurrentSource() == 19) {
                                disableViews("BT is Stopped");
                            }else {
//                            if (isLoclaDMRPlayback) {
                            *//**//*which means state is not playing*//**//*
                                showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_TIMEOUT_CONST, Constants.PREPARATION_TIMEOUT);
                                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_INITIATED);
//                            }
                            }*//*
                        LibreLogger.d(this, "Stop state is received");
                        LibreLogger.d(this, "Recieved the playstate to be" + mCurrentSceneObject.getPlaystatus());
                    } catch (Exception e) {

                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                }
                break;

                case 66: {

                    *//*String message = new String(packet.getpayload());
                    try {

                        if ( updateBuilder==null) {

                            updateBuilder = new AlertDialog.Builder(getActivity());
                            mProgressView=new TextView(getActivity());

                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);

                            mProgressView.setLayoutParams(lp);
                            mProgressView.setGravity(Gravity.CENTER);
                            mProgressView.setText(message);
                            mProgressView.setPadding(10,10,10,10);

                            updateBuilder.setTitle(getString(R.string.FirmwareUpdateinProgress)).setCancelable(false);
                            updateBuilder.setView(mProgressView);
                            updateBuilder.setPositiveButton(getString(R.string.back), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getActivity().onBackPressed();
                                }
                            }).show();

                        }
                        else{
                            mProgressView.setText(message);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();


                    }*//*

                }
                break;
                case 40: {
                    *//*            "Error_Fail" ,
                     "Error_NoURL" ,
                        "Error_LastSong"
*//*
                    String message = new String(packet.getpayload());
                    String ERROR_FAIL  = "Error_Fail";
                    String ERROR_NOURL  = "Error_NoURL";
                    String ERROR_LASTSONG  = "Error_LastSong";
                    LibreLogger.d(this,"recieved 40 "+message);
                    try {
                        if(message.equalsIgnoreCase(ERROR_FAIL)) {
                            LibreError error = new LibreError(mCurrentSceneObject.getIpAddress(), Constants.ERROR_FAIL);
                            BusProvider.getInstance().post(error);
                        }else if(message.equalsIgnoreCase(ERROR_NOURL)){
                            LibreError error = new LibreError(mCurrentSceneObject.getIpAddress(), Constants.ERROR_NOURL);
                            BusProvider.getInstance().post(error);
                        }else if(message.equalsIgnoreCase(ERROR_LASTSONG)){
                            LibreError error = new LibreError(mCurrentSceneObject.getIpAddress(), Constants.ERROR_LASTSONG);
                            BusProvider.getInstance().post(error);

                            showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                            preparingToPlay(false);
                            showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
                        }

                    } catch (Exception e) {

                    }
                }
                break;

                case 64: {

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        mCurrentSceneObject.setVolumeValueInPercentage(duration);
                        if(mCurrentSceneObject.getCurrentSource()==Constants.AIRPLAY_SOURCE
                                ||
                                mCurrentSceneObject.getCurrentSource() == Constants.SPOTIFY_SOURCE) {
                            mSeekBar.setProgress(duration);
                            mVolumeText.setText(String.valueOf(duration));
                        }
                        if (!mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {

                        }
                        LibreLogger.d(this, "Recieved the current volume to be " + mCurrentSceneObject.getVolumeValueInPercentage());
                    } catch (Exception e) {

                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                }
                break;

                case 222:{

                    *//*String message = new String(packet.payload);
                    if (message.equalsIgnoreCase("0:2")|| message.equalsIgnoreCase("2")) {

                        if (getActivity() != null) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("New Update Available!").setMessage("Do you wish to update the firmware now?")
                                    .setCancelable(false)
                                    .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();
                                        }
                                    })
                                    .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            LUCIControl favLuci = new LUCIControl(currentSceneObject.getIpAddress());
                                            favLuci.SendCommand(55, "", LSSDPCONST.LUCI_SET);

                                        }


                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
                 else   if (message.equalsIgnoreCase("0:3")|| message.equalsIgnoreCase("3")) {

                        if (getActivity() != null) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("New Update Available!").setMessage("Do you wish to update the firmware now?")
                                    .setCancelable(false)
                                    .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();
                                        }
                                    })
                                    .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            LUCIControl favLuci = new LUCIControl(currentSceneObject.getIpAddress());
                                            favLuci.SendCommand(222, "0:4", LSSDPCONST.LUCI_SET);

                                        }


                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
                    else if((message.equalsIgnoreCase("0:4"))|| message.equalsIgnoreCase("4")){
                        // forceful update
                        if (getActivity() != null) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Cast Device Updated").setMessage("Devce will reboot")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent = new Intent(getActivity(), ActiveScenesListActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }


                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }

                    }*//*

                }
                break;

                case 42: {


                    try {

                        String message = new String(packet.payload);
                        JSONObject root = new JSONObject(message);
                        int cmd_id = root.getInt(LUCIMESSAGES.TAG_CMD_ID);
                        JSONObject window = root.getJSONObject(LUCIMESSAGES.TAG_WINDOW_CONTENT);
                        LibreLogger.d(this, "PLAY JSON is \n= " + message + "\n For ip" + mCurrentIpAddress);


                        if (cmd_id == 3) {

                            showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_COMPLETED, 1000);
                            showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
                           *//* String newTrackname=window.getString("TrackName");
                            int newPlayState= window.getInt("PlayState");
                            int currentPlayState= window.getInt("Current_time");

                            if (!newTrackname.equalsIgnoreCase(currentSceneObject.getSceneName()) || newPlayState!=currentSceneObject.getPlaystatus()) {


                                if(!newTrackname.equalsIgnoreCase(currentSceneObject.getSceneName()))

                                {   *//**//* this is done to avoid  image refresh everytime the 42 message is recieved and the song playing back is the same *//**//*
                                    Picasso.with(getActivity()).invalidate("http://" + currentSceneObject.getIpAddress() + "/" + currentSceneObject.getAlbum_art());
                                }

                                currentSceneObject.setSceneName(window.getString("TrackName"));
                                currentSceneObject.setAlbum_art(window.getString("CoverArtUrl"));

                                if (currentPlayState>=0)
                                    currentSceneObject.setPlaystatus(window.getInt("PlayState"));
                            *//*
                            String newTrackname = window.getString("TrackName");
                            int newPlayState = window.getInt("PlayState");
                            int currentPlayState = window.getInt("Current_time");
                            int currentSource = window.getInt("Current Source");
                            String album_arturl = window.getString("CoverArtUrl");
                            String genre = window.getString("Genre");


                            String nAlbumName = window.getString("Album");
                            String nArtistName = window.getString("Artist");
                            long totaltime = window.getLong("TotalTime");

                            // currentSceneObject.setSceneName(window.getString("TrackName"));
                            mCurrentSceneObject.setPlaystatus(window.getInt("PlayState"));
                            mCurrentSceneObject.setAlbum_art(album_arturl);
                            String mPlayURL = window.getString("PlayUrl");
                            if(mPlayURL!=null)
                                mCurrentSceneObject.setPlayUrl(mPlayURL);
                            mCurrentSceneObject.setTrackName(window.getString("TrackName"));
                            *//*For favourite*//*
                            mCurrentSceneObject.setIsFavourite(window.getBoolean("Favourite"));

                            if(genre!=null)
                                mCurrentSceneObject.setGenre(window.getString("Genre"));


                            if (!LibreApplication.LOCAL_IP.equals("") && mCurrentSceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP))
                                isLocalDMRPlayback = true;
                            else if (mCurrentSceneObject.getCurrentSource() == DMR_SOURCE)
                                isLocalDMRPlayback = true;


                            *//*Added for Shuffle and Repeat*//*
                            if (isLocalDMRPlayback) {
                                *//**this is for local content*//*
                                RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentSceneObject.getIpAddress());
                                if (renderingDevice != null) {
                                    String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                                    PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                                    if (playbackHelper != null) {
                                        *//**shuffle is on*//*
                                        if (playbackHelper.isShuffleOn()) {
                                            mCurrentSceneObject.setShuffleState(1);
                                        } else {
                                            mCurrentSceneObject.setShuffleState(0);
                                        }
                                        *//*setting by default*//*
                                        if (playbackHelper.getRepeatState() == REPEAT_ONE) {
                                            mCurrentSceneObject.setRepeatState(REPEAT_ONE);
                                        }
                                        if (playbackHelper.getRepeatState() == REPEAT_ALL) {
                                            mCurrentSceneObject.setRepeatState(REPEAT_ALL);
                                        }
                                        if (playbackHelper.getRepeatState() == REPEAT_OFF) {
                                            mCurrentSceneObject.setRepeatState(REPEAT_OFF);
                                        }

                                    }
                                }
                            } else {
                                *//**this check made as we will not get shuffle and repeat state in 42, case of DMR
                                 * so we are updating it locally*//*
                                mCurrentSceneObject.setShuffleState(window.getInt("Shuffle"));
                                mCurrentSceneObject.setRepeatState(window.getInt("Repeat"));
                            }


                            mCurrentSceneObject.setAlbum_name(nAlbumName);
                            mCurrentSceneObject.setArtist_name(nArtistName);
                            mCurrentSceneObject.setTotalTimeOfTheTrack(totaltime);
                            mCurrentSceneObject.setCurrentSource(currentSource);

                            setViews(); //This will take care of disabling the views


*//*                            if (currentSource == 14) { // AUX
                                disableViews("Aux Playing");
                            } else if (currentSource == 19) {
                                disableViews("BT Playing");
                            }else  if (currentSource == 18) { // QQ MUSIC // KK Change For QQ MUSIC Disable the Seekbar
                                disableSeekBarNextPrevious();
                            } else {
                                enableViews();
                                setViews();
                            }*//*
                            String album_url = "";
                            if (album_arturl.contains("http://")) {
                                album_url = mCurrentSceneObject.getAlbum_art();
                            } else {
                                album_url = "http://" + mCurrentSceneObject.getIpAddress() + "/" + "coverart.jpg";
                            }

                            if (mCurrentSceneObject.getTrackName() == null
                                    || !newTrackname.equalsIgnoreCase(mCurrentSceneObject.getTrackName()))
                                PicassoTrustCertificates.getInstance(this).invalidate(album_url);
                            LibreLogger.d(this, "Recieved the scene details as trackname = " + mCurrentSceneObject.getSceneName() + ": " + mCurrentSceneObject.getCurrentPlaybackSeekPosition());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                    break;

                }
                case 54:{
                    String message = new String(packet.payload);
                    LibreLogger.d(this, " message 54 recieved  " + message);
                    try {
                        LibreError error=null;
                        if(message!=null && (message.equalsIgnoreCase(Constants.DMR_PLAYBACK_COMPLETED)||message.contains(Constants.FAIL))){

                            *//* If the song completes then make the seekbar to the starting of the song *//*
                            mCurrentSceneObject.setCurrentPlaybackSeekPosition(0);
                            mAppCompatSeekBar.setProgress(0);
                        *//* This is handled in nett
                           LibreLogger.d(this,"Libre logger sent the DMR playback completed ");
                            LibreLogger.d(this,"DMR completed for URL "+currentSceneObject.getPlayUrl());
                                if( currentSceneObject.getPlayUrl()!=null && currentSceneObject.getPlayUrl().contains(com.libre.luci.Utils.getLocalV4Address(Utils.getActiveNetworkInterface()).getHostAddress()))
                            {
                                LibreLogger.d(this,"App is going to next song");

                                doNextPrevious(true);
                            }*//*
                        }
                        else if(message.contains(Constants.FAIL)) {
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.FAIL_ALERT_TEXT));
                        }else if(message.contains(Constants.SUCCESS)){
                            //closeLoader();
                        }else if(message.contains(Constants.NO_URL)){
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.NO_URL_ALERT_TEXT));
                        }else if(message.contains(Constants.NO_PREV_SONG)){
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.NO_PREV_SONG_ALERT_TEXT));
                        }else if(message.contains(Constants.NO_NEXT_SONG)){
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.NO_NEXT_SONG_ALERT_TEXT));
                        } else if (message.contains(Constants.DMR_SONG_UNSUPPORTED)) {
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.SONG_NOT_SUPPORTED));
                        }else if(message.contains(LUCIMESSAGES.NEXTMESSAGE)){
                            doNextPrevious(true,true);
                        }else if(message.contains(LUCIMESSAGES.PREVMESSAGE)){
                            doNextPrevious(false,true);
                        }
                        PicassoTrustCertificates.getInstance(this).invalidate(mCurrentSceneObject.getAlbum_art());
                        //closeLoader();
                        if(error!=null)
                            BusProvider.getInstance().post(error);

                    } catch (Exception e) {
                        e.printStackTrace();
                        LibreLogger.d(this, " Json exception ");

                    }
                }break;



            }
        }*/
    }

    @Override
    public void onUpdatePosition(long position, long duration) {

    }

    @Override
    public void onUpdateVolume(int currentVolume) {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onStoped() {

    }

    @Override
    public void onSetURI() {

    }

    @Override
    public void onPlayCompleted() {

    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void onActionSuccess(Action action) {

    }

    @Override
    public void onActionFail(String actionCallback, UpnpResponse response, String cause) {

    }

    @Override
    public void onExceptionHappend(Action actionCallback, String mTitle, String cause) {

    }


    public interface paletteExtractor {
        void onPalette(Palette palette);
    }

    private void populateSceneAddressList() {
        Set<Map.Entry<String, SceneObject>> entries = mScanHandler.getSceneObjectFromCentralRepo().entrySet(); //ActiveSceneAdapter.mMasterSpecificSlaveAndFreeDeviceMap.entrySet();
        Iterator<Map.Entry<String, SceneObject>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, SceneObject> next = iterator.next();
            /* This is needed to make sure the unwanted free device is not coming */
            LSSDPNodes node = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(next.getValue().getIpAddress());
            if (node != null) {

                if ((node.getDeviceState() != null) && (node.getDeviceState().contains("F") || node.getDeviceState().contains("S"))) {
                    mScanHandler.removeSceneMapFromCentralRepo(node.getIP());
                    continue;
                }

            }{
                sceneAddressList.add(next.getKey());
                zones.add(next.getValue().getSceneName());
            }
        }
        try {
            mCurrentIpAddress = getIntent().getExtras().getString("current_ipaddress");
            if (mCurrentIpAddress == null) {
                LibreError error = new LibreError("Oops!", "ip address is not present");
                showErrorMessage(error);
                return;
            }
            mCurrentNowPlayingScene = sceneAddressList.indexOf(mCurrentIpAddress);
        } catch (Exception e) {
            if (getIntent().hasExtra(ActiveSceneAdapter.SCENE_POSITION)) {
                /*Modifiefd by Praveen for Redirecting the screen to Nowplaying screen */
                int position = getIntent().getExtras().getInt(ActiveSceneAdapter.SCENE_POSITION);
                mCurrentNowPlayingScene = position;
            } else {
                String m_CurrentIpadddress = getIntent().getExtras().getString("current_ipaddress");
                mCurrentNowPlayingScene = sceneAddressList.indexOf(m_CurrentIpadddress);
            }
        }

    }

    private String getIpAddress() {
        SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo(mCurrentIpAddress);
        if (sceneObject != null) {
            return sceneObject.getIpAddress();
        }
        return null;
    }



    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cumulations_now_playing_activity, menu);

        if (isGcastPlaying(mCurrentIpAddress)) {
            menu.setGroupVisible(R.id.now_playing_menu_group, false);
        }
        /*MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);
        ArrayAdapter zoneAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,zones);
        spinner.setAdapter(zoneAdapter);*/

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                menuItemView = findViewById(R.id.playlist);
                MenuItem item = menu.getItem(0);
                //menuItemViewAddSpeaker = findViewById(R.id.dummyText);
                //menuItemViewAddSpeaker = menu.findItem(R.id.addSpeaker).getActionView();
                showViewToolTip(menuItemView,getString(R.string.click_to_ope_music_sources), ViewTooltip.Position.BOTTOM, false);

            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                View dummy = findViewById(R.id.dummy);
                showViewToolTip(dummy,getString(R.string.click_for_zone_settings), ViewTooltip.Position.BOTTOM, false);

            }
        },3500);
        return true;
    }

    public void showViewToolTip(View view, String text, ViewTooltip.Position position, final boolean shouldRepeat) {
        SharedPreferences sharedPreferences = getSharedPreferences("apppreference", MODE_PRIVATE);

        if (view ==null)
        {
            //Toast.makeText(this,"Hello", Toast.LENGTH_LONG).show();
            return;
        }

        if (sharedPreferences.getBoolean(CumulationsNowPlayingActivity.class.getSimpleName(),false) == false) {
            sharedPreferences.edit().putBoolean(CumulationsNowPlayingActivity.class.getSimpleName(),true).commit();

            ViewTooltip
                    .on(view)
                    .autoHide(true,3000)
                    .clickToHide(true)
                    .align(ViewTooltip.ALIGN.CENTER)
                    .position(position)
                    .text(text)
                    .textColor(getResources().getColor(R.color.viewToolTipTextColor))
                    .color(getResources().getColor(R.color.viewToolTipColor))
                    .corner(10)
                    .onHide(new ViewTooltip.ListenerHide() {
                        @Override
                        public void onHide(View view) {
                        }
                    })
                    .show();
        }

        return;
    }

    /*this method will return current index */
    private int getSourceIndex(int currentSource) {
        int current_source_index_selected = -1;
        switch (currentSource) {
            case CumulationsNowPlayingFragment.NETWORK_DEVICES:
                /*Network ID*/
                current_source_index_selected = 0;
                break;

            case CumulationsNowPlayingFragment.DMR_SOURCE:
                /*FAV Current*/
                current_source_index_selected = 2;
                break;

            case CumulationsNowPlayingFragment.USB_SOURCE:
                /*USB current*/
                current_source_index_selected = 3;

                break;
            case CumulationsNowPlayingFragment.SD_CARD:
                /*SD card Current*/
                current_source_index_selected = 4;
                break;
            case CumulationsNowPlayingFragment.VTUNER_SOURCE:
                /*VTUNER Current*/
                current_source_index_selected = 1;

                break;
            case CumulationsNowPlayingFragment.TUNEIN_SOURCE:
                /*TUNEIN Current*/
                current_source_index_selected = 2;
                break;

            case CumulationsNowPlayingFragment.DEEZER_SOURCE:
                current_source_index_selected = 5;
                /*DEEZER*Current*/
                break;
            case CumulationsNowPlayingFragment.TIDAL_SOURCE:
                /*TIDAL Current*/
                current_source_index_selected = 6;
                break;

            case CumulationsNowPlayingFragment.FAV_SOURCE:
                /*FAV Current*/
                current_source_index_selected = 7;
                break;

            default:
                current_source_index_selected = -1;
                break;
        }
        return current_source_index_selected;
    }


    private void playlistLogic() {
        if (sceneAddressList.size() > 0
                && mCurrentNowPlayingScene >= 0 && mCurrentNowPlayingScene < sceneAddressList.size()) {
            SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo(sceneAddressList.get(mCurrentNowPlayingScene));
            if (sceneObject == null)
                return;
            if (sceneObject.getCurrentSource()== DMR_SOURCE){
                RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp( sceneAddressList.get(mCurrentNowPlayingScene));
                if (renderingDevice != null) {
                    String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                    PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                    if ( playbackHelper==null|| playbackHelper.getDmsHelper() == null) {
                            /*    when we play from DMR and kill the app and open again. The source is 2(DMR).
                                        At that instance playbackHelper or playbackHelper.getDmsHelper() is null.
                                        So navigate to activescenes.*/
                        //onBackPressed();
                        goToSourcesActivity();
                    }else{
                        m_browseObjectStack = playbackHelper.getDmsHelper().getBrowseObjectStack();
                        if (m_browseObjectStack.size() == 1) {
                            goToSourcesActivity();
                            return;
                        }
                        Intent localIntent = new Intent(CumulationsNowPlayingActivity.this, LocalDMSActivity.class);
                        localIntent.putExtra("isLocalDeviceSelected", true);
                        localIntent.putExtra("current_ipaddress", sceneAddressList.get(mCurrentNowPlayingScene));
                        localIntent.putExtra("fromActivity","Nowplaying");
                        startActivity(localIntent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }else{
                    //onBackPressed();
                    goToSourcesActivity();
                }

            }else if (sceneObject!=null && getSourceIndex(sceneObject.getCurrentSource()) >= 0) {
                Intent intent = new Intent(CumulationsNowPlayingActivity.this, RemoteSourcesList.class);
                intent.putExtra("current_ipaddress", sceneAddressList.get(mCurrentNowPlayingScene));
                intent.putExtra("current_source_index_selected", getSourceIndex(sceneObject.getCurrentSource()));
                intent.putExtra("from_now_playing", true);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else {
                //onBackPressed();
                goToSourcesActivity();
            }
        }
    }

    private void goToSourcesActivity() {
        Intent mActiveScenesList = new Intent(this, CumulationsSourcesOptionActivity.class);
        mActiveScenesList.putExtra("current_ipaddress", mCurrentIpAddress);
        mActiveScenesList.putExtra("current_source", "" + mCurrentSceneObject.getCurrentSource());
        mActiveScenesList.putExtra(Constants.FROM_ACTIVITY,"CumulationsNowPlayingActivity");
        startActivity(mActiveScenesList);
        overridePendingTransition(R.anim.enter, R.anim.exit);
//        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.playlist:
                mCurrentSceneObject = mScanHandler.getSceneObjectFromCentralRepo(mCurrentIpAddress);//ActiveSceneAdapter.mMasterSpecificSlaveAndFreeDeviceMap.get(currentIpAddress);
                final LSSDPNodes mMasterNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(mCurrentIpAddress);

                if (mCurrentSceneObject != null && mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE
                        && mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
                    LibreError error = new LibreError(mMasterNode.getFriendlyname(), getString(R.string.speaker_casting_error));
                    BusProvider.getInstance().post(error);
                    return true;
                }
                if (mCurrentSceneObject != null) {
                    /*Intent mActiveScenesList = new Intent(this, CumulationsSourcesOptionActivity.class);
                    mActiveScenesList.putExtra("current_ipaddress", mCurrentIpAddress);
                    mActiveScenesList.putExtra("current_source", "" + mCurrentSceneObject.getCurrentSource());
                    mActiveScenesList.putExtra(Constants.FROM_ACTIVITY,"CumulationsNowPlayingActivity");
                    startActivity(mActiveScenesList);
                    overridePendingTransition(R.anim.enter, R.anim.exit);*/
                    playlistLogic();
                } else {
                    Toast.makeText(this, getString(R.string.deviceRemoved) + mCurrentIpAddress, Toast.LENGTH_SHORT).show();
                    LibreLogger.d(this, "Device Got Removed" + mCurrentIpAddress);
                }
                return true;
            case R.id.zoneSettings:
                if (mCurrentSceneObject!=null && mCurrentSceneObject.getIpAddress()!=null) {
                    Intent i = new Intent(this, NewManageDevices.class);
                    i.putExtra("master_ip", "" + mCurrentSceneObject.getIpAddress());
                    i.putExtra("activity_name", "NowPlayingActivity");
                    startActivity(i);
                    finish();
                }
                return true;

            case R.id.addSpeaker:
                Intent addSpeakerIntent = new Intent(CumulationsNowPlayingActivity.this, FreeSpeakersActivity.class);
                addSpeakerIntent.putExtra("master_ip", mCurrentSceneObject.getIpAddress());
                startActivity(addSpeakerIntent);
                break;
            /*case R.id.zoneRename:
                ZoneRenameDialogFragment zoneRenameDialogFragment = ZoneRenameDialogFragment.newInstance(sceneAddressList.get(mCurrentNowPlayingScene));
                zoneRenameDialogFragment.show(getSupportFragmentManager(), "ZoneRename");
                break;*/
            case R.id.releaseZone:
                new AlertDialog.Builder(CumulationsNowPlayingActivity.this)

                        .setMessage(getString(R.string.releaseSceneMsg))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                final LSSDPNodes mMasternode = mScanHandler.getLSSDPNodeFromCentralDB(sceneAddressList.get(mCurrentNowPlayingScene));//(sceneAddressList.get(mCurrentNowPlayingScene));
                                if (mMasternode == null) {
                                    Toast.makeText(CumulationsNowPlayingActivity.this, "Opps!Master not found", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                Bundle b = new Bundle();
                                ArrayList<LSSDPNodes> mSlaveListForMyMaster = mScanHandler.getSlaveListForMasterIp(
                                        sceneAddressList.get(mCurrentNowPlayingScene),
                                        mScanHandler.getconnectedSSIDname(getApplicationContext()));

                                for (LSSDPNodes mSlaveNode : mSlaveListForMyMaster) {
                                    LUCIControl luciControl = new LUCIControl(mSlaveNode.getIP());
                                    LibreLogger.d("this", "Release Scene Slave Ip List" + mSlaveNode.getFriendlyname());

                                    luciControl.sendAsynchronousCommand();
                                    luciControl.SendCommand(MIDCONST.MID_DDMS, LUCIMESSAGES.SETFREE, LSSDPCONST.LUCI_SET);
                                }
                                LUCIControl mMluciControl = new LUCIControl(mMasternode.getIP());
                                mMluciControl.sendAsynchronousCommand();
                                mMluciControl.SendCommand(MIDCONST.MID_JOIN_OR_DROP, LUCIMESSAGES.DROP, LSSDPCONST.LUCI_SET);
                                if (mScanHandler.removeSceneMapFromCentralRepo(mMasternode.getIP())) {
                                    LibreLogger.d("this", "Release Scene Removed From Central Repo " + mMasternode.getIP());
                                }

                                Message msg = new Message();
                                msg.what = DeviceMasterSlaveFreeConstants.LUCI_SEND_FREE_COMMAND;
                                b.putString("ipAddress", mMasternode.getIP());
                                msg.setData(b);
                                mNowPlayingActivityReleaseSceneHandler.sendMessage(msg);

                                StartLSSDPScanAfterRelease();

                                /*if (mScanHandler.getSceneObjectFromCentralRepo().size() <= 0) {
                                    Intent intent = new Intent(CumulationsNowPlayingActivity.this, PlayNewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(CumulationsNowPlayingActivity.this, ActiveScenesListActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }*/
                            }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
//                                Toast.makeText(getApplicationContext(), "Device state Not Changed", Toast.LENGTH_SHORT).show();
                                    dialog.cancel();
                                }
                            })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;

        }
        return true;
    }

    /*Added to handle hardware options menu showing actionSherlock type menu*/
    @Override
    public boolean onKeyDown(int keycode, KeyEvent e) {
        switch(keycode) {
            case KeyEvent.KEYCODE_MENU:
                mToolbar.showOverflowMenu();
                return true;
        }
        return super.onKeyDown(keycode, e);
    }
}
