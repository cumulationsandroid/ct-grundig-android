package com.cumulations.grundig.nowplaying;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.cumulations.grundig.NewManageDevices.NewManageDevices;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;

/**
 * Created by cumulations on 18/9/17.
 */

public class ZoneRenameDialogFragment extends android.support.v4.app.DialogFragment {

    public ScanningHandler mScanHandler = ScanningHandler.getInstance();
    private String masterIp;

    public interface ZoneDialogListener {
        public void onDialogPositiveClick(String zoneName);
    }

    ZoneDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ZoneDialogListener)activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public static ZoneRenameDialogFragment newInstance(String masterIp) {
        Bundle args = new Bundle();
        args.putString("masterip", masterIp);
        ZoneRenameDialogFragment zoneRenameDialogFragment = new ZoneRenameDialogFragment();
        zoneRenameDialogFragment.setArguments(args);
        return zoneRenameDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        masterIp = getArguments().getString("masterip");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //to hide keyboard when showing dialog fragment
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.zone_rename_dialog_layout, null);
        final EditText zoneRename = (EditText)view.findViewById(R.id.zone_rename);
        builder.setTitle(R.string.zoneRename);
        builder.setMessage(R.string.suggestions);
        builder.setView(view)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        closeKeyboardFromFocus(zoneRename);
                       if (!zoneRename.getText().toString().equals("") && !zoneRename.getText().toString().trim().equalsIgnoreCase("NULL") && zoneRename.getText().toString().length() <= 15) {
                           LUCIControl mLuci = new LUCIControl(masterIp);
                           mLuci.sendAsynchronousCommand();
                           mLuci.SendCommand(MIDCONST.MID_SCENE_NAME, toUpperCase(zoneRename.getText().toString()), LSSDPCONST.LUCI_SET);
                           //UpdateLSSDPNodeDeviceName(masterIp, zoneRename.getText().toString());

                           LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
                           LSSDPNodes node = mNodeDB.getTheNodeBasedOnTheIpAddress(masterIp);
                           if (node != null) {
                               mLuci.sendAsynchronousCommand();
                           }
                           mNodeDB.renewLSSDPNodeDataWithNewNode(node);
                           SceneObject sceneObjectFromCentralRepo = mScanHandler.getSceneObjectFromCentralRepo(masterIp);
                           if (sceneObjectFromCentralRepo != null) {
                               sceneObjectFromCentralRepo.setSceneName(toUpperCase(zoneRename.getText().toString()));
                               mScanHandler.putSceneObjectToCentralRepo(masterIp, sceneObjectFromCentralRepo);
                           }
                           mListener.onDialogPositiveClick(toUpperCase(zoneRename.getText().toString()));
                       } else {
                            if (!getActivity().isFinishing()) {
                                if (zoneRename.getText().toString().equals("")) {
                                    new AlertDialog.Builder(getActivity())
                                            .setTitle(getString(R.string.sceneNameChange))
                                            .setMessage(" " +
                                                    getString(R.string.sceneNameEmpty))
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            }).setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();

                                } else if (zoneRename.getText().toString().trim().equalsIgnoreCase("NULL")) {
                                    new AlertDialog.Builder(getActivity())
                                            .setTitle(getString(R.string.sceneNameChange))
                                            .setMessage(" " +
                                                    getString(R.string.sceneNameNull))
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            }).setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }
                            }
                       }
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        closeKeyboardFromFocus(zoneRename);
                        ZoneRenameDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    private String toUpperCase(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public void UpdateLSSDPNodeDeviceName(String ipaddress, String mDeviceName) {

        LSSDPNodes mToBeUpdateNode = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);

        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
        if (mToBeUpdateNode != null) {
            mToBeUpdateNode.setZoneID(mDeviceName);
            //mToBeUpdateNode.setFriendlyname(mDeviceName);
            mNodeDB.renewLSSDPNodeDataWithNewNode(mToBeUpdateNode);
        }

    }

    private void closeKeyboardFromFocus(View view){
//        View view = this.getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
