package com.cumulations.grundig.nowplaying;

import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;

/**
 * Created by cumulations on 3/7/17.
 */

public class PaletteExtractor {

    private static void extractPalette(Bitmap bitmap, final CumulationsNowPlayingActivity.paletteExtractor listener) {
        if (bitmap != null && !bitmap.isRecycled()) {
            Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    listener.onPalette(palette);
                }
            });
        }
    }

    public static void getPalette(Bitmap bitmap, CumulationsNowPlayingActivity.paletteExtractor listener) {
        extractPalette(bitmap, listener);
    }
}
