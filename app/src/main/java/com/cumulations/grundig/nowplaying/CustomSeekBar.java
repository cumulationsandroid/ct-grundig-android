package com.cumulations.grundig.nowplaying;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.view.View;

import com.cumulations.grundig.RemoteSourcesList;

/**
 * Created by Anirudh Uppunda on 9/10/17.
 */

public class CustomSeekBar extends AppCompatSeekBar {

    private Paint progressPaint;
    private float seekbarProgress;
    private ValueAnimator barAnimator;

    public CustomSeekBar(Context context) {
        super(context);
    }

    public CustomSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        progressPaint = new Paint();
        progressPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }


    @Override
    public synchronized void setMax(int max) {
        super.setMax(max);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int specHeight = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(width, specHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int height = getHeight();
        int progressEndX = (int) (getWidth() * seekbarProgress / 1000f);
        progressPaint.setStrokeWidth(10);
        int color = getResources().getColor(android.R.color.holo_red_light);
        progressPaint.setColor(color);
        canvas.drawLine(0, height, progressEndX, height, progressPaint);

        // draw the unfilled portion of the bar
        progressPaint.setColor(getResources().getColor(android.R.color.holo_blue_light));
        canvas.drawLine(progressEndX, height, getWidth(), height, progressPaint);
    }

    public void setProgress(final int progress) {
        barAnimator = ValueAnimator.ofFloat(0, 1);

        seekbarProgress = progress;
        postInvalidate();
        /*barAnimator.setDuration(700);
        barAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float interpolation = (float) animation.getAnimatedValue();
                seekbarProgress = interpolation * progress;
                postInvalidate();
            }
        });

        if (!barAnimator.isStarted()) {
            barAnimator.start();
        }*/
    }
}
