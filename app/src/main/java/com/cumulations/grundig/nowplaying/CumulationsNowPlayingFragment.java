package com.cumulations.grundig.nowplaying;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.ActiveScenesListActivity;
import com.cumulations.grundig.DeviceDiscoveryFragment;
import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.PlayNewActivity;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.SourceOptions.CumulationsSourcesOptionActivity;
import com.cumulations.grundig.app.dlna.dmc.LocalDMSActivity;
import com.cumulations.grundig.app.dlna.dmc.processor.interfaces.DMRProcessor;
import com.cumulations.grundig.app.dlna.dmc.utility.DMRControlHelper;
import com.cumulations.grundig.app.dlna.dmc.utility.PlaybackHelper;
import com.cumulations.grundig.app.dlna.dmc.utility.UpnpDeviceManager;
import com.cumulations.grundig.constants.CommandType;
import com.cumulations.grundig.constants.DeviceMasterSlaveFreeConstants;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.BusProvider;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.BlurTransformation;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.PicassoTrustCertificates;
import com.cumulations.grundig.util.ShowLoader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.fourthline.cling.model.ModelUtil;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.meta.Action;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

//import com.squareup.leakcanary.RefWatcher;
/**
 * Created by cumulations on 1/8/17.
 */

public class CumulationsNowPlayingFragment extends DeviceDiscoveryFragment
        implements View.OnClickListener, LibreDeviceInteractionListner,
        DMRProcessor.DMRProcessorListener, VolumeListener.OnChangeVolumeHardKeyListener {


    private FrameLayout mRelativeLayout;
    private TextView homeButton;
    private CircleImageView mRounderAlbumArt;
    private TextView mSongName;
    private TextView mArtistName;
    private ImageButton mVolume;
    private boolean mmSeekBarVisibility = false;
    private boolean mShuffleClicked = false;
    private boolean mRepeatClicked = false;
    private boolean volumeClicked = false;
    private AppCompatSeekBar mSeekBar;
    private AppCompatSeekBar mVolumeSeekBar;
    private TextView mVolumeText;
    private ImageButton mShuffle;
    private ImageButton mRepeat;
    private TextView mSongStartTime;
    private TextView mSongStopTime;
    private AppCompatSeekBar mAppCompatSeekBar;
    private ImageButton mPlaySong;
    private ImageButton mPreviousSong;
    private ImageButton mNextSong;
    private AppCompatImageButton mPlay;
    private AppCompatSpinner mZoneSpinner;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    private RelativeLayout volumeLayout;
    private ArrayList<String> zones = new ArrayList<>();//{"zone name 1", "zone name 2", "zone name 3"};
    private int vibrant;

    private AudioManager mAudioManager;

    private ArrayList<String> sceneAddressList = new ArrayList<>();
    private String mCurrentIpAddress;
    private SceneObject mCurrentSceneObject;
    private boolean isLocalDMRPlayback;

    public static final int REPEAT_OFF = 0;
    public static final int REPEAT_ONE = 1;
    public static final int REPEAT_ALL = 2;
    public static final int DMR_SOURCE = 2;
    private String currentTrackName = "-1";

    public static final int USB_SOURCE = 5;
    public static final int SD_CARD = 6;
    public static final int DEEZER_SOURCE = 21;
    public static final int FAV_SOURCE = 23;
    public static final int TIDAL_SOURCE = 22;
    public static final int VTUNER_SOURCE = 8;
    public static final int TUNEIN_SOURCE = 9;
    public static final int NETWORK_DEVICES = 3;
    public static final int ROON_SOURCE = 27;

    private boolean shouldShowViewsWithoutAnimation = true;
    private static boolean isPhoneCallbeingReceived;
    private boolean isVolumeVisible = false;
    private int mCurrentNowPlayingScene;
    private FrameLayout mFrameLayoutForVolume;

    private ImageView loadingBar;
    private TextView loadingText;

    private static Handler handler = new Handler();

    ScanningHandler mScanHandler = ScanningHandler.getInstance();
    //private BehaviorSubject<Integer> volume = BehaviorSubject.create();
    private LUCIControl volumeControl;
    private ImageView favButton;
    private ProgressDialog progressDialog;
    private String loaderMsg;
    private Toast castToast;
    private ImageView blurredBgImage;

    /*For getActivity crash in blurBackground*/
    private Activity mActivity;

    interface getCurrentSceneObject {
        void setCurrentSceneObject(SceneObject sceneObject);
    }

    getCurrentSceneObject mGetCurrentSceneObject;

    public void setClass(getCurrentSceneObject currentSceneObject) {
        //currentSceneObject.getCurrentSceneObject();
        mGetCurrentSceneObject = currentSceneObject;
    }

    Handler showLoaderHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case Constants.PREPARATION_INITIATED:
                    //mProgressBar.setVisibility(View.VISIBLE);
                    /*disabling views while initiating */
                    ShowLoader.showLoaderInNowPlaying(loadingBar, getActivity());
                    mPlaySong.setVisibility(View.GONE);
                    preparingToPlay(false);
                    break;

                case Constants.PREPARATION_COMPLETED:
                    //mProgressBar.setVisibility(View.GONE);
                    /*enabling views while initiating */
                    loadingBar.clearAnimation();
                    loadingBar.setVisibility(View.GONE);
//                    loadingText.setVisibility(View.GONE);
                    mPlaySong.setVisibility(View.VISIBLE);
                    preparingToPlay(true);
                    break;

                case Constants.PREPARATION_TIMEOUT_CONST:

                    /*enabling views while initiating */
                    preparingToPlay(true);

                      /*posting error to application here we are using bus because it doest extend DeviceDiscoverActivity*/
                  /*
                    Commenting the error dialog as it was getting triggered for the external app setting the AV transport url

                    LibreError error = new LibreError(currentIpAddress, getResources.getString(R.string.PREPARATION_TIMEOUT_HAPPENED));
                    BusProvider.getInstance().post(error);
                    */
                    //mProgressBar.setVisibility(View.GONE);
                    loadingBar.clearAnimation();
                    loadingBar.setVisibility(View.GONE);
                    mPlaySong.setVisibility(View.VISIBLE);
                    break;

                case Constants.FAV_INITIATED_TIMEOUT:
                    closeLoader();
                    /*showing error*/
//                    Toast.makeText(getActivity(), "Adding to favorites timed out", Toast.LENGTH_SHORT).show();
                    break;
                case Constants.FAV_CLICKED:
                    showLoader(loaderMsg);
                    showLoaderHandler.sendEmptyMessageDelayed(Constants.FAV_INITIATED_TIMEOUT,10000);
                    break;

            }


        }


    };

    Handler mNowPlayingActivityReleaseSceneHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Message failedMsg = new Message();
            failedMsg.what = DeviceMasterSlaveFreeConstants.LUCI_TIMEOUT_COMMAND;
            switch (msg.what) {

                case DeviceMasterSlaveFreeConstants.LUCI_SEND_FREE_COMMAND: {
                    mNowPlayingActivityReleaseSceneHandler.sendEmptyMessageDelayed(failedMsg.what, 35000);
                    //showLoader("Releasing Scene");
                }
                break;

                case DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_FREE_COMMAND: {
                    mNowPlayingActivityReleaseSceneHandler.removeMessages(failedMsg.what);
                    Bundle b = new Bundle();
                    b = msg.getData();
                    String ipaddress = b.getString("ipAddress");
                    //closeLoader();

                    LSSDPNodes node = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);
                    /* Crashed For Rashmi, So Fixed with Null Check
                     *  */
                    if(node==null) {
                        return;
                    }
                    LibreLogger.d(this, "Command 103 " + node.getFriendlyname() + node.getDeviceState());
                    /* if ((node != null) && node.getDeviceState().equals("M")) */
                    try {
                        if ((node != null)  && sceneAddressList!=null && node.getIP().equalsIgnoreCase(sceneAddressList.get(mCurrentNowPlayingScene))) {
                            if (mScanHandler.isIpAvailableInCentralSceneRepo(ipaddress)) {
                                mScanHandler.removeSceneMapFromCentralRepo(ipaddress);
                            }
                      /*  LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();

                        boolean mMasterFound = false;
                        if (node != null)
                            UpdateLSSDPNodeList(node.getIP(), "Free");
                        for (LSSDPNodes mSlaveNode : m_ScanHandler.getSlaveListForMasterIp(node.getIP(),
                                m_ScanHandler.getconnectedSSIDname(getApplicationContext()))) {
                            UpdateLSSDPNodeList(mSlaveNode.getIP(), "Free");
                        }
                        m_ScanHandler.removeSceneMapFromCentralRepo(node.getIP());
                        for (LSSDPNodes mNode : mNodeDB.GetDB()) {
                            if (mNode.getDeviceState().contains("M") && !mNode.getIP().contains(ipaddress)) {

                                mMasterFound = true;
                            }
                            if (mMasterFound) {
                                break;
                            }
                        }*/
                            boolean mMasterFound = LSSDPNodeDB.getInstance().isAnyMasterIsAvailableInNetwork();
                            if (!mMasterFound) {
                                Intent intent = new Intent(getActivity(), PlayNewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                StartLSSDPScanAfterRelease();
                                LibreApplication.isConvertViewClicked = false;
                                ActivityCompat.finishAffinity(getActivity());
                                getActivity().finish();
                            } else {
                                Intent intent = new Intent(getActivity(), ActiveScenesListActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                startActivity(intent);
                                StartLSSDPScanAfterRelease();
                                LibreApplication.isConvertViewClicked = false;
                                getActivity().finish();

                            }
                        }
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }

                break;
                case DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_MASTER_COMMAND:
                    /*have to handle if we got 103 for any other Master*/
                    break;
                case DeviceMasterSlaveFreeConstants.LUCI_TIMEOUT_COMMAND: {
                    mNowPlayingActivityReleaseSceneHandler.removeMessages(failedMsg.what);

                    if (!(getActivity().isFinishing())) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle(getString(R.string.deviceStateChanging))
                                .setMessage(getString(R.string.failed))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                }).setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }
                break;
                case DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_SLAVE_COMMAND:{
                    Bundle b = new Bundle();
                    b = msg.getData();
                    String ipaddress = b.getString("ipAddress");
                    LSSDPNodes node = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);
                    if(node==null)
                        return;

                    UpdateLSSDPNodeList(node.getIP(), "Slave");
                    LibreLogger.d(this, "Command 103 "+node.getFriendlyname() + "change to slave");
                }
            }
        }
    };


    public void StartLSSDPScanAfterRelease() {

        final LibreApplication application = (LibreApplication) getActivity().getApplication();
        application.getScanThread().UpdateNodes();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();
            }
        }, 500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1000);

    }

    public void UpdateLSSDPNodeList(String ipaddress, String mDeviceState) {

        LSSDPNodes mToBeUpdateNode = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);
        //LSSDPNodes mMasterNode = m_ScanHandler.getLSSDPNodeFromCentralDB(mMasterIP);

        if (mToBeUpdateNode == null)
            return;

        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
        if (mDeviceState.equals("Master")) {
            mToBeUpdateNode.setDeviceState("M");
        } else if (mDeviceState.equals("Slave")) {
            mToBeUpdateNode.setDeviceState("S");
          /*  if(m_ScanHandler.getconnectedSSIDname(getApplicationContext())== m_ScanHandler.HN_MODE) {
                mToBeUpdateNode.setcSSID(mMasterNode.getcSSID());
            }else{
                mToBeUpdateNode.setZoneID(mMasterNode.getZoneID());
            }*/
        } else if (mDeviceState.equals("Free")) {
            mToBeUpdateNode.setDeviceState("F");
       /*     if(m_ScanHandler.getconnectedSSIDname(getApplicationContext())== m_ScanHandler.HN_MODE) {
                mToBeUpdateNode.setcSSID("");
            }else{
                mToBeUpdateNode.setZoneID("");
            }*/
        }
        mNodeDB.renewLSSDPNodeDataWithNewNode(mToBeUpdateNode);

    }



    /*this method will take care of views while we are preparing to play*/
    private void preparingToPlay(boolean value) {

        if (value) {
            setTheSourceIconFromCurrentSceneObject();
        } else {
            mAppCompatSeekBar.setEnabled(value);
            mAppCompatSeekBar.setClickable(value);
            mPlaySong.setEnabled(value);
            mPlaySong.setClickable(value);
            mPreviousSong.setEnabled(value);
            mPreviousSong.setClickable(value);
            mNextSong.setEnabled(value);
            mNextSong.setClickable(value);
            mSeekBar.setEnabled(value);
            mSeekBar.setClickable(value);
        }
    }

    private void setCurrentSceneObject() {
        if (mCurrentIpAddress != null) {
            mCurrentSceneObject = mScanHandler.getSceneObjectFromCentralRepo(mCurrentIpAddress); // ActiveSceneAdapter.mMasterSpecificSlaveAndFreeDeviceMap.get(currentIpAddress);
            if (mGetCurrentSceneObject!=null)
                mGetCurrentSceneObject.setCurrentSceneObject(mCurrentSceneObject);
            if (getActivity() != null) {
                setViews();
            }
            if (mCurrentSceneObject == null) {
                Log.d("NetworkChanged", "NowPlayingFragment onCreateView");
                Toast.makeText(getActivity(), getString(R.string.deviceNotFound), Toast.LENGTH_SHORT).show();
            } else if (mCurrentSceneObject.getCurrentSource() == DMR_SOURCE) {
                /* Check if the playback is through DMR and it is */
                isLocalDMRPlayback = true;
            }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity)
            mActivity = (Activity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.cumulations_now_playing_fragment_v2, container, false);
        loadingBar = (ImageView) v.findViewById(R.id.loading);
//        loadingText = (TextView)v.findViewById(R.id.loadingText);
//        loadingText.setText("");
        Bundle arguments = getArguments();
        mCurrentIpAddress = arguments.getString("scene_IP");
        shouldShowViewsWithoutAnimation = arguments.getBoolean("shouldShowViewsWithoutAnimation");
        initViews(v);
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
            animateViews();
        }
        if (shouldShowViewsWithoutAnimation) {
            //mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
            //initViews();
            showViews();
//            setCurrentSceneObject();
        }
        setCurrentSceneObject();
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void animateViews() {
        Transition sharedElementEnterTransition = getActivity().getWindow().getSharedElementEnterTransition();
        sharedElementEnterTransition.addListener(new Transition.TransitionListener() {

            @Override
            public void onTransitionStart(Transition transition) {
                setCurrentSceneObject();
            }

            @Override
            public void onTransitionEnd(Transition transition) {
                showViews();
                if (getActivity() != null) {
                    Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in);
                    //Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_anim);
                    mAppCompatSeekBar.startAnimation(fadeInAnimation);
                    mArtistName.startAnimation(fadeInAnimation);
                    mSongName.startAnimation(fadeInAnimation);
                    mSongStopTime.startAnimation(fadeInAnimation);
                    mSongStartTime.startAnimation(fadeInAnimation);
                    mPlaySong.startAnimation(fadeInAnimation);
                    mPreviousSong.startAnimation(fadeInAnimation);
                    mNextSong.startAnimation(fadeInAnimation);
                    //mShuffle.startAnimation(fadeInAnimation);
                    //mRepeat.startAnimation(fadeInAnimation);
                    mVolume.startAnimation(fadeInAnimation);
                    //mZoneSpinner.startAnimation(fadeInAnimation);
                    mAppCompatSeekBar.startAnimation(fadeInAnimation);
                }
            }

            @Override
            public void onTransitionCancel(Transition transition) {
                Log.d("TRANSITION", "Transition cancelled");
            }

            @Override
            public void onTransitionPause(Transition transition) {
                Log.d("TRANSITION", "Transition paused");
            }

            @Override
            public void onTransitionResume(Transition transition) {

            }
        });
    }

    public DMRProcessor getTheRenderer(String ipAddress) {
        RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(ipAddress);
        if (renderingDevice != null) {
            String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
            PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
            if (playbackHelper != null) {
                LibreApplication.PLAYBACK_HELPER_MAP.put(renderingUDN, playbackHelper);

                DMRControlHelper m_dmrControlHelper = playbackHelper.getDmrHelper();
                if (m_dmrControlHelper != null) {
                    DMRProcessor m_dmrProcessor = m_dmrControlHelper.getDmrProcessor();
                    m_dmrProcessor.removeListener(this);
                    m_dmrProcessor.addListener(this);
                    return m_dmrProcessor;
                } else
                    return null;
            } else
                return null;
        } else
            return null;
    }


    boolean doPlayPause(boolean isCurrentlyPlaying) {

        if (handleIfSpeakerIsInNoSourceMode()) {
            return false;
        }
        if (isLocalDMRPlayback) {
            RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentIpAddress);
            if (renderingDevice != null) {
                String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);


                try {
                    if ( playbackHelper==null|| playbackHelper.getDmsHelper() == null||( mCurrentSceneObject!=null && false==mCurrentSceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP))) {
                        Toast.makeText(getActivity(), getString(R.string.no_active_playlist_select), Toast.LENGTH_SHORT).show();


                        /* In SA mode we need to go to local content while in HN mode we will go to sources option */

                        if (LibreApplication.activeSSID.equalsIgnoreCase(Constants.DDMS_SSID)) {
                            Intent localIntent = new Intent(getActivity(), LocalDMSActivity.class);
                            localIntent.putExtra("isLocalDeviceSelected", true);
                            localIntent.putExtra("current_ipaddress", mCurrentIpAddress);
                            startActivity(localIntent);
                        } else {
                            Intent localIntent = new Intent(getActivity(), CumulationsSourcesOptionActivity.class);
                            localIntent.putExtra("current_ipaddress", mCurrentIpAddress);
                            localIntent.putExtra("current_source", "" + mCurrentSceneObject.getCurrentSource());
                            localIntent.putExtra(Constants.FROM_ACTIVITY, "NowPlayingFragment");
                            startActivity(localIntent);
                        }
                        getActivity().finish();

                        return false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                DMRProcessor theRenderer = getTheRenderer(mCurrentIpAddress);
                if (theRenderer == null)
                    return false;
                if (isCurrentlyPlaying)
                    theRenderer.pause();
                else
                    theRenderer.play();
                return true;

            }
            else if ( mCurrentSceneObject.getPlayUrl()!=null && mCurrentSceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP)){
                        /* renderer is not found and hence there is no playlist also so open the sources option */


                /* In SA mode we need to go to local content while in HN mode we will go to sources option */
                if (LibreApplication.activeSSID.equalsIgnoreCase(Constants.DDMS_SSID)) {
                    Intent localIntent = new Intent(getActivity(), LocalDMSActivity.class);
                    localIntent.putExtra("isLocalDeviceSelected", true);
                    localIntent.putExtra("current_ipaddress", mCurrentSceneObject.getIpAddress());
                    startActivity(localIntent);
                } else {
                    Intent localIntent = new Intent(getActivity(), CumulationsSourcesOptionActivity.class);
                    localIntent.putExtra("current_ipaddress", mCurrentSceneObject.getIpAddress());
                    localIntent.putExtra("current_source", "" + mCurrentSceneObject.getCurrentSource());
                    startActivity(localIntent);
                }
                return true;
            }

        } else {

            LUCIControl control = new LUCIControl(mCurrentSceneObject.getIpAddress());
            if (isCurrentlyPlaying)
                control.SendCommand(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.PAUSE, CommandType.SET);
            else {
                if(mCurrentSceneObject.getCurrentSource() == 19){
                    control.SendCommand(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.PLAY, CommandType.SET);
                }else
                    control.SendCommand(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.RESUME, CommandType.SET);
            }
        }
        if(mCurrentSceneObject.getCurrentSource()!=Constants.BT_SOURCE
                && mCurrentSceneObject.getCurrentSource()!= Constants.AUX_SOURCE
                && mCurrentSceneObject.getCurrentSource()!= 15
                && mCurrentSceneObject.getCurrentSource()!= Constants.GCAST_SOURCE) {
            showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_TIMEOUT_CONST, Constants.PREPARATION_TIMEOUT);
            showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_INITIATED);
            Log.d("showingLoader","doPlayPause, isCurrentlyPlaying = "+isCurrentlyPlaying);
        }
        return true;
    }


    private boolean handleIfSpeakerIsInNoSourceMode() {
        //Handling the No Source
        if(mCurrentSceneObject!=null && mCurrentSceneObject.getCurrentSource()==0||mCurrentSceneObject.getCurrentSource()==17){

            Toast.makeText(getActivity(), getString(R.string.no_active_playlist_select), Toast.LENGTH_SHORT).show();

/* In SA mode we need to go to local content while in HN mode we will go to sources option */
            if (LibreApplication.activeSSID.contains(Constants.DDMS_SSID)){
                Intent localIntent = new Intent(getActivity(), LocalDMSActivity.class);
                localIntent.putExtra("isLocalDeviceSelected", true);
                localIntent.putExtra("current_ipaddress", mCurrentIpAddress);
                startActivity(localIntent);
            }else {
                Intent localIntent = new Intent(getActivity(), CumulationsSourcesOptionActivity.class);
                localIntent.putExtra("current_ipaddress", mCurrentIpAddress);
                localIntent.putExtra("current_source", "" + mCurrentSceneObject.getCurrentSource());
                localIntent.putExtra(Constants.FROM_ACTIVITY, "NowPlayingFragment");
                startActivity(localIntent);
            }
            getActivity().finish();

            return true;
        }
        return false;
    }


    boolean doNextPrevious(boolean isNextPressed) {
        if (handleIfSpeakerIsInNoSourceMode()) {
            return false;
        }
        if (isLocalDMRPlayback && ( mCurrentSceneObject!=null && (mCurrentSceneObject.getCurrentSource()==0||mCurrentSceneObject.getCurrentSource()==2) )) {
            RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentIpAddress);
            if (renderingDevice != null) {
                String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);

                if ( playbackHelper==null|| playbackHelper.getDmsHelper() == null||(mCurrentSceneObject!=null && false==mCurrentSceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP)) ) {
                    Toast.makeText(getActivity(), getString(R.string.no_active_playlist_select), Toast.LENGTH_SHORT).show();

                    /* In SA mode we need to go to local content while in HN mode we will go to sources option */
                    if (LibreApplication.activeSSID.contains(Constants.DDMS_SSID)){
                        Intent localIntent = new Intent(getActivity(), LocalDMSActivity.class);
                        localIntent.putExtra("isLocalDeviceSelected", true);
                        localIntent.putExtra("current_ipaddress", mCurrentIpAddress);
                        startActivity(localIntent);
                    }else {
                        Intent localIntent = new Intent(getActivity(), CumulationsSourcesOptionActivity.class);
                        localIntent.putExtra("current_ipaddress", mCurrentIpAddress);
                        localIntent.putExtra("current_source", "" + mCurrentSceneObject.getCurrentSource());
                        localIntent.putExtra(Constants.FROM_ACTIVITY, "NowPlayingFragment");
                        startActivity(localIntent);
                    }
                    getActivity().finish();

                    return false;
                }

                showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_TIMEOUT_CONST, Constants.PREPARATION_TIMEOUT);
                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_INITIATED);
                Log.d("showingLoader","doNextPrevious, isNextPressed = "+isNextPressed);


                if ((mCurrentSceneObject.getShuffleState() == 0) && (mCurrentSceneObject.getRepeatState() == REPEAT_OFF)) {

                    if (isNextPressed) {
                        if (playbackHelper.isThisTheLastSong() || playbackHelper.isThisOnlySong()) {
                            Toast.makeText(getActivity(), getString(R.string.lastSongPlayed), Toast.LENGTH_LONG).show();
                            showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                            return false;
                        }
                    } else {
                        if (playbackHelper.isThisFirstSong() || playbackHelper.isThisOnlySong()) {
                            Toast.makeText(getActivity(), getString(R.string.onlyOneSong), Toast.LENGTH_LONG).show();
                            showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                            return false;
                        }
                    }
                }


                if (isNextPressed) {
                    playbackHelper.playNextSong(1);
                } else {
                     /* Setting the current seekbar progress -Start*/
                    float  duration = mCurrentSceneObject.getCurrentPlaybackSeekPosition();
                    Log.d("Current Duration ", "Duration = " + duration / 1000);
                    float durationInSeeconds = duration/1000;
                    if(durationInSeeconds <5) {
                        playbackHelper.playNextSong(-1);
                    }else{
                        playbackHelper.playNextSong(0);
                    }
                }
                return true;


            } else
                return false;
        } else {



            if (mCurrentSceneObject!=null && mCurrentSceneObject.getCurrentSource()==21){

                String trackname=mCurrentSceneObject.getTrackName();

                if (isNextPressed){

                    if (trackname!=null&&trackname.contains(Constants.DEZER_SONGSKIP))
                    {

                        //closeLoader();
                        Toast.makeText(getActivity(),"Activate Deezer Premium+ from your computer",Toast.LENGTH_LONG).show();
                        return false;
                    }

                }
            }

            if(mCurrentSceneObject.getCurrentSource()!=Constants.BT_SOURCE
                    && mCurrentSceneObject.getCurrentSource()!= Constants.AUX_SOURCE
                    && mCurrentSceneObject.getCurrentSource()!= 15
                    && mCurrentSceneObject.getCurrentSource()!= Constants.GCAST_SOURCE) {
                showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_TIMEOUT_CONST, Constants.PREPARATION_TIMEOUT);
                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_INITIATED);
                Log.d("showingLoader","doNextPrevious, isNextPressed = "+isNextPressed);
            }
            LUCIControl control = new LUCIControl(mCurrentSceneObject.getIpAddress());
            if (isNextPressed)
                control.SendCommand(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.PLAY_NEXT, CommandType.SET);
            else
                control.SendCommand(MIDCONST.MID_PLAYCONTROL, LUCIMESSAGES.PLAY_PREV, CommandType.SET);
            return true;
        }
    }


    boolean doNextPrevious(boolean isNextPressed,boolean mValue) {


        if (isLocalDMRPlayback && (mCurrentSceneObject != null && (mCurrentSceneObject.getCurrentSource() == 0 || mCurrentSceneObject.getCurrentSource() == 2))) {
            RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentIpAddress);
            if (renderingDevice != null) {
                String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);

                if (playbackHelper == null || playbackHelper.getDmsHelper() == null || (mCurrentSceneObject != null && false == mCurrentSceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP))) {
                    if (mValue)
                        return true;
                    Toast.makeText(getActivity(), getString(R.string.no_active_playlist_select), Toast.LENGTH_SHORT).show();

                    /* In SA mode we need to go to local content while in HN mode we will go to sources option */
                    if (LibreApplication.activeSSID.contains(Constants.DDMS_SSID)) {
                        Intent localIntent = new Intent(getActivity(), LocalDMSActivity.class);
                        localIntent.putExtra("isLocalDeviceSelected", true);
                        localIntent.putExtra("current_ipaddress", mCurrentIpAddress);
                        startActivity(localIntent);
                    } else {
                        Intent localIntent = new Intent(getActivity(), CumulationsSourcesOptionActivity.class);
                        localIntent.putExtra("current_ipaddress", mCurrentIpAddress);
                        localIntent.putExtra("current_source", "" + mCurrentSceneObject.getCurrentSource());
                        localIntent.putExtra(Constants.FROM_ACTIVITY, "CumulationsNowPlayingActivity");
                        startActivity(localIntent);
                    }
                    getActivity().finish();

                    return false;
                }

                showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_TIMEOUT_CONST, Constants.PREPARATION_TIMEOUT);
                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_INITIATED);
                Log.d("showingLoader","doNextPrevious, isNextPressed = "+isNextPressed+", mValue = "+mValue);


                if (isNextPressed) {
                    playbackHelper.playNextSong(1);
                } else {
                   /* Setting the current seekbar progress -Start*/
                    float  duration = mCurrentSceneObject.getCurrentPlaybackSeekPosition();
                    Log.d("Current Duration ", "Duration = " + duration / 1000);
                    float durationInSeeconds = duration/1000;
                    if(durationInSeeconds <5) {
                        playbackHelper.playNextSong(-1);
                    }else{
                        playbackHelper.playNextSong(0);
                    }
                }
                return true;


            } else
                return false;
        }
        return true;
    }


    boolean doVolumeChange(int currentVolumePosition) {

//        if (isLoclaDMRPlayback) {
//
//            DMRProcessor theRenderer = getTheRenderer(currentIpAddress);
//
//            if (theRenderer == null)
//                return false;
//
//            theRenderer.setVolume(currentVolumePosition);
//            return true;
//        }
//        else {
        /* We can make use of CurrentIpAddress instead of CurrenScneObject.getIpAddress*/
        LUCIControl control = new LUCIControl(mCurrentIpAddress);
        if(mCurrentSceneObject.getCurrentSource() != Constants.AIRPLAY_SOURCE &&
                mCurrentSceneObject.getCurrentSource() != Constants.SPOTIFY_SOURCE) {
            //control.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + currentVolumePosition, CommandType.SET);
            control.SendCommand(MIDCONST.ZONE_VOLUME, "" + currentVolumePosition, CommandType.SET);
        }else{
            control.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + currentVolumePosition, CommandType.SET);
        }
        SceneObject sceneObjectFromCentralRepo = mScanHandler.getSceneObjectFromCentralRepo(mCurrentIpAddress);
        //currentSceneObject.setVolumeValueInPercentage(currentVolumePosition);
        mCurrentSceneObject.setvolumeZoneInPercentage(currentVolumePosition);
        if (sceneObjectFromCentralRepo != null) {
            //sceneObjectFromCentralRepo.setVolumeValueInPercentage(currentVolumePosition);
            sceneObjectFromCentralRepo.setvolumeZoneInPercentage(currentVolumePosition);
            mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, sceneObjectFromCentralRepo);
        }
        return true;
//        }


    }


    Runnable volumeAnimationRunnable = new Runnable() {
        @Override
        public void run() {
            if (getActivity() != null) {
                isVolumeVisible = false;
                mVolumeText.setVisibility(View.GONE);
                animatemSeekBar(0, false);
                Animation fadeOutAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_out);
                fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mFrameLayoutForVolume.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                mFrameLayoutForVolume.startAnimation(fadeOutAnimation);
            }
        }
    };



    /*private BehaviorSubject<Integer> volumeObservable(int keyCode) {
        return volumeLogic(volume, keyCode);
    }

    private BehaviorSubject<Integer> volumeLogic(BehaviorSubject<Integer> volume, int keyCode) {
        volume.onNext(mSeekBar.getProgress());
        return volume;

    }*/


    /*this method will get called whenever hardware volume is pressed*/
    @Override
    public void updateVolumeChangesFromHardwareKey(final int keyCode) {

        /*if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            mSeekBar.setProgress(mSeekBar.getProgress() + 5);
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            mSeekBar.setProgress(mSeekBar.getProgress() - 5);
        }
        mVolumeText.setText(String.valueOf(mSeekBar.getProgress()));*/
         /* Added to avoid user pressing the volume change when user is getting call*/
        if (((CumulationsNowPlayingActivity)getActivity()).isPhoneCallbeingReceived)
            return;

        if (mCurrentSceneObject == null) {
            Log.d("VolumeHardKey", "Current Scene is--" + null);
            return;
        }
        if (mCurrentSceneObject!=null && mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE){
            LibreLogger.d(this,"source is cast. So, dont update volume");
            if (castToast != null) {
                castToast.cancel();
            }
            castToast = Toast.makeText(getActivity(), getString(R.string.please_change_the_volume_through_cast_source), Toast.LENGTH_SHORT);
            castToast.show();
            return;
        }

        Log.d("VolumeHardKey", "Device ip is--" + mCurrentSceneObject.getIpAddress());

        //volumeControl = new LUCIControl(mCurrentSceneObject.getIpAddress());

        /*if (!isVolumeVisible) {
            isVolumeVisible = true;
            mFrameLayoutForVolume.setVisibility(View.VISIBLE);
            Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in);
            mFrameLayoutForVolume.startAnimation(fadeInAnimation);
            fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mVolume.setVisibility(View.VISIBLE);
                    //mVolume.setImageResource(R.drawable.ic_close_white_24dp);
                    changeVolumeBackgroundColor(true, true);
                    animatemSeekBar(380, true);
                    handler.removeCallbacks(volumeAnimationRunnable);
                    handler.postDelayed(volumeAnimationRunnable, 3000);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
        handler.removeCallbacks(volumeAnimationRunnable);
        handler.postDelayed(volumeAnimationRunnable, 3000);
*/

        /*volumeObservable(keyCode)
                .debounce(1000, TimeUnit.MILLISECONDS)
                .observeOn(Schedulers.single())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Integer integer) {
                        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                            Log.d("VOLUMELIBRE", "INSIDE ONNEXT VOLUME UP!!!!!!!!!!");
                            *//*if (integer > 85) {
                                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + 100, CommandType.SET);
                            } else {*//*
                                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + (mSeekBar.getProgress()), CommandType.SET);
                            //}
                        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                            Log.d("VOLUMELIBRE", "INSIDE ONNEXT VOLUME DOWN!!!!!!!!!!");
                            *//*if (mSeekBar.getProgress() < 15) {
                                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + 0, CommandType.SET);
                            } else {*//*
                                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + (mSeekBar.getProgress()), CommandType.SET);
                            //}
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
*/
        /* Added to avoid user pressing the volume change when user is getting call*/
        if (((CumulationsNowPlayingActivity)getActivity()).isPhoneCallbeingReceived)
            return;

        if (mCurrentSceneObject == null) {
            Log.d("VolumeHardKey", "Current Scene is--" + null);
            return;
        }
        if (mCurrentSceneObject!=null && mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE){
            LibreLogger.d(this,"source is cast. So, dont update volume");
            return;
        }

        Log.d("VolumeHardKey", "Device ip is--" + mCurrentSceneObject.getIpAddress());

        LUCIControl volumeControl = new LUCIControl(mCurrentSceneObject.getIpAddress());

        if (!isVolumeVisible) {
            isVolumeVisible = true;
            mFrameLayoutForVolume.setVisibility(View.VISIBLE);
            Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in);
            mFrameLayoutForVolume.startAnimation(fadeInAnimation);
            fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mVolume.setVisibility(View.VISIBLE);
                    //mVolume.setImageResource(R.drawable.ic_close_white_24dp);
                    changeVolumeBackgroundColor(true, true);
                    animatemSeekBar(380, true);
                    handler.removeCallbacks(volumeAnimationRunnable);
                    handler.postDelayed(volumeAnimationRunnable, 3000);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
        handler.removeCallbacks(volumeAnimationRunnable);
        handler.postDelayed(volumeAnimationRunnable, 3000);

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {

            if (mSeekBar.getProgress() > 85) {
                //  volumeControl.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + 100, CommandType.SET);
                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + 100, CommandType.SET);
            } else {
                //volumeControl.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + (volumeBar.getProgress() + 10), CommandType.SET);
                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + (mSeekBar.getProgress() + 3), CommandType.SET);
            }
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            if (mSeekBar.getProgress() < 15) {
                //volumeControl.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + 0, CommandType.SET);
                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + 0, CommandType.SET);
            } else {
                //volumeControl.SendCommand(MIDCONST.VOLUEM_CONTROL, "" + (volumeBar.getProgress() - 10), CommandType.SET);
                volumeControl.SendCommand(MIDCONST.ZONE_VOLUME, "" + (mSeekBar.getProgress() - 3), CommandType.SET);
            }


        }

    }


    /*Setting views here*/
    private void setViews() {

        if (mCurrentSceneObject == null) {
            return;
        }

        getActivity().invalidateOptionsMenu();
        /*making visibility gone*/
        mShuffle.setVisibility(View.GONE);
        mRepeat.setVisibility(View.GONE);

        if (LibreApplication.ZONE_VOLUME_MAP.containsKey(mCurrentSceneObject.getIpAddress())) {
            mSeekBar.setProgress(LibreApplication.ZONE_VOLUME_MAP.get(mCurrentSceneObject.getIpAddress()));
            mVolumeText.setText(String.valueOf(LibreApplication.ZONE_VOLUME_MAP.get(mCurrentSceneObject.getIpAddress())));
        } else {
            LUCIControl control = new LUCIControl(mCurrentSceneObject.getIpAddress());
            control.SendCommand(MIDCONST.ZONE_VOLUME, null, LSSDPCONST.LUCI_GET);
            if (mCurrentSceneObject.getvolumeZoneInPercentage() >= 0) {
                mSeekBar.setProgress(mCurrentSceneObject.getvolumeZoneInPercentage());
                mVolumeText.setText(String.valueOf(mCurrentSceneObject.getvolumeZoneInPercentage()));
            }
        }

        if (mCurrentSceneObject.getCurrentSource() == Constants.AIRPLAY_SOURCE ||
                mCurrentSceneObject.getCurrentSource() == Constants.SPOTIFY_SOURCE) {
            mSeekBar.setProgress(mCurrentSceneObject.getVolumeValueInPercentage());
            mVolumeText.setText(String.valueOf(mCurrentSceneObject.getVolumeValueInPercentage()));
        }
        if (mCurrentSceneObject.getArtist_name() != null && !mCurrentSceneObject.getArtist_name().equalsIgnoreCase("NULL")) {
            mArtistName.setText(mCurrentSceneObject.getArtist_name());
            LibreLogger.d(this, "" + mCurrentSceneObject.getArtist_name());
        } else {
            mArtistName.setText("");
        }
        if (mCurrentSceneObject.getAlbum_name() != null && !mCurrentSceneObject.getAlbum_name().equalsIgnoreCase("NULL")) {
            mSongName.setText(mCurrentSceneObject.getAlbum_name());
            LibreLogger.d(this, "" + mCurrentSceneObject.getAlbum_name());
        } else {
            mSongName.setText("");
        }
        if (mCurrentSceneObject.getTrackName() != null && !mCurrentSceneObject.getTrackName().equalsIgnoreCase("NULL")
                && !mCurrentSceneObject.getTrackName().isEmpty()) {
            String trackname = mCurrentSceneObject.getTrackName();

            /* This change is done to handle the case of deezer where the song name is appended by radio or skip enabled */
            if (trackname != null && trackname.contains(Constants.DEZER_RADIO))
                trackname = trackname.replace(Constants.DEZER_RADIO, "");

            if (trackname != null && trackname.contains(Constants.DEZER_SONGSKIP))
                trackname = trackname.replace(Constants.DEZER_SONGSKIP, "");

            mSongName.setText(trackname);


            LibreLogger.d(this, "" + mCurrentSceneObject.getTrackName());
        } else {
            //songName.setText("");
        }

        /*this condition making sure that shuffle and repeat is being shown only for USB/SD card*/
        /*added for deezer/tidal source*/
        if (mCurrentSceneObject.getCurrentSource() == USB_SOURCE
                || mCurrentSceneObject.getCurrentSource() == SD_CARD
                || mCurrentSceneObject.getCurrentSource() == DEEZER_SOURCE
                || mCurrentSceneObject.getCurrentSource() == TIDAL_SOURCE
                || mCurrentSceneObject.getCurrentSource() == Constants.SPOTIFY_SOURCE
                || mCurrentSceneObject.getCurrentSource() == NETWORK_DEVICES
                || mCurrentSceneObject.getCurrentSource() == FAV_SOURCE
                || mCurrentSceneObject.getCurrentSource() == DMR_SOURCE
                ) {

            /*making visibile only for USB/SD card*/
            mShuffle.setVisibility(View.VISIBLE);
            mRepeat.setVisibility(View.VISIBLE);
            if (getActivity() != null) {
                Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in_with_extra_time);
                //mShuffle.startAnimation(fadeInAnimation);
                //mRepeat.startAnimation(fadeInAnimation);
            }
           /* mShuffle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mShuffleClicked = !mShuffleClicked;
                    if (mShuffleClicked) {
                        mShuffle.setImageResource(R.drawable.ic_shuffle_black_24dp);
                        //mShuffle.setColorFilter(vibrant);
                    } else {
                        mShuffle.setImageResource(R.drawable.ic_shuffle_black_24dp_less_opacity);
                        //mShuffle.setColorFilter(Color.parseColor("#FFFFFFFF"));
                    }
                }
            });*/

            /*mRepeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mRepeatClicked = !mRepeatClicked;
                    if (mRepeatClicked) {
                        mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp);
                        //mRepeat.setColorFilter(vibrant);
                    } else {
                        mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp_less_opacity);
                        //mRepeat.setColorFilter(Color.parseColor("#FFFFFFFF"));
                    }
                }
            });*/

            if (mCurrentSceneObject.getShuffleState() == 0) {
            /*which means shuffle is off*/
                mShuffle.setImageResource(R.drawable.ic_shuffle_black_24dp_less_opacity);
            } else {
            /*shuffle is on */
                mShuffle.setImageResource(R.drawable.ic_shuffle_black_24dp);

            }

            /*if other is playing local DMR we should hide repeat and shuffle*/
            if ( mCurrentSceneObject.getPlayUrl()!=null && !mCurrentSceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP)&& (mCurrentSceneObject.getCurrentSource()==DMR_SOURCE)) {
                mShuffle.setVisibility(View.GONE);
                mRepeat.setVisibility(View.GONE);
            }

            switch (mCurrentSceneObject.getRepeatState()) {
                case REPEAT_OFF:
                    mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp_less_opacity);
                    break;
                case REPEAT_ONE:
                    mRepeat.setImageResource(R.drawable.ic_repeat_one_white_24dp);
                    break;
                case REPEAT_ALL:
                    mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp);
                    break;
            }
        }


        //sceneName.setText("" + currentSceneObject.getSceneName());
        if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
            if (shudUpdateIcons(mCurrentIpAddress, mCurrentSceneObject)) {
                //mPlaySong.setText("{fa-pause}");
                /*mPlaySong.setImageResource(R.mipmap.pause_with_glow);
                Log.e("sumacheck","playcheck1");
                mNextSong.setImageResource(R.mipmap.next_with_glow);
                mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*/
                mPlaySong.setImageResource(R.drawable.nowplaying_pause);
                mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                resumeEqualizer();
            }
        } else {
            if (shudUpdateIcons(mCurrentIpAddress, mCurrentSceneObject)) {
                //mPlaySong.setText("{fa-play}");
                /*mPlaySong.setImageResource(R.mipmap.play_with_gl);
                mNextSong.setImageResource(R.mipmap.next_without_glow);
                mPreviousSong.setImageResource(R.mipmap.prev_without_glow);*/
                mPlaySong.setImageResource(R.drawable.nowplaying_play);
                mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                stopEqualizer();
            }
        }
        if (mCurrentSceneObject.getCurrentSource() == MIDCONST.GCAST_SOURCE) {
            //mPlaySong.setText("{fa-pause}");
            /*mPlaySong.setImageResource(R.mipmap.pause_with_glow);
            mNextSong.setImageResource(R.mipmap.next_without_glow);
            mPreviousSong.setImageResource(R.mipmap.prev_without_glow);*/
            mPlaySong.setImageResource(R.drawable.nowplaying_play);
            mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
            mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
            viewsWithLessOpacity();
            //resumeEqualizer();
        }
        /* Setting the current seekbar progress -Start*/
        float duration = mCurrentSceneObject.getCurrentPlaybackSeekPosition();
        mAppCompatSeekBar.setMax((int) mCurrentSceneObject.getTotalTimeOfTheTrack() / 1000);
        Log.d("SEEK", "Duration = " + duration / 1000);
        Log.d("second","progr = "+mAppCompatSeekBar.getSecondaryProgress());
        mAppCompatSeekBar.setProgress((int) duration / 1000);

        DecimalFormat twoDForm = new DecimalFormat("#.##");
        mSongStartTime.setText(convertMilisecondsToTimeString((int) duration / 1000));
        mSongStopTime.setText(convertMilisecondsToTimeString(mCurrentSceneObject.getTotalTimeOfTheTrack() / 1000));
        /* Setting the current seekbar progress -END*/


        if (mCurrentIpAddress != null && mCurrentSceneObject.getTrackName() != null
                && (!currentTrackName.equalsIgnoreCase(mCurrentSceneObject.getTrackName()))) {

            currentTrackName = mCurrentSceneObject.getTrackName();
            updateAlbumArt(mCurrentSceneObject);

        }

        if (mCurrentSceneObject == null) {
            LSSDPNodes mNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(mCurrentIpAddress);
            if (mNode != null) {
                updateAlbumArt(mNode);
            }
        }
        setTheSourceIconFromCurrentSceneObject();

        if (mCurrentSceneObject.getCurrentSource() == DEEZER_SOURCE
                || mCurrentSceneObject.getCurrentSource() == TIDAL_SOURCE
                || mCurrentSceneObject.getCurrentSource() == VTUNER_SOURCE
                || mCurrentSceneObject.getCurrentSource() == TUNEIN_SOURCE
                || mCurrentSceneObject.getCurrentSource() == FAV_SOURCE) {
            favButton.setVisibility(View.VISIBLE);
            if(mCurrentSceneObject.isFavourite())
                favButton.setImageResource(R.drawable.ic_favorite_blue_48dp);
            else
                favButton.setImageResource(R.drawable.ic_favorite_border_blue_48dp);
        }
    }


    public void enableViews() {
        mAppCompatSeekBar.setEnabled(true);
        mAppCompatSeekBar.setClickable(true);
        mPlaySong.setClickable(true);
        mPlaySong.setEnabled(true);
        // //volumeBar.setClickable(false);
        // volumeBar.setEnabled(false);
        //mutebutton.setClickable(true);
        //mutebutton.setEnabled(true);
        mPreviousSong.setClickable(true);
        mPreviousSong.setEnabled(true);
        mNextSong.setEnabled(true);
        mNextSong.setClickable(true);
        //cast_layout.setVisibility(View.GONE);
        //mVolumeSeekBar.setEnabled(true);
        //mVolumeSeekBar.setClickable(true);
        viewsWithFullOpacity();


    }

    /* This function takes care of setting the image next to sources button depending on the
current source that is being played.
*/
    private void setTheSourceIconFromCurrentSceneObject() {

        //if (mSources != null) {

            /* Enabling the views by default which gets updated to disabled based on the need */

            enableViews();

            Drawable img = getActivity().getResources().getDrawable(
                    R.mipmap.ic_sources);
            if (mCurrentSceneObject == null)
                return;
            /*if (mCurrentSceneObject.getCurrentSource() == WiFi_For_Gear4) {
                // This is done because when they switch between BT / AUX to WiFi mode which is the normal mode. We recieve source as 0
                //do nothing because we have used enableviews() which does the work
                disableViews(mCurrentSceneObject.getCurrentSource(), "WiFi mode");
                LibreLogger.d("this", " Gear4 Recieved sources to be " + mCurrentSceneObject.getCurrentSource() + " - returned to WiFi mode by hardkey press");
            }*/
            if (mCurrentSceneObject.getCurrentSource() == 1) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.airplay_logo);
                disableViews(mCurrentSceneObject.getCurrentSource(), "AirPlay Mode");
            }

            if (mCurrentSceneObject.getCurrentSource() == DMR_SOURCE) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.dmr_icon);
            }
            if (mCurrentSceneObject.getCurrentSource() == 3) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.network);
            }

            if (mCurrentSceneObject.getCurrentSource() == 4) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.spotify);
            }
            if (mCurrentSceneObject.getCurrentSource() == 5) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.usb);
            }
            if (mCurrentSceneObject.getCurrentSource() == 6) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.sdcard);
            }
            if (mCurrentSceneObject.getCurrentSource() == VTUNER_SOURCE) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.vtuner_logo);

                /*disabling views for VTUNER*/
                disableViews(mCurrentSceneObject.getCurrentSource(), mCurrentSceneObject.getAlbum_name());
                //favButton.setVisibility(View.VISIBLE);

            }
            if (mCurrentSceneObject.getCurrentSource() == TUNEIN_SOURCE) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.tunein_logo1);

                /*disabling views for TUNEIN*/
                disableViews(mCurrentSceneObject.getCurrentSource(), mCurrentSceneObject.getAlbum_name());
                //favButton.setVisibility(View.VISIBLE);

            }
            if (mCurrentSceneObject.getCurrentSource() == 14) { // Mail
                img = getActivity().getResources().getDrawable(
                        R.mipmap.aux_in);
                /* added to make sure we dont show the album art during aux */


                disableViews(mCurrentSceneObject.getCurrentSource(), getString(R.string.aux));
            }
            if (mCurrentSceneObject.getCurrentSource() == 18) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.qqmusic);
                disableViews(mCurrentSceneObject.getCurrentSource(), "");

            }
            if (mCurrentSceneObject.getCurrentSource() == 19) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.bluetooth);

                if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED) {
                    disableViews(mCurrentSceneObject.getCurrentSource(), getString(R.string.btOn));
                } else if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PAUSED) {
                    disableViews(mCurrentSceneObject.getCurrentSource(), getString(R.string.btOn));
                } else {
                    disableViews(mCurrentSceneObject.getCurrentSource(), getString(R.string.btOn));

                }


            }
            if (mCurrentSceneObject.getCurrentSource() == DEEZER_SOURCE) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.deezer_logo);
                //favButton.setVisibility(View.VISIBLE);
                disableViews(mCurrentSceneObject.getCurrentSource(), mCurrentSceneObject.getTrackName());


            }
            if (mCurrentSceneObject.getCurrentSource() == TIDAL_SOURCE) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.tidal_white_logo);
                //favButton.setVisibility(View.VISIBLE);
                disableViews(mCurrentSceneObject.getCurrentSource(),"");
            }

            if (mCurrentSceneObject.getCurrentSource() == FAV_SOURCE) {
                img = getActivity().getResources().getDrawable(
                        R.drawable.ic_favorite_border_blue_48dp);
                disableViews(mCurrentSceneObject.getCurrentSource(),"");
            }

            if (mCurrentSceneObject.getCurrentSource() == 24) {
                img = getActivity().getResources().getDrawable(
                        R.mipmap.ic_cast_white_24dp_2x);

                disableViews(mCurrentSceneObject.getCurrentSource(), "gCast is playing");
            }
        //}
    }



    public void viewsWithLessOpacity() {
        mPlaySong.setAlpha((float) 0.5);
        mPreviousSong.setAlpha((float) 0.5);
        mNextSong.setAlpha((float) 0.5);
    }

    public void viewsWithFullOpacity(){
        mPlaySong.setAlpha((float) 1);
        mPreviousSong.setAlpha((float) 1);
        mNextSong.setAlpha((float) 1);
    }


        public void disableViews(int currentSrc, String message) {

        /* Dont have to call this function explicitly make use of setrceIco */
        favButton.setVisibility(View.GONE);
        switch (currentSrc) {

            case 0: {
                   /*setting seek to zero*/
                mAppCompatSeekBar.setProgress(0);
                mAppCompatSeekBar.setEnabled(false);
                mAppCompatSeekBar.setClickable(false);
                mSongStartTime.setText("0:00");
                mSongStopTime.setText("0:00");

                /* disable previous and next */
  /*              previous.setClickable(false);
                previous.setEnabled(false);
                next.setEnabled(false);
                next.setClickable(false);
*/

                mPlaySong.setClickable(false);
                mPlaySong.setEnabled(false);

                mArtistName.setText("");
                //albumName.setText("");
                mSongName.setText("");
                //favButton.setVisibility(View.GONE);
                LibreLogger.d(this, "Libre - return from WiFi mode through hard key");

                break;
            }

            case 12: {
                   /*setting seek to zero*/
                mAppCompatSeekBar.setProgress(0);
                mAppCompatSeekBar.setEnabled(false);
                mAppCompatSeekBar.setClickable(false);
                mSongStartTime.setText("0:00");
                mSongStopTime.setText("0:00");

                /* disable previous and next */
  /*              previous.setClickable(false);
                previous.setEnabled(false);
                next.setEnabled(false);
                next.setClickable(false);
*/

                mPlaySong.setClickable(false);
                mPlaySong.setEnabled(false);

                mArtistName.setText("");
                //albumName.setText("");
                mSongName.setText("");
                LibreLogger.d(this, "Gear4 - return from WiFi mode through hard key");

                break;
            }

            case 1: {/* For Air Play */
                mAppCompatSeekBar.setEnabled(false);
                mAppCompatSeekBar.setClickable(false);
            }
            break;
            case VTUNER_SOURCE:
                //vtuner
            {
                /*setting seek to zero*/
                mAppCompatSeekBar.setProgress(0);
                mAppCompatSeekBar.setEnabled(false);
                mAppCompatSeekBar.setClickable(false);
                mSongStartTime.setText("0:00");
                mSongStopTime.setText("0:00");

                mShuffle.setVisibility(View.GONE);
                mRepeat.setVisibility(View.GONE);

                /* disable previous and next */
              /*  previous.setClickable(false);
                previous.setEnabled(false);
                next.setEnabled(false);
                next.setClickable(false);

                *//* disable play and pause *//*
                play.setClickable(false);
                play.setEnabled(false);*/
                //mPlaySong.setImageResource(R.drawable.disabled_play);
//                viewsWithLessOpacity();
                favButton.setVisibility(View.VISIBLE);
                mPreviousSong.setAlpha(0.5f);
                mNextSong.setAlpha(0.5f);
                break;

            }

            case TUNEIN_SOURCE:
                //tunein
            {
                /*setting seek to zero*/
                mAppCompatSeekBar.setProgress(0);
                mAppCompatSeekBar.setEnabled(false);
                mAppCompatSeekBar.setClickable(false);
                mSongStartTime.setText("0:00");
                mSongStopTime.setText("0:00");

                mShuffle.setVisibility(View.GONE);
                mRepeat.setVisibility(View.GONE);
                mPreviousSong.setAlpha(0.5f);
                mNextSong.setAlpha(0.5f);
                /* disable previous and next */
               /* previous.setClickable(false);
                previous.setEnabled(false);
                next.setEnabled(false);
                next.setClickable(false);*/
                //mPlaySong.setImageResource(R.drawable.disabled_play);
//                viewsWithLessOpacity();
                 /* disable play and pause */
//                mPlaySong.setClickable(false);
//                mPlaySong.setEnabled(false);
                //tuneInInfoButton.setEnabled(true);
                //tuneInInfoButton.setClickable(true);
                favButton.setVisibility(View.VISIBLE);
                break;

            }
            case 14:
                //Aux

            {       /*setting seek to zero*/
                mAppCompatSeekBar.setProgress(0);
                mAppCompatSeekBar.setEnabled(false);
                mAppCompatSeekBar.setClickable(false);
                mSongStartTime.setText("0:00");
                mSongStopTime.setText("0:00");
                //favButton.setVisibility(View.GONE);
                /* disable previous and next */
                // previous.setClickable(false);
                /// previous.setEnabled(false);
                //next.setEnabled(false);
                // next.setClickable(false);

                // play.setClickable(false);
                //play.setEnabled(false);
                mPlaySong.setImageResource(R.drawable.nowplaying_play);
                mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                viewsWithLessOpacity();
                mArtistName.setText(message);
                //albumName.setText("");
                mSongName.setText("");
                LibreLogger.d(this, "we set the song name to empty for disabling " + mCurrentSceneObject.getTrackName() + " in disabling view where artist name is " + message);


                break;

            }

            case 18: {   //QQ

                   /*setting seek to zero*/
              /*  seekSongBar.setProgress(0);
                seekSongBar.setEnabled(false);
                seekSongBar.setClickable(false);*/
                /*currentPlayPosition.setText("0:00");
                totalPlayPosition.setText("0:00");*/

                /* disable previous and next */
                //  previous.setClickable(false);
              /*  previous.setEnabled(false);
                next.setEnabled(false);*/
                //  next.setClickable(false);
    /*making it without glow for qqmusic */
                mNextSong.setImageResource(R.mipmap.next_without_glow);
                mPreviousSong.setImageResource(R.mipmap.prev_without_glow);

                break;
            }
            case 19:
                //Bluetooth
            {
                      /*setting seek to zero*/
                mAppCompatSeekBar.setProgress(0);
                mAppCompatSeekBar.setEnabled(false);
                mAppCompatSeekBar.setClickable(false);
                mSongStartTime.setText("0:00");
                mSongStopTime.setText("0:00");
                mArtistName.setText(message);
                //albumName.setText("");
                mSongName.setText("");
                // hide repeat and shuffle
                mRepeat.setVisibility(View.GONE);
                mShuffle.setVisibility(View.GONE);
                //favButton.setVisibility(View.GONE);
                final LSSDPNodes mNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(mCurrentIpAddress);
                if (mNode == null) {
                    return;
                }
                LibreLogger.d(this, "BT controller value in sceneobject " + mNode.getBT_CONTROLLER());
                //    Toast.makeText(getContext(), "BT controller value in sceneobject " + currentSceneObject.getBT_CONTROLLER(), Toast.LENGTH_SHORT);
                if (mNode.getBT_CONTROLLER() == 1 || mNode.getBT_CONTROLLER() == 2 || mNode.getBT_CONTROLLER() == 3) {            // play.setClickable(true);
                    //play.setEnabled(true);
                    //   previous.setClickable(true);
                    //  previous.setEnabled(true);
                    //  next.setEnabled(true);
                    //   next.setClickable(true);
                    //mVolumeSeekBar.setEnabled(true);
                    //mVolumeSeekBar.setClickable(true);
                } else {
                    /* disable previous and next */
                    //   previous.setClickable(false);
                    //  previous.setEnabled(false);
                    //  next.setEnabled(false);
                    // next.setClickable(false);

                    //  play.setClickable(false);
                    //   play.setEnabled(false);
                    mPlaySong.setImageResource(R.drawable.nowplaying_play);
                    mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                    mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                    viewsWithLessOpacity();
                }
                break;

            }

            case Constants.GCAST_SOURCE: {
                //gCast is Playing

                  /*setting seek to zero*/
                mAppCompatSeekBar.setProgress(0);
                mAppCompatSeekBar.setEnabled(false);
                mAppCompatSeekBar.setClickable(false);
                mSongStartTime.setText("0:00");
                mSongStopTime.setText("0:00");

                /* disable previous and next */
                // previous.setClickable(false);
                // previous.setEnabled(false);
                // next.setEnabled(false);
                // next.setEnabled(false);
                //  next.setClickable(false);

                //   play.setClickable(false);
                // play.setEnabled(false);
                mPlaySong.setImageResource(R.drawable.nowplaying_play);
                mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);

                //mVolumeSeekBar.setProgress(0);
                //mVolumeSeekBar.setClickable(false);
                //mVolumeSeekBar.setEnabled(false);
                mArtistName.setText("Casting");
                //albumName.setText("");
                mSongName.setText("");
                //cast_layout.setVisibility(View.VISIBLE);
                /*if (mCurrentSceneObject!=null && mCurrentSceneObject.getGenre()!=null&& !mCurrentSceneObject.getGenre().equalsIgnoreCase("null"))
                    //genreTextField.setText(currentSceneObject.getGenre());
                else
                {
                    //genreTextField.setText("");
                }*/
                break;
            }


            //Deezer
                /*If the current source is deezer, we need to check the trackname.
                if trackname contains :;:radio
                */
            case DEEZER_SOURCE: {

                String trackname = message;
                if (message != null && message.contains(Constants.DEZER_RADIO)) {
                    mPreviousSong.setEnabled(false);
                    mPreviousSong.setClickable(false);
                }
                favButton.setVisibility(View.VISIBLE);
                break;
            }

            case TIDAL_SOURCE :
            case FAV_SOURCE:
                favButton.setVisibility(View.VISIBLE);
                break;

        }
            Log.d("second","progr = "+mAppCompatSeekBar.getSecondaryProgress());


    }


    @Override
     public void onResume() {
        super.onResume();
        registerForDeviceEvents(this);
        /*Praveena*
        /*this should not be commented -else source switching from DMR tro other wont work*/
        isLocalDMRPlayback =false;


        //animateViews();
        if(shouldShowViewsWithoutAnimation)

        {
            Log.d("TRANSITION", "Inside fade in views");
            fadeInViews();
            //setViews();
        }

        setViews();
        sendLuci();

        if(mCurrentSceneObject ==null)
            return;

    /* This is done to make sure we retain the album
     art even when the fragment gets recycled in viewpager */
        String album_url = "";
        updateAlbumArt(mCurrentSceneObject);

    }


    private boolean mInvalidateTheAlbumArt(SceneObject scene,String album_url){
        if( !scene.getmPreviousTrackName().equalsIgnoreCase(scene.getTrackName())) {

            PicassoTrustCertificates.getInstance(getActivity()).invalidate(album_url);
            scene.setmPreviousTrackName(scene.getTrackName());
            SceneObject sceneObjectFromCentralRepo = ScanningHandler.getInstance().getSceneObjectFromCentralRepo(scene.getIpAddress());
            if (sceneObjectFromCentralRepo != null) {
                sceneObjectFromCentralRepo.setmPreviousTrackName(scene.getTrackName());
            }

            return true;
        }
        return false;
    }

    public String convertMilisecondsToTimeString(long time) {

        DecimalFormat twoDForm = new DecimalFormat("#.##");
        int seconds = (int) time % 60;
        int mins = (int) (time / 60) % 60;
        if (seconds < 10)
            return mins + ":0" + seconds;
        return mins + ":" + seconds;
    }

    private boolean shudUpdateIcons(String ipaddress, SceneObject sceneObject){
        LSSDPNodes mNodeWeGotForControl = ScanningHandler.getInstance().getLSSDPNodeFromCentralDB(ipaddress);
        if (sceneObject!=null  && (/*sceneObject.getCurrentSource() == Constants.VTUNER_SOURCE
                || sceneObject.getCurrentSource() == Constants.TUNEIN_SOURCE
                ||*/ ((sceneObject.getCurrentSource() == Constants.BT_SOURCE
                && ((mNodeWeGotForControl.getgCastVerision() == null
                && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                ||(mNodeWeGotForControl.getgCastVerision() != null
                && (mNodeWeGotForControl.getBT_CONTROLLER() < 2)))))
                || sceneObject.getCurrentSource() == Constants.AUX_SOURCE
                || sceneObject.getCurrentSource() == 0)) {
            return false;
        }
        return true;
    }



    public void onStop() {
        super.onStop();
        unRegisterForDeviceEvents();
        showLoaderHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity = null;
//        RefWatcher refWatcher = LibreApplication.getRefWatcher(getActivity());
//        refWatcher.watch(this);
    }

    private void blurImage(Drawable d) {
//        Bitmap blurredBitmap = returnBlurredBitmap(d);
//        if (blurredBitmap != null)
//            mRelativeLayout.setBackground(new BitmapDrawable(getResources(), blurredBitmap));
    }

    private void blurBackground(String url){
        /*Picasso.with(getActivity()).load(url)
                .transform(new BlurTransformation(getActivity(),20)).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mRelativeLayout.setBackground(new BitmapDrawable(getResources(),bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                setDefaultBlurAlbumArt();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                setDefaultBlurAlbumArt();
            }
        });*/
        try {
            if (/*getActivity()*/mActivity!=null && /*getActivity()*/!mActivity.isFinishing()) {
                /*Picasso.with(mActivity).load(url)
                        .transform(new BlurTransformation(getActivity()*//*,20*//*))
                        .placeholder(R.mipmap.album_art_blurred)
                        .error(R.mipmap.album_art_blurred)
//                .centerCrop()
                        .into(blurredBgImage);*/

                PicassoTrustCertificates.getInstance(mActivity).load(url)
                        .transform(new BlurTransformation(getActivity()/*,20*/))
                        .placeholder(R.mipmap.album_art_blurred)
                        .error(R.mipmap.album_art_blurred)
//                .centerCrop()
                        .into(blurredBgImage);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    private void setDefaultBlurAlbumArt() {
        blurredBgImage.setImageResource(R.mipmap.album_art_blurred);
    }

    private void fadeInViews() {
        showViews();
        Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        //mRelativeLayout.startAnimation(fadeInAnimation);
        mAppCompatSeekBar.startAnimation(fadeInAnimation);
        mArtistName.startAnimation(fadeInAnimation);
        mSongName.startAnimation(fadeInAnimation);
        mSongStopTime.startAnimation(fadeInAnimation);
        mSongStartTime.startAnimation(fadeInAnimation);
        mPlaySong.startAnimation(fadeInAnimation);
        mPreviousSong.startAnimation(fadeInAnimation);
        mNextSong.startAnimation(fadeInAnimation);
        //mShuffle.startAnimation(fadeInAnimation);
        //mRepeat.startAnimation(fadeInAnimation);
        mVolume.startAnimation(fadeInAnimation);
        //mZoneSpinner.startAnimation(fadeInAnimation);
        mAppCompatSeekBar.startAnimation(fadeInAnimation);
    }

    private void hideViews() {
        //mProgressBar.setVisibility(View.GONE);
        mArtistName.setVisibility(View.GONE);
        mSongName.setVisibility(View.GONE);
        mSongStopTime.setVisibility(View.GONE);
        mSongStartTime.setVisibility(View.GONE);
        mVolumeText.setVisibility(View.GONE);
        mPlaySong.setVisibility(View.GONE);
        mPreviousSong.setVisibility(View.GONE);
        mNextSong.setVisibility(View.GONE);
        mShuffle.setVisibility(View.GONE);
        mRepeat.setVisibility(View.GONE);
        mVolume.setVisibility(View.GONE);
        //mZoneSpinner.setVisibility(View.GONE);
        mAppCompatSeekBar.setVisibility(View.GONE);
    }

    private void showViews() {
        //mRelativeLayout.setVisibility(View.VISIBLE);
        mArtistName.setVisibility(View.VISIBLE);
        mSongName.setVisibility(View.VISIBLE);
        mSongStopTime.setVisibility(View.VISIBLE);
        mSongStartTime.setVisibility(View.VISIBLE);
        mPlaySong.setVisibility(View.VISIBLE);
        mPreviousSong.setVisibility(View.VISIBLE);
        mNextSong.setVisibility(View.VISIBLE);
        //mVuMeterView.setVisibility(View.VISIBLE);
        mVolume.setVisibility(View.VISIBLE);
        //mZoneSpinner.setVisibility(View.VISIBLE);
        mAppCompatSeekBar.setVisibility(View.VISIBLE);
    }

    private void initViews(View v) {
        mRelativeLayout = (FrameLayout)v.findViewById(R.id.now_playing_fragment_layout);
        //mPlay = (AppCompatImageButton)findViewById(R.id.play_song);
        homeButton = (TextView)v.findViewById(R.id.home);
        mRounderAlbumArt = (CircleImageView)v.findViewById(R.id.rounded_album_art);
        mProgressBar = (ProgressBar)v.findViewById(R.id.loading_progressbar);
        mArtistName = (TextView)v.findViewById(R.id.artist_name);
        mSongName = (TextView)v.findViewById(R.id.song_name);
        mSongName.setSelected(true);
        mSongStartTime = (TextView)v.findViewById(R.id.song_start_time);
        mSongStopTime = (TextView)v.findViewById(R.id.song_stop_time);
        mVolumeText = (TextView)v.findViewById(R.id.volumeText);
        mPlaySong = (ImageButton) v.findViewById(R.id.play_song);
        mPreviousSong = (ImageButton)v.findViewById(R.id.previous_song);
        mNextSong = (ImageButton)v.findViewById(R.id.next_song);
        mShuffle = (ImageButton) v.findViewById(R.id.shufflesong);
        mRepeat = (ImageButton) v.findViewById(R.id.repeatsongnowplaying);
        mToolbar = (Toolbar)v.findViewById(R.id.toolbar);
        mVolume = (ImageButton) v.findViewById(R.id.volume);
        mSeekBar = (AppCompatSeekBar) v.findViewById(R.id.volume_bar);
        mFrameLayoutForVolume = (FrameLayout)v.findViewById(R.id.framelayoutVolume);
        //mZoneSpinner = (AppCompatSpinner) v.findViewById(R.id.zone_spinner);
        mAppCompatSeekBar = (AppCompatSeekBar)v.findViewById(R.id.seekBar);
        //volumeLayout = (RelativeLayout)findViewById(R.id.relative_volume);
        favButton = (ImageView) v.findViewById(R.id.favoriteSong);
        blurredBgImage = (ImageView) v.findViewById(R.id.now_playing_fragment_layout_blurred_image);
        setClickListeners();
    }

    private void setClickListeners() {
        mPlaySong.setOnClickListener(this);
        mNextSong.setOnClickListener(this);
        mPreviousSong.setOnClickListener(this);
        mRounderAlbumArt.setOnClickListener(this);
        mShuffle.setOnClickListener(this);
        mRepeat.setOnClickListener(this);
        mVolume.setOnClickListener(this);
        favButton.setOnClickListener(this);


        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                handler.removeCallbacks(volumeAnimationRunnable);
                handler.postDelayed(volumeAnimationRunnable, 3000);
                if (!doVolumeChange(seekBar.getProgress()))
                    Toast.makeText(getActivity(), getString(R.string.actionFailed), Toast.LENGTH_SHORT).show();


            }
        });

        Log.d("setClickListeners","second progr = "+mAppCompatSeekBar.getSecondaryProgress());
        mAppCompatSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                LibreLogger.d(this, "Bhargav SEEK:Seekbar Position track " + seekBar.getProgress() + "  " + seekBar.getMax());

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                handler.removeCallbacks(volumeAnimationRunnable);
                handler.postDelayed(volumeAnimationRunnable, 3000);
                LibreLogger.d(this, " Bhargav SEEK:Seekbar Position trackstop" + seekBar.getProgress() + "  " + seekBar.getMax());

                DMRProcessor theRenderer = getTheRenderer(mCurrentIpAddress);
                if (isLocalDMRPlayback && theRenderer == null)
                    Toast.makeText(getActivity() ,getString(R.string.NoRenderer),Toast.LENGTH_LONG).show();

                else if (doSeek(seekBar.getProgress()) == false) {
                    if (mCurrentSceneObject != null) {
                        LibreError error = new LibreError(mCurrentSceneObject.getIpAddress(),getResources().getString(R.string.RENDERER_NOT_PRESENT));
                        BusProvider.getInstance().post(error);
                    }
                }

            }
        });
    }

    /* Seek should work on both Luci command and DMR command */
    boolean doSeek(int pos) {

        String duration = Integer.toString(pos * 1000);

      /*  String duration = Integer.toString(pos * 1000);
        LUCIControl control = new LUCIControl(currentSceneObject.getIpAddress());
        control.SendCommand(40, "SEEK:" + duration, LSSDPCONST.LUCI_SET);*/
        if (isLocalDMRPlayback) {
            DMRProcessor theRenderer = getTheRenderer(mCurrentIpAddress);

            if (theRenderer == null)
                return false;

            String format = ModelUtil.toTimeString(pos);
            theRenderer.seek(format);
            LibreLogger.d(this, "LocalDMR pos = " + pos + " total time of the song " + mCurrentSceneObject.getTotalTimeOfTheTrack() / 1000 + "format = " + format);
            return true;
        }

        /*this is to prevent playback pause when clicking on seekbar while vtuner and tunein */
        else if (mCurrentSceneObject.getCurrentSource() == VTUNER_SOURCE || mCurrentSceneObject.getCurrentSource() == TUNEIN_SOURCE) {
            Toast.makeText(getActivity(), getString(R.string.seek_not_allowed), Toast.LENGTH_SHORT).show();
            return true;
        } else {
            LUCIControl control = new LUCIControl(mCurrentSceneObject.getIpAddress());
            LibreLogger.d(this, "Remote seek = " + pos + " total time of the song " + mCurrentSceneObject.getTotalTimeOfTheTrack());

            //duration = (pos * 0.01 * currentSceneObject.getTotalTimeOfTheTrack()) + "";
            duration = (pos * 1000) + "";
            mAppCompatSeekBar.setProgress(pos);
            Log.d("second","progr = "+mAppCompatSeekBar.getSecondaryProgress());
            LibreLogger.d(this, "Rempote Seek  = " + duration + " total time of the song " + mCurrentSceneObject.getTotalTimeOfTheTrack());
            control.SendCommand(40, "SEEK:" + duration, LSSDPCONST.LUCI_SET);

        }
        return true;

    }




    /*this method is to get UI and asynchronous call*/
    private void sendLuci() {

        LibreLogger.d(this, "NowPlaying: Sending the async command and 41 and 64 for ip address " + mCurrentIpAddress);
        LUCIControl luciControl = new LUCIControl(mCurrentIpAddress);
                /*Sending asynchronous registration*/
        luciControl.sendAsynchronousCommand();

        luciControl.SendCommand(41, "GETUI:PLAY", 2);
        luciControl.SendCommand(MIDCONST.ZONE_VOLUME, null, 1);
        luciControl.SendCommand(MIDCONST.VOLUEM_CONTROL, null, 1);
        /* Getting SCene Name To Update in UI */
        luciControl.SendCommand(MIDCONST.MID_SCENE_NAME, null, 1);
        // luciControl.SendCommand(64, null, 1);

        /* This is where we get the currentSource */
        luciControl.SendCommand(50, null, 1);

        luciControl.SendCommand(49, null, 1);

        //luciControl.SendCommand(222, "0:6", 1);
    }



    @Override
    public void onClick(View view) {
        if (mCurrentSceneObject == null) {
            Toast.makeText(getActivity(), getString(R.string.Devicenotplaying), Toast.LENGTH_SHORT).show();
        } else {
            LUCIControl controlPlay = new LUCIControl(mCurrentSceneObject.getIpAddress());
            LSSDPNodes mNodeWeGotForControl = mScanHandler.getLSSDPNodeFromCentralDB(mCurrentIpAddress);
            if (mNodeWeGotForControl == null)
                return;
            if (mCurrentSceneObject.getCurrentSource() == DMR_SOURCE) {
                isLocalDMRPlayback = true;
            } else {
                isLocalDMRPlayback = false;
            }
            switch (view.getId()) {
                case R.id.play_song:
                    if(mCurrentSceneObject.getCurrentSource() == Constants.AUX_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE
                            /*|| mCurrentSceneObject.getCurrentSource() == Constants.VTUNER_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.TUNEIN_SOURCE*/
                            || (mCurrentSceneObject.getCurrentSource() == Constants.BT_SOURCE
                            && ((mNodeWeGotForControl.getgCastVerision() == null
                            && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                            || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                            ||(mNodeWeGotForControl.getgCastVerision() != null
                            && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){

                        LibreError error = new LibreError("", getResources().getString(R.string.PLAY_PAUSE_NOT_ALLOWED), 1);
                        BusProvider.getInstance().post(error);
                        return;
                    }
                    if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {

                        if (doPlayPause(true) == true) {
                            mCurrentSceneObject.setPlaystatus(SceneObject.CURRENTLY_STOPED);
                            //mPlaySong.setText("{fa-pause}");
                            /*mPlaySong.setImageResource(R.mipmap.play_without_glow);
                            mNextSong.setImageResource(R.mipmap.next_without_glow);
                            mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*/
                            mPlaySong.setImageResource(R.drawable.nowplaying_play);
                            mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                            mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                            stopEqualizer();
                            //mutebutton.setImageResource(R.mipmap.mut_without_glow);
                        }
                    } else {

                        if (doPlayPause(false) == true) {

                            mCurrentSceneObject.setPlaystatus(SceneObject.CURRENTLY_PLAYING);
                            //mPlaySong.setText("{fa-play}");
                            /*mPlaySong.setImageResource(R.mipmap.pause_with_glow);
                            mNextSong.setImageResource(R.mipmap.next_with_glow);
                            mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*/
                            mPlaySong.setImageResource(R.drawable.nowplaying_pause);
                            mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                            mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                            resumeEqualizer();
                            //mutebutton.setImageResource(R.mipmap.mut_with_glow);
                        }

                    }
                    break;

                case R.id.previous_song:
                    if(mCurrentSceneObject.getCurrentSource() == Constants.AUX_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.VTUNER_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.TUNEIN_SOURCE
                            || (mCurrentSceneObject.getCurrentSource() == Constants.BT_SOURCE
                            && ((mNodeWeGotForControl.getgCastVerision() == null
                            && ((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                            || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                            ||(mNodeWeGotForControl.getgCastVerision() != null
                            && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){
                        LibreError error = new LibreError("", getResources().getString(R.string.NEXT_PREVIOUS_NOT_ALLOWED),1);
                        BusProvider.getInstance().post(error);
                        return ;
                    }
                    if (mPreviousSong.isEnabled()) {
                        doNextPrevious(false);
                    }else{
                        LibreError error = new LibreError(mCurrentIpAddress,getString(R.string.requestTimeout));
                        BusProvider.getInstance().post(error);
                    }
                    break;
                case R.id.next_song:
                    if(mCurrentSceneObject.getCurrentSource() == Constants.AUX_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.VTUNER_SOURCE
                            || mCurrentSceneObject.getCurrentSource() == Constants.TUNEIN_SOURCE
                            || (mCurrentSceneObject.getCurrentSource() == Constants.BT_SOURCE
                            && (mNodeWeGotForControl.getgCastVerision() == null
                            && (((mNodeWeGotForControl.getBT_CONTROLLER() == 4)
                            || mNodeWeGotForControl.getBT_CONTROLLER() == 0))
                            ||(mNodeWeGotForControl.getgCastVerision() != null
                            && (mNodeWeGotForControl.getBT_CONTROLLER() < 2))))){
                        LibreError error = new LibreError("", getResources().getString(R.string.NEXT_PREVIOUS_NOT_ALLOWED),1);
                        BusProvider.getInstance().post(error);
                        return ;
                    }
                    if (mNextSong.isEnabled()) {
                        doNextPrevious(true);
                    }else{
                        LibreError error = new LibreError(mCurrentIpAddress, getString(R.string.requestTimeout));
                        BusProvider.getInstance().post(error);
                    }
                    break;

                case R.id.rounded_album_art:
                    break;
                case R.id.volume:
                    //TransitionManager.beginDelayedTransition(mRelativeLayout);
                    /*if (!volumeClicked)
                        mmSeekBarVisibility = !mmSeekBarVisibility;
                    if (mmSeekBarVisibility) {
                        if (!volumeClicked) {
                            volumeClicked = true;
                            //mVolume.setImageResource(R.drawable.cancwel);
                            //getResources().getDrawable(R.drawable.cumulations_circle_background).setColorFilter(Color.parseColor("#9E9E9E"), PorterDuff.Mode.SRC_IN);
                            mVolume.setImageResource(R.drawable.ic_close_white_24dp);
                            //mVolumeText.setBackgroundResource(R.drawable.cumulations_rounded_rectangle_for_text);
                            changeVolumeBackgroundColor(true, volumeClicked);
                            animatemSeekBar(380, true);
                        }
                    } else {
                        //mVolume.setImageResource(R.mipmap.mut_with_glow);
                        //mVolume.setImageResource(R.drawable.ic_volume_up_black_24dp);
                        if (!volumeClicked) {
                            volumeClicked = true;
                            animatemSeekBar(0, false);
                            //changeVolumeBackgroundColor(false);
                            mVolumeText.setVisibility(View.GONE);
                        }
                    }*/
                    break;

                case R.id.favoriteSong:

                    if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PAUSED
                            && !mCurrentSceneObject.isFavourite()){
                        Toast.makeText(getActivity(), getString(R.string.favPausedError), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    LUCIControl favLuci = new LUCIControl(mCurrentSceneObject.getIpAddress());
                    if (mCurrentSceneObject.isFavourite()) {
                        favLuci.SendCommand(MIDCONST.MID_FAVOURITE, "GENERIC_FAV_DELETE", LSSDPCONST.LUCI_SET);
                        showLoaderHandler.sendEmptyMessage(Constants.FAV_CLICKED);
//                        loaderMsg = "Removing from favorites..";
                    } else {

                        if (mCurrentSceneObject.getCurrentSource() == FAV_SOURCE){
                            Toast.makeText(getActivity(), getString(R.string.favSourceError), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        favLuci.SendCommand(MIDCONST.MID_FAVOURITE, "FAV_SAVE", LSSDPCONST.LUCI_SET);
//                        loaderMsg = "Adding to favorites..";
                        showLoaderHandler.sendEmptyMessage(Constants.FAV_CLICKED);
                    }


//                    Toast.makeText(getActivity(), "Action completed", Toast.LENGTH_SHORT).show();

//                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Action completed", Snackbar.LENGTH_LONG)
//                            .setAction("Error", null)
//                            .show();

                    break;

                case R.id.shufflesong:

                    /*this is added to support shuffle and repeat option for Local Content*/
                    if (isLocalDMRPlayback) {
                        RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentSceneObject.getIpAddress());
                        if (renderingDevice != null) {
                            String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                            PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                            if (playbackHelper != null) {
                                if (mCurrentSceneObject.getShuffleState() == 0) {
                                /*which means shuffle is off hence making it on*/
                                    playbackHelper.setIsShuffleOn(true);
                                    mCurrentSceneObject.setShuffleState(1);
                                } else {
                                    /*which means shuffle is on hence making it off*/
                                    mCurrentSceneObject.setShuffleState(0);
                                    playbackHelper.setIsShuffleOn(false);
                                }
                                /*inserting to central repo*/
                                mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                                setViews();
                            }
                        }
                    } else {
                        LUCIControl luciControl = new LUCIControl(mCurrentSceneObject.getIpAddress());

                        if (mCurrentSceneObject.getShuffleState() == 0) {
                        /*which means shuffle is off*/
                            luciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "SHUFFLE:ON", LSSDPCONST.LUCI_SET);

                        } else
                            luciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "SHUFFLE:OFF", LSSDPCONST.LUCI_SET);
                    }
                    break;


                case R.id.repeatsongnowplaying:

                     /*this is added to support shuffle and repeat option for Local Content*/
                    if (isLocalDMRPlayback) {
                        RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentSceneObject.getIpAddress());
                        if (renderingDevice != null) {
                            String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                            PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                            if (playbackHelper != null) {
                                if (mCurrentSceneObject.getRepeatState() == REPEAT_ALL) {
                                    playbackHelper.setRepeatState(REPEAT_OFF);
                                    //mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp_less_opacity);
                                    mCurrentSceneObject.setRepeatState(REPEAT_OFF);
                                } else if (mCurrentSceneObject.getRepeatState() == REPEAT_OFF) {
                                    playbackHelper.setRepeatState(REPEAT_ONE);
                                    //mRepeat.setImageResource(R.drawable.ic_repeat_one_white_24dp);
                                    mCurrentSceneObject.setRepeatState(REPEAT_ONE);
                                } else if (mCurrentSceneObject.getRepeatState() == REPEAT_ONE) {
                                    playbackHelper.setRepeatState(REPEAT_ALL);
                                    //mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp);
                                    mCurrentSceneObject.setRepeatState(REPEAT_ALL);
                                }
                                /*inserting to central repo*/
                                mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                                setViews();
                            }
                        }
                    } else {
                        /**/
                        LUCIControl shuffleLuciControl = new LUCIControl(mCurrentSceneObject.getIpAddress());
                        if (mCurrentSceneObject.getRepeatState() == REPEAT_ALL) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:OFF", LSSDPCONST.LUCI_SET);
                            mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp_less_opacity);
                            mCurrentSceneObject.setRepeatState(REPEAT_OFF);
                        }
                        //Not equal to spotify
                        else if (mCurrentSceneObject.getRepeatState() == REPEAT_OFF && mCurrentSceneObject.getCurrentSource() != Constants.SPOTIFY_SOURCE) {

                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ONE", LSSDPCONST.LUCI_SET);
                            mRepeat.setImageResource(R.drawable.ic_repeat_one_white_24dp);
                            mCurrentSceneObject.setRepeatState(REPEAT_ONE);
                        }

                        /* If the current source is spotify  */
                        else if (mCurrentSceneObject.getRepeatState() == REPEAT_OFF && mCurrentSceneObject.getCurrentSource() == Constants.SPOTIFY_SOURCE) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ALL", LSSDPCONST.LUCI_SET);
                            mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp);
                            mCurrentSceneObject.setRepeatState(REPEAT_ALL);
                        } else if (mCurrentSceneObject.getRepeatState() == REPEAT_ONE) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ALL", LSSDPCONST.LUCI_SET);
                            mRepeat.setImageResource(R.drawable.ic_repeat_black_24dp);
                            mCurrentSceneObject.setRepeatState(REPEAT_ALL);
                        }
                        setViews();
                        /**//*
                        LUCIControl shuffleLuciControl = new LUCIControl(mCurrentSceneObject.getIpAddress());
                        if (mCurrentSceneObject.getRepeatState() == REPEAT_ALL) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:OFF", LSSDPCONST.LUCI_SET);
                        } else if (mCurrentSceneObject.getRepeatState() == REPEAT_OFF) {
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ONE", LSSDPCONST.LUCI_SET);
                        } else if (mCurrentSceneObject.getRepeatState() == REPEAT_ONE)
                            shuffleLuciControl.SendCommand(MIDCONST.MID_PLAYCONTROL, "REPEAT:ALL", LSSDPCONST.LUCI_SET);
*/
                    }break;

            }
        }
    }

    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

    }

    @Override
    public void deviceGotRemoved(String ipaddress) {

    }

    @Override
    public void messageRecieved(NettyData dataRecived) {
        LibreLogger.d(this, "Nowplaying: New message appeared for the device " + dataRecived.getRemotedeviceIp());
        if(dataRecived!=null &&
                (new LUCIPacket(dataRecived.getMessage()).getCommand() == MIDCONST.MID_DEVICE_STATE_ACK)){
        }
        if (mCurrentSceneObject != null && dataRecived.getRemotedeviceIp().equalsIgnoreCase(mCurrentIpAddress)) {
            LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());
            LibreLogger.d(this, "Packet is _" + packet.getCommand());

            switch (packet.getCommand()) {
              /*  case MIDCONST.MID_DEVICE_STATE_ACK:
                    deviceCount.setText("" + mScanHandler.getNumberOfSlavesForMasterIp(currentIpAddress,
                            mScanHandler.getconnectedSSIDname(getActivity().getApplicationContext())));
                    break;*/
                case MIDCONST.MID_SCENE_NAME: {
                    /* This message box indicates the Scene Name*//*
;*/                 /* if Command Type 1 , then Scene Name information will be come in the same packet
if command type 2 and command status is 1 , then data will be empty., at that time we should not update the value .*/
                    if (packet.getCommandStatus() == 1
                            && packet.getCommandType() == 2) {
                        return;
                    }
                    String message = new String(packet.getpayload());
                    //int duration = Integer.parseInt(message);
                    try {
                        mCurrentSceneObject.setSceneName(message);// = duration/60000.0f;
                        LibreLogger.d(this, "Recieved the Scene Name to be " + mCurrentSceneObject.getSceneName());
                        if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                            mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                            //sceneName.setText(currentSceneObject.getSceneName());
                        }
                    } catch (Exception e) {

                    }
                }
                break;

                case 103:
                    if (packet.getCommand() == 103) {
                        LibreLogger.d(this, "Command 103 " + new String(packet.getpayload()));
                        Message successMsg = new Message();
                        String message = new String(packet.getpayload());
                        Bundle b = new Bundle();
                        b.putString("ipAddress", dataRecived.getRemotedeviceIp());
                        successMsg.setData(b);
                        successMsg.obj = mScanHandler.getLSSDPNodeFromCentralDB(dataRecived.getRemotedeviceIp());
                        if (message.contains("FREE")) {
                            successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_FREE_COMMAND;
                        } else if (message.contains("SLAVE")) {
                            successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_SLAVE_COMMAND;
                        } else if (message.contains("MASTER")) {
                            successMsg.what = DeviceMasterSlaveFreeConstants.LUCI_SUCCESS_MASTER_COMMAND;
                        }
                        mNowPlayingActivityReleaseSceneHandler.sendMessage(successMsg);
                    }
                    break;

                case 49: {
//                This message box indicates the current playing status of the scene, information like current seek position*//*
                    String message = new String(packet.getpayload());
                    if (!message.equals("")) {
                        long longDuration = Long.parseLong(message);

                        mCurrentSceneObject.setCurrentPlaybackSeekPosition(longDuration);

                        /* Setting the current seekbar progress -Start*/
                        float duration = mCurrentSceneObject.getCurrentPlaybackSeekPosition();
                        mAppCompatSeekBar.setMax((int) mCurrentSceneObject.getTotalTimeOfTheTrack() / 1000);
                        Log.d("SEEK", "Duration = " + duration / 1000);
                        mAppCompatSeekBar.setProgress((int) duration / 1000);
                        Log.d("second","progr = "+mAppCompatSeekBar.getSecondaryProgress());

                        DecimalFormat twoDForm = new DecimalFormat("#.##");
                        mSongStartTime.setText(convertMilisecondsToTimeString((int) duration / 1000));
                        mSongStopTime.setText(convertMilisecondsToTimeString(mCurrentSceneObject.getTotalTimeOfTheTrack() / 1000));
                        /* Setting the current seekbar progress -END*/

                        setTheSourceIconFromCurrentSceneObject();


                     /*   if (seekSongBar.isClickable()) {
                            seekSongBar.setProgress((int) duration / 1000);
                            DecimalFormat twoDForm = new DecimalFormat("#.##");
                            currentPlayPosition.setText(convertMilisecondsToTimeString(duration / 1000));
                            totalPlayPosition.setText(convertMilisecondsToTimeString(currentSceneObject.getTotalTimeOfTheTrack() / 1000));

                            Log.d("Bhargav SEEK", "Duration = " + duration / 1000);
                            Log.d("SEEK", "Duration = " + duration / 1000);
b
                        }*/


                    }

                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }

                    getActivity().invalidateOptionsMenu();
                    LibreLogger.d(this, "Nowplaying: Recieved the current Seek position to be " + (int) mCurrentSceneObject.getCurrentPlaybackSeekPosition());
                    break;
                }

                case 50: {

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);

                        mCurrentSceneObject.setCurrentSource(duration);
                        setTheSourceIconFromCurrentSceneObject();
                        updateAlbumArt(mCurrentSceneObject);
                        LibreLogger.d(this, "Recieved the current source as  " + mCurrentSceneObject.getCurrentSource());
                        /*if (duration == 14) {
                            disableViews("Aux Playing");
                        } else if (duration == 19) {
                            disableViews("BT Playing");
                        } else if (duration == 18) {
                            disableSeekBarNextPrevious();
                        } else {
                            enableViews();
                        }
*/
                    } catch (Exception e) {

                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                    disableViews(mCurrentSceneObject.getCurrentSource(),getString(R.string.btOn));
                    getActivity().invalidateOptionsMenu();
                    break;
                }
                case MIDCONST.ZONE_VOLUME: {

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        mCurrentSceneObject.setvolumeZoneInPercentage(duration);

                        if (mCurrentSceneObject.getCurrentSource() == Constants.AIRPLAY_SOURCE ||
                                mCurrentSceneObject.getCurrentSource() == Constants.SPOTIFY_SOURCE) {
                            mSeekBar.setProgress(mCurrentSceneObject.getVolumeValueInPercentage());
                            mVolumeText.setText(String.valueOf(mCurrentSceneObject.getVolumeValueInPercentage()));
                        } else {
                            mSeekBar.setProgress(duration);
                            mVolumeText.setText(String.valueOf(duration));
                        }

                        //LibreLogger.d(this, "Recieved the current volume to be " + currentSceneObject.getvolumeZoneInPercentage());
                    } catch (Exception e) {

                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                }
                break;
                case MIDCONST.MID_FAVOURITE: {
                    closeLoader();
                    if (showLoaderHandler.hasMessages(Constants.FAV_INITIATED_TIMEOUT))
                        showLoaderHandler.removeMessages(Constants.FAV_INITIATED_TIMEOUT);
                    String message = new String(packet.getpayload());
                    LibreLogger.d(this, "70 msg box" + "payload->" + message);
                    switch (message) {
                        case "GENERIC_FAV_SAVE_SUCCESS":
                            /**fav success*/
                            mCurrentSceneObject.setIsFavourite(true);
                            favButton.setImageResource(R.drawable.ic_favorite_blue_48dp);
                            Toast.makeText(getActivity(), getString(R.string.FAV_SUCCESSFUL), Toast.LENGTH_SHORT).show();
                            break;
                        case "GENERIC_FAV_DELETE_SUCCESS":
                            /**fav deleted*/
                            mCurrentSceneObject.setIsFavourite(false);
                            favButton.setImageResource(R.drawable.ic_favorite_border_blue_48dp);
                            Toast.makeText(getActivity(), getResources().getString(R.string.FAV_DELETED), Toast.LENGTH_SHORT).show();
                            break;
                        case "GENERIC_FAV_SAVE_FAILED":
                            Toast.makeText(getActivity(), getResources().getString(R.string.FAV_FAILED), Toast.LENGTH_SHORT).show();
                            break;
                        case "GENERIC_FAV_EXISTS":
                            Toast.makeText(getActivity(), getResources().getString(R.string.FAV_EXISTS), Toast.LENGTH_SHORT).show();
                            break;
                        case "GENERIC_FAV_VTUNER_DEL":
                            Toast.makeText(getActivity(), getResources().getString(R.string.VTUNER_FAV_DELETE), Toast.LENGTH_SHORT).show();
                            break;
                        default:
                    }
                }
                break;
                case 51: {
                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        mCurrentSceneObject.setPlaystatus(duration);
                        if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
                            if (mCurrentSceneObject.getCurrentSource() != MIDCONST.GCAST_SOURCE
                                    && !(mCurrentSceneObject.getCurrentSource() == Constants.AUX_SOURCE
                                    && mCurrentSceneObject.getmPreviousTrackName().equals("-1"))) {
                                //mPlaySong.setText("{fa-pause}");
                                //mPlaySong.setImageResource(R.mipmap.pause_with_glow);
                                mPlaySong.setImageResource(R.drawable.nowplaying_pause);
                                mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                                mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                                resumeEqualizer();
                                /*mNextSong.setImageResource(R.mipmap.next_with_glow);
                                mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*/
                            }
                            if (mCurrentSceneObject.getCurrentSource() != 19) {
                                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                                showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
                            }

                        } else {
                            if (mCurrentSceneObject.getCurrentSource() != MIDCONST.GCAST_SOURCE) {
                                //mPlaySong.setText("{fa-play}");
                                /*mPlaySong.setImageResource(R.mipmap.play_with_gl);
                                mNextSong.setImageResource(R.mipmap.next_without_glow);
                                mPreviousSong.setImageResource(R.mipmap.prev_with_glow);*/
                                mPlaySong.setImageResource(R.drawable.nowplaying_play);
                                mNextSong.setImageResource(R.drawable.ic_skip_next_black_48dp);
                                mPreviousSong.setImageResource(R.drawable.ic_skip_previous_white_48dp);
                                stopEqualizer();

                            }
                            if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PAUSED) {
                            /* this case happens only when the user has paused from the App so close the existing loader if any */
                                if (mCurrentSceneObject.getCurrentSource() != 19) {
                                    showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                                    showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
                                }

                            }
                            if (mCurrentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED && mCurrentSceneObject.getCurrentSource() != 0) {

                                if (mCurrentSceneObject.getCurrentSource() != Constants.BT_SOURCE
                                        && mCurrentSceneObject.getCurrentSource() != Constants.AUX_SOURCE
                                        && mCurrentSceneObject.getCurrentSource() != 15
                                        && mCurrentSceneObject.getCurrentSource() != Constants.GCAST_SOURCE) {
                                    /* When I am Stopping it , will change the album art to Default instead of Invalidate */
                                   /* String default_album_url = "http://" + currentSceneObject.getIpAddress() + "/" + "coverart1.jpg";
                                    PicassoTrustCertificates.getInstance(getActivity()).load(default_album_url).
                                            placeholder(R.mipmap.album_art)
                                            .error(R.mipmap.album_art).into(albumArt);

    */
                                    showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_TIMEOUT_CONST, Constants.PREPARATION_TIMEOUT);
                                    showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_INITIATED);
                                    Log.d("showingLoader","messageRecieved, 51");
                                }
                            }
                        }


/*                            if(currentSceneObject.getCurrentSource() == 19){
                                disableViews("BT is Playing");
                            }else {
//                            if (isLoclaDMRPlayback) {
                            *//*device is playing now so closing loader*//*
                                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                                showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
//                            }

                            }
                            LibreLogger.d(this, "Play state is received");


                        } else if (currentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PAUSED) {
                            if(currentSceneObject.getCurrentSource() == 19){
                                disableViews("BT is Paused");
                            }
                            LibreLogger.d(this, "Pause state is received");
                        } else {
                            *//*which means stopped*//*
                            play.setImageResource(R.mipmap.play_with_gl);
                            next.setImageResource(R.mipmap.next_without_glow);
                            previous.setImageResource(R.mipmap.prev_without_glow);

                            if(currentSceneObject.getCurrentSource() == 19) {
                                disableViews("BT is Stopped");
                            }else {
//                            if (isLoclaDMRPlayback) {
                            *//*which means state is not playing*//*
                                showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_TIMEOUT_CONST, Constants.PREPARATION_TIMEOUT);
                                showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_INITIATED);
//                            }
                            }*/
                        LibreLogger.d(this, "Stop state is received");
                        LibreLogger.d(this, "Recieved the playstate to be" + mCurrentSceneObject.getPlaystatus());
                    } catch (Exception e) {

                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                }
                setTheSourceIconFromCurrentSceneObject();
                getActivity().invalidateOptionsMenu();
                break;

                case 66: {

                    /*String message = new String(packet.getpayload());
                    try {

                        if ( updateBuilder==null) {

                            updateBuilder = new AlertDialog.Builder(getActivity());
                            mProgressView=new TextView(getActivity());

                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);

                            mProgressView.setLayoutParams(lp);
                            mProgressView.setGravity(Gravity.CENTER);
                            mProgressView.setText(message);
                            mProgressView.setPadding(10,10,10,10);

                            updateBuilder.setTitle(getString(R.string.FirmwareUpdateinProgress)).setCancelable(false);
                            updateBuilder.setView(mProgressView);
                            updateBuilder.setPositiveButton(getString(R.string.back), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getActivity().onBackPressed();
                                }
                            }).show();

                        }
                        else{
                            mProgressView.setText(message);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();


                    }*/

                }
                break;
                case 40: {
                    /*            "Error_Fail" ,
                     "Error_NoURL" ,
                        "Error_LastSong"
*/
                    String message = new String(packet.getpayload());
                    String ERROR_FAIL = "Error_Fail";
                    String ERROR_NOURL = "Error_NoURL";
                    String ERROR_LASTSONG = "Error_LastSong";
                    LibreLogger.d(this, "recieved 40 " + message);
                    try {
                        if (message.equalsIgnoreCase(ERROR_FAIL)) {
                            LibreError error = new LibreError(mCurrentSceneObject.getIpAddress(), Constants.ERROR_FAIL);
                            BusProvider.getInstance().post(error);
                        } else if (message.equalsIgnoreCase(ERROR_NOURL)) {
                            LibreError error = new LibreError(mCurrentSceneObject.getIpAddress(), Constants.ERROR_NOURL);
                            BusProvider.getInstance().post(error);
                        } else if (message.equalsIgnoreCase(ERROR_LASTSONG)) {
                            LibreError error = new LibreError(mCurrentSceneObject.getIpAddress(), Constants.ERROR_LASTSONG);
                            BusProvider.getInstance().post(error);

                            showLoaderHandler.sendEmptyMessage(Constants.PREPARATION_COMPLETED);
                            preparingToPlay(false);
                            showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
                        }

                    } catch (Exception e) {

                    }
                }
                break;

                case 64: {

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        mCurrentSceneObject.setVolumeValueInPercentage(duration);
                        if (mCurrentSceneObject.getCurrentSource() == Constants.AIRPLAY_SOURCE
                                ||
                                mCurrentSceneObject.getCurrentSource() == Constants.SPOTIFY_SOURCE) {
                            mSeekBar.setProgress(duration);
                            mVolumeText.setText(String.valueOf(duration));
                        }
                        if (!mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {

                        }
                        LibreLogger.d(this, "Recieved the current volume to be " + mCurrentSceneObject.getVolumeValueInPercentage());
                    } catch (Exception e) {

                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                }
                break;

                case 222: {

                    /*String message = new String(packet.payload);
                    if (message.equalsIgnoreCase("0:2")|| message.equalsIgnoreCase("2")) {

                        if (getActivity() != null) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("New Update Available!").setMessage("Do you wish to update the firmware now?")
                                    .setCancelable(false)
                                    .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();
                                        }
                                    })
                                    .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            LUCIControl favLuci = new LUCIControl(currentSceneObject.getIpAddress());
                                            favLuci.SendCommand(55, "", LSSDPCONST.LUCI_SET);

                                        }


                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
                 else   if (message.equalsIgnoreCase("0:3")|| message.equalsIgnoreCase("3")) {

                        if (getActivity() != null) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("New Update Available!").setMessage("Do you wish to update the firmware now?")
                                    .setCancelable(false)
                                    .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.cancel();
                                        }
                                    })
                                    .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            LUCIControl favLuci = new LUCIControl(currentSceneObject.getIpAddress());
                                            favLuci.SendCommand(222, "0:4", LSSDPCONST.LUCI_SET);

                                        }


                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
                    else if((message.equalsIgnoreCase("0:4"))|| message.equalsIgnoreCase("4")){
                        // forceful update
                        if (getActivity() != null) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Cast Device Updated").setMessage("Devce will reboot")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent = new Intent(getActivity(), ActiveScenesListActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }


                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }

                    }*/

                }
                break;

                case 42: {


                    try {

                        String message = new String(packet.payload);
                        JSONObject root = new JSONObject(message);
                        int cmd_id = root.getInt(LUCIMESSAGES.TAG_CMD_ID);
                        JSONObject window = root.getJSONObject(LUCIMESSAGES.TAG_WINDOW_CONTENT);
                        LibreLogger.d(this, "PLAY JSON is \n= " + message + "\n For ip" + mCurrentIpAddress);


                        if (cmd_id == 3) {

                            showLoaderHandler.sendEmptyMessageDelayed(Constants.PREPARATION_COMPLETED, 1000);
                            showLoaderHandler.removeMessages(Constants.PREPARATION_TIMEOUT_CONST);
                           /* String newTrackname=window.getString("TrackName");
                            int newPlayState= window.getInt("PlayState");
                            int currentPlayState= window.getInt("Current_time");

                            if (!newTrackname.equalsIgnoreCase(currentSceneObject.getSceneName()) || newPlayState!=currentSceneObject.getPlaystatus()) {


                                if(!newTrackname.equalsIgnoreCase(currentSceneObject.getSceneName()))

                                {   *//* this is done to avoid  image refresh everytime the 42 message is recieved and the song playing back is the same *//*
                                    Picasso.with(getActivity()).invalidate("http://" + currentSceneObject.getIpAddress() + "/" + currentSceneObject.getAlbum_art());
                                }

                                currentSceneObject.setSceneName(window.getString("TrackName"));
                                currentSceneObject.setAlbum_art(window.getString("CoverArtUrl"));

                                if (currentPlayState>=0)
                                    currentSceneObject.setPlaystatus(window.getInt("PlayState"));
                            */
                            String newTrackname = window.getString("TrackName");
                            int newPlayState = window.getInt("PlayState");
                            int currentPlayState = window.getInt("Current_time");
                            int currentSource = window.getInt("Current Source");
                            String album_arturl = window.getString("CoverArtUrl");
                            String genre = window.getString("Genre");


                            String nAlbumName = window.getString("Album");
                            String nArtistName = window.getString("Artist");
                            long totaltime = window.getLong("TotalTime");

                            // currentSceneObject.setSceneName(window.getString("TrackName"));
                            mCurrentSceneObject.setPlaystatus(window.getInt("PlayState"));
                            mCurrentSceneObject.setAlbum_art(album_arturl);
                            String mPlayURL = window.getString("PlayUrl");
                            if (mPlayURL != null)
                                mCurrentSceneObject.setPlayUrl(mPlayURL);
                            mCurrentSceneObject.setTrackName(window.getString("TrackName"));
                            /*For favourite*/
                            mCurrentSceneObject.setIsFavourite(window.getBoolean("Favourite"));

                            if (genre != null)
                                mCurrentSceneObject.setGenre(window.getString("Genre"));


                            if (!LibreApplication.LOCAL_IP.equals("") && mCurrentSceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP))
                                isLocalDMRPlayback = true;
                            else if (mCurrentSceneObject.getCurrentSource() == DMR_SOURCE)
                                isLocalDMRPlayback = true;


                            /*Added for Shuffle and Repeat*/
                            if (isLocalDMRPlayback) {
                                /**this is for local content*/
                                RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(mCurrentSceneObject.getIpAddress());
                                if (renderingDevice != null) {
                                    String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                                    PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                                    if (playbackHelper != null) {
                                        /**shuffle is on*/
                                        if (playbackHelper.isShuffleOn()) {
                                            mCurrentSceneObject.setShuffleState(1);
                                        } else {
                                            mCurrentSceneObject.setShuffleState(0);
                                        }
                                        /*setting by default*/
                                        if (playbackHelper.getRepeatState() == REPEAT_ONE) {
                                            mCurrentSceneObject.setRepeatState(REPEAT_ONE);
                                        }
                                        if (playbackHelper.getRepeatState() == REPEAT_ALL) {
                                            mCurrentSceneObject.setRepeatState(REPEAT_ALL);
                                        }
                                        if (playbackHelper.getRepeatState() == REPEAT_OFF) {
                                            mCurrentSceneObject.setRepeatState(REPEAT_OFF);
                                        }

                                    }
                                }
                            } else {
                                /**this check made as we will not get shuffle and repeat state in 42, case of DMR
                                 * so we are updating it locally*/
                                mCurrentSceneObject.setShuffleState(window.getInt("Shuffle"));
                                mCurrentSceneObject.setRepeatState(window.getInt("Repeat"));
                            }


                            mCurrentSceneObject.setAlbum_name(nAlbumName);
                            mCurrentSceneObject.setArtist_name(nArtistName);
                            mCurrentSceneObject.setTotalTimeOfTheTrack(totaltime);
                            mCurrentSceneObject.setCurrentSource(currentSource);String album_url = "";
                            if (album_arturl.contains("http://")) {
                                album_url = mCurrentSceneObject.getAlbum_art();
                            } else {
                                album_url = "http://" + mCurrentSceneObject.getIpAddress() + "/" + "coverart.jpg";
                            }
                            if (mCurrentSceneObject.getTrackName() == null
                                    || !newTrackname.equalsIgnoreCase(mCurrentSceneObject.getTrackName()))
                                PicassoTrustCertificates.getInstance(getActivity()).invalidate(album_url);
                            LibreLogger.d(this, "Recieved the scene details as trackname = " + mCurrentSceneObject.getSceneName() + ": " + mCurrentSceneObject.getCurrentPlaybackSeekPosition());

                            setViews(); //This will take care of disabling the views

                            if (currentSource == 14) { // AUX
                                disableViews(currentSource,getString(R.string.aux));
                            } else if (currentSource == 19) { // BT
                                disableViews(currentSource,getString(R.string.btOn));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (mScanHandler.isIpAvailableInCentralSceneRepo(mCurrentIpAddress)) {
                        mScanHandler.putSceneObjectToCentralRepo(mCurrentIpAddress, mCurrentSceneObject);
                    }
                    break;

                }
                case 54: {
                    String message = new String(packet.payload);
                    LibreLogger.d(this, " message 54 recieved  " + message);
                    try {
                        LibreError error = null;
                        if (message != null && (message.equalsIgnoreCase(Constants.DMR_PLAYBACK_COMPLETED) || message.contains(Constants.FAIL))) {

                            /* If the song completes then make the seekbar to the starting of the song */
                            mCurrentSceneObject.setCurrentPlaybackSeekPosition(0);
                            mAppCompatSeekBar.setProgress(0);
                            Log.d("second","progr = "+mAppCompatSeekBar.getSecondaryProgress());
                        /* This is handled in nett
                           LibreLogger.d(this,"Libre logger sent the DMR playback completed ");
                            LibreLogger.d(this,"DMR completed for URL "+currentSceneObject.getPlayUrl());
                                if( currentSceneObject.getPlayUrl()!=null && currentSceneObject.getPlayUrl().contains(com.libre.luci.Utils.getLocalV4Address(Utils.getActiveNetworkInterface()).getHostAddress()))
                            {
                                LibreLogger.d(this,"App is going to next song");

                                doNextPrevious(true);
                            }*/
                        } else if (message.contains(Constants.FAIL)) {
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.FAIL_ALERT_TEXT));
                        } else if (message.contains(Constants.SUCCESS)) {
                            //closeLoader();
                        } else if (message.contains(Constants.NO_URL)) {
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.NO_URL_ALERT_TEXT));
                        } else if (message.contains(Constants.NO_PREV_SONG)) {
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.NO_PREV_SONG_ALERT_TEXT));
                        } else if (message.contains(Constants.NO_NEXT_SONG)) {
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.NO_NEXT_SONG_ALERT_TEXT));
                        } else if (message.contains(Constants.DMR_SONG_UNSUPPORTED)) {
                            error = new LibreError(mCurrentIpAddress, getResources().getString(R.string.SONG_NOT_SUPPORTED));
                        } else if (message.contains(LUCIMESSAGES.NEXTMESSAGE)) {
                            doNextPrevious(true, true);
                        } else if (message.contains(LUCIMESSAGES.PREVMESSAGE)) {
                            doNextPrevious(false, true);
                        }
                        PicassoTrustCertificates.getInstance(getActivity()).invalidate(mCurrentSceneObject.getAlbum_art());
                        //closeLoader();
                        if (error != null)
                            BusProvider.getInstance().post(error);
                        getActivity().invalidateOptionsMenu();

                    } catch (Exception e) {
                        e.printStackTrace();
                        LibreLogger.d(this, " Json exception ");

                    }
                }
                break;
            }
            }
    }


    private void changeVolumeBackgroundColor(boolean isGrey, boolean volumeClicked) {

        Drawable background = mVolume.getBackground();
        if (background instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable)background;
            if (isGrey && volumeClicked)
                gradientDrawable.setColor(Color.parseColor("#424949"));
            else
                gradientDrawable.setColor(Color.TRANSPARENT);
        }
    }

    private void getScreenWidth() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    private void animatemSeekBar(int width, final boolean isGrey) {
        //volumeLayout.setVisibility(View.VISIBLE);
        mSeekBar.setVisibility(View.VISIBLE);
        //mVolumeText.setVisibility(View.VISIBLE);
        Log.d("MAXHEIGHT", "" + mVolume.getHeight() / 10);
        mSeekBar.setPadding((mVolume.getHeight() / 2) + 8, mVolume.getHeight() / 4, mVolume.getHeight() / 4, mVolume.getHeight() / 4);
        //mSeekBar.setPadding(2, 2, 2, 2);
        DisplayMetrics metrics = new DisplayMetrics();
        if (getActivity() != null) {
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            final ValueAnimator anim;
            if (width == 0) {
                anim = ValueAnimator.ofInt(mSeekBar.getMeasuredWidth(), width);
            } else {
                anim = ValueAnimator.ofInt(mSeekBar.getMeasuredWidth(), metrics.widthPixels / 2);
            }
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = mSeekBar.getLayoutParams();
                    layoutParams.width = val;
                    mSeekBar.setLayoutParams(layoutParams);
                }
            });
            anim.setDuration(400);
            anim.start();
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    volumeClicked = false;
                    if (!isGrey) {
                        changeVolumeBackgroundColor(isGrey, volumeClicked);
                        //mVolumeText.setVisibility(View.GONE);
                        mVolume.setImageResource(R.drawable.ic_volume_up_black_24dp);
                    } else {
                        mVolumeText.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    super.onAnimationCancel(animation);
                    mVolumeText.setVisibility(View.GONE);
                }
            });
        }
    }


    private Bitmap returnBlurredBitmap(Drawable drawable) {
        Bitmap myBitmap = ((BitmapDrawable)drawable).getBitmap();
        if (getActivity() != null) {
            Bitmap blurredBitmap = BlurBuilder.blur( getActivity().getApplicationContext(), myBitmap );
            return blurredBitmap;
        }
        return null;
    }

    private void stopEqualizer() {
        //mVuMeterView.setSpeed(0);
    }

    private void resumeEqualizer() {
        //mVuMeterView.setSpeed(0);
    }

    private Bitmap drawableToBitmap(Drawable drawable) {
        return ((BitmapDrawable)drawable).getBitmap();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setSeekbarBackground(Palette palette) {
        int defaultColor = 0xffffff;
        vibrant = palette.getDarkVibrantColor(defaultColor);
        Log.d("SEEKBAR", " " + vibrant);
        //mVuMeterView.setColor(vibrant);
        mAppCompatSeekBar.getProgressDrawable().setColorFilter(vibrant, PorterDuff.Mode.SRC_IN);
        //mAppCompatSeekBar.setProgressTintList(ColorStateList.valueOf(Color.WHITE));

        // Change progress color and background color of seekbar.
        try {
            LayerDrawable progressBarDrawable = (LayerDrawable)mAppCompatSeekBar.getProgressDrawable();
            Drawable backgroundDrawable = progressBarDrawable.getDrawable(0);
            Drawable progressDrawable = progressBarDrawable.getDrawable(1);
            backgroundDrawable.setColorFilter(Color.parseColor("#80FFFFFF"), PorterDuff.Mode.SRC_IN);
            //progressBarDrawable.setColorFilter(vibrant, PorterDuff.Mode.SRC_ATOP);
        } catch( Exception e) {
            e.printStackTrace();
        }
        //progressDrawable.setColorFilter(vibrant, PorterDuff.Mode.SRC_IN);

        //mAppCompatSeekBar.getThumb().setColorFilter(vibrant, PorterDuff.Mode.SRC_IN);
    }


    /*private void extractPaletteAndSetSeekbarProgressColor(Drawable d) {
        PaletteExtractor.getPalette(drawableToBitmap(d), new CumulationsNowPlayingActivity.paletteExtractor() {
            @Override
            public void onPalette(Palette palette) {
                setSeekbarBackground(palette);
            }
        });
    }*/

    @Override
    public void onUpdatePosition(long position, long duration) {

    }

    @Override
    public void onUpdateVolume(int currentVolume) {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onStoped() {

    }

    @Override
    public void onSetURI() {

    }

    @Override
    public void onPlayCompleted() {

    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void onActionSuccess(Action action) {

    }

    @Override
    public void onActionFail(String actionCallback, UpnpResponse response, String cause) {

    }

    @Override
    public void onExceptionHappend(Action actionCallback, String mTitle, String cause) {

    }

    private void showLoader(String msg){

        if (loaderMsg == null || loaderMsg.isEmpty())
            return;
        if (progressDialog==null)
            progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void closeLoader(){
        if (progressDialog!=null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void updateAlbumArt(SceneObject mCurrentSceneObject){
        if (mCurrentSceneObject.getCurrentSource() == 14
                || mCurrentSceneObject.getCurrentSource() == 19
                || mCurrentSceneObject.getCurrentSource() == 0
                || mCurrentSceneObject.getCurrentSource() == 12
                || mCurrentSceneObject.getCurrentSource() == Constants.GCAST_SOURCE) {
            mRounderAlbumArt.setImageResource(R.mipmap.album_art);
            setDefaultBlurAlbumArt();
            return;
        }

        String album_url ="";
        if (mCurrentSceneObject.getCurrentSource() != 14
                && mCurrentSceneObject.getCurrentSource() != 19
                && mCurrentSceneObject.getCurrentSource() != Constants.GCAST_SOURCE) {
            if (mCurrentSceneObject.getAlbum_art() != null
                    && mCurrentSceneObject.getAlbum_art().equalsIgnoreCase("coverart.jpg")) {
                album_url = "http://" + mCurrentSceneObject.getIpAddress() + "/" + "coverart.jpg";

                        /* If Track Name is Different just Invalidate the Path
                        * And if we are resuming the Screen(Screen OFF and Screen ON) , it will not re-download it */
                boolean mInvalidated = mInvalidateTheAlbumArt(mCurrentSceneObject, album_url);
                LibreLogger.d(this, "Invalidated the URL " + album_url + " Status " + mInvalidated);

                final String finalAlbum_url = album_url;
                setDefaultBlurAlbumArt();
                PicassoTrustCertificates.getInstance(getActivity()).load(album_url)
                        .error(R.mipmap.album_art).placeholder(R.mipmap.album_art)
                        .into(mRounderAlbumArt, new Callback() {
                            @Override
                            public void onSuccess() {
                                blurImage(mRounderAlbumArt.getDrawable());
                                blurBackground(finalAlbum_url);
                                //extractPaletteAndSetSeekbarProgressColor(mRounderAlbumArt.getDrawable());
                            }

                            @Override
                            public void onError() {
                                setDefaultBlurAlbumArt();
                            }
                        });
            } else if (mCurrentSceneObject.getAlbum_art() == null ||
                    "".equals(mCurrentSceneObject.getAlbum_art().trim())) {
                mRounderAlbumArt.setImageDrawable(getResources().getDrawable(R.mipmap.album_art));
                setDefaultBlurAlbumArt();
                //extractPaletteAndSetSeekbarProgressColor(mRounderAlbumArt.getDrawable());
            } else {
                album_url = mCurrentSceneObject.getAlbum_art();
                boolean mInvalidated = mInvalidateTheAlbumArt(mCurrentSceneObject, album_url);
                LibreLogger.d(this, "Invalidated the URL " + album_url + " Status " + mInvalidated);
                if (!album_url.trim().equalsIgnoreCase("")) {
                    final String finalAlbum_url1 = album_url;
                    setDefaultBlurAlbumArt();
                    PicassoTrustCertificates.getInstance(getActivity()).load(album_url).placeholder(R.mipmap.album_art)
                                /*.memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)*/
                            .error(R.mipmap.album_art)
                            .into(mRounderAlbumArt, new Callback() {
                                @Override
                                public void onSuccess() {
                                    blurImage(mRounderAlbumArt.getDrawable());
                                    blurBackground(finalAlbum_url1);
                                    //extractPaletteAndSetSeekbarProgressColor(mRounderAlbumArt.getDrawable());
                                }

                                @Override
                                public void onError() {
                                    setDefaultBlurAlbumArt();
                                }
                            });
                }
            }
        } else {
            mRounderAlbumArt.setImageResource(R.mipmap.album_art);
            setDefaultBlurAlbumArt();
            /*PicassoTrustCertificates.getInstance(getActivity())
                    .load(R.mipmap.album_art)
                    .placeholder(R.mipmap.album_art)
                    .memoryPolicy(MemoryPolicy.NO_STORE);*/
        }
    }

    private void updateAlbumArt(LSSDPNodes lssdpNode){
        if (lssdpNode.getCurrentSource() == MIDCONST.GCAST_SOURCE) {
            mRounderAlbumArt.setImageResource(R.mipmap.album_art);
            setDefaultBlurAlbumArt();
        }
    }
}
