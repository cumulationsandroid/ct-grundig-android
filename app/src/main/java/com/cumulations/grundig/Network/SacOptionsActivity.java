/*
package com.cumulations.grundig.Network;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.DMRDeviceListenerForegroundService;
import com.cumulations.grundig.DeviceDiscoveryActivity;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.Network.PlayTestSoundForChromeCastDevice;
import com.cumulations.grundig.Network.SacActivityScreenForLaunchWifiSettings;
import com.cumulations.grundig.Network.WifiConnection;
import com.cumulations.grundig.R;
import com.cumulations.grundig.SacSettingsOption;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.SplashScreenActivity;
import com.cumulations.grundig.util.LibreLogger;

import org.eclipse.jetty.server.handler.ContextHandler;

import me.relex.circleindicator.CircleIndicator;

import static com.cumulations.grundig.R.id.button;
import static com.cumulations.grundig.R.id.container;
import static com.cumulations.grundig.R.id.view;
import static com.cumulations.grundig.R.id.viewpager;
import static junit.runner.Version.id;
import static org.seamless.xhtml.XHTML.ELEMENT.object;
import static org.seamless.xhtml.XHTML.ELEMENT.tr;

public class SacOptionsActivity  extends AppCompatActivity {

    int[] mResources = {
            R.layout.add_new_device,
            R.layout.add_new_device2

    };
    private OnClickListener backClickListner;
     ViewPager viewPager;
    private CircleIndicator mCircleIndicator;
     private Button nextButton;
    boolean isUserComingBackFromWiFiSettings=false;
    ProgressDialog mProgressDialog;
    private String mCurrentSsidItGotConnected;
    protected AlertDialog alert;
    private WifiConfigurationStepsPageAdapter wifiConfigurationStepsPageAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sac_options);

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        mCircleIndicator = (CircleIndicator)findViewById(R.id.viewpagerindicator);
         wifiConfigurationStepsPageAdapter = new WifiConfigurationStepsPageAdapter(this);
        viewPager.setAdapter(wifiConfigurationStepsPageAdapter);
        TextView backButton= (TextView) findViewById(R.id.back);
        backButton.setOnClickListener(

                backClickListner=new OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        {
                            onBackPressed();
                        }
                    }
                });



    }

    public class WifiConfigurationStepsPageAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        Button button;


        public WifiConfigurationStepsPageAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView;
            if (position%2==0) {
                itemView = mLayoutInflater.inflate(R.layout.add_new_device, container, false);
                nextButton = (Button)itemView.findViewById(R.id.nextButton);
                nextButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewPager != null) {
                            viewPager.setCurrentItem(1);
                        }
                    }
                });
            }
            else
            {
                itemView = mLayoutInflater.inflate(R.layout.add_new_device2, container, false);
                 button = (Button) itemView.findViewById(R.id.play_new_button);
                button.setTag(1);

                if(mCurrentSsidItGotConnected.contains(Constants.WAC_SSID))
                      button.setText("Next");



                    button.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(mCurrentSsidItGotConnected.contains(Constants.WAC_SSID)) {
                                Intent newIntent=new Intent(SacOptionsActivity.this, PlayTestSoundForChromeCastDevice.class);
                                startActivity(newIntent);
                                }
                                else{
                                    showLoader("Settings");
                                    isUserComingBackFromWiFiSettings = true;

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            closeLoader();
                            */
/* Giving User Access to Connect to Specific WAC_SSIDXXXX*//*

                                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                        }
                                    }, 1000);
                                }
                        }
                    });


            }
            container.addView(itemView);

            return itemView;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }


        public Button getButton(){
            return button;
        }
    }

    public String getconnectedSSIDname(Context mContext) {
        WifiManager wifiManager;
        wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        LibreLogger.d(this, "getconnectedSSIDname wifiInfo = " + wifiInfo.toString());
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }


        return ssid;
    }

    @Override
    public void onBackPressed() {

        if(viewPager!=null && viewPager.getCurrentItem()==0) {
            finish();
        }else {
            viewPager.setCurrentItem(0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mCurrentSsidItGotConnected = getconnectedSSIDname(SacOptionsActivity.this);
        if ( isUserComingBackFromWiFiSettings &&   !mCurrentSsidItGotConnected.contains(Constants.WAC_SSID)
                && !mCurrentSsidItGotConnected.endsWith(".d")
                && !LibreApplication.activeSSID.equalsIgnoreCase(mCurrentSsidItGotConnected)) {


            if (!SacOptionsActivity.this.isFinishing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SacOptionsActivity.this);
                alert = null;
                builder.setMessage(getResources().getString(R.string.title_error_connection) + "( " + mCurrentSsidItGotConnected + ")" +
                        " \n" + getString(R.string.networkChangeRestartApp))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                mCurrentSsidItGotConnected = "";
                                restartApp(SacOptionsActivity.this);
                                alert.dismiss();
                            }
                        });
                if (alert == null) {
                    alert = builder.show();
                    TextView messageView = (TextView) alert.findViewById(android.R.id.message);
                    messageView.setGravity(Gravity.CENTER);
                }

                alert.show();

            }


        }

        if(viewPager!=null && mCurrentSsidItGotConnected.contains(Constants.WAC_SSID))
        {
           if( wifiConfigurationStepsPageAdapter.button!=null)
               wifiConfigurationStepsPageAdapter.button.setText("Next");

        }
        */
/*else if (mCurrentSsidItGotConnected.contains(Constants.WAC_SSID)){

            Intent newIntent=new Intent(SacOptionsActivity.this, PlayTestSoundForChromeCastDevice.class);
            startActivity(newIntent);

        }*//*



    }


    */
/* Created by Karuna, To fix the RestartApp Issue
    * * Till i HAave to Analyse more on this code *//*

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void restartApp(Context context) {
        Log.d("NetworkChanged", "App is Restarting");
        //finish();
        */
/* Stopping ForeGRound Service Whenwe are Restarting the APP *//*

        Intent in = new Intent(SacOptionsActivity.this, DMRDeviceListenerForegroundService.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(in);

        Intent mStartActivity = new Intent(context, SplashScreenActivity.class);
                */
/*sending to let user know that app is restarting*//*

        mStartActivity.putExtra(SplashScreenActivity.APP_RESTARTING, true);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 200, mPendingIntent);

        */
/* * Finish this activity, and tries to finish all activities immediately below it
     * in the current task that have the same affinity.*//*

        ActivityCompat.finishAffinity(this);
        */
/* Killing our Android App with The PID For the Safe Case *//*

        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        //System.exit(0);

    }


    public void showLoader(final String msg) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog == null) {
                    mProgressDialog = ProgressDialog.show(SacOptionsActivity.this, getResources().getString(R.string.loading), getResources().getString(R.string.loading) + "...", true, true, null);
                }
                mProgressDialog.setCancelable(false);
                if (!mProgressDialog.isShowing()) {
                    if (!(SacOptionsActivity.this.isFinishing())) {
                        mProgressDialog = ProgressDialog.show(SacOptionsActivity.this, getResources().getString(R.string.loading), getResources().getString(R.string.loading) + "...", true, true, null);
                    }
                }

            }
        });
    }

    public void closeLoader() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    if (!(SacOptionsActivity.this.isFinishing())) {
                        mProgressDialog.setCancelable(false);
                        mProgressDialog.dismiss();
                        mProgressDialog.cancel();
                    }
                }
            }
        });

    }

}
*/
