package com.cumulations.grundig.Network;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.cumulations.grundig.ActiveScenesListActivity;
import com.cumulations.grundig.AddNewSpeakerToWifiHelpScreenActivity;
import com.cumulations.grundig.R;
import com.cumulations.grundig.serviceinterface.LSDeviceClient;
import com.cumulations.grundig.util.LibreLogger;
import com.skyfishjy.library.RippleBackground;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PlayTestSoundForChromeCastDevice extends AppCompatActivity {
    Button btnPlayTestSound ;
    private ProgressDialog mProgressDialog;
    private RippleBackground mRippleBackground;
    private int userPressingNoCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_test_sound_for_chrome_cast_device);
        mRippleBackground = (RippleBackground)findViewById(R.id.view);
        btnPlayTestSound = (Button) findViewById(R.id.btnPlaySound);
        btnPlayTestSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRippleBackground.startRippleAnimation();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        playTestSound();
                    }
                }, 500);
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent i = new Intent(PlayTestSoundForChromeCastDevice.this, AddNewSpeakerToWifiHelpScreenActivity.class);
        startActivity(i);
        finish();
        /*Intent intent = new Intent(PlayTestSoundForChromeCastDevice.this, NewSacActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();*/
    }

    private void playTestSound() {
        LSDeviceClient lsDeviceClient = new LSDeviceClient();
        LSDeviceClient.DeviceNameService playTestSound = lsDeviceClient.getDeviceNameService();
        playTestSound.playTestSound(new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRippleBackground.stopRippleAnimation();
                        showAlertForSucessfullPost();
                    }
                }, 1000);
            }

            @Override
            public void failure(RetrofitError error) {
                /*btnPlayTestSound.performClick();*/
                showAlertForSucessfullPost();
            }
        });


    }
    public String getconnectedSSIDname(Context mContext) {
        WifiManager wifiManager;
        wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        LibreLogger.d(this, "getconnectedSSIDname wifiInfo = " + wifiInfo.toString());
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }


        return ssid;
    }
    AlertDialog mAlertForSuccessfullPost;
    AlertDialog alertUserToIncreaseSpeakerVolume;

    private void showAlertForUserToIncreaseSpeakerVolume() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                PlayTestSoundForChromeCastDevice.this);

        // set title
        alertDialogBuilder.setTitle(R.string.configuration_help);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.increase_speaker_volume)
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertUserToIncreaseSpeakerVolume.cancel();
                    }
                });
        if (alertUserToIncreaseSpeakerVolume == null) {
            alertUserToIncreaseSpeakerVolume = alertDialogBuilder.create();
        }
        alertUserToIncreaseSpeakerVolume.show();
    }
    private void showAlertForSucessfullPost() {
        if(!PlayTestSoundForChromeCastDevice.this.isFinishing()) {


            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    PlayTestSoundForChromeCastDevice.this);

            // set title
            alertDialogBuilder.setTitle(R.string.play_test);

            // set dialog message
            alertDialogBuilder
                    .setMessage(R.string.did_you_hear_the_sound)
                    .setCancelable(false)
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (userPressingNoCount == 2) {
                                userPressingNoCount = 0;
                                showAlertForUserToIncreaseSpeakerVolume();
                            } else {
                                userPressingNoCount++;
                            }
                            mRippleBackground.stopRippleAnimation();
                            mAlertForSuccessfullPost.cancel();
                        }
                    })
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mAlertForSuccessfullPost.cancel();
                            getTheDeviceName();

                        }
                    });
            if (mAlertForSuccessfullPost == null)
                mAlertForSuccessfullPost = alertDialogBuilder.create();
            mAlertForSuccessfullPost.show();
        }
    }


    public void showLoader(final String msg) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog == null) {
                    mProgressDialog = ProgressDialog.show(PlayTestSoundForChromeCastDevice.this, getResources().getString(R.string.loading), getResources().getString(R.string.loading) + "...", true, true, null);
                }
                mProgressDialog.setCancelable(false);
                if (!mProgressDialog.isShowing()) {
                    if (!(PlayTestSoundForChromeCastDevice.this.isFinishing())) {
                        mProgressDialog = ProgressDialog.show(PlayTestSoundForChromeCastDevice.this, getResources().getString(R.string.loading), getResources().getString(R.string.loading) + "...", true, true, null);
                    }
                }

            }
        });
    }

    public void closeLoader() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    if (!(PlayTestSoundForChromeCastDevice.this.isFinishing())) {
                        mProgressDialog.setCancelable(false);
                        mProgressDialog.dismiss();
                        mProgressDialog.cancel();
                    }
                }
            }
        });

    }



    private void getTheDeviceName() {
        showLoader("Getting Device Name");

        LSDeviceClient lsDeviceClient = new LSDeviceClient();
        LSDeviceClient.DeviceNameService playTestSound = lsDeviceClient.getDeviceNameService();
        playTestSound.getSacDeviceName(new Callback<String>() {


            @Override
            public void success(String s, Response response) {

                Intent ssid = new Intent(PlayTestSoundForChromeCastDevice.this, SAC_WiFiListingActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                String mSsid = getconnectedSSIDname(PlayTestSoundForChromeCastDevice.this);
                ssid.putExtra("DeviceIP", "192.168.43.1");
                ssid.putExtra("activity", "SacListActivity");
                ssid.putExtra("DeviceSSID", mSsid);
                ssid.putExtra("activityName", "PlayTest");
                ssid.putExtra("DeviceName", s);

                startActivity(ssid);
                finish();
                closeLoader();

            }

            @Override
            public void failure(RetrofitError error) {
/*btnPlayTestSound.performClick();*/
                showAlertForSucessfullPost();
            }
        });
    }


    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
