package com.cumulations.grundig.Network;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.DeviceDiscoveryActivity;
import com.cumulations.grundig.Gcast;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.LibreWebViewActivity;
import com.cumulations.grundig.NewManageDevices.DeviceData;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.ShowLoader;

public class LSSDPDeviceNetworkSettings extends DeviceDiscoveryActivity implements AdapterView.OnItemSelectedListener, LibreDeviceInteractionListner {

    Button btnSave;
    RelativeLayout btnNetworkSettings;
    RelativeLayout btnAdvanceSettings;
    RelativeLayout btnOpenSettingsInCast;
    private Button networkSettingsButton;
    private Button openSettingsInCastButton;
    private Button AdvanceSettingsButton;
    TextView mBack;
    Spinner mSpinner;
    Spinner mPresetSpinner;
    String deviceSSID, speakerIpaddress, activityName;
    String DeviceName;
    EditText deviceName;
    TextView ipAddress, firmwareVersion, title,hostVersion,networkSettings;
    ImageView btnDeviceName;
    String mAudioOutput = "";
    String mAudioPreset = "";
    boolean mSpinnerChanged = false;
    boolean mDeviceNameChanged = false;
    private Dialog m_progressDlg;
    private AppCompatSeekBar seekBar;
    private CoordinatorLayout rootLayout;

    public final int STEREO = 0;
    LSSDPNodes deviceNode;
    @Override
    protected void onResume() {
        super.onResume();
        speakerIpaddress = getIntent().getStringExtra("ip_address");
        activityName = getIntent().getStringExtra("activityName");
        LibreLogger.d(this,"IPADDRESS " + speakerIpaddress);
        LSSDPNodes mNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(speakerIpaddress);
        if(mNode!=null)
            setViews(mNode);

        //enableOrDisableSerialNumber(LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(speakerIpaddress));
    }

    public final int LEFT = 1;
    public final int RIGHT = 2;

    RelativeLayout ledController;

    private int LED_SUCCESS = 12;
    private int LED_FAILURE = 13;
    private int LED_TIMEOUT = 14;
    private int TIMEOUT = 4000;

    private ImageView loading;
    private TextView loadingText;

    ScanningHandler mScanHandler = ScanningHandler.getInstance();

    public void StartLSSDPScan() {

        final LibreApplication application = (LibreApplication) getApplication();
        application.getScanThread().clearNodes();
        application.getScanThread().UpdateNodes();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();
            }
        }, 150);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 650);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1150);
    }


    Handler ledHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == LED_SUCCESS) {
                closeLoader();
                //ledHandler.removeMessages(LED_TIMEOUT);
                //ledController.setVisibility(View.VISIBLE);
            }

            if (msg.what == LED_FAILURE) {
                closeLoader();
                /*ledHandler.removeMessages(LED_TIMEOUT);
                ledController.setVisibility(View.GONE);*/
            }

            if (msg.what == LED_TIMEOUT) {
                closeLoader();
                //ledController.setVisibility(View.GONE);
            }
        }
    };
    private void setViews(LSSDPNodes mNode){

        if(activityName==null)
            activityName="default";

        deviceName.setText(mNode.getFriendlyname());
        DeviceName = deviceName.getText().toString();
        deviceName.setText(mNode.getFriendlyname());
        if (mNode!=null && mNode.getVersion() != null) {
            String dutExistingFirmware = mNode.getVersion();

            String[] arrayString = dutExistingFirmware.split("\\.");
            dutExistingFirmware = dutExistingFirmware.substring(0, dutExistingFirmware.indexOf('.'));
            String dutExistingHostVersion = arrayString[1].replaceAll("[a-zA-z]","");
            /*String mFirmwareVersionToDisplay = dutExistingFirmware.replaceAll("[a-zA-z]", "")+"."+arrayString[1]+"."+
                    arrayString[2];*/
            firmwareVersion.setText(dutExistingFirmware.replaceAll("[a-zA-z]", ""));
            hostVersion.setText(dutExistingHostVersion);
        }
        else {
            firmwareVersion.setVisibility(View.GONE);
            hostVersion.setVisibility(View.GONE);
        }

        enableOrDisableEditDeviceNameButton();
        deviceName.setEnabled(false);
        ipAddress.setText(speakerIpaddress);
        try {
            if (mNode != null) {
                if (Integer.valueOf(mNode.getSpeakerType()) == STEREO) {
                    //mSpinner.setSelection(0);
                    seekBar.setProgress(50);
                } else if (Integer.valueOf(mNode.getSpeakerType()) == LEFT) {
                    //mSpinner.setSelection(1);
                    seekBar.setProgress(0);
                } else if (Integer.valueOf(mNode.getSpeakerType()) == RIGHT) {
                    //mSpinner.setSelection(2);
                    seekBar.setProgress(100);
                }
            } else {
                //mSpinner.setSelection(0);
                seekBar.setProgress(0);
            }
        } catch (Exception e) {

        }
        if (activityName!=null && activityName.contains("ManageDevices")
                ||
                ( mNode!=null && !mNode.getNetworkMode().equalsIgnoreCase("WLAN")
                )){
            //btnNetworkSettings.setClickable(false);
            //btnNetworkSettings.setVisibility(View.INVISIBLE);
            //networkSettings.setVisibility(View.GONE);
        }

        if(( mNode!=null &&( (!mNode.getNetworkMode().equalsIgnoreCase("WLAN"))
                || mNode.getgCastVerision()==null
        )
        )){
            btnOpenSettingsInCast.setVisibility(View.GONE);
            btnAdvanceSettings.setVisibility(View.GONE);
        }else{
            btnOpenSettingsInCast.setVisibility(View.VISIBLE);
            btnAdvanceSettings.setVisibility(View.VISIBLE);
        }

    }
    public void enableOrDisableSerialNumber(LSSDPNodes node){
        /*LSSDPNodes node = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(speakerIpaddress);*/
        //networkSettings = (TextView) findViewById(R.id.tvNetworkSettings);
        /*if(node.getSerialNumber()!=null)
            networkSettings.setText("Serial Number");
        else
            networkSettings.setVisibility(View.GONE);*//*
        if(node!=null && node.getgCastVerision()==null){
            if(node.getSerialNumber()==null) {
                networkSettings.setVisibility(View.GONE);
            }
        }else{
            if(node!=null && node.getSerialNumber()!=null) {
                networkSettings.setText("Serial Number");
            }

        }*/

    }
    private void enableOrDisableEditDeviceNameButton(){
        LSSDPNodes mNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(speakerIpaddress);

        if(mNode!=null &&
                mNode.getgCastVerision()!=null) {
            /*we need to show toast so don't hide*/
            btnDeviceName.setVisibility(View./*INVISIBLE*/VISIBLE);
        }else{
            btnDeviceName.setVisibility(View.VISIBLE);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speaker_settings_layout);
        loading = (ImageView)findViewById(R.id.loading);
        loadingText = (TextView)findViewById(R.id.loadingText);
        loadingText.setText(getString(R.string.pleaseWait));
        rootLayout = (CoordinatorLayout)findViewById(R.id.rootLayout);
        mBack = (TextView)findViewById(R.id.back);
        seekBar = (AppCompatSeekBar)findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser)
                    return;
                TransitionManager.beginDelayedTransition(rootLayout, new ChangeBounds());
                if (progress < 40) {
                    seekBar.setProgress(0);
                } else if (progress > 60) {
                    seekBar.setProgress(100);
                } else {
                    seekBar.setProgress(50);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                LUCIControl luci_ = new LUCIControl(speakerIpaddress);
                if (seekBar.getProgress() == 50) {
                    luci_.SendCommand(MIDCONST.MID_SPEAKER_TYPE, "SETSTEREO", LSSDPCONST.LUCI_SET);
                } else if (seekBar.getProgress() == 0) {
                    luci_.SendCommand(MIDCONST.MID_SPEAKER_TYPE, "SETLEFT", LSSDPCONST.LUCI_SET);
                } else if (seekBar.getProgress() == 100) {
                    luci_.SendCommand(MIDCONST.MID_SPEAKER_TYPE, "SETRIGHT", LSSDPCONST.LUCI_SET);
                }
            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.network_config_text).setSelected(true);
        findViewById(R.id.google_cast_settings_text).setSelected(true);

        Intent myIntent = getIntent(); // gets the previously created intent
        deviceSSID = myIntent.getStringExtra("DeviceName");
        speakerIpaddress = myIntent.getStringExtra("ip_address");
        activityName = myIntent.getStringExtra("activityName");
        if(activityName==null)
            activityName="default";

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        registerForDeviceEvents(this);

        showLoader();

        /*checking led status*/
        LUCIControl luciControl = new LUCIControl(speakerIpaddress);
        luciControl.sendAsynchronousCommand();
        luciControl.SendCommand(208, "READ_LEDControl", 2);
        LibreLogger.d(this, "bhargav LED SENT");
        luciControl.SendCommand(208, "READ_audiopreset", 2);
        LibreLogger.d(this, "bhargav AP SENT");
        luciControl.SendCommand(145, null, LSSDPCONST.LUCI_GET);

        new LUCIControl(speakerIpaddress).SendCommand(MIDCONST.SERIAL_NUMBER,null,LSSDPCONST.LUCI_GET);
        ledHandler.sendEmptyMessageDelayed(LED_TIMEOUT, TIMEOUT);

        //ledController = (RelativeLayout) findViewById(R.id.led_control_layout);
        /*ledController.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent ledIntent = new Intent(LSSDPDeviceNetworkSettings.this, LedActivity.class);
                ledIntent.putExtra("DeviceName", deviceSSID);
                ledIntent.putExtra("ip_address", speakerIpaddress);
                startActivity(ledIntent);


            }
        });*/


         deviceNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(speakerIpaddress);
       // enableOrDisableSerialNumber(deviceNode);
        btnSave = (Button) findViewById(R.id.btnSaveNetworkSettings);
        /*btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deviceName.isEnabled()) {
                    new AlertDialog.Builder(LSSDPDeviceNetworkSettings.this)
                            .setTitle(getString(R.string.sceneNameChanging))
                            .setMessage(getString(R.string.sceneNameChangingMsg))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    goNext();

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert)
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                } else
                    goNext();

            }
        });*/
        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
        final LSSDPNodes node = mNodeDB.getTheNodeBasedOnTheIpAddress(speakerIpaddress);
        title = (TextView) findViewById(R.id.devicenameText);
        title.setText(getString(R.string.deviceName));
        ipAddress = (TextView) findViewById(R.id.ipAddressValue);
        firmwareVersion = (TextView) findViewById(R.id.systemFirmwareValue);
        hostVersion = (TextView) findViewById(R.id.hostFirmwareValue);
        //networkSettings = (TextView) findViewById(R.id.tvNetworkSettings);
        deviceName = (EditText) findViewById(R.id.deviceNameValue);
        btnDeviceName = (ImageView) findViewById(R.id.editImage);
        /*this is the data for Audio Presets*/
        final String presetSpinnerData[] = getResources().getStringArray(R.array.audio_preset_array);
        //mPresetSpinner = (Spinner) findViewById(R.id.audio_preset_spinner);
        ArrayAdapter<String> presetDataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, presetSpinnerData);
        presetDataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        //mPresetSpinner.setAdapter(presetDataAdapter);


        /*Speaker type data*/
        final String spinnerData[] = getResources().getStringArray(R.array.audio_output_array);
        mSpinner = (Spinner) findViewById(R.id.ssidSpinner);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, spinnerData);
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        //mSpinner.setAdapter(dataAdapter);





        /*networkSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(node!=null && node.getSerialNumber()!=null){
                    displayBarCode(node.getSerialNumber());
               }
            }
        });*/

        //speakerIpaddress
        btnAdvanceSettings = (RelativeLayout) findViewById(R.id.googlecastSettings_layout);
        AdvanceSettingsButton = (Button)findViewById(R.id.googleCastSettingsButton);
        btnOpenSettingsInCast= (RelativeLayout) findViewById(R.id.opensettings_layout);
        openSettingsInCastButton = (Button)findViewById(R.id.opensettingsButton);

        findViewById(R.id.open_settings_googlecast).setSelected(true);

        /* LS9 App Will have disable Gcast Settings */
        if (deviceNode!=null&& deviceNode.getgCastVerision()!=null) {
            btnAdvanceSettings.setVisibility(View.VISIBLE);
            btnOpenSettingsInCast.setVisibility(View.VISIBLE);
        }
        else {
            btnAdvanceSettings.setVisibility(View.INVISIBLE);
            btnOpenSettingsInCast.setVisibility(View.INVISIBLE);
        }

        AdvanceSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(googleTosCheckIsSuccess()) {
                    Intent ledIntent = new Intent(LSSDPDeviceNetworkSettings.this, Gcast.class);
                    ledIntent.putExtra("DeviceName", deviceSSID);
                    ledIntent.putExtra("ip_address", speakerIpaddress);
                    startActivity(ledIntent);
                }


            }
        });

        openSettingsInCastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // deep linking
                if (googleTosCheckIsSuccess()){
                    // launchTheApp("com.google.android.apps.chromecast.app");
                    LibreLogger.d(this,"intenting to Chrome cast app. Deeplink-> DEVICE_SETTINGS");
                    launchTheApp("com.google.android.apps.chromecast.app","DEVICE_SETTINGS");

                }
            }
        });

        btnDeviceName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!deviceName.isEnabled()) {

                    LSSDPNodes mNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(speakerIpaddress);
                    if(false && mNode!=null && mNode.getgCastVerision()!=null) {
                        Toast.makeText(LSSDPDeviceNetworkSettings.this, getString(R.string.castDeviceNameChangeMsg), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    LibreLogger.d(this, "bhargav123 clicked");
                    btnDeviceName.setImageResource(R.drawable.ic_check_white_24dp);
                    deviceName.setClickable(true);
                    deviceName.setEnabled(true);
                    deviceName.setFocusableInTouchMode(true);
                    deviceName.setFocusable(true);
                    deviceName.setSelection(deviceName.getText().length());
                    deviceName.requestFocus();
                    //deviceName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cancwel, 0);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(deviceName, InputMethodManager.SHOW_IMPLICIT);
                } else {
                    LibreLogger.d(this,"bhargav123 clicked again");
                    btnDeviceName.setImageResource(R.drawable.ic_edit_white_24dp);
                    //deviceName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//                    deviceName.setClickable(false);
//                    deviceName.setEnabled(false);
                    if (mDeviceNameChanged) {
                        if (!deviceName.getText().toString().equals("")&& !deviceName.getText().toString().trim().equalsIgnoreCase("NULL")) {
                            if(deviceName.getText().toString().getBytes().length>50) {
                                new AlertDialog.Builder(LSSDPDeviceNetworkSettings.this)
                                        .setTitle(getString(R.string.deviceNameChanging))
                                        .setMessage(getString(R.string.deviceLength))
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                                return;
                            }else {
                                LUCIControl mLuci = new LUCIControl(speakerIpaddress);
                                mLuci.sendAsynchronousCommand();
                                mLuci.SendCommand(MIDCONST.MID_DEVNAME, toUpperCase(deviceName.getText().toString()), LSSDPCONST.LUCI_SET);

                                UpdateLSSDPNodeDeviceName(speakerIpaddress, deviceName.getText().toString());
                                deviceName.setClickable(false);
                                deviceName.setEnabled(false);
                            }
                        } else {
                            if (!(LSSDPDeviceNetworkSettings.this.isFinishing())) {
                                if (deviceName.getText().toString().equals("")) {
                                    new AlertDialog.Builder(LSSDPDeviceNetworkSettings.this)
                                            .setTitle(getString(R.string.deviceNameChanging))
                                            .setMessage(getString(R.string.deviceNameEmpty))
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            }).setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }
                            }
                        }


                    }
                }
            }
        });


        final LSSDPNodes mNode = mScanHandler.getLSSDPNodeFromCentralDB(speakerIpaddress);
        final int[] mPreviousLength = new int[1];
        final String[] mTextWatching = new String[1];
        deviceName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                Log.d("Karuna","After Text Changed");
                if(deviceName.getText().toString().getBytes().length==50 && deviceName.isEnabled()) {
                    Toast.makeText(LSSDPDeviceNetworkSettings.this, getString(R.string.deviceLengthReached), Toast.LENGTH_SHORT).show();

                }

                if(deviceName.getText().toString().getBytes().length>50) {
                    if(mTextWatching[0].isEmpty()||mTextWatching[0]==null){

                        deviceName.setText(utf8truncate(deviceName.getText().toString(),50));
                        deviceName.setSelection(mTextWatching[0].length());
                    }else {
                        deviceName.setText(toUpperCase(mTextWatching[0]));
                        deviceName.setSelection(mTextWatching[0].length());
                    }
                    Toast.makeText(LSSDPDeviceNetworkSettings.this,getString(R.string.deviceLength),Toast.LENGTH_SHORT).show();/*
                    LibreError error = new LibreError("Sorry!!!!", getString(R.string.deviceLength));
                    showErrorMessage(error);*/
                  /*  new AlertDialog.Builder(LSSDPDeviceNetworkSettings.this)
                            .setTitle(getString(R.string.deviceNameChanging))
                            .setMessage(getString(R.string.deviceLength))
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();

                                }
                            }).setIcon(android.R.drawable.ic_dialog_alert)
                            .show();*/
                }else {
                    mTextWatching[0] = deviceName.getText().toString();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                Log.d("Karuna","Before Text Changed");
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                Log.d("Karuna","On Text Changed");
                // if(! deviceName.isClickable()) {
                mDeviceNameChanged = true;
            }
        });

        deviceName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    try {
                        if (event.getRawX() >= (deviceName.getRight() - deviceName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            deviceName.setText("");
                            return true;
                        }
                    } catch (Exception e) {
                        //Toast.makeText(getApplication(), "dsd", Toast.LENGTH_SHORT).show();
                        LibreLogger.d(this, "ignore this log");
                    }
                }
                return false;
            }
        });




        /*mPresetSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2,
                                       long arg3) {
                //Toast.makeText(getApplicationContext(), "ItemSelected" + spinnerdata[i], Toast.LENGTH_SHORT).show();
                mAudioPreset = "" + (arg2 + 1);
//                mSpinnerChanged = true;

//                String item = (String) parent.getItemAtPosition(arg2);
//                ((TextView) parent.getChildAt(0)).setTextColor(Color.RED);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Toast.makeText(getApplicationContext(), "Nothing Item Selected", Toast.LENGTH_SHORT).show();
//                mSpinnerChanged = false;
            }
        });*/

        /*mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(), "ItemSelected" + spinnerdata[i], Toast.LENGTH_SHORT).show();
                mAudioOutput = spinnerData[i];
                mSpinnerChanged = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Toast.makeText(getApplicationContext(), "Nothing Item Selected", Toast.LENGTH_SHORT).show();
                mSpinnerChanged = false;
            }
        });*/

        btnNetworkSettings = (RelativeLayout) findViewById(R.id.networkConfiguration_layout);
        networkSettingsButton = (Button)findViewById(R.id.networkConfigurationButton);
        btnNetworkSettings.setClickable(true);

        if (mScanHandler.getconnectedSSIDname(this)== ScanningHandler.SA_MODE) {
            btnNetworkSettings.setVisibility(View.GONE);
            btnAdvanceSettings.setVisibility(View.GONE);
            btnOpenSettingsInCast.setVisibility(View.GONE);
        } else {
            btnNetworkSettings.setVisibility(View.VISIBLE);
        }
        networkSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ScanningHandler m_scanHandler = ScanningHandler.getInstance();
                final LSSDPNodes node = m_scanHandler.getLSSDPNodeFromCentralDB(speakerIpaddress);
                if (node==null){
                    return;
                }
                //fix for  Second : When the �NETWORK CONFIGURATION� has been pressed, screen should move to the gcast app to configure the device.
                if (node.getgCastVerision()!=null && false) {
                    callGcastAppBasedOnIpAddress();
                }else {
                    Intent ssid = new Intent(LSSDPDeviceNetworkSettings.this, SAC_WiFiListingActivity.class);
                    ssid.putExtra("DeviceIP", speakerIpaddress);
                    ssid.putExtra("DeviceSSID", DeviceName);
                    ssid.putExtra("DeviceName", DeviceName);
                    ssid.putExtra("activityName", "LSSDPDeviceNetworkSettings");
                    startActivity(ssid);
                }


                /*if(googleTosCheckIsSuccess()) {
                    Intent ledIntent = new Intent(LSSDPDeviceNetworkSettings.this, Gcast.class);
                    ledIntent.putExtra("DeviceName", deviceSSID);
                    ledIntent.putExtra("ip_address", speakerIpaddress);
                    startActivity(ledIntent);
                }*/

            }

        });




    }


    private String toUpperCase(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    /*private void displayBarCode(final String mSerialNumber) {
        Bitmap icon = createBarCode(mSerialNumber, BarcodeFormat.CODE_128,512,1024);
        if(icon==null)
            return;


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.barcodelayout,
                (ViewGroup) findViewById(R.id.goProDialoglayout));
        ImageView image = (ImageView) layout.findViewById(R.id.goProDialogImage);
        TextView serialText = (TextView) layout.findViewById(R.id.tvSerialTxt);
        serialText.setText("Sl.No. :"+ mSerialNumber);
        image.setImageBitmap(icon);
        imageDialog.setView(layout);
        imageDialog.setPositiveButton("OK", new DialogInterface.OnClickListener(){

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });


        imageDialog.create();
        imageDialog.show();
    }*/
    /*public  Bitmap createBarCode (String codeData, BarcodeFormat barcodeFormat, int codeHeight, int codeWidth) {

        try {
            if(codeData==null && codeData.length()==0){
                LibreError mError = new LibreError("","Serial Number is Empty");
                showErrorMessage(mError);
                return null;
            }

            Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel> ();
            ErrorCorrectionLevel put = hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

            Writer codeWriter;
            if (barcodeFormat == BarcodeFormat.QR_CODE) {
                codeWriter = new QRCodeWriter();
            } else if (barcodeFormat == BarcodeFormat.CODE_128) {
                codeWriter = new Code128Writer();
            } else {
                throw new RuntimeException ("Format Not supported.");
            }

            BitMatrix byteMatrix = codeWriter.encode (
                    codeData,
                    barcodeFormat,
                    codeWidth,
                    codeHeight,
                    hintMap
            );

            int width   = byteMatrix.getWidth ();
            int height  = byteMatrix.getHeight ();

            Bitmap imageBitmap = Bitmap.createBitmap (width, height, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < width; i ++) {
                for (int j = 0; j < height; j ++) {
                    imageBitmap.setPixel (i, j, byteMatrix.get (i, j) ? Color.BLACK: Color.WHITE);
                }
            }

            return imageBitmap;

        } catch (WriterException e) {
            e.printStackTrace ();
            return null;
        }
    }*/
    public  String utf8truncate(String input, int length) {
        StringBuffer result = new StringBuffer(length);
        int resultlen = 0;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            int charlen = 0;
            if (c <= 0x7f) {
                charlen = 1;
            } else if (c <= 0x7ff) {
                charlen = 2;
            } else if (c <= 0xd7ff) {
                charlen = 3;
            } else if (c <= 0xdbff) {
                charlen = 4;
            } else if (c <= 0xdfff) {
                charlen = 0;
            } else if (c <= 0xffff) {
                charlen = 3;
            }
            if (resultlen + charlen > length) {
                break;
            }
            result.append(c);
            resultlen += charlen;
        }
        return result.toString();
    }

    public void launchTheApp(String appPackageName) {

        Intent intent = getPackageManager().getLaunchIntentForPackage(appPackageName);
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else {
            redirectingToPlayStore(intent, appPackageName);
        }

    }

    public void launchTheApp(String appPackageName,String deepLinkExtension) {

        Intent intent = getPackageManager().getLaunchIntentForPackage(appPackageName);
        if (intent != null) {
            LibreLogger.d(this,"Chrome cast app is available in phone. Deeplink-> DEVICE_SETTINGS");
          /*  intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
            intent = new Intent(appPackageName+"."+deepLinkExtension);
            intent.putExtra(appPackageName+".extra.IP_ADDRESS",speakerIpaddress);
            startActivity(intent);

        } else {
            LibreLogger.d(this,"Chrome cast app is not available in phone, redirecting to playstore. Deeplink-> DEVICE_SETTINGS");
            redirectingToPlayStore(intent, appPackageName);
        }

    }
    public void redirectingToPlayStore(Intent intent, String appPackageName) {

        try {

            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + appPackageName));
            startActivity(intent);

        } catch (android.content.ActivityNotFoundException anfe) {

            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName));
            startActivity(intent);

        }

    }

    @Override
    public void onBackPressed() {

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        /* if the user is coming from nowplaying activity then we need to launch the ActiveScenesList */
        // super.onBackPressed();
        if (deviceName.isEnabled()) {
            new AlertDialog.Builder(LSSDPDeviceNetworkSettings.this)
                    .setTitle(getString(R.string.deviceNameChanging))
                    .setMessage(getString(R.string.deviceNameChangingMsg))
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            LSSDPDeviceNetworkSettings.super.onBackPressed();

                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert)
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        } else {
            super.onBackPressed();
        }

    }

    private void showLoader() {
        /*if (LSSDPDeviceNetworkSettings.this.isFinishing())
            return;
        if (m_progressDlg == null)
            m_progressDlg = ProgressDialog.show(LSSDPDeviceNetworkSettings.this, getString(R.string.notice), getString(R.string.loading), true, true, null);
        if (!m_progressDlg.isShowing()) {
            m_progressDlg.show();
        }*/
        ShowLoader.showLoader(loading, loadingText, this);
    }

    private void closeLoader() {
        /*if (m_progressDlg != null) {
            if (m_progressDlg.isShowing()) {
                m_progressDlg.dismiss();
            }
        }*/
        loading.clearAnimation();
        loading.setVisibility(View.GONE);
        loadingText.setVisibility(View.GONE);
    }

    public void UpdateLSSDPNodeDeviceName(String ipaddress, String mDeviceName) {

        LSSDPNodes mToBeUpdateNode = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);

        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
        if (mToBeUpdateNode != null) {
            mToBeUpdateNode.setFriendlyname(mDeviceName);
            mNodeDB.renewLSSDPNodeDataWithNewNode(mToBeUpdateNode);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        closeLoader();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterForDeviceEvents();
        closeLoader();
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        switch (position) {
            case 0:
                // Whatever you want to happen when the first item gets selected
                break;
            case 1:
                // Whatever you want to happen when the second item gets selected
                break;
            case 2:
                // Whatever you want to happen when the thrid item gets selected
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
     //   getMenuInflater().inflate(R.menu.menu_sac_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

    }

    @Override
    public void deviceGotRemoved(String ipaddress) {
    if (ipaddress!=null && speakerIpaddress!=null && ipaddress.equals(speakerIpaddress)){
       onBackPressed();
        }
    }

    @Override
    public void messageRecieved(NettyData dataRecived) {

        byte[] buffer = dataRecived.getMessage();
        String ipaddressRecieved = dataRecived.getRemotedeviceIp();

        LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());
        Log.d("LedStatus", "Message recieved for ipaddress " + ipaddressRecieved + "command is " + packet.getCommand());

        if (speakerIpaddress.equalsIgnoreCase(ipaddressRecieved)) {
            switch (packet.getCommand()) {
                case MIDCONST.SERIAL_NUMBER: {
                    LSSDPNodes mNode = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(dataRecived.getRemotedeviceIp());
                    String message = new String(packet.getpayload());
                    if (mNode != null) {
                        if (message != null && message.length() != 0) {
                            mNode.setSerialNumber(message);
                            LSSDPNodeDB.getInstance().renewLSSDPNodeDataWithNewNode(mNode);
                            enableOrDisableSerialNumber(mNode);
                        }
                    }
                }
                break;
                case 208:
                    String message = new String(packet.getpayload());
                    LibreLogger.d(this, "bhargav " + message);
                    String[] messageArray = message.split(":");

                    if (messageArray.length == 0 || messageArray.length == 1)
                        return;
                    String audioPreset = messageArray[0];
                    String status = messageArray[1];
                    if (audioPreset != null && audioPreset.equalsIgnoreCase("audiopreset") && !status.equalsIgnoreCase("0")) {
                        mPresetSpinner.setSelection(Integer.parseInt(status) - 1);
                        LibreLogger.d(this, messageArray[1] + "bhargav" + mPresetSpinner.getSelectedItem());
                        return;
                    }
                    if (status.equalsIgnoreCase("0")) {
                        ledHandler.sendEmptyMessage(LED_FAILURE);
                        return;
                    }
                    if (status.equalsIgnoreCase("1")) {
                        ledHandler.sendEmptyMessage(LED_SUCCESS);
                    }

                    break;


                case 145:
                    String audioMessage = new String(packet.getpayload());
                    Log.d("AudioPresetValue", "Received Data is - " + audioMessage);
                    break;

                case MIDCONST.MID_DEVICE_STATE_ACK:
                    String mMessage = new String(packet.getpayload());
                    Log.d("MID_DEVICE_STATE_ACK", "mmsg = " + mMessage);
                    String[] mMessageArray = mMessage.split(",");
                    if (mMessageArray.length > 0) {
                        if (mMessageArray[1].equalsIgnoreCase("STEREO")) {
                            seekBar.setProgress(50);
                        }
                        if (mMessageArray[1].equalsIgnoreCase("LEFT")) {
                            seekBar.setProgress(0);
                        }
                        if (mMessageArray[1].equalsIgnoreCase("RIGHT")) {
                            seekBar.setProgress(100);
                        }
                    }
                    break;

                case MIDCONST.MID_SCENE_NAME: {
                    if (packet.getCommandStatus() == 1
                            && packet.getCommandType() == 2) {
                        return;
                    }
                    String msg = new String(packet.getpayload());
                    LSSDPNodes mNode = mScanHandler.getLSSDPNodeFromCentralDB(speakerIpaddress);
                    if (mNode != null && !msg.isEmpty()) {
                        Log.d("MID_SCENE_NAME", "device name = " + msg);
                        mNode.setFriendlyname(msg);
                        deviceName.setText(toUpperCase(msg));
                    }
                }
                break;
            }
        }

    }

    private boolean googleTosCheckIsSuccess() {
        return getSharedPreferences(Constants.SHOWN_GOOGLE_TOS_PREF,MODE_PRIVATE)
                .getBoolean(Constants.SHOWN_GOOGLE_TOS,false);
    }
}
