package com.cumulations.grundig.Network;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.R;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.netty.BusProvider;
import com.cumulations.grundig.netty.RemovedLibreDevice;

import static org.seamless.xhtml.XHTML.ELEMENT.li;
import static org.seamless.xhtml.XHTML.ELEMENT.th;

/**
 * Created by cumulations on 7/8/17.
 */

public class WiFiSelectionDialogFragment extends DialogFragment {

    int style = DialogFragment.STYLE_NO_TITLE;

    private TextView wiFiName;
    private String ssid;
    private String deviceIp;
    private EditText mPassword;
    public Button mSavebtn,mCancel;
    DialogfragmentListner listner;


    public void setPassword(String password) {
        this.mPassword.setText(password);
    }

    public EditText getPassword() {
        return mPassword;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ssid = getArguments().getString("WiFiName");
        deviceIp = getArguments().getString("ipaddress");
        setStyle(style, R.style.MyTheme_DeezerDialog);
    }

    public void setListner(DialogfragmentListner listner) {
        this.listner = listner;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.cumulations_wifi_selection_dialog_layout, container);

        wiFiName = (TextView)v.findViewById(R.id.WiFiConfigureName);
        wiFiName.setText(ssid);

        mPassword = (EditText)v.findViewById(R.id.password);
        mSavebtn = (Button)v.findViewById(R.id.savebtn);
        mCancel = (Button)v.findViewById(R.id.cancelbtn);


        mPassword.setText(getSSIDPasswordFromSharedPreference(ssid));


        mSavebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WifiConnection.getInstance().setMainSSID(ssid);
                WifiConnection.getInstance().setMainSSIDPwd(getPassword().getText().toString());
                WifiConnection.getInstance().setMainSSIDSec("WPA-PSK");


                 listner.clickedFromDialogFragment(R.id.savebtn,mPassword.getText().toString());
                closeKeyboardFromFocus(mPassword);
                 dismiss();

           }
        });

        mCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                listner.clickedFromDialogFragment(R.id.cancelbtn,"");
                closeKeyboardFromFocus(mPassword);
                dismiss();

            }
        });
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return v;
  /*It will get password for corresponding ssid */

    }

    private String getSSIDPasswordFromSharedPreference(String deviceSSID) {

        try {
            SharedPreferences pref = getActivity().getApplicationContext()

                    .getSharedPreferences("Your_Shared_Prefs", Context.MODE_PRIVATE);

            return pref.getString(deviceSSID, "");
        }catch (Exception e){
            return "";
        }
    }

    private void closeKeyboardFromFocus(View view){
//        View view = this.getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



}
