package com.cumulations.grundig.Network;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.R;
import com.cumulations.grundig.SourceOptions.CumulationsCastingSourcesRecyclerAdapter;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by cumulations on 18/8/17.
 */

public class SSIDRecyclerAdapter extends RecyclerView.Adapter<SSIDRecyclerAdapter.MyViewHolder> {

    private ArrayList<String> ssidList;
    private Context context;
    private TreeMap<String, String> hashMap;
    private String deviceIp;


    public void setUnsortedHashMap(TreeMap<String, String> hashMap) {
        this.hashMap = hashMap;
    }

    SSIDRecyclerAdapter(Context context, ArrayList<String> ssidList, String deviceIp) {
        this.context = context;
        this.ssidList = ssidList;
        this.deviceIp = deviceIp;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.ssid);
        }
    }

    @Override
    public SSIDRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cumulations_ssid_layout, parent, false);


        SSIDRecyclerAdapter.MyViewHolder myViewHolder = new SSIDRecyclerAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(SSIDRecyclerAdapter.MyViewHolder myViewHolder, final int position) {
        TextView name = myViewHolder.name;
        name.setText(ssidList.get(position));
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = ssidList.get(position);
                if (hashMap.get(item) != "NONE") {

                    FragmentManager fm = ((SAC_WiFiListingActivity) context).getSupportFragmentManager();

                    Bundle args = new Bundle();
                    args.putString("WiFiName", item);
                    args.putString("ipaddress", deviceIp);
                    WiFiSelectionDialogFragment fragment = new WiFiSelectionDialogFragment();
                    fragment.setArguments(args);
                    fragment.setListner((SAC_WiFiListingActivity) context);
                    fragment.show(fm, "wifiselection");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return ssidList.size();
    }
}
