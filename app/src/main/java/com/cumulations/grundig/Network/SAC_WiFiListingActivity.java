package com.cumulations.grundig.Network;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.ActiveScenesListActivity;
import com.cumulations.grundig.DeviceDiscoveryActivity;
import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.Ls9Sac.ConnectingToMainNetwork;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.netty.BusProvider;
import com.cumulations.grundig.serviceinterface.LSDeviceClient;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.ShowLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.R.attr.fragment;
import static org.seamless.xhtml.XHTML.ELEMENT.th;


/**

 * Created by karunakaran on 8/2/2015.

 */

public class SAC_WiFiListingActivity extends DeviceDiscoveryActivity implements DialogfragmentListner{


    String deviceSSID = "";
    String deviceNameStr="";

    String deviceIp = "";

    String activityName = "";

    public ProgressDialog mProgressDialog;

    HttpURLConnection httpPostingDataUrlConnection = null;
    private WifiConnection wifiConnect;
    private RecyclerView recyclerView;
    private TextView mBack;
    private TextView mLoadingText;
    private ImageView mLoading;


    @Override
    protected void onStop() {

        if (httpPostingDataUrlConnection != null) {

            httpPostingDataUrlConnection.disconnect();

            httpPostingDataUrlConnection = null;

        }

        super.onStop();

    }

    @Override
    protected void onDestroy() {

        try {

            if (mHandler.hasMessages(Constants.CONNECTED_TO_MAIN_SSID_SUCCESS)) {

                mHandler.removeMessages(Constants.CONNECTED_TO_MAIN_SSID_SUCCESS);

            }

            if (mHandler.hasMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL)) {

                mHandler.removeMessages(Constants.CONNECTED_TO_MAIN_SSID_FAIL);

            }
            mHandler.removeCallbacks(null);
        } catch (Exception e) {



        }


        super.onDestroy();

    }


    public void closeLoader() {

        runOnUiThread(new Runnable() {

            @Override

            public void run() {
                mLoading.clearAnimation();
                mLoading.setVisibility(View.GONE);
                mLoadingText.setVisibility(View.GONE);
            }

        });

    }

    public void showLoader(final String msg) {

        runOnUiThread(new Runnable() {

            @Override

            public void run() {
                ShowLoader.showLoader(mLoading, mLoadingText, SAC_WiFiListingActivity.this);
            }
        });

    }






    protected  void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sac_wifiisting);
        mBack = (TextView)findViewById(R.id.back);
        mLoading = (ImageView)findViewById(R.id.loading);
        mLoadingText = (TextView)findViewById(R.id.loadingText);
        mLoadingText.setText(getString(R.string.loadingWifiPoints));
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Intent myIntent = getIntent(); // gets the previously created intent
        deviceSSID = myIntent.getStringExtra("DeviceSSID");
        deviceIp = myIntent.getStringExtra("DeviceIP");
        deviceNameStr=myIntent.getStringExtra("DeviceName");
        activityName=myIntent.getStringExtra("activityName");


        recyclerView= (RecyclerView) findViewById(R.id.recycler_view);

        showLoader("Getting Device ScanResults..");

        getScanResultsForIp(deviceIp);
        wifiConnect =  WifiConnection.getInstance();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getScanResultsForIp(final String deviceIp) {

        final String BASE_URL = "http://"+deviceIp+":80";

        LSDeviceClient lsDeviceClient = new LSDeviceClient(BASE_URL);

        LSDeviceClient.DeviceNameService ScanResult = lsDeviceClient.getDeviceNameService();



        ScanResult.getScanResult(new Callback<Object>() {

            @Override

            public void success(Object mScanResult, Response response) {

                if(mScanResult==null) {

                    closeLoader();

                }

                parseJson(mScanResult.toString());

                closeLoader();

            }



            @Override

            public void failure(RetrofitError error) {

                getScanResultsForIp(deviceIp);

            }

        });

    }


    private void parseJson(String mJson) {

        try {

            JSONObject jsonRootObject = new JSONObject(mJson);



            //Get the instance of JSONArray that contains JSONObjects

            JSONArray jsonArray = jsonRootObject.optJSONArray("Items");



            //Iterate the jsonArray and print the info of JSONObjects

            for(int i=0; i < jsonArray.length(); i++){

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String SSIDName = jsonObject.optString("SSID").toString();

                String Security = jsonObject.optString("Security").toString();

                String mSSSIDNAmeDummy = Html.fromHtml(SSIDName).toString();

                String mSecurityNAmeDummy = Html.fromHtml(Security).toString();

                LibreLogger.d(this, "SSIDNAMe" + mSSSIDNAmeDummy + " :: Security" + mSecurityNAmeDummy);
                mUnSortedHashmap.put(mSSSIDNAmeDummy, mSecurityNAmeDummy);

            }
            SortTheList(mUnSortedHashmap);
            /*To Load the Screen After the Json is Loaded*/


            /*final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>

                    (this, android.R.layout.simple_list_item_1, wifiConnect.getFilteredSSIDsArrayList().toArray(new String[0]));*/


            SSIDRecyclerAdapter ssidRecyclerAdapter = new SSIDRecyclerAdapter(SAC_WiFiListingActivity.this, wifiConnect.getFilteredSSIDsArrayList(), deviceIp);
            ssidRecyclerAdapter.setUnsortedHashMap(mUnSortedHashmap);
            recyclerView.setAdapter(ssidRecyclerAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            closeLoader();


        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    private TreeMap<String,String> mUnSortedHashmap = new TreeMap<String,String>(String.CASE_INSENSITIVE_ORDER);
    private void SortTheList(TreeMap<String,String> mUnMap){

        Map<String, String> treeMap = new TreeMap<String, String>(mUnMap);
        Set s = treeMap.entrySet();
        Iterator it = s.iterator();
        while ( it.hasNext() ) {
            Map.Entry entry = (Map.Entry) it.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            System.out.println(key + " => " + value);
            WifiConnection.getInstance().putWifiScanResultSecurity(key,value);
        }
    }




    private  void goNext(String ssid_password_str,String deviceNameStr,String activityName) {

        /* We are storing the friendly name of the device being configured as we need
       to show the suceess message after its discovery */

        LibreApplication.thisSACConfiguredFromThisApp = deviceNameStr;

        if (activityName.equals("LSSDPDeviceNetworkSettings")) {

            LUCIControl mLuci = new LUCIControl(deviceIp);

            mLuci.SendCommand(125, WifiConnection.getInstance().getMainSSID() + "," + ssid_password_str, 2);

            showLoader("Configuring Device");



           Runnable runner= new Runnable() {
                @Override
                public void run() {
                    closeLoader();
                    BusProvider.getInstance().post(deviceIp);
                    Intent ssid = new Intent(SAC_WiFiListingActivity.this, ActiveScenesListActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(ssid);               }
            };

            new Handler().postDelayed(runner,1000);

        }else {

            Thread t = new Thread(new sendPostRunnable(deviceNameStr));
            t.start();

        }

        showLoader("Configuring Device");

    }

    @Override
    public void clickedFromDialogFragment(int id, String password) {

        /*Fragment prev = getSupportFragmentManager().findFragmentByTag("wifiselection");
        if (prev != null) {
            DialogFragment df = (DialogFragment) prev;
            df.dismiss();
        }*/
        if(id==R.id.savebtn) {

            goNext(password, deviceNameStr, activityName);

        }
        else if (id==R.id.cancelbtn){

        }


    }


    class sendPostRunnable implements Runnable {

        String strTxt = null;



        //

        public sendPostRunnable(String strTxt) {

            this.strTxt = strTxt;

        }



        @Override

        public void run() {

            String result = sendPostDataToInternet(strTxt);

        }



    }



    private String sendPostDataToInternet(String strTxt) {


        String mURLLink = "";

        /* HTTP Post */

        if (activityName.equals("LSSDPDeviceNetworkSettings")) {

            mURLLink = "http://" + deviceIp + ":80/goform/ProfileSettingHandler";

        } else {

            mURLLink = "http://192.168.43.1:80/goform/HandleSACConfiguration";

        }

        LibreLogger.d(this, "Connection Link  " + mURLLink);

        try {

            String sDeviceName = wifiConnect.getssidDeviceNameSAC(deviceNameStr);//deviceName.getText().toString(); //wifiConnect.getssidDeviceNameSAC(strTxt);

            URL url = new URL(mURLLink);

            httpPostingDataUrlConnection = (HttpURLConnection) url.openConnection();

            httpPostingDataUrlConnection.setConnectTimeout(60000);

            httpPostingDataUrlConnection.setReadTimeout(60000);

            httpPostingDataUrlConnection.setRequestMethod("POST");

            Map<String, Object> params = new LinkedHashMap<>();

            params.put("data", strTxt);

            if (activityName.equals("LSSDPDeviceNetworkSettings")) {



                /* Here we need to add the "\n as this indicates the manual config"*/

                params.put("SSID", wifiConnect.getMainSSID() + "\n");

            } else {

                params.put("SSID", wifiConnect.getMainSSID());

            }

            LibreLogger.d(this, "sending wifi ssid " + wifiConnect.getMainSSID());

            if (wifiConnect.getMainSSIDPwd() != null)

                params.put("Passphrase", wifiConnect.getMainSSIDPwd().trim());

            else

                params.put("Passphrase", "");

            LibreLogger.d(this, "sending wifi passphrase " + wifiConnect.getMainSSIDPwd());

               /* params.put("Passphrase", wifiConnect.getMainSSIDPwd());*/

            LibreLogger.d(this, "sending wifi security as " + wifiConnect.getMainSSIDSec());

            params.put("Security", wifiConnect.getMainSSIDSec());



            if(wifiConnect.getMainSSIDSec().equalsIgnoreCase("WEP")) {

                String mValue = WifiConnection.getInstance().getKeyIndexForWEP();

                if(mValue!=null) {

                    params.put("Key_Index", mValue);

                }

            }



            if (strTxt.equals("") || activityName.equals("LSSDPDeviceNetworkSettings") || strTxt.equals(sDeviceName)) {


                params.put("Devicename", "");

            } else {



                    if (!(SAC_WiFiListingActivity.this.isFinishing())) {

                        if (strTxt.toString().equals("")) {

                            new AlertDialog.Builder(SAC_WiFiListingActivity.this)

                                    .setTitle(getString(R.string.deviceNameChanging))

                                    .setMessage( getString(R.string.failed)+"\n " +

                                            getString(R.string.deviceNamecannotBeEmpty))

                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.cancel();

                                        }

                                    }).setIcon(android.R.drawable.ic_dialog_alert)

                                    .show();

                        } else if (strTxt.getBytes().length > 50) {

                            new AlertDialog.Builder(SAC_WiFiListingActivity.this)

                                    .setTitle(getString(R.string.deviceNameChanging))

                                    .setMessage( getString(R.string.failed)+" \n " +

                                            getString(R.string.deviceLength))

                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.cancel();

                                        }

                                    }).setIcon(android.R.drawable.ic_dialog_alert)

                                    .show();

                        }

                    }

                    params.put("Devicename", "");



            }





            StringBuilder postData = new StringBuilder();

            for (Map.Entry<String, Object> param : params.entrySet()) {

                if (postData.length() != 0) postData.append('&');

                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));

                postData.append('=');

                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));

            }



            /*Storing ssid password in shared preference*/

            storeSSIDInfoToSharedPreferences(wifiConnect.getMainSSID(), wifiConnect.getMainSSIDPwd());





            String urlParameters = postData.toString();



            httpPostingDataUrlConnection.setDoOutput(true);



            OutputStreamWriter writer = new OutputStreamWriter(httpPostingDataUrlConnection.getOutputStream());

            LibreLogger.d(this, "UrLParameter Going To REtuen 1 Ssid" + wifiConnect.getMainSSID());

            LibreLogger.d(this, "UrLParameter Going To REtuen 2 Password" + wifiConnect.getMainSSIDPwd());

            LibreLogger.d(this, "UrLParameter Going To REtuen 3 security" + wifiConnect.getMainSSIDSec());

            LibreLogger.d(this, "UrLParameter Going To REtuen " + urlParameters);

            writer.write(urlParameters);

            writer.flush();

            writer.close();

            wifiConnect.setmSACDevicePostDone(true);







            mHandler.sendEmptyMessageDelayed(Constants.HTTP_POST_DONE_SUCCESSFULLY, 60000);

            try {

                LibreLogger.d(this, "Got the Response");

                int responseCode = httpPostingDataUrlConnection.getResponseCode();



                BufferedReader bufferedReader = null;

                String str = "";

                if (200 <= httpPostingDataUrlConnection.getResponseCode() && httpPostingDataUrlConnection.getResponseCode() <= 299) {

                    bufferedReader = new BufferedReader(new InputStreamReader((httpPostingDataUrlConnection.getInputStream())));

                } else {

                    bufferedReader = new BufferedReader(new InputStreamReader((httpPostingDataUrlConnection.getErrorStream())));

                }

                if (bufferedReader != null) {

                    String line = "";

                    try {

                        while ((line = bufferedReader.readLine()) != null) {

                            str += (line + "\n");

                        }

                    } catch (IOException e) {

                        LibreLogger.d(this, "Exception Happend IO ");

                    }



                    System.out.println("\nSending 'POST' request to URL : " + url);

                    System.out.println("Post parameters : " + urlParameters);

                    System.out.println("Response Code : " + responseCode);

                    System.out.println("Response Message : " + str);

                    if (str.contains("SAC credentials received ")) {

                        mHandler.removeMessages(Constants.HTTP_POST_DONE_SUCCESSFULLY);

                        mHandler.sendEmptyMessage(Constants.HTTP_POST_DONE_SUCCESSFULLY);

                        LibreError error = new LibreError("Successfully Credentials posted ,", str);

                        BusProvider.getInstance().post(error);

                    } else {

                        LibreError error = new LibreError("Some Error Happend , Start Connecting To Main Network Credientials  ," +

                                "and Got Response Message as ", str);

                        BusProvider.getInstance().post(error);

                    }

                }



            } catch (Exception e) {

                e.printStackTrace();



                mHandler.removeMessages(Constants.HTTP_POST_DONE_SUCCESSFULLY);

                mHandler.sendEmptyMessage(Constants.HTTP_POST_DONE_SUCCESSFULLY);



/*

                LibreError error = new LibreError("Exception  Happend , Start Connecting To Main Network Credientials  ," ,

                        "in getting Response" );

                BusProvider.getInstance().post(error);*/

            }

            wifiConnect.setmSACDevicePostDone(true);

        } catch (Exception e) {

            wifiConnect.setmSACDevicePostDone(false);

            e.printStackTrace();

            LibreLogger.d(this, "Exception Happend ");

            mHandler.sendEmptyMessage(Constants.HTTP_POST_FAILED);

        }

        return "DONE";

    }



    /*It will store ssid password to shared preference*/

    private void storeSSIDInfoToSharedPreferences(String deviceSSID, String password) {

        SharedPreferences pref = getApplicationContext()

                .getSharedPreferences("Your_Shared_Prefs", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = pref.edit();

        editor.putString(deviceSSID, password);

        editor.apply();

    }








    Handler mHandler = new Handler() {



        @Override

        public void handleMessage(Message msg) {

            super.handleMessage(msg);



            if (msg.what == Constants.HTTP_POST_DONE_SUCCESSFULLY) {

                closeLoader();

                Intent ssid = new Intent(SAC_WiFiListingActivity.this, ConnectingToMainNetwork.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ssid);
                finish();



            } else if (msg.what == Constants.CONNECTED_TO_MAIN_SSID_SUCCESS) {



                LibreLogger.d(this, "Connected To Main SSID ");

                if (!wifiConnect.getPreviousSSID().equalsIgnoreCase(wifiConnect.getMainSSID())) {

                    LibreError error = new LibreError("Connected To ", wifiConnect.getMainSSID() + "\n App is Restarting");

                    BusProvider.getInstance().post(error);

                    closeLoader();


                    restartApplicationForNetworkChanges(SAC_WiFiListingActivity.this, wifiConnect.getMainSSID());

                } else {


                    closeLoader();

                    restartApplicationForNetworkChanges(SAC_WiFiListingActivity.this, wifiConnect.getMainSSID());

                    Intent ssid = new Intent(SAC_WiFiListingActivity.this, NewSacActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);



                    startActivity(ssid);

                    ActivityCompat.finishAffinity(SAC_WiFiListingActivity.this);



                }

            } else if (msg.what == Constants.HTTP_POST_FAILED) {

            } else if (msg.what == Constants.HTTP_POST_FAILED) {

                closeLoader();

                Toast.makeText(SAC_WiFiListingActivity.this, getString(R.string.httpPostFailed), Toast.LENGTH_LONG).show();



            } else {

                LibreError error = new LibreError("Not able to connect " , wifiConnect.getMainSSID());

                BusProvider.getInstance().post(error);

                ;  closeLoader();


                finish();

                /* * Finish this activity, and tries to finish all activities immediately below it

                * in the current task that have the same affinity.*/

                ActivityCompat.finishAffinity(SAC_WiFiListingActivity.this);

                 /* Killing our Android App with The PID For the Safe Case */

                int pid = android.os.Process.myPid();

                android.os.Process.killProcess(pid);

                /* Intent ssid = new Intent(ssid_configuration.this, NewSacActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(ssid); */

            }

        }

    };





}