package com.cumulations.grundig.app.dlna.dmc.utility;

/**
 * Created by bhargavkumar on 4/14/17.
 */
public interface MediaServerListener {
    boolean hasPrepared = false;

    public void onMediaLoadingComplete();
    public void onMediaLoadingInitiated();
    public void onMediaLoadingFailed();
}
