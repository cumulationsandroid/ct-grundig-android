package com.cumulations.grundig.app.dlna.dmc.utility;

/**
 * Created by bhargavkumar on 4/14/17.
 */
public class MediaServerUtil {

    private boolean hasPrepared;
    static MediaServerUtil instance = null;
    private MediaServerUtil(){
    }
    public static MediaServerUtil getInstance(){
        if (instance == null){
            instance = new MediaServerUtil();
        }
        return instance;
    }
    public static MediaServerUtil getInstanceWithUpdatedValue(boolean hasPrepared){
        if (instance == null){
            instance = new MediaServerUtil();
        }
        instance.setHasMediaPrepared(hasPrepared);
        return instance;
    }
    private void setHasMediaPrepared(boolean hasPrepared){
        this.hasPrepared  = hasPrepared;
    }
    public boolean hasMediaPrepared(){
        return hasPrepared;
    }
}
