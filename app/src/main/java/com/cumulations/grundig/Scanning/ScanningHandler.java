/*
 * ********************************************************************************************
 *  *
 *  * Copyright (C) 2014 Libre Wireless Technology
 *  *
 *  * "Libre Sync" Project
 *  *
 *  * Libre Sync Android App
 *  * Author: Android App Team
 *  *
 *  **********************************************************************************************
 */

package com.cumulations.grundig.Scanning;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;

import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.Sources;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by karunakaran on 8/1/2015.
 */
public class ScanningHandler {


    private static ScanningHandler instance = new ScanningHandler();
    public ConcurrentHashMap<String, SceneObject> centralSceneObjectRepo = new ConcurrentHashMap<>();
    public Handler mSACHandler = null;
    public LSSDPNodeDB lssdpNodeDB = LSSDPNodeDB.getInstance();
    public static final int HN_MODE = 0;
    public static final int SA_MODE = 1;

    protected ScanningHandler() {
        // Exists only to defeat instantiation.
    }

    public static ScanningHandler getInstance() {
        if (instance == null) {
            instance = new ScanningHandler();
        }
        return instance;
    }

    public ConcurrentHashMap<String, SceneObject> getSceneObjectFromCentralRepo() {
        return centralSceneObjectRepo;
    }


    public LSSDPNodes getLSSDPNodeFromCentralDB(String ip) {
        return lssdpNodeDB.getTheNodeBasedOnTheIpAddress(ip);
    }


    public void addHandler(Handler mHandler) {
        mSACHandler = mHandler;
    }

    public void removeHandler(Handler mHandler) {
        mSACHandler = null;
    }

    public Handler getHandler() {
        return mSACHandler;
    }


    public boolean findDupicateNode(LSSDPNodes mNode) {
        boolean found = false;
        int size = lssdpNodeDB.GetDB().size();
        /*preventing array index out of bound*/
        try {
            for (int i = 0; i < size; i++) {
                if (lssdpNodeDB.GetDB().get(i).getIP().equals(mNode.getIP())) {
                    LSSDPNodes oldNode = lssdpNodeDB.GetDB().get(i);
                    lssdpNodeDB.renewLSSDPNodeDataWithNewNode(mNode);

                    found = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LibreLogger.d(this, "Exception occurred while finding duplicates");
        }
        return found;
    }


    public String parseHeaderValue(String content, String headerName) {
        Scanner s = new Scanner(content);
        s.nextLine(); // Skip the start line

        while (s.hasNextLine()) {
            String line = s.nextLine();
            if (line.equals(""))
                return null;
            int index = line.indexOf(':');
            if (index == -1)
                return null;
            String header = line.substring(0, index);
            if (headerName.equalsIgnoreCase(header.trim())) {
                /*Have Commented the Trim For parsing the DeviceState & FriendlyName with Space*/
                return line.substring(index + 1);
            }
        }

        return null;
    }

    private String parseStartLine(String content) {
        Scanner s = new Scanner(content);
        return s.nextLine();
    }

    public InetAddress getInetAddressFromSocketAddress(SocketAddress mSocketAddress) {
        InetAddress address = null;
        if (mSocketAddress instanceof InetSocketAddress) {
            address = ((InetSocketAddress) mSocketAddress).getAddress();

        }
        return address;
    }

    public SceneObject getSceneObjectFromCentralRepo(String mMasterIp) {
        try {
            return centralSceneObjectRepo.get(mMasterIp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clearSceneObjectsFromCentralRepo() {
        centralSceneObjectRepo.clear();
        ;
    }

    public boolean isIpAvailableInCentralSceneRepo(String mMasterIP) {
        return centralSceneObjectRepo.containsKey(mMasterIP);
    }


    public ArrayList<LSSDPNodes> getFreeDeviceList() {
        ArrayList<LSSDPNodes> mFreeDevice = new ArrayList<>();
        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
        try {
            for (LSSDPNodes mNode : mNodeDB.GetDB()) {
                if ((mNode.getDeviceState().equals("F"))
                        ) {

                    {

                        if (!mFreeDevice.contains(mNode))
                            mFreeDevice.add(mNode);
                    }
                    LibreLogger.d(this,"Current Source of Node "+ mNode.getFriendlyname() +
                    " is "+mNode.getCurrentSource());
                }
                LibreLogger.d(this, "Get Slave List For MasterIp :" +
                        mFreeDevice.size() + "DB Size" +
                        mNodeDB.GetDB().size());
            }
        } catch (Exception e) {
            LibreLogger.d(this, e.getMessage() + "Exception happened while interating in getFreeDeviceList");
        }
        return mFreeDevice;
    }


    public ArrayList<LSSDPNodes> getSlaveListForMasterIp(String mMasterIp, int mMode) {
        LSSDPNodes mMasterNode = getLSSDPNodeFromCentralDB(mMasterIp);
        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();
        if (mMasterNode.getNetworkMode().contains("WLAN") == false) {
            mMode = SA_MODE;
        } else {
            mMode = HN_MODE;
        }
        ArrayList<LSSDPNodes> mSlaveIpList = new ArrayList<>();
        try {
            for (LSSDPNodes mSlaveNode : mNodeDB.GetDB()) {

                LibreLogger.d(this, "Name Of the Speaker in the Loop :" + mSlaveNode.getFriendlyname() + "Device State " +
                        mSlaveNode.getDeviceState()
                        + " Zone ID " + mSlaveNode.getZoneID());

                if (mMode == HN_MODE && (mMasterNode != null) && (mSlaveNode != null) &&
                        mSlaveNode.getcSSID().equals(mMasterNode.getcSSID()) &&
                        mSlaveNode.getDeviceState().equals("S")) {
                    LibreLogger.d(this, "HN Mode Slave Speakers Found For MasterIp :" + mSlaveNode.getFriendlyname() + "mMaster" +
                            mMasterNode.getFriendlyname());
                    mSlaveIpList.add(mSlaveNode);
                } else if (mMode == SA_MODE && (mMasterNode != null) && (mSlaveNode != null)
                        && mSlaveNode.getZoneID().equals(mMasterNode.getZoneID()) &&
                        mSlaveNode.getDeviceState().equals("S")) {
                    LibreLogger.d(this, "SA Mode Slave Speakers Found For MasterIp :" + mSlaveNode.getFriendlyname() + "mMaster" +
                            mMasterNode.getFriendlyname());
                    mSlaveIpList.add(mSlaveNode);
                }
            }
            LibreLogger.d(this, "Get Slave List For MasterIp :" + mSlaveIpList.size());
        } catch (Exception e) {
            LibreLogger.d(this, e.getMessage() + "Exception happened while interating in getSlaveListForMasterIp");
        }
        return mSlaveIpList;
    }


    public boolean removeSceneMapFromCentralRepo(String mMasterIp) {
        centralSceneObjectRepo.remove(mMasterIp);
        if (isIpAvailableInCentralSceneRepo(mMasterIp)) {
            return false;
        }
        return true;
    }

    public void putSceneObjectToCentralRepo(String nodeMasterIp, SceneObject mScene) {
        centralSceneObjectRepo.put(nodeMasterIp, mScene);
    }

    public int getconnectedSSIDname(Context mContext) {
        WifiManager wifiManager;
        wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        LibreLogger.d(this, "getconnectedSSIDname wifiInfo = " + wifiInfo.toString());
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }

        LibreLogger.d(this, "Connected SSID" + ssid);
        if (ssid.contains(Constants.DDMS_SSID)) {
            return SA_MODE;
        }else{
            if(lssdpNodeDB.GetDB().size()>0){
                LSSDPNodes mSampleNode = lssdpNodeDB.GetDB().get(0);
                if(mSampleNode.getNetworkMode().equalsIgnoreCase("WLAN") == false){
                    return SA_MODE;
                }else{
                    return HN_MODE;
                }
            }
        }
        return HN_MODE;
    }

    public int getNumberOfSlavesForMasterIp(String mMasterIp, int mMode) {
        LSSDPNodes mMasterNode = getLSSDPNodeFromCentralDB(mMasterIp);
        if (mMasterNode == null) {

            ScanningHandler mScanHandler = ScanningHandler.getInstance();
            if (mScanHandler.isIpAvailableInCentralSceneRepo(mMasterIp)){
            LibreLogger.d(this,"Removed the sceneobject explicitly");
                mScanHandler.removeSceneMapFromCentralRepo(mMasterIp);
            }

           /* We are removing the Node and SceneObject in CoreUpnpService Itself
            *  *//**Node is not present hence removing scene object
             *//* this is to resolve 0 number of device issue*//*
            if(mMasterIp!=null || !mMasterIp.isEmpty())
                BusProvider.getInstance().post((mMasterIp));*/
            return 1;
        }
        if (mMasterNode.getNetworkMode().contains("WLAN") == false) {
            mMode = SA_MODE;
        } else {
            mMode = HN_MODE;
        }
        int NumberOfSlaves = 1;
        ArrayList<LSSDPNodes> mNodeDBData = (ArrayList<LSSDPNodes>) lssdpNodeDB.GetDB().clone(); //mNodeDB.GetDB();
        LibreLogger.d(this, "Number Of Speakers in the Network:" + mNodeDBData.size());
        try {
            for (LSSDPNodes mSlaveNode : mNodeDBData) {
                // LibreLogger.d(this," Slave Speakers Found  :"+mSlaveNode.getFriendlyname()+"mMaster"+
                // mSlaveNode.getcSSID() + "Zone ID " + mSlaveNode.getZoneID() + "mMAster AND SLAVE" + mMasterNode.getZoneID());
                if (mMode == HN_MODE && (mMasterNode != null) && (mSlaveNode != null) && (mSlaveNode.getcSSID().equals(mMasterNode.getcSSID()))
                        && (mSlaveNode.getDeviceState().equals("S"))) {
                    LibreLogger.d(this, "HN Mode Slave Speakers Found For MasterIp :" + mSlaveNode.getFriendlyname() + "mMaster" +
                            mMasterNode.getFriendlyname());
                    NumberOfSlaves = NumberOfSlaves + 1;
                } else if (mMode == SA_MODE && (mMasterNode != null) && (mSlaveNode != null) && (mSlaveNode.getZoneID().equals(mMasterNode.getZoneID()))
                        && (mSlaveNode.getDeviceState().equals("S"))) {
                    LibreLogger.d(this, "SA Mode Slave Speakers Found For MasterIp :" + mSlaveNode.getFriendlyname() + "mMaster" +
                            mMasterNode.getFriendlyname());
                    NumberOfSlaves = NumberOfSlaves + 1;
                }
            }
        } catch (Exception e) {
            LibreLogger.d(this, e.getMessage() + "Exception happened while interating in getNumberOfSlavesForMasterIp");
        }
        LibreLogger.d(this, "Number Of Speakers For MasterIp :" + NumberOfSlaves);
        return NumberOfSlaves;
    }
    public  Sources getSources(String mHexString,String deviceName){
        Sources mNewSources = new Sources();
        long mInputSource;
        BigInteger value = null;
      /*  try {
            mInputSource = Integer.decode(mHexString);
            if(!mHexString.contains("0x"))
                mHexString ="0x"+mHexString;
            value = new BigInteger(mHexString);
        }catch(Exception e){
            //mInputSource = Integer.parseInt(mHexString, 16);
             value = new BigInteger(mHexString, 16);
        }*/
   /*     if(!mHexString.contains("0x"))
            mHexString ="0x"+mHexString;*/
        if(mHexString.startsWith("0x")) {
            value = new BigInteger(mHexString.substring(2),16);
        }else {
            value = new BigInteger(mHexString, 16);
        }
        for(int position=0;position<Long.SIZE;position++){

            boolean mResult = value.testBit(position);
            switch (position + 1) {
                case Constants.AIRPLAY_SOURCE:
                    mNewSources.setAirplay(mResult);
                    break;
                case Constants.DMR_SOURCE:
                    mNewSources.setDmr(mResult);
                    break;
                case Constants.DMP_SOURCE:
                    mNewSources.setDmp(mResult);
                    break;
                case Constants.SPOTIFY_SOURCE:
                    mNewSources.setSpotify(mResult);
                    break;
                case Constants.USB_SOURCE:
                    mNewSources.setUsb(mResult);
                    break;
                case Constants.SDCARD_SOURCE:
                    mNewSources.setSDcard(mResult);
                    break;
                case Constants.MELON_SOURCE:
                    mNewSources.setMelon(mResult);
                    break;
                case Constants.VTUNER_SOURCE:
                    mNewSources.setvTuner(mResult);
                    break;
                case Constants.TUNEIN_SOURCE:
                    mNewSources.setTuneIn(mResult);
                    break;
                case Constants.MIRACAST_SOURCE:
                    mNewSources.setMiracast(mResult);
                    break;
                case Constants.DDMSSLAVE_SOURCE:
                    mNewSources.setDDMS_Slave(mResult);
                    break;
                case Constants.AUX_SOURCE:
                    mNewSources.setAuxIn(mResult);
                    break;
                case Constants.APPLEDEVICE_SOURCE:
                    mNewSources.setAppleDevice(mResult);
                    break;
                case Constants.DIRECTURL_SOURCE:
                    mNewSources.setDirect_URL(mResult);
                    break;
                case Constants.BT_SOURCE:
                    mNewSources.setBluetooth(mResult);
                    break;
                case Constants.DEEZER_SOURCE:
                    mNewSources.setDeezer(mResult);
                    break;
                case Constants.TIDAL_SOURCE:
                    mNewSources.setTidal(mResult);
                    break;
                case Constants.FAVOURITES_SOURCE:
                    mNewSources.setFavourites(mResult);
                    break;
                case Constants.GCAST_SOURCE:
                    mNewSources.setGoogleCast(mResult);
                    break;
                case Constants.EXTERNAL_SOURCE:
                    mNewSources.setExternalSource(mResult);
                    break;

            }
        }
        System.out.println(mNewSources.toPrintString(deviceName));
        return mNewSources;
    }
    private String mParseHexFromDeviceCap(String mInputString){
        int indexOfSemoColon = mInputString.indexOf("::");
        String mToBeOutput =mInputString.substring(indexOfSemoColon+2,mInputString.length());
        return mToBeOutput;
    }
    public LSSDPNodes getLSSDPNodeFromMessage(SocketAddress socketAddress, String inputString) {
        LSSDPNodes lssdpNode = null;
        String DEFAULT_ZONEID = "239.255.255.251:3000";
        String s1 = parseStartLine(inputString);
        //   Log.e("Scan_Netty", s1);
        if (s1.equals(Constants.SL_NOTIFY)) {
            String dp = inputString;

            String st1 = parseHeaderValue(dp, "PORT");
            String st2 = parseHeaderValue(dp, "DeviceName");
            String st3 = parseHeaderValue(dp, "State");
            String st4 = parseHeaderValue(dp, "USN");
            String netmode = parseHeaderValue(dp, "NetMODE");
            String type = parseHeaderValue(dp, "SPEAKERTYPE");
            String SSID = parseHeaderValue(dp, "DDMSConcurrentSSID");
            String sCastVersionNumber = parseHeaderValue(inputString, "CAST_FWVERSION");
            String fw = parseHeaderValue(dp, "FWVERSION");
            String sDeviceCap = parseHeaderValue(inputString, "SOURCE_LIST");
			String sCastTimeZone = parseHeaderValue(inputString, "CAST_TIMEZONE");
            String sWifiBand = parseHeaderValue(inputString,"WIFIBAND");
            /**LatestDiscoveryChanges
             *
             */
            String firstNotification = parseHeaderValue(dp, "FN");


            if (type == null)
                type = "0";
            String zoneid = parseHeaderValue(dp, "ZoneID");

            if (zoneid == null || zoneid.equals(""))
                zoneid = DEFAULT_ZONEID;
            InetAddress addr = null;
            if (socketAddress instanceof InetSocketAddress) {
                addr = ((InetSocketAddress) socketAddress).getAddress();

            }

            LSSDPNodes lssdpNode1 = new LSSDPNodes(addr, st2, st1, "0", st3, type, "0", st4, zoneid, SSID);

            if (sDeviceCap != null) {
                String[] mSplitUpDeviceCap = sDeviceCap.split("::");
                lssdpNode1.createDeviceCap(mSplitUpDeviceCap[0], (mSplitUpDeviceCap[1]), getSources(mParseHexFromDeviceCap(sDeviceCap),st2));
				}
            if(sWifiBand!=null){
                lssdpNode1.setmWifiBand(sWifiBand);
            }
           /*
                lssdpNode1.setDeviceCap(mSplitUpDeviceCap[0]);
                lssdpNode1.setmSourceList(getSources(mParseHexFromDeviceCap(sDeviceCap)));*//*
            }*/
                /**LatestDiscoveryChanges
                 setting first notification to LSSDP node*/
                if (firstNotification != null)
                    lssdpNode1.setFirstNotification(firstNotification);

                lssdpNode1.setNetworkMode(netmode);

                if (fw != null)
                    lssdpNode1.setVersion(fw);


            /*adding for the google cast version */
                if (sCastVersionNumber != null)
                    lssdpNode1.setgCastVerision(sCastVersionNumber);


            if(sCastTimeZone!=null)
                lssdpNode1.setmTimeZone(sCastTimeZone);
                return lssdpNode1;
            } else if (s1.equals(Constants.SL_OK)) {
                String fw = parseHeaderValue(inputString, "FWVERSION");
                String usn = parseHeaderValue(inputString, "USN");
                String port = parseHeaderValue(inputString, "PORT");
                String devicename = parseHeaderValue(inputString, "DeviceName");
                String state = parseHeaderValue(inputString, "State");
                String netmode = parseHeaderValue(inputString, "NetMODE");
                String speakertype = parseHeaderValue(inputString, "SPEAKERTYPE");
                String tcpport = parseHeaderValue(inputString, "TCPPORT");
                String zone_id = parseHeaderValue(inputString, "ZoneID");
                String SSID = parseHeaderValue(inputString, "DDMSConcurrentSSID");
                String sCastVersionNumber = parseHeaderValue(inputString, "CAST_FWVERSION");
                String sDeviceCap = parseHeaderValue(inputString, "SOURCE_LIST");
                String sCastTimeZone = parseHeaderValue(inputString, "CAST_TIMEZONE");
                String sWifiBand = parseHeaderValue(inputString,"WIFIBAND");
                /**LatestDiscoveryChanges
                 *
                 */
                String firstNotification = parseHeaderValue(inputString, "FN");


            if (speakertype == null) {
                speakertype = "0";
            }
            if (zone_id == null || zone_id.equals("")) {
                zone_id = Constants.DEFAULT_ZONEID;
            }
            InetAddress address = null;
            if (socketAddress instanceof InetSocketAddress) {
                address = ((InetSocketAddress) socketAddress).getAddress();

            }
            Log.e("LSSDP " + "Socket Address : " + socketAddress.toString(), "InetAddress" + address.toString() + "Host ADDress" + address.getHostAddress());

            /* Setting the netmode parameter to make sure that the zoneid is
                    * incremented even for ethernet mode and not just SA mode*/

                lssdpNode = new LSSDPNodes(address, devicename, port, "0", state, speakertype, "0", usn, zone_id, SSID);
                if (sDeviceCap != null) {
                    String[] mSplitUpDeviceCap = sDeviceCap.split("::");
                    lssdpNode.createDeviceCap(mSplitUpDeviceCap[0], (mSplitUpDeviceCap[1]), getSources(mParseHexFromDeviceCap(sDeviceCap),devicename));
                }
                if(sWifiBand!=null){
                    lssdpNode.setmWifiBand(sWifiBand);
                }
                /**LatestDiscoveryChanges
                 setting first notification to LSSDP node*/
                if (firstNotification != null)
                    lssdpNode.setFirstNotification(firstNotification);

                lssdpNode.setNetworkMode(netmode);

            /*adding for the google cast version */
                if (sCastVersionNumber != null)
                    lssdpNode.setgCastVerision(sCastVersionNumber);

                if (fw != null)
                    lssdpNode.setVersion(fw);

                if (sCastVersionNumber != null)
                    lssdpNode.setgCastVerision(sCastVersionNumber);


            if(sCastTimeZone!=null)
                lssdpNode.setmTimeZone(sCastTimeZone);
            return lssdpNode;
        } else {
            return null;
        }
    }

    public boolean isAnyDeviceGcasting(SceneObject currentSceneObject){
        if (currentSceneObject!=null && currentSceneObject.getCurrentSource() == MIDCONST.GCAST_SOURCE
                && currentSceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING){
            return true;
        } else {
            ArrayList<LSSDPNodes> lssdpNodesArrayList = LSSDPNodeDB.getInstance().GetDB();
            if (lssdpNodesArrayList.size() == 0) {
                return false;
            } else {
                for (LSSDPNodes lssdpNode : lssdpNodesArrayList){
                    if (lssdpNode!=null && lssdpNode.getCurrentSource() == MIDCONST.GCAST_SOURCE
                            && lssdpNode.getmPlayStatus() == SceneObject.CURRENTLY_PLAYING){
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean isDeviceGcastPlaying(LSSDPNodes lssdpNode){
        return lssdpNode!=null && lssdpNode.getCurrentSource() == MIDCONST.GCAST_SOURCE
                && lssdpNode.getmPlayStatus() == SceneObject.CURRENTLY_PLAYING;
    }

}
