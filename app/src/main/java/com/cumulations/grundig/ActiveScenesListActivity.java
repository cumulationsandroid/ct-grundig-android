package com.cumulations.grundig;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.ManageDevice.CreateNewScene;
import com.cumulations.grundig.Network.NewSacActivity;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.app.dlna.dmc.server.ContentTree;
import com.cumulations.grundig.app.dlna.dmc.utility.PlaybackHelper;
import com.cumulations.grundig.app.dlna.dmc.utility.UpnpDeviceManager;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.PicassoTrustCertificates;
import com.github.florent37.viewtooltip.ViewTooltip;

import org.fourthline.cling.model.meta.RemoteDevice;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import static com.cumulations.grundig.Scanning.Constants.DMR_SOURCE;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.REPEAT_ALL;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.REPEAT_OFF;
import static com.cumulations.grundig.nowplaying.CumulationsNowPlayingFragment.REPEAT_ONE;

/**
 * Created by karunakaran on 7/22/2015.
 */
public class ActiveScenesListActivity extends DeviceDiscoveryActivity implements LibreDeviceInteractionListner {

    private LinearLayout footerViewsecond;
    private RelativeLayout footerView;
    static ProgressDialog mProgressDialog;
    private String[] mDrawerTitles;
    private DrawerLayout drawerLayout;
    private boolean isDoubleTap;
    private FloatingActionButton mFloatingActionButton;
    ImageView img_arrow;
    public static final int RELEASE_ALL_SUCCESS = 0x75;
    public static final int RELEASE_ALL_STARTED = 0x76;
    public static final int APP_NETWROK_CHANGED = 0x77;

    public static final int RELEASE_ALL_TIMEOUT = 3000;

    public static final int PREPARATION_INIT = 0x77;
    public static final int PREPARATION_COMPLETED = 0x78;
    public static final int PREPARATION_TIMEOUT_CONST = 0x79;

    private boolean mDropAllIniatiated = false;
    String TAG = "SCeneListActivity";
    ListView SceneListView;
    ActiveSceneAdapter activeAdapter;
    ScanningHandler mScanHandler = ScanningHandler.getInstance();
    public SwipeRefreshLayout swipeRefreshLayout;
    boolean mDeviceFound = false, mMasterFound = false, clearSceneObjectsFromCentralRepo = true;
    private Toolbar toolbar;
    private TextView mAppVersion;
    private NavigationView navigationView;
    private TextView dummyText;
    private ViewTooltip.TooltipView toolTipView;

    private boolean isLocalDMRPlayback;
    private TextView marqueeTitleText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_scenes_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        dummyText = (TextView)findViewById(R.id.dummy);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        initNavigationDrawer();
/*
        mFloatingActionButton = (FloatingActionButton)findViewById(R.id.myFAB);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActiveScenesListActivity.this, CreateNewScene.class);
                startActivity(i);
            }
        });*/
        SceneListView = (ListView) findViewById(R.id.active_scene_list);
        Log.e("ActiveScenesList", "active_scene_list");
        activeAdapter = new ActiveSceneAdapter(this, R.layout.scenes_listview_layout,handler);

        SceneListView.setAdapter(activeAdapter);
        activeAdapter.clear();
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        img_arrow = (ImageView) findViewById(R.id.imgView_arrow);
        footerViewsecond = (LinearLayout) findViewById(R.id.footer_second_view);
        footerView = (RelativeLayout) findViewById(R.id.footer_first);
        footerView.setOnClickListener(onclickListner);

        findViewById(R.id.playnew_icon).setOnClickListener(onclickListner);
        findViewById(R.id.favorites_icon).setOnClickListener(onclickListner);
        findViewById(R.id.config_item).setOnClickListener(onclickListner);

        findViewById(R.id.imgView_arrow).setOnClickListener(onclickListner);
        findViewById(R.id.footerHead).setOnClickListener(onclickListner);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                swipeRefreshLayout.setRefreshing(true);
               /* activeAdapter.clear();
                activeAdapter.notifyDataSetChanged();
                mScanHandler.clearSceneObjectsFromCentralRepo();*/

                handler.removeCallbacksAndMessages(null);

                refreshDevices();
            }
        });


    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        setIntent(intent);
//        if (intent.hasExtra(SpalshScreenActivity.APP_RESTARTING)) {
//            Log.d(TAG, "onNewIntent() called with: " + "intent = [" + intent + "]");
//            swipeRefreshLayout.setRefreshing(true);
//        }
//    }


    public void initNavigationDrawer() {

        navigationView = (NavigationView)findViewById(R.id.navigation_view);
        //mAppVersion = (TextView)navigationView.getHeaderView(0).findViewById(R.id.appVersion);
        //mAppVersion = (TextView) navigationView.getMenu().getItem(7);
        mAppVersion = (TextView) navigationView.getMenu().getItem(8).getActionView().findViewById(R.id.appversion);
        try {
            mAppVersion.setText(getString(R.string.version)+" : "+getVersion(getApplicationContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*Enabling marquee for Navigation Menu items*/
        TextView addNewSpeakerText = (TextView) navigationView.getMenu().getItem(1).getActionView().findViewById(R.id.marqueeTitle);
        addNewSpeakerText.setSelected(true);

        TextView createNewZoneTv = (TextView) navigationView.getMenu().getItem(2).getActionView().findViewById(R.id.marqueeTitle);
        createNewZoneTv.setText(getString(R.string.create_new_zone));
        createNewZoneTv.setSelected(true);

        TextView whatIsHnModeTv = (TextView) navigationView.getMenu().getItem(4).getActionView().findViewById(R.id.marqueeTitle);
        whatIsHnModeTv.setText(getString(R.string.what_is_hn_mode));
        whatIsHnModeTv.setPadding(100,0,0,0);
        whatIsHnModeTv.setSelected(true);

        TextView whatIsSAModeTv = (TextView) navigationView.getMenu().getItem(5).getActionView().findViewById(R.id.marqueeTitle);
        whatIsSAModeTv.setText(getString(R.string.what_is_sa_mode));
        whatIsSAModeTv.setPadding(100,0,0,0);
        whatIsSAModeTv.setSelected(true);

        TextView howToCreateNewZoneTv = (TextView) navigationView.getMenu().getItem(6).getActionView().findViewById(R.id.marqueeTitle);
        howToCreateNewZoneTv.setText(getString(R.string.how_to_create_master));
        howToCreateNewZoneTv.setPadding(100,0,0,0);
        howToCreateNewZoneTv.setSelected(true);

        TextView howToAddNewSpeakerTv = (TextView) navigationView.getMenu().getItem(7).getActionView().findViewById(R.id.marqueeTitle);
        howToAddNewSpeakerTv.setText(getString(R.string.how_to_add_new_speaker));
        howToAddNewSpeakerTv.setPadding(100,0,0,0);
        howToAddNewSpeakerTv.setSelected(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id){
                    case R.id.home:
                        Intent i = new Intent(ActiveScenesListActivity.this, AddNewSpeakerToWifiHelpScreenActivity.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpSAMode:
                        Intent helpSaModeIntent = new Intent(ActiveScenesListActivity.this, SAModeHelpScreenActivity.class);
                        startActivity(helpSaModeIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpHNMode:
                        Intent helpHnModeIntent = new Intent(ActiveScenesListActivity.this, HNModeHelpScreenActivity.class);
                        startActivity(helpHnModeIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpCreateMaster:
                        Intent helpCreateMasterIntent = new Intent(ActiveScenesListActivity.this, CreateMasterHelpScreenActivity.class);
                        startActivity(helpCreateMasterIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.createNewZone:
                        Intent intent = new Intent(ActiveScenesListActivity.this, CreateNewScene.class);
                        startActivity(intent);
                        menuItem.setChecked(false);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.addNewSpeaker:
                        Intent addNewSpeakerIntent = new Intent(ActiveScenesListActivity.this, AddNewSpeakerHelpScreenActivity.class);
                        startActivity(addNewSpeakerIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.logout:
                       // finish();

                }
                return true;
            }
        });
        View header = navigationView.getHeaderView(0);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){

            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (toolTipView != null) {
            toolTipView.remove();
        }
    }


    @Override
    protected void onDestroy() {
        try{
            if(mTaskHandlerForSendingMSearch.hasMessages(MSEARCH_TIMEOUT_SEARCH)) {
                mTaskHandlerForSendingMSearch.removeMessages(MSEARCH_TIMEOUT_SEARCH);
            }
            mTaskHandlerForSendingMSearch.removeCallbacks(mMyTaskRunnableForMSearch);
        }catch(Exception e){
            e.printStackTrace();
        }
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();
        clearSceneObjectsFromCentralRepo = true;
        /* WHy We have to Clear the Adapter in Onresume, Screen lock and Unlock*/
        activeAdapter.clear();
        LibreApplication.isConvertViewClicked = false;
        /*registering for message events*/
        registerForDeviceEvents(this);

        updateFromCentralRepositryDeviceList();
//        activeAdapter.notifyDataSetChanged();


        /* on returning to ActiveScene List if we dont have master we will intiate the ssh*/

        String[] mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);
        if (mMasterIpKeySet.length <= 0) {
            swipeRefreshLayout.setRefreshing(true);
            refreshDevices();
        }

        /* Setting the footer image  */
        setTheFooterIcons();

        /*Karuna For Testing ... */
        /*refreshDevicesWithoutClearing();*/
        /*new Handler().post(new Runnable() {
            @Override
            public void run() {
                View dummyToolTipView = findViewById(R.id.dummy);
                showViewToolTip(dummyToolTipView,getString(R.string.click_to_open_menu_help), ViewTooltip.Position.BOTTOM, true);
            }
        });*/
    }

    public void setTheFooterIcons() {

       if( LSSDPNodeDB.isCastDevicesExistsInTheNetwork()) {
           TextView icon = (TextView) findViewById(R.id.favorites_icon);
           Drawable top = getResources().getDrawable(R.mipmap.ic_cast_black_24dp);
           icon.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
           icon.setText(R.string.favorite_button_playcast);
       }else{
           TextView icon = (TextView) findViewById(R.id.favorites_icon);
           /*Drawable top = getResources().getDrawable(R.mipmap.ic_favorites);
           icon.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
           icon.setText(R.string.favorite_button);*/
           icon.setVisibility(View.GONE);
       }
    }

    public void refreshDevicesWithoutClearing() {
        LibreLogger.d(this, "RefreshDevices Without clearing");
        final LibreApplication application = (LibreApplication) getApplication();
        // application.getScanThread().clearNodes();
        application.getScanThread().UpdateNodes();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 10000);
/* Commenting for the Refresh we Have to Do one time Not Multiple  And wait For 10 Seconds*/
      /*  new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();
            }
        }, 150);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 650);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1150);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1650);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 2150);*/

      /*  new Handler().postDelayed(new Runnable() { *//* Swipe Refersh UI will show the circular bar of 1750ms , in this three M-search will happen in the interval of time 500ms *//*
            @Override
            public void run() {

                //application.getScanThread().UpdateNodes();

                activeAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);


                String[] mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);
                if (mMasterIpKeySet.length <= 0) {
                    LibreLogger.d(this, "refresh");
                    if (!mDropAllIniatiated) {
                    *//* if at the end of active scenes if we dont have the master then we will call the playnew scenes *//*
                        Intent newIntent = new Intent(ActiveScenesListActivity.this, PlayNewActivity.class);
                        startActivity(newIntent);
                        finish();
                    }

                }


            }
        }, 5650); // Callign For Some reasons*/
    }
    private final int MSEARCH_TIMEOUT_SEARCH = 2000;
    private Handler mTaskHandlerForSendingMSearch = new Handler();
    public boolean mBackgroundMSearchStoppedDeviceFound = false;
    private Runnable mMyTaskRunnableForMSearch = new Runnable() {
        @Override
        public void run() {
            LibreLogger.d(this, "My task is Sending 1 Minute Once M-Search");
            /* do what you need to do */
            if (mBackgroundMSearchStoppedDeviceFound)
                return;
            final LibreApplication application = (LibreApplication) getApplication();
            application.getScanThread().UpdateNodes();

            String[] mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);
            if (mMasterIpKeySet.length <= 0) {
                    /* if at the end of active scenes if we dont have the master then we will call the playnew scenes */
                    if(LibreApplication.mCleanUpIsDoneButNotRestarted==false) {
                        Intent newIntent = new Intent(ActiveScenesListActivity.this, PlayNewActivity.class);
                        startActivity(newIntent);
                        finish();
                    }

            }
            /* and here comes the "trick" */
           // mTaskHandlerForSendingMSearch.postDelayed(this, MSEARCH_TIMEOUT_SEARCH);
        }
    };
   /* private void sendMSearchInIntervalOfTime() {
        mTaskHandlerForSendingMSearch.postDelayed(mMyTaskRunnableForMSearch, MSEARCH_TIMEOUT_SEARCH);

    }*/
    public void refreshDevices() {
        LibreLogger.d(this, "RefreshDevices With clearing");
        final LibreApplication application = (LibreApplication) getApplication();
        //application.getScanThread().clearNodes();
        application.getScanThread().UpdateNodes();

        mTaskHandlerForSendingMSearch.postDelayed(mMyTaskRunnableForMSearch,10000);
       new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);

            }
        }, 10000);
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();
            }
        }, 150);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 650);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1150);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 1650);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                application.getScanThread().UpdateNodes();

            }
        }, 2150);

        new Handler().postDelayed(new Runnable() { *//* Swipe Refersh UI will show the circular bar of 1750ms , in this three M-search will happen in the interval of time 500ms *//*
            @Override
            public void run() {

                //application.getScanThread().UpdateNodes();

                activeAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);


                String[] mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);
                if (mMasterIpKeySet.length <= 0) {
                    *//* if at the end of active scenes if we dont have the master then we will call the playnew scenes *//*
                    Intent newIntent = new Intent(ActiveScenesListActivity.this, PlayNewActivity.class);
                    startActivity(newIntent);
                    finish();

                }


            }
        }, 2650);*/
    }

    private void updateFromCentralRepositryDeviceList() {


        String[] mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);
        LibreLogger.d(this, "Master is Getting Added T Active Scenes Adapter" + mScanHandler.getSceneObjectFromCentralRepo().keySet());
        /* Fix For If Master is Available But SceneObject is Not Available in the Network */
        if(mMasterIpKeySet.length<=0){

            if(LSSDPNodeDB.getInstance().isAnyMasterIsAvailableInNetwork()){
                for(LSSDPNodes nodes : LSSDPNodeDB.getInstance().GetDB()){
                    if (nodes.getDeviceState() != null && nodes.getDeviceState().contains("M")) {
                        if (!ScanningHandler.getInstance().isIpAvailableInCentralSceneRepo(nodes.getIP())) {
                            SceneObject sceneObjec = new SceneObject(" ", nodes.getFriendlyname(), 0, nodes.getIP());
                            ScanningHandler.getInstance().putSceneObjectToCentralRepo(nodes.getIP(), sceneObjec);
                            LibreLogger.d(this, "Master is not available in Central Repo So Created SceneObject " + nodes.getIP());
                        }
                    }
                }
            }
        }
        mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);
        for (String mMasterIp : mMasterIpKeySet) {
            LibreLogger.d(this, "Master is Getting Added T Active Scenes Adapter" + mMasterIp);
            mDeviceFound = false;
            mMasterFound = true;

            /* This is needed to make sure the unwanted free device is not coming */
            LSSDPNodes node = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(mMasterIp);
            if (node != null) {

                if (node.getDeviceState().contains("F") || node.getDeviceState().contains("S")) {
                    mScanHandler.removeSceneMapFromCentralRepo(node.getIP());
                    continue;
                }

            }


            try {
                LUCIControl luciControl = new LUCIControl(mMasterIp);


                ArrayList<LUCIPacket> luciPackets = new ArrayList<>();
                LUCIPacket mSceneNamePacket = new LUCIPacket(null, (short) 0, (short) MIDCONST.MID_SCENE_NAME, (byte) LSSDPCONST.LUCI_GET);
                /*removing it to resolve flickering issue*/
//                LUCIPacket volumePacket = new LUCIPacket(null, (short) 0, (short) MIDCONST.ZONE_VOLUME, (byte) LSSDPCONST.LUCI_GET);
                LUCIPacket currentSourcePacket = new LUCIPacket(null, (short) 0, (short) MIDCONST.MID_CURRENT_SOURCE, (byte) LSSDPCONST.LUCI_GET);
                LUCIPacket getUIPacket = new LUCIPacket(LUCIMESSAGES.GET_PLAY.getBytes(), (short) LUCIMESSAGES.GET_PLAY.length(),
                        (short) MIDCONST.MID_REMOTE_UI, (byte) LSSDPCONST.LUCI_SET);

                luciPackets.add(mSceneNamePacket);
//                luciPackets.add(volumePacket);
                luciPackets.add(getUIPacket);
                luciPackets.add(currentSourcePacket);

                luciControl.SendCommand(luciPackets);

            } catch (Exception e) {
                e.printStackTrace();
            }

            activeAdapter.add(mScanHandler.getSceneObjectFromCentralRepo(mMasterIp));
            activeAdapter.notifyDataSetChanged();
        }

    }


    protected void onStop() {
        super.onStop();

        /* This is important to avoid firing the PlayNew activity
        * after the refreshDevices timeout happens when there a*/
        mDropAllIniatiated = true;
        LibreApplication.isConvertViewClicked = false;
        unRegisterForDeviceEvents();
        clearScenePreparationFlags();
        /*removing all messages from handler*/
        if (handler != null)
            handler.removeCallbacksAndMessages(null);
    }


    private void clearScenePreparationFlags() {
        ConcurrentHashMap<String, SceneObject> mSceneList = mScanHandler.getSceneObjectFromCentralRepo();
        for (String masterIp : mSceneList.keySet()) {
            SceneObject sceneObject = mScanHandler.getSceneObjectFromCentralRepo(masterIp);
            if (sceneObject != null)
                sceneObject.setPreparingState(SceneObject.PREPARING_STATE.PREPARING_SUCCESS);
        }
    }


    /* Libre device interaction methods - START */
    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }


    @Override
    public void newDeviceFound(LSSDPNodes node) {
        ArrayList<String> mSlaveIp = new ArrayList<>();

        LibreLogger.d(this, "New device is found with the ip address = " + node.getIP() + "Device State" + node.getDeviceState());
        mDeviceFound = true;
        if (node.getDeviceState().contains("M")) {

            mDeviceFound = false;
            mMasterFound = true;
            LUCIControl luciControl = new LUCIControl(node.getIP());
            try {


                ArrayList<LUCIPacket> luciPackets = new ArrayList<>();
                /*removing it to resolve flickering issue*/
//                LUCIPacket volumePacket = new LUCIPacket(null, (short) 0, (short) MIDCONST.ZONE_VOLUME, (byte) LSSDPCONST.LUCI_GET);
                LUCIPacket currentScenePacket = new LUCIPacket(null, (short) 0, (short) MIDCONST.MID_SCENE_NAME, (byte) LSSDPCONST.LUCI_GET);
                LUCIPacket getUIPacket = new LUCIPacket(LUCIMESSAGES.GET_PLAY.getBytes(), (short) LUCIMESSAGES.GET_PLAY.length(),
                        (short) MIDCONST.MID_REMOTE_UI, (byte) LSSDPCONST.LUCI_SET);
                luciPackets.add(getUIPacket);
                luciPackets.add(currentScenePacket);
                ;

//                luciPackets.add(volumePacket);


                luciControl.SendCommand(luciPackets);


            } catch (Exception e) {
                e.printStackTrace();
            }


            LibreLogger.d(this, "New device is found with the ip address = " + node.getIP());
            SceneObject sceneObjec = new SceneObject(" ", node.getFriendlyname(), 0, node.getIP());
            LibreLogger.d(this, "New device is found with the ip address = " + node.getFriendlyname());
            if (!mScanHandler.isIpAvailableInCentralSceneRepo(node.getIP())) {
                mScanHandler.putSceneObjectToCentralRepo(node.getIP(), sceneObjec);
            } else {
                SceneObject mSceneObj = mScanHandler.getSceneObjectFromCentralRepo(node.getIP());
                if (mSceneObj != null)
                    sceneObjec.setSceneName(mSceneObj.getSceneName());
            }
            if (!activeAdapter.checkSceneobjectIsPresentinList(node.getIP())) {
                activeAdapter.add(sceneObjec);
                activeAdapter.notifyDataSetChanged();
            }
            /*
             commenting to avoid immediate refresh */
            //    swipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void deviceGotRemoved(String mIpAddress) {
        LibreLogger.d(this, "Device is Removed with the ip address = " + mIpAddress);

        SceneObject scneObj = activeAdapter.getSceneObjectFromAdapter(mIpAddress);
        if (scneObj != null) {
            activeAdapter.remove(scneObj);
            LibreLogger.d("ActiveScenesAdapaterBhargav", "" + scneObj.getSceneName());
            /*Commenting For HAri Changes , Because we are not going to remove anything from Data*/
           /* swipeRefreshLayout.setRefreshing(true);*/
            /* we are calling it SwipeRefreshLayout */
            /*refreshDevices();*/
        }
        activeAdapter.notifyDataSetChanged();


    }

    @Override
    public void messageRecieved(NettyData dataRecived) {

        LibreLogger.d(this, "New message appeared for the device " + dataRecived.getRemotedeviceIp());
        SceneObject sceneObject = activeAdapter.getSceneObjectFromAdapter(dataRecived.getRemotedeviceIp());
        if(sceneObject==null){
            sceneObject = mScanHandler.getSceneObjectFromCentralRepo(dataRecived.getRemotedeviceIp());
        }
        LUCIPacket packet = new LUCIPacket(dataRecived.getMessage());
        /**/
        if(packet.getCommand() ==103 ) {
            String msg = new String(packet.payload);
                    /* A device became master */
            if (msg.contains("MASTER")) {
                        /* Will ask for the play status and update the UI */

                LUCIControl luciControl = new LUCIControl(dataRecived.getRemotedeviceIp());
                try {
                            /*which means i have already registered for 3 that's why i got 103
                            * SO no need to send it again*/
                    luciControl.sendAsynchronousCommand();


                    ArrayList<LUCIPacket> luciPackets = new ArrayList<>();
                    LUCIPacket mScenePacket = new LUCIPacket(null, (short) 0, (short) MIDCONST.MID_SCENE_NAME, (byte) LSSDPCONST.LUCI_GET);
                            /*removing it to resolve flickering issue*/
//                            LUCIPacket volumePacket = new LUCIPacket(null, (short) 0, (short) MIDCONST.ZONE_VOLUME, (byte) LSSDPCONST.LUCI_GET);
                    LUCIPacket getUIPacket = new LUCIPacket(LUCIMESSAGES.GET_PLAY.getBytes(), (short) LUCIMESSAGES.GET_PLAY.length(),
                            (short) MIDCONST.MID_REMOTE_UI, (byte) LSSDPCONST.LUCI_SET);

                    luciPackets.add(mScenePacket);
//                            luciPackets.add(volumePacket);
                    luciPackets.add(getUIPacket);

                    luciControl.SendCommand(luciPackets);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                LibreLogger.d(this, "A device became master found with the ip addrs = " + dataRecived.getRemotedeviceIp());
                LSSDPNodes node = LSSDPNodeDB.getInstance().getTheNodeBasedOnTheIpAddress(dataRecived.getRemotedeviceIp());

                SceneObject sceneObjec;
                if (node != null) {
                    sceneObjec = new SceneObject(" ", node.getFriendlyname(), 0, dataRecived.getRemotedeviceIp());
                    LibreLogger.d(this, "device became master its name  = " + node.getFriendlyname());

                } else {
                    sceneObjec = new SceneObject(" ", "", 0, dataRecived.getRemotedeviceIp());

                }
                if (node!=null && !mScanHandler.isIpAvailableInCentralSceneRepo(node.getIP())) {
                    mScanHandler.putSceneObjectToCentralRepo(node.getIP(), sceneObjec);
                }

                activeAdapter.add(sceneObjec);
                activeAdapter.notifyDataSetChanged();
            }/* A device became slave or free so remove it from sceneobject if it is present and refresh UI*/
            else {
                LibreLogger.d(this, "A device became slave or free so remove it from sceneobject if it is present and refresh UI" + dataRecived.getRemotedeviceIp());
                if (mScanHandler.isIpAvailableInCentralSceneRepo(dataRecived.getRemotedeviceIp())) {
                    mScanHandler.removeSceneMapFromCentralRepo(dataRecived.getRemotedeviceIp());

                }
                SceneObject sceneObjec = new SceneObject(" ", "", 0, dataRecived.getRemotedeviceIp());
                activeAdapter.remove(sceneObjec);
                activeAdapter.notifyDataSetChanged();

            }
            //activeAdapter.notifyDataSetChanged();
             /* Handle the case when there is no sceneobjetc remaining in the UI  */
            String[] mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);
            if (mMasterIpKeySet.length <= 0) {
                LibreLogger.d(this, "Handle the case when there is no sceneobjetc remaining in the UI" + dataRecived.getRemotedeviceIp());
                swipeRefreshLayout.setRefreshing(true);
                refreshDevices();
            }
            return;
        }
        if (sceneObject != null) {
            LibreLogger.d(this, "Recieved the scene details as zoneVolume For Before checking  = " + sceneObject.getvolumeZoneInPercentage());
            LibreLogger.d(this, "New message appeared for the device " + dataRecived.getRemotedeviceIp() + "Command " + packet.getCommand());
            switch (packet.getCommand()) {
                case MIDCONST.MID_SCENE_NAME: {
                    /* This message box indicates the Scene Name*//*
;*/
                     /* if Command Type 1 , then Scene Name information will be come in the same packet
if command type 2 and command status is 1 , then data will be empty., at that time we should not update the value .*/
                    if(packet.getCommandStatus()==1
                            && packet.getCommandType()==2){
                        return;
                    }
                    String message = new String(packet.getpayload());
                    //int duration = Integer.parseInt(message);
                    try {
                        sceneObject.setSceneName(message);// = duration/60000.0f;
                        LibreLogger.d(this, "Recieved the Scene Name to be " + sceneObject.getSceneName());
                        mScanHandler.putSceneObjectToCentralRepo(dataRecived.getRemotedeviceIp(), sceneObject);
                        activeAdapter.add(sceneObject);
                        activeAdapter.notifyDataSetChanged();
                    } catch (Exception e) {

                    }
                }
                break;
                case 49: {
                    /* This message box indicates the current playing status of the scene, information like current seek position*//*
;*/
                    String message = new String(packet.getpayload());
                    int duration=0;
                    try {
                         duration = Integer.parseInt(message);
                    }catch(Exception e){
                        LibreLogger.d(this,"Handling the app crash for 49 message box value sent ="+duration);
                        break;
                    }
                    sceneObject.setCurrentPlaybackSeekPosition(duration);// = duration/60000.0f;
                    LibreLogger.d(this, "Recieved the current Seek position to be " + sceneObject.getCurrentPlaybackSeekPosition());
                    mScanHandler.putSceneObjectToCentralRepo(dataRecived.getRemotedeviceIp(), sceneObject);


                    break;
                }


                case 64: {
                    /*this message box is to get volume*/

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        if (sceneObject.getVolumeValueInPercentage() != duration) {
                            sceneObject.setVolumeValueInPercentage(duration);
                            mScanHandler.putSceneObjectToCentralRepo(dataRecived.getRemotedeviceIp(), sceneObject);
                            LibreLogger.d(this, "Recieved the current volume to be" + sceneObject.getVolumeValueInPercentage());
                            activeAdapter.add(sceneObject);
                            activeAdapter.notifyDataSetChanged();
                        }


                    } catch (Exception e) {

                    }
                }
                break;


                case 50: {

                    /*this MB to get current sources*/
                    String message = new String(packet.getpayload());
                    try {
                        int mSource = Integer.parseInt(message);

                        sceneObject.setCurrentSource(mSource);
                        mScanHandler.putSceneObjectToCentralRepo(dataRecived.getRemotedeviceIp(), sceneObject);
                        LSSDPNodes mGcastCheckNode = mScanHandler.getLSSDPNodeFromCentralDB(dataRecived.getRemotedeviceIp());

                       /* if(mGcastCheckNode.getCurrentSource() == MIDCONST.GCAST_SOURCE){
                            if(mGcastCheckNode.getmPlayStatus() == SceneObject.CURRENTLY_PLAYING){
                                activeAdapter.remove(sceneObject);
                                activeAdapter.notifyDataSetChanged();
                                return;
                            }
                        }*/
                        LibreLogger.d(this, "Recieved the current source as  " + sceneObject.getCurrentSource());
                        activeAdapter.add(sceneObject);
                        activeAdapter.notifyDataSetChanged();
                    } catch (Exception e) {

                    }
                    break;
                }

                case 42: {
                    /*this MB to get UI layout*/

                    try {

                        String message = new String(packet.payload);
                        JSONObject root = new JSONObject(message);
                        int cmd_id = root.getInt(LUCIMESSAGES.TAG_CMD_ID);
                        JSONObject window = root.getJSONObject(LUCIMESSAGES.TAG_WINDOW_CONTENT);
                        LibreLogger.d(this, "PLAY JSON is \n= " + message);


                        if (!sceneObject.getIpAddress().equals(dataRecived.getRemotedeviceIp()))
                            return;
                        if (cmd_id == 3) {

                            /*Karuna change : Yet to redefine it */
                            Message msg = new Message();
                            msg.what = PREPARATION_COMPLETED;
                            Bundle data = new Bundle();
                            data.putString("ipAddress", dataRecived.getRemotedeviceIp());
                            msg.setData(data);
                            handler.sendMessage(msg);
                            activeAdapter.notifyDataSetChanged();

                            /**/
                            String newTrackname = window.getString("TrackName");
                            int newPlayState = window.getInt("PlayState");
                            int currentPlayState = window.getInt("Current_time");

                            String nAlbumName = window.getString("Album");
                            String nArtistName = window.getString("Artist");
                            long totaltime = window.getLong("TotalTime");

                            // sceneObject.setSceneName(window.getString("TrackName"));
                            sceneObject.setTrackName(window.getString("TrackName"));
                            sceneObject.setAlbum_art(window.getString("CoverArtUrl"));
                            sceneObject.setPlaystatus(window.getInt("PlayState"));
                            String mPlayURL = window.getString("PlayUrl");
                            if(mPlayURL!=null)
                                sceneObject.setPlayUrl(mPlayURL);
                           // sceneObject.setPlayUrl(window.getString("PlayUrl"));

                            /*For favourite*/
                            sceneObject.setIsFavourite(window.getBoolean("Favourite"));


                            int currentSource = window.getInt("Current Source");


                            /*Added for Shuffle and Repeat*/
//                            sceneObject.setShuffleState(window.getInt("Shuffle"));
//                            sceneObject.setRepeatState(window.getInt("Repeat"));
                            if (!LibreApplication.LOCAL_IP.equals("") && sceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP))
                                isLocalDMRPlayback = true;
                            else if (sceneObject.getCurrentSource() == DMR_SOURCE)
                                isLocalDMRPlayback = true;


                            /*Added for Shuffle and Repeat*/
                            if (isLocalDMRPlayback) {
                                /**this is for local content*/
                                RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(sceneObject.getIpAddress());
                                if (renderingDevice != null) {
                                    String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                                    PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                                    if (playbackHelper != null) {
                                        /**shuffle is on*/
                                        if (playbackHelper.isShuffleOn()) {
                                            sceneObject.setShuffleState(1);
                                        } else {
                                            sceneObject.setShuffleState(0);
                                        }
                                        /*setting by default*/
                                        if (playbackHelper.getRepeatState() == REPEAT_ONE) {
                                            sceneObject.setRepeatState(REPEAT_ONE);
                                        }
                                        if (playbackHelper.getRepeatState() == REPEAT_ALL) {
                                            sceneObject.setRepeatState(REPEAT_ALL);
                                        }
                                        if (playbackHelper.getRepeatState() == REPEAT_OFF) {
                                            sceneObject.setRepeatState(REPEAT_OFF);
                                        }

                                    }
                                }
                            } else {
                                /**this check made as we will not get shuffle and repeat state in 42, case of DMR
                                 * so we are updating it locally*/
                                sceneObject.setShuffleState(window.getInt("Shuffle"));
                                sceneObject.setRepeatState(window.getInt("Repeat"));
                            }


                            sceneObject.setAlbum_name(nAlbumName);
                            sceneObject.setArtist_name(nArtistName);
                            sceneObject.setTotalTimeOfTheTrack(totaltime);
                            sceneObject.setCurrentSource(currentSource);
                            if (currentSource == 14) {
                                sceneObject.setArtist_name("Aux Playing");

                            }

                            /* this is done to avoid  image refresh everytime the 42 message is recieved and the song playing back is the same */
                            LibreLogger.d(this, "Invalidating the scene name for  = " + sceneObject.getIpAddress() + "/" + "coverart.jpg");

                            LibreLogger.d(this, "Recieved the scene details as trackname = " + sceneObject.getSceneName() + ": " + sceneObject.getCurrentPlaybackSeekPosition());
                            LibreLogger.d(this, "Recieved the scene details as zoneVolume For checking  = " + sceneObject.getvolumeZoneInPercentage());

                            mScanHandler.putSceneObjectToCentralRepo(dataRecived.getRemotedeviceIp(), sceneObject);
                            activeAdapter.add(sceneObject);
                            activeAdapter.notifyDataSetChanged();


                        }


                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    break;
                }

                case 51: {
                    String str = new String(packet.payload);
                    if (!str.equals("")) {
                        int playstatus = Integer.parseInt(str);
                        LSSDPNodes mGcastCheckNode = mScanHandler.getLSSDPNodeFromCentralDB(dataRecived.getRemotedeviceIp());
                        /*if(mGcastCheckNode.getCurrentSource() == MIDCONST.GCAST_SOURCE){
                           if(mGcastCheckNode.getmPlayStatus() == SceneObject.CURRENTLY_PLAYING){
                                activeAdapter.remove(sceneObject);
                                activeAdapter.notifyDataSetChanged();
                                return;
                            }else{
                                if(!activeAdapter.checkSceneobjectIsPresentinList(sceneObject.getIpAddress())) {
                                    activeAdapter.add(sceneObject);
                                    activeAdapter.notifyDataSetChanged();
                                }
                            }
                        }*/
                        if (sceneObject.getPlaystatus() != playstatus) {
                            sceneObject.setPlaystatus(playstatus);
                            mScanHandler.putSceneObjectToCentralRepo(dataRecived.getRemotedeviceIp(), sceneObject);

                            activeAdapter.add(sceneObject);
                            activeAdapter.notifyDataSetChanged();
                        }
                        if (sceneObject.getCurrentSource() != 19 &&
                                mGcastCheckNode.getCurrentSource() != MIDCONST.GCAST_SOURCE) {
                            if (sceneObject.getPlaystatus() == SceneObject.CURRENTLY_PLAYING) {
                                LibreLogger.d(this, "playing state is received");
                            /*closing progress loader*/
//                            handler.sendEmptyMessage(PREPARATION_COMPLETED);

                                Message msg = new Message();
                                msg.what = PREPARATION_COMPLETED;
                                Bundle data = new Bundle();
                                data.putString("ipAddress", dataRecived.getRemotedeviceIp());
                                msg.setData(data);

                                handler.sendMessage(msg);
                            } else if (sceneObject.getPlaystatus() == SceneObject.CURRENTLY_PAUSED) {
                                LibreLogger.d(this, "Pause state is received");
                            } else {
                            /*initiating progress loader*/
                                if (sceneObject.getPlaystatus() == SceneObject.CURRENTLY_STOPED
                                        && sceneObject.getCurrentSource() != Constants.NO_SOURCE) {
                                    Message msg = new Message();
                                    msg.what = PREPARATION_INIT;
                                    Bundle data = new Bundle();
                                    data.putString("ipAddress", dataRecived.getRemotedeviceIp());
                                    msg.setData(data);
                                    handler.sendMessage(msg);

                                    LibreLogger.d(this, "Stop state is received");
                                }

                            }
                            activeAdapter.notifyDataSetChanged();
                        }


                    }
                    break;
                }

                case MIDCONST.ZONE_VOLUME: {

                    String message = new String(packet.getpayload());
                    try {
                        int duration = Integer.parseInt(message);
                        if (sceneObject.getvolumeZoneInPercentage() != duration) {
                            sceneObject.setvolumeZoneInPercentage(duration);
                            mScanHandler.putSceneObjectToCentralRepo(dataRecived.getRemotedeviceIp(), sceneObject);
                            LibreLogger.d(this, "Recieved the current volume to be" + sceneObject.getvolumeZoneInPercentage());
                            activeAdapter.add(sceneObject);
                            activeAdapter.notifyDataSetChanged();
                        }


                    } catch (Exception e) {

                    }
                }
                break;

                case 54:{
                    String message = new String(packet.payload);
                    LibreLogger.d(this, " message 54 recieved  " + message);
                    try {
                        LibreError error=null;
                        if(message.contains(Constants.FAIL)) {
                            error = new LibreError(dataRecived.getRemotedeviceIp(), Constants.FAIL_ALERT_TEXT);

                        }else if(message.contains(Constants.SUCCESS)){
                            closeLoader();

                            Message msg = new Message();
                            msg.what = PREPARATION_COMPLETED;
                            Bundle data = new Bundle();
                            data.putString("ipAddress", dataRecived.getRemotedeviceIp());
                            msg.setData(data);
                            handler.sendMessage(msg);


                        } else if (message.contains(Constants.NO_URL)) {
                            error = new LibreError(dataRecived.getRemotedeviceIp(), Constants.NO_URL_ALERT_TEXT);
                        }else if(message.contains(Constants.NO_PREV_SONG)){
                            error = new LibreError(dataRecived.getRemotedeviceIp(), getResources().getString(R.string.NO_PREV_SONG_ALERT_TEXT));
                        }else if(message.contains(Constants.NO_NEXT_SONG)){
                            error = new LibreError(dataRecived.getRemotedeviceIp(), getResources().getString(R.string.NO_NEXT_SONG_ALERT_TEXT));
                        }
                        else if (message.contains(Constants.DMR_SONG_UNSUPPORTED)) {
                            error = new LibreError(dataRecived.getRemotedeviceIp(), getResources().getString(R.string.SONG_NOT_SUPPORTED));
                        }else if(message.contains(LUCIMESSAGES.NEXTMESSAGE)){
                            activeAdapter.doNextPrevious(true,sceneObject,true);
                        }else if(message.contains(LUCIMESSAGES.PREVMESSAGE)){
                            activeAdapter.doNextPrevious(false,sceneObject,true);
                        }
                        PicassoTrustCertificates.getInstance(ActiveScenesListActivity.this).invalidate(sceneObject.getAlbum_art());
                        closeLoader();
                        if (error != null)
                            showErrorMessage(error);

                    } catch (Exception e) {
                        e.printStackTrace();
                        LibreLogger.d(this, " Json exception ");

                    }
                }break;

            }
        }


    }

    View.OnClickListener onclickListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId())

            {


                case R.id.footer_first:
                    break;

                case R.id.footerHead:
                    //dont put break here;
                case R.id.imgView_arrow: {
                    if (footerViewsecond.getVisibility() == View.VISIBLE) {
                        footerViewsecond.setVisibility(View.GONE);
                        img_arrow.setRotation(180);
                    } else {
                        footerViewsecond.setVisibility(View.VISIBLE);
                        img_arrow.setRotation(0);
                    }
                }
                break;

                case R.id.footer_second_view:

                    break;

                case R.id.playnew_icon: {

                    Intent configIntent = new Intent(ActiveScenesListActivity.this, CreateNewScene.class);
                    startActivity(configIntent);
                }
                break;

                case R.id.favorites_icon:


                    if (LSSDPNodeDB.isCastDevicesExistsInTheNetwork()){

               /*         SharedPreferences sharedPreferences = getApplicationContext()
                                .getSharedPreferences(Constants.SHOWN_GOOGLE_TOS, Context.MODE_PRIVATE);
                        String shownGoogle = sharedPreferences.getString(Constants.SHOWN_GOOGLE_TOS, null);*/

                        // now TOS is recieved in msg box 224, so no need to check shared pref for this dialog
                        /*String shownGoogle = "Yes";
                        if (shownGoogle == null) {
                            showGoogleTosDialog(ActiveScenesListActivity.this);
                        }
                        else {
                            Intent intent=new Intent(ActiveScenesListActivity.this,GcastSources.class);
                            startActivity(intent);
                        }*/

                        //when play cast is clicked, open gCast app
                        openGHomeDialog();
                    }

                    else {
                        Toast.makeText(ActiveScenesListActivity.this, getString(R.string.favourits_not_implemented), Toast.LENGTH_LONG).show();
                    }

                    break;

                case R.id.config_item: {
                    Intent configIntent = new Intent(ActiveScenesListActivity.this, NewSacActivity.class);
                    startActivity(configIntent);
                    finish();
                }
                break;

            }

        }

    };
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == RELEASE_ALL_STARTED) {
                showLoader();
            }

            if (msg.what == RELEASE_ALL_SUCCESS) {
                closeLoader();
                handler.removeMessages(RELEASE_ALL_TIMEOUT);

                  /*to ensure we are updating adapter*/
                activeAdapter.clear();
                activeAdapter.notifyDataSetChanged();
                /*making sure to call PlayNewActtivity activity Bug : 2277*/
                Intent intent = new Intent(ActiveScenesListActivity.this, PlayNewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            }

            if (msg.what == RELEASE_ALL_TIMEOUT) {
                closeLoader();

                  /*to ensure we are updating adapter*/
                activeAdapter.clear();
                if (!clearSceneObjectsFromCentralRepo){
                    updateFromCentralRepositryDeviceList();
                    String[] mMasterIpKeySet = mScanHandler.getSceneObjectFromCentralRepo().keySet().toArray(new String[0]);
                    if (mMasterIpKeySet.length <= 0) {
                        swipeRefreshLayout.setRefreshing(true);
                        refreshDevices();
                    }
                    setTheFooterIcons();
                } else {
                    activeAdapter.notifyDataSetChanged();
                /*making sure to call PlayNewActtivity activity Bug : 2277 */
                    Intent intent = new Intent(ActiveScenesListActivity.this, PlayNewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }

            if (msg.what == PREPARATION_INIT) {

                String ipaddress = msg.getData().getString("ipAddress");
                triggerTimeOutHandlerForIpAddress(ipaddress);

            }
            if (msg.what == PREPARATION_COMPLETED) {
                String ipaddress = msg.getData().getString("ipAddress");
                removeTimeOutTriggerForIpaddress(ipaddress);

            }
            if (msg.what == PREPARATION_TIMEOUT_CONST) {
                String ipaddress = msg.getData().getString("ipAddress");


                    /*posting error to application here we are using bus because it doest extend DeviceDiscoverActivity*/
                LibreError error = new LibreError(ipaddress, getResources().getString(R.string.PREPARATION_TIMEOUT_HAPPENED));
                showErrorMessage(error);

                SceneObject mSceneObject = mScanHandler.getSceneObjectFromCentralRepo(ipaddress);
                if (mSceneObject != null) {
                    mSceneObject.setPreparingState(SceneObject.PREPARING_STATE.PREPARING_FAILED);
                }
                activeAdapter.notifyDataSetChanged();

            }


        }
    };

    private void removeTimeOutTriggerForIpaddress(String ipAddress) {

        handler.removeMessages(PREPARATION_TIMEOUT_CONST, ipAddress);
        SceneObject mSceneObject = mScanHandler.getSceneObjectFromCentralRepo(ipAddress);
        if (mSceneObject != null) {
            mSceneObject.setPreparingState(SceneObject.PREPARING_STATE.PREPARING_SUCCESS);
        }
        activeAdapter.notifyDataSetChanged();
    }

    private void triggerTimeOutHandlerForIpAddress(String ipAddress) {
        Message timeOutMessage = new Message();

        /* Added by Praveen to address the crash for PREPARATION_TIMEOUT_CONST */
        Bundle data = new Bundle();
        data.putString("ipAddress", ipAddress);
        timeOutMessage.setData(data);

        timeOutMessage.obj = ipAddress;
        timeOutMessage.what = PREPARATION_TIMEOUT_CONST;
        handler.sendMessageDelayed(timeOutMessage, Constants.PREPARATION_TIMEOUT);

        SceneObject mSceneObject = mScanHandler.getSceneObjectFromCentralRepo(ipAddress);
        if (mSceneObject != null) {
            mSceneObject.setPreparingState(SceneObject.PREPARING_STATE.PREPARING_INITIATED);
        }
        activeAdapter.notifyDataSetChanged();
    }


    private void showLoader() {

        if (mProgressDialog == null) {

            if (!(ActiveScenesListActivity.this.isFinishing())) {
                mProgressDialog = ProgressDialog.show(ActiveScenesListActivity.this,getString(R.string.notice),
                        getString(R.string.pleaseWait), true, true, null);
            }
        }

		/*We got the CrashAnalytics Logs , if progressDialog is Null*/
        if (mProgressDialog != null)
            mProgressDialog.setCancelable(false);

        if (!mProgressDialog.isShowing()) {
            Log.e("LUCIControl", "is Showing");
            if (!(ActiveScenesListActivity.this.isFinishing())) {
                mProgressDialog = ProgressDialog.show(ActiveScenesListActivity.this, getString(R.string.notice),
                        getString(R.string.pleaseWait), true, true, null);
            }

            mProgressDialog.show();
        }

    }

       private void closeLoader() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            Log.e("LuciControl", "progress Dialog Closed");
            mProgressDialog.setCancelable(true);
            mProgressDialog.dismiss();
            mProgressDialog.cancel();
        }
    }

    /*This function gets Called When Drop All Pressed*/

    private void dropAllDevices() {
        ScanningHandler mScanningHandler = ScanningHandler.getInstance();
        ConcurrentHashMap<String, SceneObject> centralSceneObjectRepo = mScanningHandler.getSceneObjectFromCentralRepo();


        LSSDPNodeDB mNodeDB = LSSDPNodeDB.getInstance();

        ArrayList<LSSDPNodes> mNodeDBArrayList = mNodeDB.GetDB();


        for (LSSDPNodes node : mNodeDBArrayList) {

            if (mScanHandler.isDeviceGcastPlaying(node)){
                        /* For free speaker if it's gcast playing dont send join cmd*/
                LibreError error = new LibreError(node.getFriendlyname(), getString(R.string.speaker_casting) + " "+
                        getString(R.string.errorCastingDropAll));
                showErrorMessage(error);
                clearSceneObjectsFromCentralRepo = false;
                break;
            }

            LUCIControl luciControl = new LUCIControl(node.getIP());
            LibreLogger.d("this", "Release Scene Slave Ip List" + node.getFriendlyname());
            //* Sending Asynchronous Registration *//*
            luciControl.sendAsynchronousCommand();
            luciControl.SendCommand(MIDCONST.MID_DDMS, LUCIMESSAGES.SETFREE, LSSDPCONST.LUCI_SET);
        }


        /*clearing central repository*/
        if (clearSceneObjectsFromCentralRepo)
        mScanningHandler.clearSceneObjectsFromCentralRepo();
    }

    /* This function gets called when release all pressed*/
    private void releaseAllDevicesDialogue(int size) {

        if (ActiveScenesListActivity.this.isFinishing())
            return;

        String numberOfDevices = size + " ";

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ActiveScenesListActivity.this);
        alertDialogBuilder.setTitle(getString(R.string.appHasDetected) + numberOfDevices + getString(R.string.devicesText));
        alertDialogBuilder
                .setMessage(getString(R.string.clearAllMsg))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        /**
                         * sending free to all devices including DB
                         */
                        dialog.dismiss();

                        handler.sendEmptyMessage(RELEASE_ALL_STARTED);
                        mDropAllIniatiated = true;

                        /* this is Drop All Function */
                        dropAllDevices();
/*                    *//*this is synchronous function*//*
                        releaseAllMasterandSlave();*/
                    /*sometime we have slave without master this will handle boundary condition */
                    /*this is synchronous function*/
//                        freeAllDevicesFromDB();

                        handler.sendEmptyMessageDelayed(RELEASE_ALL_TIMEOUT,5000);

                        /*ScanThread.getInstance().UpdateNodes();*/


                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public String getVersion(Context context) {
        String Version = getString(R.string.title_activity_welcome);
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pInfo != null)
            Version = pInfo.versionName;

        return Version;
    }

    public void showViewToolTip(View view, final String text, ViewTooltip.Position position, final boolean shouldRepeat) {
        SharedPreferences sharedPreferences = getSharedPreferences("apppreference", MODE_PRIVATE);

        if (view ==null)
        {
            //Toast.makeText(this,"Hello", Toast.LENGTH_LONG).show();
            return;
        }

        if (sharedPreferences.getBoolean(ActiveScenesListActivity.class.getSimpleName(),false) == false) {
            sharedPreferences.edit().putBoolean(ActiveScenesListActivity.class.getSimpleName(),true).commit();

            /*if (!shouldRepeat) {
                ViewTooltip
                        .on(view)
                        .autoHide(false,1000)
                        .clickToHide(true)
                        .align(ViewTooltip.ALIGN.START)
                        .position(ViewTooltip.Position.RIGHT)
                        .text(text)
                        .textColor(Color.WHITE)
                        .color(getResources().getColor(R.color.blue))
                        .corner(10)
                        .show();
            } else {*/
                toolTipView = ViewTooltip
                        .on(view)
                        .autoHide(true,3000)
                        .clickToHide(true)
                        .align(ViewTooltip.ALIGN.CENTER)
                        .position(position)
                        .text(text)
                        .textColor(getResources().getColor(R.color.viewToolTipTextColor))
                        .color(getResources().getColor(R.color.viewToolTipColor))
                        .corner(10)
                        .onHide(new ViewTooltip.ListenerHide() {
                            @Override
                            public void onHide(View view) {
                                if (shouldRepeat) {
                                    new Handler().post(new Runnable() {
                                        @Override
                                        public void run() {
                                            View menuItemView = findViewById(R.id.myFAB);
                                            showViewToolTip(menuItemView, text, ViewTooltip.Position.BOTTOM, false);

                                        }
                                    });
                                }
                            }
                        })
                        .show();


        }

        return;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play_activescenes, menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                View menuItemView = findViewById(R.id.myFAB);
                showViewToolTip(menuItemView,getString(R.string.add_one_more_zone), ViewTooltip.Position.BOTTOM, true);

            }
        });
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id) {
            /*case R.id.release_all:
                try {

                    ArrayList<LSSDPNodes> nodesList = LSSDPNodeDB.getInstance().GetDB();
                    if (nodesList == null)
                        return true;

                    releaseAllDevicesDialogue(nodesList.size());
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this,getString(R.string.some_error_occured_while_freeing), Toast.LENGTH_SHORT).show();
                }
                break;*/
            case R.id.myFAB:
                Intent i = new Intent(ActiveScenesListActivity.this, CreateNewScene.class);
                startActivity(i);
                break;
        }
        /*if (id == R.id.release_all) {
            try {

                ArrayList<LSSDPNodes> nodesList = LSSDPNodeDB.getInstance().GetDB();
                if (nodesList == null)
                    return true;

                releaseAllDevicesDialogue(nodesList.size());
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this,getString(R.string.some_error_occured_while_freeing), Toast.LENGTH_SHORT).show();
            }
        }*/

        return super.onOptionsItemSelected(item);
    }


    /*Releasing all devices*/
    private void releaseAllMasterandSlave() {
        ScanningHandler mScanningHandler = ScanningHandler.getInstance();
        ConcurrentHashMap<String, SceneObject> centralSceneObjectRepo = mScanningHandler.getSceneObjectFromCentralRepo();

            /*which means no master present hence all devices are free so need to do anything*/
        if (centralSceneObjectRepo == null || centralSceneObjectRepo.size() == 0) {
            LibreLogger.d(this, "No master present");
            return;
        }

        for (String masterIPAddress : centralSceneObjectRepo.keySet()) {
            ArrayList<LSSDPNodes> lssdpNodesArrayList = mScanningHandler
                    .getSlaveListForMasterIp(masterIPAddress, mScanningHandler.getconnectedSSIDname(getApplicationContext()));

            if (lssdpNodesArrayList != null && lssdpNodesArrayList.size() != 0) {
                for (LSSDPNodes mSlaves : lssdpNodesArrayList) {
                /*freeing slave for particular master*/
                    LUCIControl luciControl = new LUCIControl(mSlaves.getIP());
                    /*send this command only when you bother about 103*/
                    luciControl.sendAsynchronousCommand();
                    luciControl.SendCommand(MIDCONST.MID_DDMS, LUCIMESSAGES.SETFREE, LSSDPCONST.LUCI_SET);

                    try {
                        Thread.sleep(150);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            /*freeing master*/
            LUCIControl luciControl = new LUCIControl(masterIPAddress);
            luciControl.sendAsynchronousCommand();
            luciControl.SendCommand(MIDCONST.MID_DDMS, LUCIMESSAGES.SETFREE, LSSDPCONST.LUCI_SET);

        }
        /*clearing central repository*/
        mScanningHandler.clearSceneObjectsFromCentralRepo();
    }

    /*freeing all devices from DB and clearing it*/
    private boolean freeAllDevicesFromDB() {

        ArrayList<LSSDPNodes> nodesList = LSSDPNodeDB.getInstance().GetDB();
        if (nodesList != null && nodesList.size() > 0) {

            for (LSSDPNodes nodes : nodesList) {
                LUCIControl luciControl = new LUCIControl(nodes.getIP());
                    /*send this command only when you bother about 103*/
                luciControl.sendAsynchronousCommand();
                luciControl.SendCommand(MIDCONST.MID_DDMS, LUCIMESSAGES.SETFREE, LSSDPCONST.LUCI_SET);
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        LSSDPNodeDB.getInstance().clearDB();
        handler.sendEmptyMessage(RELEASE_ALL_SUCCESS);
        return true;
    }


    /*this method will release local DMR playback songs*/
    private void ensureDMRPlaybackStopped() {

        ScanningHandler mScanningHandler = ScanningHandler.getInstance();
        ConcurrentHashMap<String, SceneObject> centralSceneObjectRepo = mScanningHandler.getSceneObjectFromCentralRepo();

        /*which means no master present hence all devices are free so need to do anything*/
        if (centralSceneObjectRepo == null || centralSceneObjectRepo.size() == 0) {
            LibreLogger.d(this, "No master present");
            return;
        }

        for (String masterIPAddress : centralSceneObjectRepo.keySet()) {
            RemoteDevice renderingDevice = UpnpDeviceManager.getInstance().getRemoteDMRDeviceByIp(masterIPAddress);

            String mIpaddress = masterIPAddress;
            ScanningHandler mScanHandler = ScanningHandler.getInstance();
            SceneObject currentSceneObject = mScanHandler.getSceneObjectFromCentralRepo(mIpaddress);


            if (renderingDevice != null
                    && currentSceneObject != null
                    && currentSceneObject.getPlayUrl() != null
                    && !currentSceneObject.getPlayUrl().equalsIgnoreCase("")
                    && currentSceneObject.getPlayUrl().contains(LibreApplication.LOCAL_IP)
                    && currentSceneObject.getPlayUrl().contains(ContentTree.AUDIO_PREFIX)
                    ) {

               String renderingUDN = renderingDevice.getIdentity().getUdn().toString();
                PlaybackHelper playbackHelper = LibreApplication.PLAYBACK_HELPER_MAP.get(renderingUDN);
                if (playbackHelper != null)
                    playbackHelper.StopPlayback();

            }

        }
        Intent in = new Intent(ActiveScenesListActivity.this, DMRDeviceListenerForegroundService.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(in);
    }


    @Override
    public void onBackPressed() {
        if (isDoubleTap) {
            ensureDMRPlaybackStopped();
            super.onBackPressed();
            ensureAppKill();
            return;
        }
        Toast.makeText(this, getString(R.string.doubleTapToExit), Toast.LENGTH_SHORT).show();
        isDoubleTap = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isDoubleTap = false;
            }
        }, 2000);
    }


    public void ensureAppKill(){

            Log.d("NetworkChanged", "App is Restarting");
            //finish();
        /* Stopping ForeGRound Service Whenwe are Restarting the APP */
            Intent in = new Intent(ActiveScenesListActivity.this, DMRDeviceListenerForegroundService.class);
            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            in.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
            stopService(in);


        /* * Finish this activity, and tries to finish all activities immediately below it
     * in the current task that have the same affinity.*/
            ActivityCompat.finishAffinity(this);
        /* Killing our Android App with The PID For the Safe Case */
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);

        }

    }


