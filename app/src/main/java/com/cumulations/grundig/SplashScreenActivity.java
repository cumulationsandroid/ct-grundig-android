package com.cumulations.grundig;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.Utils;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;
import com.skyfishjy.library.RippleBackground;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by cumulations on 4/8/17.
 */

public class SplashScreenActivity extends DeviceDiscoveryActivity implements LibreDeviceInteractionListner {

    ImageView image1;
    ImageView image2;
    ImageView image3;
    ImageView image4;
    ImageView mImageView;
    RippleBackground ripple1;
    RippleBackground ripple2;
    RippleBackground ripple3;
    RippleBackground ripple4;
    ImageView grundigText;
    TextView fineArtsText;
    RelativeLayout textBackground;
    int position = 1;
    private Handler ha;
    private RelativeLayout parent;
    //private View view;



    private static final java.lang.String TAG = "SplashScreen";
    public static final String APP_RESTARTING = "RestartingApplication";
    HandlerThread mThread;
    SplashScreenActivity.ServiceHandler mServiceHandler;
    private boolean mRequestPermissioninProgress = false;
    boolean mCoarseLocation = false;
    boolean mFineLocation = false;
    boolean mStorage=false;

    ArrayList<String> devList = new ArrayList<String>();
    final int DEVICES_FOUND = 412;
    final int TIME_EXPIRED = 413;
    final int MSEARCH_REQUEST = 414;
    final int MEDIA_PROCESS_INIT = 415;
    final int MEDIA_PROCESS_DONE = 416;
    final int NETWORK_IS_NULL = 417;

    boolean deviceFound, freeDeviceFound;
    LibreApplication application;
    private ProgressBar m_progressDlg;

    TextView mAppVersion;
    AlertDialog alert;

    //         for cast
    public String gCastDevice;

    public ScanningHandler mScanHandler = ScanningHandler.getInstance();
    private RippleBackground view;

    public String getVersion(Context context) {
        String Version = getString(R.string.title_activity_welcome);
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pInfo != null)
            Version = pInfo.versionName;

        return Version;
    }

    private boolean googleDialogShown;
    private HashMap<String,Integer> indexMap;
    private HashMap<String,String> supportedLangCodeMap;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashcreen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!mRequestPermissioninProgress)
                requestPermission();
        }
        registerForDeviceEvents(this);
        application = (LibreApplication) getApplication();
        image1 = (ImageView)findViewById(R.id.image1);
        image2 = (ImageView)findViewById(R.id.image2);
        image3 = (ImageView)findViewById(R.id.image3);
        image4 = (ImageView)findViewById(R.id.image4);
        mImageView = (ImageView)findViewById(R.id.imageview);
        ripple1 = (RippleBackground)findViewById(R.id.ripple1);
        ripple2 = (RippleBackground)findViewById(R.id.ripple2);
        ripple3 = (RippleBackground)findViewById(R.id.ripple3);
        ripple4 = (RippleBackground)findViewById(R.id.ripple4);
        grundigText = (ImageView) findViewById(R.id.grundigText);
        fineArtsText = (TextView)findViewById(R.id.fineArtsText);
        textBackground = (RelativeLayout)findViewById(R.id.textBackground);
        parent = (RelativeLayout)findViewById(R.id.parentlayout);
        view = (RippleBackground)findViewById(R.id.view);

        googleDialogShown = getSharedPreferences(Constants.SHOWN_GOOGLE_TOS_PREF,MODE_PRIVATE)
                .getBoolean(Constants.SHOWN_GOOGLE_TOS,false);
        if (googleDialogShown) {
            ha = new Handler();
            ha.postDelayed(new AnimateImage1(), 600);
        }

        supportedLangCodeMap = new HashMap<>();
        supportedLangCodeMap.put("en","English");
        supportedLangCodeMap.put("de","Germany");
        supportedLangCodeMap.put("es","Spain");
        supportedLangCodeMap.put("fi","Finland");
        supportedLangCodeMap.put("fr","France");
        supportedLangCodeMap.put("it","Italy");
        supportedLangCodeMap.put("no","Norwegian");
        supportedLangCodeMap.put("tr","Turkey");
        indexMap = prepareLangIndexMap();
    }

    private void requestPermission() {
        if (hasPermission((Manifest.permission.ACCESS_FINE_LOCATION)) && hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION) && hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE) && hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            mCoarseLocation = true;
            mFineLocation = true;
            mStorage=true;

        } else {
//            String[] perms = {"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"};
            String[] perms = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};

            int permsRequestCode = 200;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(perms, permsRequestCode);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private void clearAnimation() {
        image1.clearAnimation();
        image2.clearAnimation();
        image3.clearAnimation();
        image4.clearAnimation();
    }

    private void clearRippleAnimation() {
        ripple1.stopRippleAnimation();
        ripple2.stopRippleAnimation();
        ripple3.stopRippleAnimation();
        ripple4.stopRippleAnimation();
        view.stopRippleAnimation();
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void removeRelativeLayoutRules() {
        RelativeLayout.LayoutParams layoutParams1 =
                (RelativeLayout.LayoutParams)ripple1.getLayoutParams();
        layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        layoutParams1.removeRule(RelativeLayout.LEFT_OF);
        layoutParams1.removeRule(RelativeLayout.BELOW);
        ripple1.setLayoutParams(layoutParams1);
        RelativeLayout.LayoutParams layoutParams2 =
                (RelativeLayout.LayoutParams)ripple2.getLayoutParams();
        layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        layoutParams2.removeRule(RelativeLayout.LEFT_OF);
        layoutParams2.removeRule(RelativeLayout.RIGHT_OF);
        layoutParams2.removeRule(RelativeLayout.ABOVE);
        ripple2.setLayoutParams(layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 =
                (RelativeLayout.LayoutParams)ripple3.getLayoutParams();
        layoutParams3.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        layoutParams3.removeRule(RelativeLayout.ABOVE);
        layoutParams3.removeRule(RelativeLayout.RIGHT_OF);
        ripple3.setLayoutParams(layoutParams3);
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams)ripple4.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        layoutParams.removeRule(RelativeLayout.BELOW);
        layoutParams.removeRule(RelativeLayout.RIGHT_OF);
        ripple4.setLayoutParams(layoutParams);
    }

    private void mergeAllImages() {
        final ChangeBounds transition = new ChangeBounds();
        transition.setDuration(450L); // Sets a duration of 600 milliseconds
        TransitionManager.beginDelayedTransition(parent, transition);
        removeRelativeLayoutRules();
    }

    private void fadeOutImages() {
        Animation fadeOutAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);

        image1.startAnimation(fadeOutAnimation);
        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //ripple1.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                fadeInGrundigLogo();
                view.setVisibility(View.VISIBLE);
                //mImageView.setBackgroundResource(R.drawable.ellipse_splashscreen);
                mImageView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                view.startRippleAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        image2.startAnimation(fadeOutAnimation);
        image3.startAnimation(fadeOutAnimation);
        image4.startAnimation(fadeOutAnimation);
    }

    private void fadeInGrundigLogo() {
        image1.setVisibility(View.GONE);
        image2.setVisibility(View.GONE);
        image3.setVisibility(View.GONE);
        image4.setVisibility(View.GONE);
        Animation fadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_grundig_logo);
        grundigText.setVisibility(View.VISIBLE);
        //fineArtsText.setVisibility(View.VISIBLE);
        textBackground.setVisibility(View.VISIBLE);
        grundigText.startAnimation(fadeInAnimation);
        //fineArtsText.startAnimation(fadeInAnimation);
        textBackground.startAnimation(fadeInAnimation);
    }

    private class AnimateImage1 implements Runnable {

        @Override
        public void run() {
            Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_anim);
            anim.setRepeatCount(Animation.INFINITE);
            anim.setRepeatMode(Animation.RESTART);
            switch (position) {
                case 1:
                    //image1.startAnimation(anim);
                    ripple2.setVisibility(View.VISIBLE);
                    ripple2.startRippleAnimation();
                    break;
                case 2:
                    //image2.startAnimation(anim);
                    ripple3.setVisibility(View.VISIBLE);
                    ripple3.startRippleAnimation();
                    break;
                case 3:
                    //image3.startAnimation(anim);
                    ripple4.setVisibility(View.VISIBLE);
                    ripple4.startRippleAnimation();
                    break;
                case 4:
                    //image4.startAnimation(anim);
                    ripple1.setVisibility(View.VISIBLE);
                    ripple1.startRippleAnimation();
                    break;
            }
            if (position > 4) {
                clearRippleAnimation();
                mergeAllImages();
                clearAnimation();
                fadeOutImages();
                /*DisplayMetrics dm = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(dm);
                Log.d("CENTER", "width: " + dm.widthPixels / 2 + ":    height: " + dm.heightPixels / 2);
                Log.d("CENTER", "width: " + view.getPivotX() / 2 + ":    height: " + view.getPivotY() / 2);*/
            } else {
                ha.postDelayed(new AnimateImage1(), 600);
            }
            position++;
        }
    }
    private SpannableString clickableString(String sampleString) {
        SpannableString spannableString = new SpannableString(sampleString);
        ClickableSpan googleTermsClicked = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent=new Intent(getApplicationContext(),LibreWebViewActivity.class);
                intent.putExtra("url", "http://www.google.com/intl/en/policies/privacy/");
                startActivity(intent);
            }
        };


        ClickableSpan termsClicked = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent intent=new Intent(getApplicationContext(),LibreWebViewActivity.class);
                intent.putExtra("url","https://www.google.com/intl/en/policies/terms/");
                startActivity(intent);
            }
        };

        ClickableSpan privacyClicked = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent intent=new Intent(getApplicationContext(),LibreWebViewActivity.class);
                intent.putExtra("url","http://www.google.com/intl/en/policies/privacy/");
                startActivity(intent);
            }
        };

        return formatSpannableString(spannableString,googleTermsClicked,termsClicked,privacyClicked);
    }

    public void ensureAppKill(){

        Log.d("Declined", "App is Killed ");
        //finish();
        /* Stopping ForeGRound Service Whenwe are Restarting the APP */
        Intent in = new Intent(SplashScreenActivity.this, DMRDeviceListenerForegroundService.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(in);


        /* * Finish this activity, and tries to finish all activities immediately below it
     * in the current task that have the same affinity.*/
        ActivityCompat.finishAffinity(this);
        /* Killing our Android App with The PID For the Safe Case */
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);

    }
    public void showGoogleTosDialog(Context context) {
        /*changes for crash in splash screen*/
        SpannableString contentText = clickableString(getResources().getString((R.string.google_tos)));
        LayoutInflater factory = LayoutInflater.from(this);
        final View gcastDialog = factory.inflate(
                R.layout.gcast_alert_dialog, null);
        TextView textView= (TextView)gcastDialog. findViewById(R.id.google_text);
        textView.setText(contentText);
        textView.setAutoLinkMask(RESULT_OK);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setGravity(Gravity.LEFT);
        Linkify.addLinks(contentText, Linkify.WEB_URLS);

        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
        builder/*.setTitle(Html.fromHtml("<b>"+getString(R.string.googleTerms)+"</b>"))*/
                .setCancelable(false)
                //                .setMessage(clickableString(getString((R.string.google_tos))))
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        getSharedPreferences(Constants.SHOWN_GOOGLE_TOS_PREF,MODE_PRIVATE)
                                .edit()
                                .putBoolean(Constants.SHOWN_GOOGLE_TOS,false)
                                .apply();
                        ensureAppKill();
                        LibreApplication.GOOGLE_TOS_ACCEPTED=false;
                        if(application.getScanThread()!=null ) {
                            application.getScanThread().clearNodes();
                            application.getScanThread().UpdateNodes();


                            handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 500);
                            handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 1000);
                            handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 1500);
                        }

                        showLoader();

                        mThread = new HandlerThread(TAG, android.os.Process.THREAD_PRIORITY_BACKGROUND);
                        mThread.start();
                        mServiceHandler = new SplashScreenActivity.ServiceHandler(mThread.getLooper());

                        //m_progressDlg.setVisibility(View.VISIBLE);
                        /* initiating the search DMR */
                        m_upnpProcessor.searchDMR();

                        handler.sendEmptyMessage(MEDIA_PROCESS_INIT);


                    }
                })
                .setPositiveButton((Html.fromHtml("<b>"+getString(R.string.accept)+"</b>")), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences sharedpreferences = getApplicationContext()
                                .getSharedPreferences(Constants.SHOWN_GOOGLE_TOS, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Constants.SHOWN_GOOGLE_TOS, "Yes");
                        editor.apply();
                        LibreApplication.GOOGLE_TOS_ACCEPTED=true;
                        if(application.getScanThread()!=null ) {
                            application.getScanThread().clearNodes();
                            application.getScanThread().UpdateNodes();


                            handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 500);
                            handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 1000);
                            handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 1500);

                        }
                        showLoader();

                        mThread = new HandlerThread(TAG, android.os.Process.THREAD_PRIORITY_BACKGROUND);
                        mThread.start();
                        mServiceHandler = new SplashScreenActivity.ServiceHandler(mThread.getLooper());

                        //m_progressDlg.setVisibility(View.VISIBLE);
                        /* initiating the search DMR */
                        m_upnpProcessor.searchDMR();

                        handler.sendEmptyMessage(MEDIA_PROCESS_INIT);

                        getSharedPreferences(Constants.SHOWN_GOOGLE_TOS_PREF,MODE_PRIVATE)
                                .edit()
                                .putBoolean(Constants.SHOWN_GOOGLE_TOS,true)
                                .apply();

                        ha = new Handler();
                        ha.postDelayed(new AnimateImage1(), 600);


                    }

                });

        if(alert==null) {
            alert = builder.create();
            alert.setView(gcastDialog);
        }

        if(alert!=null && !alert.isShowing()) {

            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                    btnPositive.setTypeface(btnPositive.getTypeface(),Typeface.BOLD);
                }
            });

            //Customise Title
            TextView title = new TextView(this);
            title.setText(Html.fromHtml("<b>"+getString(R.string.googleTerms)+"</b>"));
            title.setPadding(16, 20, 16, 0);
            title.setGravity(Gravity.CENTER);
            title.setTextColor(Color.BLACK);
            title.setTextSize(18);
            alert.setCustomTitle(title);
            alert.show();
        }
    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);


            switch (msg.what) {

                case DEVICES_FOUND:

                {
                    LibreLogger.d(this, "Devices found");
                                                                                                                                                                                                                                                                                 /*loading stop with success message*/
                }
                break;

                case TIME_EXPIRED: {

                    //m_progressDlg.setVisibility(View.INVISIBLE);
                    if (!SplashScreenActivity.this.isFinishing()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
                        builder.setMessage(getString(R.string.noNetworkFound))
                                //.setCancelable(false)
                                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        System.exit(0);
                                        finish();

                                    }

                                })
                                .setNegativeButton("Go to settings", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Settings.ACTION_SETTINGS));
                                    }
                                });

                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
                break;

                case MSEARCH_REQUEST:
                    LibreLogger.d(this, "Sending the msearch nodes");
                    if (application!=null && application.getScanThread()!=null)
                        application.getScanThread().UpdateNodes();

                    break;

                case MEDIA_PROCESS_INIT:
                    showLoader();
                    mServiceHandler.sendEmptyMessage(MEDIA_PROCESS_INIT);
                    break;

                case MEDIA_PROCESS_DONE:

                    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo == null)
                        return;


                    if (gCastDevice != null) {


                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
                        builder.setCancelable(false)
                                .setMessage(gCastDevice)
                                .setNegativeButton(getString(R.string.noThanks), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        goNext();

                                    }
                                })
                                .setPositiveButton(getString(R.string.learnMore), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //https://support.google.com/googlecast/answer/6076570
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.setData(Uri.parse("https://www.google.com/cast/audio/learn"));
                                        startActivity(intent);

                                    }

                                });

                        AlertDialog alert = builder.create();
                        alert.show();
                        gCastDevice = null;
                    } else
                        goNext();

                    break;


            }


        }
    };

    private void goNext() {
        if (deviceFound) {
            unRegisterForDeviceEvents();
            Intent newIntent = new Intent(SplashScreenActivity.this, ActiveScenesListActivity.class);
            startActivity(newIntent);
            finish();

        } else {
            unRegisterForDeviceEvents();
            Intent newIntent = new Intent(SplashScreenActivity.this, PlayNewActivity.class);
            startActivity(newIntent);
            finish();
        }
    }

    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

                                                                                                                                                                                                                                                                    /*
                                                                                                                                                                                                                                                                    Doesnt need to handle
                                                                                                                                                                                                                                                                     */
    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

        //using shared preference - for gCast configure alert
/*
        SharedPreferences sharedPreferences = getApplicationContext()
            .getSharedPreferences("sac_configured", Context.MODE_PRIVATE);
        */
/* If this function gets called then it means that there are devices in the network and hence we can throw
          * device found handler *//*

        LibreLogger.d(this, "New device is found with the ip address = " + node.getIP() + "Device State" + node.getDeviceState());
        //////////////////////////////shared preference for gCast
        String str = sharedPreferences.getString("deviceFriendlyName", "");
        if (str != null && str.equalsIgnoreCase(node.getFriendlyname().toString()) && node.getgCastVerision() != null) {
            gCastDevice = node.getFriendlyname().toString();
            //sending time zone to the gCast device
            LUCIControl luciControl = new LUCIControl(node.getIP());
            luciControl.SendCommand(222, "2:" + TimeZone.getDefault().getID().toString(), LSSDPCONST.LUCI_SET);
            //Toast.makeText(getApplicationContext(),str + " in preference",Toast.LENGTH_SHORT).show();
            sharedPreferences.edit().remove("deviceFriendlyName").commit();
        } else if (node.getgCastVerision() == null && str.equalsIgnoreCase(node.getFriendlyname().toString())) {
            sharedPreferences.edit().remove("deviceFriendlyName").commit();
            gCastDevice = null;
        }
*/

        //if (deviceFound == false) {
        if (node.getDeviceState().equals("M")) {
            LibreLogger.d(this, "Master found");
            //unRegisterForDeviceEvents();

            deviceFound = true;
            freeDeviceFound = false;
            handler.sendEmptyMessage(DEVICES_FOUND);

            handler.removeMessages(TIME_EXPIRED);
            //if(! mScanHandler.isIpAvailableInCentralSceneRepo(node.getIP())) // Review
            {
                SceneObject sceneObjec = new SceneObject(" ", node.getFriendlyname(), 0, node.getIP());
                mScanHandler.putSceneObjectToCentralRepo(node.getIP(), sceneObjec);

            }
            devList.add(node.getIP());

        } else if (node.getDeviceState().equals("F")) {
            freeDeviceFound = true;
            handler.sendEmptyMessageDelayed(DEVICES_FOUND, 3000);
            handler.removeMessages(TIME_EXPIRED);
        }
    }

    @Override
    public void deviceGotRemoved(String mIpAddress) {

    }

    @Override
    public void messageRecieved(NettyData data) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public String getconnectedSSIDname(Context mContext) {
        WifiManager wifiManager;
        wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        LibreLogger.d(this, "getconnectedSSIDname wifiInfo = " + wifiInfo.toString());
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }


        return ssid;
    }
    @Override
    protected void onResume() {
        super.onResume();
        /* App is Not Restarting Properly When a App in SpalshScreen
        * And when a Screen is Transition is happening , In DeviceDiscoveryActivity in OnCreate
        * we are Setting activeSSID , its wrong .
        *
        * Scenario : In Spalshscreen WIFI network is Changed , at that time ,it will launched the ActiveScenes,
        * in Background it will update the activeSSID , So it will not restart because of that Duplicate /
        * Searching the Renderer issue will come. this si what happening for HN-SA-HN */
        LibreApplication.activeSSID =getconnectedSSIDname(SplashScreenActivity.this);
        LibreApplication.mActiveSSIDBeforeWifiOff= LibreApplication.activeSSID;
        registerForDeviceEvents(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unRegisterForDeviceEvents();

    }

    protected void onPause() {
        super.onPause();
    }
    final private int PERMISSION_DEVICE_DISCOVERY = 100;
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_DEVICE_DISCOVERY: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && permissions[0].contains(Manifest.permission.READ_EXTERNAL_STORAGE)
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    LibreLogger.d(this, permissions[0]+" storage permission Granted");
                    afterPermit();
                    return;

                } else {

                    // permission denied!
                    LibreLogger.d(this, permissions[0]+" seeked permission Denied. So, requesting again");
                    Toast.makeText(SplashScreenActivity.this, getString(R.string.storagePermissionToast), Toast.LENGTH_SHORT).show();
                    request();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    public void request() {
            /*if (!shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // user checked Never Ask again
                LibreLogger.d(this, "permit READ_EXTERNAL_STORAGE Denied for ever");
                // show dialog
                AlertDialog.Builder requestPermission = new AlertDialog.Builder(SpalshScreenActivity.this);
                requestPermission.setTitle("Permission is Not Available")
                        .setMessage("Permission to access storage is required to load songs,please goto settings and enable \'Storage\' permission and restart the app")
                        .setPositiveButton("Go To Setting", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //navigate to settings
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                intent.setData(uri);
                                startActivity(intent);
                            }
                        })
                        .setCancelable(false);
                if (alert == null) {
                    alert = requestPermission.create();
                }
                if (alert != null && !alert.isShowing())
                    alert.show();

                return;
            }
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_DEVICE_DISCOVERY);
*/
        return;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void storagePermit(){
        LibreLogger.d(this, "permit storagePermit");
        //seeking permission
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            LibreLogger.d(this, "permit READ_EXTERNAL_STORAGE Granted");
            LibreApplication.mStoragePermissionGranted = true;
            return;
        }else{
            LibreLogger.d(this, "permit READ_EXTERNAL_STORAGE not Granted");
            LibreApplication.mStoragePermissionGranted = false;
            // request is denied by the user. so requesting
            //  request();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PERMISSION_DEVICE_DISCOVERY) {
            LibreLogger.d(this, "permit onActivityResult");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    // SYSTEM_ALERT_WINDOW permission not granted.
                    LibreLogger.d(this,"permit SYSTEM_ALERT_WINDOW not granted");
                    Toast.makeText(SplashScreenActivity.this, getString(R.string.overlaypermissiontoast), Toast.LENGTH_SHORT).show();
                    // Permission not granted so requesting again
                    requestManageOverlayPermission();

                } else {
                    //SYSTEM_ALERT_WINDOW permission granted. So requesting storage permissions.
                    LibreLogger.d(this, "permit SYSTEM_ALERT_WINDOW granted.So,requesting storage permissions.");
                    afterPermit();
                    //storagePermit();
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestManageOverlayPermission() {
        //This is to request user for 'Dangerous Permissions' like SYSTEM_ALERT_WINDOW
        //first check whether the permission is granted
        LibreLogger.d(this, "permit OverlayPermissions");
        LibreLogger.d(this, "permit sdk is >=23");
        afterPermit();
        storagePermit();
            /*if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                //requesting the permission
                startActivityForResult(intent, PERMISSION_DEVICE_DISCOVERY);

            }else{
                LibreLogger.d(this, "permit overlay permissions are granted already. Now asking for storage permissions");
                afterPermit();
        //        storagePermit();
            }*/

    }


    public void onStartComplete() {
        super.onStartComplete();
        /* Sac Configured After that App have to restarted */
/*
        *//* Why We have to Initiate the ServiceHandler After that we havve to Check whether Netif is there or not
        * before that itself we should identify it and Stop the User to go beyond that and ServiceHandler *//*
        NetworkInterface mNetIf = Utils.getActiveNetworkInterface();
        if (mNetIf == null)
            handler.sendEmptyMessage(TIME_EXPIRED);*/

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            LibreLogger.d(this, "permit SDK is >= 23");
            requestManageOverlayPermission();
        }else{
            LibreLogger.d(this, "permit SDK is < 23");
            afterPermit();
        }*/
        afterPermit();
    }

    public void afterPermit(){
                                                                                                                                                                                                                                                                 /* Google cast settings*/
        /*
         No need to show the Google dialog in spalsh screen*/
       /* SharedPreferences sharedPreferences = getApplicationContext()
                .getSharedPreferences(Constants.SHOWN_GOOGLE_TOS, Context.MODE_PRIVATE);
        String shownGoogle = sharedPreferences.getString(Constants.SHOWN_GOOGLE_TOS, null);*/
        // now TOS is recieved in msg box 224, so no need to check shared pref for this dialog


        /*String shownGoogle = "Yes";
        SharedPreferences sharedpreferences = getApplicationContext()
                .getSharedPreferences(Constants.SHOWN_GOOGLE_TOS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.SHOWN_GOOGLE_TOS, "Yes");
        editor.commit();
        if (shownGoogle == null) {
            showGoogleTosDialog(this);
        }*/

        if (!googleDialogShown){
            showGoogleTosDialog(this);
        } else {
            LibreApplication.GOOGLE_TOS_ACCEPTED=true;
            if(application.getScanThread()!=null ) {
                application.getScanThread().clearNodes();
                application.getScanThread().UpdateNodes();


                handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 1000);
                handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 2000);
                handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 3000);
                handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 4000);
//            handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 2500);
//            handler.sendEmptyMessageDelayed(MSEARCH_REQUEST, 3000);


            }
            showLoader();

            mThread = new HandlerThread(TAG, android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mThread.start();
            mServiceHandler = new SplashScreenActivity.ServiceHandler(mThread.getLooper());

            /*m_progressDlg.setVisibility(View.VISIBLE);*/
            /* initiating the search DMR */
            m_upnpProcessor.searchDMR();

            handler.sendEmptyMessage(MEDIA_PROCESS_INIT);
        }


/*
MusicServer musicServer = MusicServer.getMusicServer();
musicServer.prepareMediaServer(this,getUpnpBinder());
*/

/*if (deviceFound){
    unRegisterForDeviceEvents();
    Intent newIntent=new Intent(SpalshScreenActivity.this,ActiveScenesListActivity.class);
    startActivity(newIntent);
    finish();

}else {
    unRegisterForDeviceEvents();
    Intent newIntent=new Intent(SpalshScreenActivity.this,PlayNewActivity.class);
    startActivity(newIntent);
    finish();
}
*/


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeLoader();
        clearRippleAnimation();
        Log.d("UpnpSplashScreen", "destroyed");
        if (ha!=null)
            ha.removeCallbacksAndMessages(null);
        if (handler!=null)
            handler.removeCallbacksAndMessages(null);
        if (mServiceHandler!=null)
            mServiceHandler.removeCallbacksAndMessages(null);
        application = null;
        ha = null;
        handler = null;
        mServiceHandler = null;
        mThread = null;
    }

    private void showLoader() {

        /*if (m_progressDlg == null)
            m_progressDlg = ProgressDialog.show(this, "Data Loading", "Loading local Content", true, true, null);

        if (m_progressDlg.isShowing() == false) {
            m_progressDlg.show();
        }*/
    }

    private void closeLoader() {
        //        if (m_progressDlg != null) {
        //            if (m_progressDlg.isShowing() == true) {
        //                m_progressDlg.dismiss();
        //            }
        //        }
    }

    class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            try {

                //isCommandinProcessing=true;;
                switch (msg.what) {


                    case MEDIA_PROCESS_INIT: {
                        long startTime = System.currentTimeMillis();

                        NetworkInterface mNetIf = Utils.getActiveNetworkInterface();

                        if (mNetIf == null)
                            handler.sendEmptyMessage(TIME_EXPIRED);

                        else {
                            LibreApplication.activeSSID = getconnectedSSIDname(getApplicationContext());
                            LibreApplication.mActiveSSIDBeforeWifiOff= LibreApplication.activeSSID;
                            long finishedTime = System.currentTimeMillis();
                            /*Intent msgIntent = new Intent(SpalshScreenActivity.this, LoadLocalContentService.class);
                            startService(msgIntent);*/

                            if (finishedTime - startTime < 5500)
                                handler.sendEmptyMessageDelayed(MEDIA_PROCESS_DONE, 5500 - (finishedTime - startTime));

                            else
                                handler.sendEmptyMessage(MEDIA_PROCESS_DONE);
                        }
                    }
                }
            } catch (Exception e) {

            }
        }
    }

    private String getLanguageCode(){
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = Resources.getSystem().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            locale = Resources.getSystem().getConfiguration().locale;
        }

        return locale.getLanguage();
    }

    private SpannableString formatSpannableString(SpannableString spannableString,ClickableSpan googleTermsClicked, ClickableSpan termsClicked, ClickableSpan privacyClicked){
        String langCode = getLanguageCode();
        if (!supportedLangCodeMap.containsKey(langCode)){
            langCode = "en";
        }
        Log.d("SpannableString","lang code = "+langCode);

        spannableString.setSpan(googleTermsClicked,indexMap.get(langCode+"_gtc_start"),indexMap.get(langCode+"_gtc_end"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(termsClicked,indexMap.get(langCode+"_tc_start"),indexMap.get(langCode+"_tc_end"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(privacyClicked,indexMap.get(langCode+"_pc_start"),indexMap.get(langCode+"_pc_end"), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.brand_orange)), indexMap.get(langCode+"_gtc_start"),indexMap.get(langCode+"_gtc_end"),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.brand_orange)), indexMap.get(langCode+"_tc_start"),indexMap.get(langCode+"_tc_end"),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.brand_orange)), indexMap.get(langCode+"_pc_start"),indexMap.get(langCode+"_pc_end"),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableString;
    }

    private HashMap<String,Integer> prepareLangIndexMap(){

        /*Imp Note
        * When you take string value from resources, if it is having any escape characters
        * and new line or new tab characters, those should be ignored for character count*/
        HashMap<String,Integer> langIndexMap =new HashMap<>();

        langIndexMap.put("en_gtc_start",4);
        langIndexMap.put("en_gtc_end",25);
        langIndexMap.put("en_tc_start",395);
        langIndexMap.put("en_tc_end",418);
        langIndexMap.put("en_pc_start",422);
        langIndexMap.put("en_pc_end",444);

        langIndexMap.put("tr_gtc_start",0);
        langIndexMap.put("tr_gtc_end",26);
        langIndexMap.put("tr_tc_start",366);
        langIndexMap.put("tr_tc_end",391);
        langIndexMap.put("tr_pc_start",395);
        langIndexMap.put("tr_pc_end",424);

        langIndexMap.put("de_gtc_start",4);
        langIndexMap.put("de_gtc_end",31);
        langIndexMap.put("de_tc_start",471);
        langIndexMap.put("de_tc_end",496);
        langIndexMap.put("de_pc_start",505);
        langIndexMap.put("de_pc_end",532);

        langIndexMap.put("es_gtc_start",3);
        langIndexMap.put("es_gtc_end",35);
        langIndexMap.put("es_tc_start",440);
        langIndexMap.put("es_tc_end",471);
        langIndexMap.put("es_pc_start",481);
        langIndexMap.put("es_pc_end",513);

        langIndexMap.put("fi_gtc_start",0);
        langIndexMap.put("fi_gtc_end",29);
        langIndexMap.put("fi_tc_start",415);
        langIndexMap.put("fi_tc_end",434);
        langIndexMap.put("fi_pc_start",438);
        langIndexMap.put("fi_pc_end",465);

        langIndexMap.put("fr_gtc_start",0);
        langIndexMap.put("fr_gtc_end",41);
        langIndexMap.put("fr_tc_start",495);
        langIndexMap.put("fr_tc_end",526);
        langIndexMap.put("fr_pc_start",540);
        langIndexMap.put("fr_pc_end",568);

        langIndexMap.put("it_gtc_start",3);
        langIndexMap.put("it_gtc_end",35);
        langIndexMap.put("it_tc_start",411);
        langIndexMap.put("it_tc_end",440);
        langIndexMap.put("it_pc_start",446);
        langIndexMap.put("it_pc_end",478);

        langIndexMap.put("no_gtc_start",0);
        langIndexMap.put("no_gtc_end",40);
        langIndexMap.put("no_tc_start",428);
        langIndexMap.put("no_tc_end",447);
        langIndexMap.put("no_pc_start",470);
        langIndexMap.put("no_pc_end",480);

        return langIndexMap;
    }



}
