package com.cumulations.grundig.NewManageDevices;

/**
 * Created by cumulations on 31/7/17.
 */

public class FreeSpeakers {
    private String ipAddress;
    private boolean isSelected;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
