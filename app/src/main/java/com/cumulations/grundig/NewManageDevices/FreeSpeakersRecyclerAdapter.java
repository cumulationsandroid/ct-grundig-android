package com.cumulations.grundig.NewManageDevices;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.R;
import com.cumulations.grundig.SourceOptions.CumulationsOnlineSourcesRecyclerAdapter;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.github.florent37.viewtooltip.ViewTooltip;

import java.util.ArrayList;

/**
 * Created by cumulations on 31/7/17.
 */

public class FreeSpeakersRecyclerAdapter extends RecyclerView.Adapter<FreeSpeakersRecyclerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<FreeSpeakers> freeSpeakers;
    private CheckBox mFirstCheckBoxReference;
    interface onItemClickListener{
        void onItemClick(ArrayList<FreeSpeakers> freeSpeakers);
    }
    private onItemClickListener mOnItemClickListener;

    public void setFreeSpeakers(ArrayList<FreeSpeakers> freeSpeakers) {
        this.freeSpeakers = freeSpeakers;
    }


    public FreeSpeakersRecyclerAdapter(Context context, ArrayList<FreeSpeakers> freeSpeakers, onItemClickListener onItemClick) {
        this.context = context;
        this.freeSpeakers = freeSpeakers;
        this.mOnItemClickListener = onItemClick;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;
        TextView name;
        public MyViewHolder(View itemView) {
            super(itemView);
            mCheckBox = (CheckBox)itemView.findViewById(R.id.checkbox);
            name = (TextView)itemView.findViewById(R.id.freeSpeakerName);
        }
    }

    @Override
    public FreeSpeakersRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_cumulations_free_speakers_single_row, parent, false);


        FreeSpeakersRecyclerAdapter.MyViewHolder myViewHolder = new FreeSpeakersRecyclerAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(FreeSpeakersRecyclerAdapter.MyViewHolder myViewHolder, final int position) {
        CheckBox checkBox = myViewHolder.mCheckBox;
        if (position == 0) {
            mFirstCheckBoxReference = checkBox;
        }
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    freeSpeakers.get(position).setSelected(true);
                    mOnItemClickListener.onItemClick(freeSpeakers);
                } else {
                    freeSpeakers.get(position).setSelected(false);
                    mOnItemClickListener.onItemClick(freeSpeakers);
                }
            }
        });
        TextView name = myViewHolder.name;
        if (freeSpeakers.get(position).isSelected()) {
            checkBox.setChecked(true);
        }
        name.setText(freeSpeakers.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return freeSpeakers.size();
    }


    public ViewTooltip.TooltipView showFreeSpeakersToolTip() {
        return showViewToolTip(mFirstCheckBoxReference, context.getString(R.string.select_speaker), ViewTooltip.Position.BOTTOM, false);
    }

    private ViewTooltip.TooltipView showViewToolTip(View view, String text, ViewTooltip.Position position, final boolean shouldRepeat) {
        ViewTooltip.TooltipView toolTipView = null;
        SharedPreferences sharedPreferences = context.getSharedPreferences("appreference", context.MODE_PRIVATE);

        if (view ==null)
        {
            //Toast.makeText(context,"Hello", Toast.LENGTH_LONG).show();
            return null;
        }

        if (sharedPreferences.getBoolean(FreeSpeakersActivity.class.getSimpleName(),false) == false) {
            sharedPreferences.edit().putBoolean(FreeSpeakersActivity.class.getSimpleName(),true).commit();

            toolTipView = ViewTooltip
                    .on(view)
                    .autoHide(true,3000)
                    .clickToHide(true)
                    .align(ViewTooltip.ALIGN.CENTER)
                    .position(position)
                    .text(text)
                    .textColor(context.getResources().getColor(R.color.viewToolTipTextColor))
                    .color(context.getResources().getColor(R.color.viewToolTipColor))
                    .corner(10)
                    .show();
        }

        return toolTipView;
    }

}
