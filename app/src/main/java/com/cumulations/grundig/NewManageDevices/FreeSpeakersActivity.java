package com.cumulations.grundig.NewManageDevices;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.AddNewSpeakerToWifiHelpScreenActivity;
import com.cumulations.grundig.DeviceDiscoveryActivity;
import com.cumulations.grundig.LErrorHandeling.LibreError;
import com.cumulations.grundig.LibreApplication;
import com.cumulations.grundig.R;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.SceneObject;
import com.cumulations.grundig.constants.CommandType;
import com.cumulations.grundig.constants.DeviceMasterSlaveFreeConstants;
import com.cumulations.grundig.constants.LSSDPCONST;
import com.cumulations.grundig.constants.LUCIMESSAGES;
import com.cumulations.grundig.constants.MIDCONST;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.luci.LUCIPacket;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;
import com.cumulations.grundig.util.ShowLoader;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.github.johnpersano.supertoasts.SuperToast;

import java.util.ArrayList;

/**
 * Created by cumulations on 31/7/17.
 */

public class FreeSpeakersActivity extends DeviceDiscoveryActivity implements LibreDeviceInteractionListner {

    private ScanningHandler mScanHandler = ScanningHandler.getInstance();
    ArrayList<LSSDPNodes> mFreeDeviceList;
    ArrayList<FreeSpeakers> mFreeSpeakersArrayList;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    RelativeLayout done;
    private String mMasterIP;
    private TextView mCancel;
    private TextView mSelectAll;
    private FreeSpeakersRecyclerAdapter freeSpeakersRecyclerAdapter;
    private ProgressDialog mProgressDialog;
    private TextView configureText;
    private TextView noOfFreeSpeakersSelected;
    private RelativeLayout mRelativeLayout;
    private TextView mEmptyView;
    private ArrayList<FreeSpeakers> freeSpeakersList;
    private ImageView mLoading;
    private TextView mLoadingText;
    private ViewTooltip.TooltipView tooltipView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cumulations_free_speakers);
        mEmptyView = (TextView) findViewById(R.id.empty_view);
        mLoading = (ImageView)findViewById(R.id.loading);
        mLoadingText = (TextView)findViewById(R.id.loadingText);
        mLoadingText.setText(getString(R.string.pleaseWait));
        done = (RelativeLayout) findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FreeSpeakersActivity.this, AddNewSpeakerToWifiHelpScreenActivity.class);
                startActivity(i);
            }
        });
        mSelectAll = (TextView) findViewById(R.id.selectAll);
        mCancel = (TextView) findViewById(R.id.cancel);
        configureText = (TextView) findViewById(R.id.configureText);
        noOfFreeSpeakersSelected = (TextView) findViewById(R.id.noOfSpeakersSelected);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.connectLayout);

        findViewById(R.id.title).setSelected(true);
        mCancel.setSelected(true);
        mSelectAll.setSelected(true);
        noOfFreeSpeakersSelected.setSelected(true);


        mRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < mFreeSpeakersArrayList.size(); i++) {
                    if (mFreeSpeakersArrayList.get(i).isSelected()) {
                        final LSSDPNodes clickedNode = mScanHandler.getLSSDPNodeFromCentralDB(mFreeSpeakersArrayList.get(i).getIpAddress());
                        final LSSDPNodes mMasterNode = mScanHandler.getLSSDPNodeFromCentralDB(mMasterIP);
                        if (clickedNode == null || mMasterNode == null)
                            return;
                    /*to prevent user's multiple action on device once change has been initiated*/
                        if (clickedNode.getCurrentState() == LSSDPNodes.STATE_CHANGE_STAGE.CHANGE_INITIATED)
                            return;

                /*If Master is casting you should not allow to add SLAVE or do SETFREE on any of the devices */
                        if (mMasterNode.getCurrentSource() == MIDCONST.GCAST_SOURCE &&
                                mMasterNode.getmPlayStatus() == SceneObject.CURRENTLY_PLAYING) {
                            Toast.makeText(FreeSpeakersActivity.this, getString(R.string.errorSpeakerCasting), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (clickedNode.getCurrentSource() == MIDCONST.GCAST_SOURCE &&
                                clickedNode.getmPlayStatus() == SceneObject.CURRENTLY_PLAYING) {
                            LibreError error = new LibreError(clickedNode.getFriendlyname(), getString(R.string.speaker_casting) + " "
                                    + getString(R.string.addingCastSlaveError));
                            showErrorMessage(error);
                            return;
                        }
                        clickingOnFreeDeviceOld(clickedNode, mMasterNode);
                    }
                }
            }
        });
        mMasterIP = getIntent().getStringExtra("master_ip");
        mRecyclerView = (RecyclerView) findViewById(R.id.free_speakers_recycler_view);
        freeSpeakersRecyclerAdapter = new FreeSpeakersRecyclerAdapter(this, new ArrayList<FreeSpeakers>(), new FreeSpeakersRecyclerAdapter.onItemClickListener() {
            @Override
            public void onItemClick(ArrayList<FreeSpeakers> freeSpeakers) {
                mFreeSpeakersArrayList = freeSpeakers;

                int count = 0;
                for (FreeSpeakers speakers : mFreeSpeakersArrayList) {
                    if (speakers.isSelected()) {
                        count++;
                    }
                }
                if (count == 0) {
                    configureText.setVisibility(View.VISIBLE);
                    noOfFreeSpeakersSelected.setVisibility(View.GONE);
                    mRelativeLayout.setVisibility(View.GONE);
                } else {
                    configureText.setVisibility(View.GONE);
                    noOfFreeSpeakersSelected.setVisibility(View.VISIBLE);
                    String numFreeSpeakersSelected = String.valueOf(count)+"  "+getString(R.string.number_speaker_selected);
                    noOfFreeSpeakersSelected.setText(numFreeSpeakersSelected);
                    mRelativeLayout.setVisibility(View.VISIBLE);
                }
            }
        });
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (freeSpeakersList.size() > 0) {
                    for (FreeSpeakers freeSpeakers : freeSpeakersList) {
                        freeSpeakers.setSelected(true);
                    }
                    freeSpeakersRecyclerAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(FreeSpeakersActivity.this, getString(R.string.no_free_speakers_to_select), Toast.LENGTH_LONG).show();
                }
            }
        });
        mLinearLayoutManager = new LinearLayoutManager(this);
        /*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.HORIZONTAL);*/
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        //mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setAdapter(freeSpeakersRecyclerAdapter);
        populateAdapterWithFreeSpeakers();
    }



    @Override
    protected void onResume() {
        super.onResume();
        registerForDeviceEvents(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               tooltipView = freeSpeakersRecyclerAdapter.showFreeSpeakersToolTip();
            }
        }, 1000);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (tooltipView != null) {
            tooltipView.remove();
        }
    }

    public void showErrorMessage(final LibreError message) {
        try {

            if (LibreApplication.hideErrorMessage)
                return;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    LibreLogger.d(this, "Showing the supertaloast " + System.currentTimeMillis());
                    SuperToast superToast = new SuperToast(FreeSpeakersActivity.this);

                    if (message != null && message.getErrorMessage().contains("is no longer available")) {
                        LibreLogger.d(this, "Device go removed showing error in Device Discovery");
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                    }
                    if (message.getmTimeout() == 0) {//TimeoutDefault
                        superToast.setDuration(SuperToast.Duration.LONG);
                    } else {
                        superToast.setDuration(SuperToast.Duration.VERY_SHORT);
                    }
                    superToast.setText("" + message);
                    superToast.setAnimations(SuperToast.Animations.FLYIN);
                    superToast.setIcon(SuperToast.Icon.Dark.INFO, SuperToast.IconPosition.LEFT);
                    superToast.show();
                    if (message != null && message.getErrorMessage().contains(""))
                        superToast.setGravity(Gravity.CENTER, 0, 0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mShowAlertToastForFailingGrouping() {
        new android.app.AlertDialog.Builder(FreeSpeakersActivity.this)
                .setTitle(getString(R.string.alertTitle))
                .setMessage(" " +
                        getString(R.string.alertMessageForCantGroup))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void clickingOnFreeDeviceOld(LSSDPNodes clickedNode, LSSDPNodes mMasterNode) {
        if (clickedNode.getmWifiBand() == null
                || mMasterNode.getmWifiBand() == null) {
            mShowAlertToastForFailingGrouping();
            return;
        }
        if (clickedNode.getmWifiBand() != null &&
                mMasterNode.getmWifiBand() != null) {
            if (!mMasterNode.getmWifiBand().equalsIgnoreCase(clickedNode.getmWifiBand())) {
                mShowAlertToastForFailingGrouping();
                return;
            }
        }
        //User is trying to Make this  Slave
        Message msg = new Message();
        msg.what = DeviceMasterSlaveFreeConstants.ACTION_TO_CREATE_SLAVE_INITIATED;
        /* Adding the Bundle data to make sure to identify the device in NewManageDevices screen*/
        Bundle data = new Bundle();
        data.putString("ipAddress", clickedNode.getIP());
        msg.setData(data);
        //NewManageDevices.mManageDevicesHandler.sendMessage(msg);


        if (mMasterNode != null) {
            String mCSSID = mMasterNode.getcSSID();
            String mZoneID = mMasterNode.getZoneID();
                         /* Sending Luci Command For Slave*/
            LUCIControl luciControl = new LUCIControl(clickedNode.getIP());
                         /* Sending Asynchronous Registration*/
            luciControl.sendAsynchronousCommand();

            /*To Check Whether A Master is Avaliable or not , So we ae trying to Read Play State */
            LUCIControl luciControlForMaster = new LUCIControl(mMasterNode.getIP());
            ArrayList<LUCIPacket> luciPackets = new ArrayList<LUCIPacket>();
            LUCIPacket packet7 = new LUCIPacket(null, (short) 0, (short) MIDCONST.MID_CURRENT_PLAY_STATE, (byte) LSSDPCONST.LUCI_GET);
            luciPackets.add(packet7);
            luciControlForMaster.SendCommand(luciPackets);

            ArrayList<LUCIPacket> setOfCommandsWhileMakingSlave = new ArrayList<LUCIPacket>();
            if (mCSSID != null) {
                LUCIPacket concurrentSsid105Packet = new LUCIPacket(mCSSID.getBytes(), (short) mCSSID.length(), (short) MIDCONST.MID_SSID, (byte) LSSDPCONST.LUCI_SET);
                setOfCommandsWhileMakingSlave.add(concurrentSsid105Packet);
            }
            LUCIPacket zoneid104Packet = new LUCIPacket(mZoneID.getBytes(), (short) mZoneID.length(), (short) MIDCONST.MID_DDMS_ZONE_ID, (byte) LSSDPCONST.LUCI_SET);
            LibreLogger.d(this, "Slave ZoneID" + mZoneID);
            setOfCommandsWhileMakingSlave.add(zoneid104Packet);

            LUCIPacket setSlave100Packet = new LUCIPacket(LUCIMESSAGES.SETSLAVE.getBytes(), (short) LUCIMESSAGES.SETSLAVE.length(), (short) MIDCONST.MID_DDMS, (byte) LSSDPCONST.LUCI_SET);
            setOfCommandsWhileMakingSlave.add(setSlave100Packet);


            luciControl.SendCommand(setOfCommandsWhileMakingSlave);


            /*setting flag to LSSDP Node*/
            clickedNode.setCurrentState(LSSDPNodes.STATE_CHANGE_STAGE.CHANGE_INITIATED);

            LibreLogger.d(this, "We are trying to make " + clickedNode.getIP() + "as Slave for master device=" + mMasterNode.getIP() + " Where we have  master cssid= " + mCSSID + " zoneid=" + mZoneID);
            LibreLogger.d(this, "Second_We are trying to make " + clickedNode.getFriendlyname() + "as Slave for master device=" + mMasterNode.getFriendlyname() + " Where we have  master cssid= " + mCSSID + " zoneid=" + mZoneID);


        }

        showLoader(" Please wait...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                closeLoader();
                finish();
            }
        }, 3000);

    }


    private void populateAdapterWithFreeSpeakers() {
        mFreeDeviceList = mScanHandler.getFreeDeviceList();
        FreeSpeakers freeSpeakers;
        freeSpeakersList = new ArrayList<>();
        for (LSSDPNodes mFreeDeviceNode : mFreeDeviceList) {
            freeSpeakers = new FreeSpeakers();
            freeSpeakers.setIpAddress(mFreeDeviceNode.getIP());
            freeSpeakers.setSelected(false);
            LUCIControl luciControl = new LUCIControl(mFreeDeviceNode.getIP());
            luciControl.sendAsynchronousCommand();
            luciControl.SendCommand(MIDCONST.MID_CURRENT_SOURCE, null, CommandType.GET);
            DeviceData deviceData = new DeviceData(mFreeDeviceNode.getFriendlyname(), mFreeDeviceNode.getIP());
            Log.d("FREESPEAKERS", "" + deviceData.getDeviceName());
            freeSpeakers.setName(deviceData.getDeviceName());
            freeSpeakersList.add(freeSpeakers);
        }
        freeSpeakersRecyclerAdapter.setFreeSpeakers(freeSpeakersList);
        freeSpeakersRecyclerAdapter.notifyDataSetChanged();
        if (freeSpeakersList.size() <= 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
        }
    }

    public void showLoader(String msg) {
        ShowLoader.showLoader(mLoading, mLoadingText, FreeSpeakersActivity.this);
    }

    public void closeLoader() {
        mLoading.clearAnimation();
        mLoading.setVisibility(View.GONE);
        mLoadingText.setVisibility(View.GONE);
    }

    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {
        callDelayedPopulateAdapterWithFreeSpeakers();
    }

    @Override
    public void deviceGotRemoved(String ipaddress) {
        if (ipaddress.equalsIgnoreCase(mMasterIP)) {
            Toast.makeText(this, R.string.zone_removed, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        callDelayedPopulateAdapterWithFreeSpeakers();
    }

    @Override
    public void messageRecieved(NettyData packet) {

        LUCIPacket mLucipacket = new LUCIPacket(packet.getMessage());
        if (mLucipacket.getCommand() == 103) {
            if (packet.getRemotedeviceIp().equalsIgnoreCase(mMasterIP) && ! new String(packet.getMessage()).equalsIgnoreCase("MASTER")){
               onBackPressed();
                return;
            }

            callDelayedPopulateAdapterWithFreeSpeakers();
            String msg = new String(packet.getMessage());

            if (msg.contains("FREE") && packet.getRemotedeviceIp().equalsIgnoreCase(mMasterIP)) {
                Toast.makeText(this, R.string.zone_removed, Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
                callDelayedPopulateAdapterWithFreeSpeakers();
        }
    }


    public void callDelayedPopulateAdapterWithFreeSpeakers() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                populateAdapterWithFreeSpeakers();

            }
        }, 175);

    }
}