package com.cumulations.grundig;
/*********************************************************************************************
 * Copyright (C) 2014 Libre Wireless Technology
 * <p/>
 * "Junk Yard Lab" Project
 * <p/>
 * Libre Sync Android App
 * Author: Subhajeet Roy
 ***********************************************************************************************/

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.cumulations.grundig.Ls9Sac.GcastUpdateData;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanThread;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.app.dlna.dmc.server.MusicServer;
import com.cumulations.grundig.app.dlna.dmc.utility.PlaybackHelper;
import com.cumulations.grundig.luci.LSSDPNodeDB;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.util.GoogleTOSTimeZone;
import com.cumulations.grundig.util.LibreLogger;

import org.fourthline.cling.controlpoint.ControlPoint;

import java.net.SocketException;
import java.util.HashMap;
import java.util.LinkedHashMap;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

//import android.support.multidex.MultiDex;

//import cumulations.cutekit.CuteKit;

public class LibreApplication extends Application {


    public static boolean haveShowPropritaryGooglePopup=false;
    public static boolean mCleanUpIsDoneButNotRestarted=false;

    public static boolean mStoragePermissionGranted = false;

    public static final String MY_MODEL = android.os.Build.MODEL;
    public static Handler commandQueuHandler;
    public static boolean GOOGLE_TOS_ACCEPTED = false;
    public static String thisSACDeviceNeeds226="";
    public static String thisSACConfiguredFromThisApp = "";
    public int m_screenstate;
    public static boolean mLuciThreadInitiated = false;


    public static int activityOnUiIndex = 0;


    public static boolean hideErrorMessage;
    private static final String TAG = LibreApplication.class.getSimpleName();
    //private UpnpDeviceManager deviceManager = new UpnpDeviceManager();
    private int imageViewSize = 700;
    private String currentdmrDeviceUdn = "";
    private String currentdevicename = "";
    public static String SSID;

    private boolean isPlayNewSong = false;
    public static String activeSSID;
    public static String mActiveSSIDBeforeWifiOff;


    /*this map is having all devices volume(64) based on IP address*/
    public static HashMap<String, Integer> INDIVIDUAL_VOLUME_MAP = new HashMap<>();

    public static HashMap<String, GoogleTOSTimeZone> GOOGLE_TIMEZONE_MAP= new HashMap<>();

    public static LinkedHashMap<String,GcastUpdateData> GCAST_UPDATE_AVAILABE_LIST_DATA = new LinkedHashMap<>();

    /*this map is having zone volume(219) only for master to avoid flickering in active scene*/
    public static HashMap<String, Integer> ZONE_VOLUME_MAP = new HashMap<>();

    public String getDeviceIpAddress() {
        return deviceIpAddress;
    }

    public void setDeviceIpAddress(String deviceIpAddress) {
        this.deviceIpAddress = deviceIpAddress;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    private String deviceIpAddress;


    /**
     * **************************************************************
     */
    ScanThread wt = null;
    Thread scanthread = null;

    /**
     * *****************************************************************
     */

    public String getCurrentDmrDeviceUdn() {
        return currentdmrDeviceUdn;
    }

    public void setCurrentDmrDeviceUdn(String dmrDeviceUdn) {
        this.currentdmrDeviceUdn = dmrDeviceUdn;
    }

    public void setSpeakerName(String DevName) {
        this.currentdevicename = DevName;
    }

    public String getSpeakerName() {
        return currentdevicename;
    }


    public boolean isPlayNewSong() {
        return isPlayNewSong;
    }

    public void setPlayNewSong(boolean isPlayNewSong) {
        this.isPlayNewSong = isPlayNewSong;
    }
    public void initiateServices(){
        initLUCIServices();

    }
//    public static RefWatcher getRefWatcher(Context context) {
//        LibreApplication application = (LibreApplication) context.getApplicationContext();
//        return application.refWatcher;
//    }
//
//    private RefWatcher refWatcher;
    boolean mExecuted = false;
    public static boolean isConvertViewClicked = false;

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        Log.i(TAG, "onCreated");
        //LayoutInflaterCompat.setFactory((LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE ), new IconicsLayoutInflater(getDelegate()));
        super.onCreate();

        MultiDex.install(this);
        //Iconify.with(new FontAwesomeModule());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/GrundigDIN-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        /*if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }*/
      //  TestFairy.begin(this, "ec18ae8b89bae286bc0885b8f5125406efc32368");
//        CuteKit.install(this);

        Fabric.with(this, new Crashlytics());

        /*added to check leaks*/
//        refWatcher = LeakCanary.install(this);

        setImageViewSize();
        isPlayNewSong = false;
        final ConnectivityManager connection_manager =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkRequest.Builder request = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP && !mExecuted) {
            request = new NetworkRequest.Builder();

            request.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);

            connection_manager.registerNetworkCallback(request.build(), new ConnectivityManager.NetworkCallback() {

                @Override
                public void onAvailable(Network network) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ConnectivityManager.setProcessDefaultNetwork(network);
                        WifiManager wifiMan = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        WifiInfo wifiInf = wifiMan.getConnectionInfo();
                        int ipAddress = wifiInf.getIpAddress();
                        String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
                        LibreApplication.LOCAL_IP = ip;
                        initiateServices();
                        LibreLogger.d(this,"Karuna" + "App Called Here Device");

                    }
                }
            });


        }else{
            LibreLogger.d(this,"Karuna" + "App Called Here Device 1" );
            WifiManager wifiMan = (WifiManager)getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            int ipAddress = wifiInf.getIpAddress();
            String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
            LibreApplication.LOCAL_IP = ip;
            initiateServices();

        }

        musicServer = getMusicServer();

        if (BuildConfig.DEBUG) {

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .detectDiskWrites()
                    .penaltyLog()
                    .build());
        }
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityStarted(Activity activity) {

                activityOnUiIndex++;
            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

                activityOnUiIndex--;
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        }) ;

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            mStoragePermissionGranted = (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        }



    }
    private void myHandlingWhenAppForceStopped(Thread paramThread, Throwable paramThrowable) {
        Log.e("Alert", "Lets See if it Works !!!" +
                "paramThread:::" + paramThread +
                "paramThrowable:::" + paramThrowable);
         /* Killing our Android App with The PID For the Safe Case */
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        System.exit(0);

    }


    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
    }

    /* Created by Karuna, To fix the RestartApp Issue
        * * Till i HAave to Analyse more on this code */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void restartApp(Context context) {

        /* Stopping ForeGRound Service Whenwe are Restarting the APP */
        Intent in = new Intent(getApplicationContext(), DMRDeviceListenerForegroundService.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        stopService(in);

        Intent mStartActivity = new Intent(context, SplashScreenActivity.class);
        /*sending to let user know that app is restarting*/
        mStartActivity.putExtra(SplashScreenActivity.APP_RESTARTING, true);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 200, mPendingIntent);


        /* Killing our Android App with The PID For the Safe Case */
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        System.exit(0);

    }
    private void setImageViewSize() {
        // TODO Auto-generated method stub
        int width = getResources().getDisplayMetrics().widthPixels;
        imageViewSize = (int) (width * 0.75f);
    }

    public void initLUCIServices() {
        try {

            wt = ScanThread.getInstance();
            LibreLogger.d(this,"Karuna mRunning " + mLuciThreadInitiated);
            if(!LibreApplication.mLuciThreadInitiated) {
                scanthread = new Thread(wt);

                scanthread.start();
                LibreApplication.mLuciThreadInitiated = true;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


    }
    public void closeScanThread(){
        wt.close();
    }
    public ScanThread getScanThread() {

        /* If Scan Thread is Not Started Because of Some Reasons, we are restarrting it .
        * " Creating the Multiple Threads because we are creating a new Objects " Bug Number : 2968 */
      /* if(wt == null  || (scanthread.getState()!= Thread.State.RUNNABLE
        && scanthread.getState()!= Thread.State.NEW)){

            initLUCIServices();
            LibreLogger.d(this, "ScanThread is Status" + scanthread.getState());
        }*/

        return wt;

    }

    /**
     * clearing all collections related to application
     */
    public void clearApplicationCollections() {
        try {
            Log.d("Scan_Netty", "clearApplicationCollections() called with: " + "");
            PLAYBACK_HELPER_MAP.clear();
            INDIVIDUAL_VOLUME_MAP.clear();
            ZONE_VOLUME_MAP.clear();
            LUCIControl.luciSocketMap.clear();
            LSSDPNodeDB.getInstance().clearDB();
            ScanningHandler.getInstance().clearSceneObjectsFromCentralRepo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LSSDPScan() {
        wt.UpdateNodes();
        return;
    }

    public synchronized void restart() throws SocketException {

        wt.close();

        try {


            scanthread = new Thread(wt);
            scanthread.start();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This hashmap stores the DMRplayback helper for a udn
     */
    public static HashMap<String, PlaybackHelper> PLAYBACK_HELPER_MAP = new HashMap<String, PlaybackHelper>();
    public static String LOCAL_UDN = "";
    public static String LOCAL_IP = "";
    /* main Music server */
    private MusicServer musicServer;
    private ControlPoint controlPoint;


    public MusicServer getMusicServer() {
        return MusicServer.getMusicServer();
    }

    public void setControlPoint(ControlPoint controlPoint) {
        this.controlPoint = controlPoint;
    }


    public static boolean isApplicationActiveOnUI(){
        if (activityOnUiIndex>0)
            return true;
        else
            return false;
    }




}
