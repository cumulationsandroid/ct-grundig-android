package com.cumulations.grundig;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.grundig.ManageDevice.CreateNewScene;
import com.cumulations.grundig.Scanning.Constants;
import com.cumulations.grundig.Scanning.ScanningHandler;
import com.cumulations.grundig.luci.LSSDPNodes;
import com.cumulations.grundig.luci.LUCIControl;
import com.cumulations.grundig.netty.LibreDeviceInteractionListner;
import com.cumulations.grundig.netty.NettyData;
import com.cumulations.grundig.util.LibreLogger;
import com.github.florent37.viewtooltip.ViewTooltip;


public class ConfigureActivity extends DeviceDiscoveryActivity implements LibreDeviceInteractionListner {
    private ScanningHandler mScanHandler = ScanningHandler.getInstance();
    private Button mConfigureButton;
    private DrawerLayout drawerLayout;
    private TextView mAppVersion;
    private Toolbar toolbar;
    private LibreApplication application;
    private View menuItemView;
    private ViewTooltip.TooltipView toolTipView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure);
        application = (LibreApplication) getApplication();
        mConfigureButton = (Button) findViewById(R.id.configure_button);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initNavigationDrawer();
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        mConfigureButton.setSelected(true);
        mConfigureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ConfigureActivity.this, AddNewSpeakerToWifiHelpScreenActivity.class);
                startActivity(i);
            }
        });
        /*findViewById(R.id.favorites_icon).setOnClickListener(this);
        findViewById(R.id.playnew_icon).setOnClickListener(this);
        findViewById(R.id.config_item).setOnClickListener(this);

        findViewById(R.id.config_button).setOnClickListener(this);*/


    }


    public String getVersion(Context context) {
        String Version = getString(R.string.title_activity_welcome);
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pInfo != null)
            Version = pInfo.versionName;

        return Version;
    }

    public void initNavigationDrawer() {

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        //mAppVersion = (TextView) navigationView.getHeaderView(0).findViewById(R.id.appVersion);
        //mAppVersion = (TextView) navigationView.getMenu().getItem(7);
        mAppVersion = (TextView) navigationView.getMenu().getItem(8).getActionView().findViewById(R.id.appversion);
        try {
            mAppVersion.setText(getString(R.string.version)+" : "+getVersion(getApplicationContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*Enabling marquee for Navigation Menu items*/
        TextView addNewSpeakerText = (TextView) navigationView.getMenu().getItem(1).getActionView().findViewById(R.id.marqueeTitle);
        addNewSpeakerText.setSelected(true);

        TextView createNewZoneTv = (TextView) navigationView.getMenu().getItem(2).getActionView().findViewById(R.id.marqueeTitle);
        createNewZoneTv.setText(getString(R.string.create_new_zone));
        createNewZoneTv.setSelected(true);

        TextView whatIsHnModeTv = (TextView) navigationView.getMenu().getItem(4).getActionView().findViewById(R.id.marqueeTitle);
        whatIsHnModeTv.setText(getString(R.string.what_is_hn_mode));
        whatIsHnModeTv.setPadding(100,0,0,0);
        whatIsHnModeTv.setSelected(true);

        TextView whatIsSAModeTv = (TextView) navigationView.getMenu().getItem(5).getActionView().findViewById(R.id.marqueeTitle);
        whatIsSAModeTv.setText(getString(R.string.what_is_sa_mode));
        whatIsSAModeTv.setPadding(100,0,0,0);
        whatIsSAModeTv.setSelected(true);

        TextView howToCreateNewZoneTv = (TextView) navigationView.getMenu().getItem(6).getActionView().findViewById(R.id.marqueeTitle);
        howToCreateNewZoneTv.setText(getString(R.string.how_to_create_master));
        howToCreateNewZoneTv.setPadding(100,0,0,0);
        howToCreateNewZoneTv.setSelected(true);

        TextView howToAddNewSpeakerTv = (TextView) navigationView.getMenu().getItem(7).getActionView().findViewById(R.id.marqueeTitle);
        howToAddNewSpeakerTv.setText(getString(R.string.how_to_add_new_speaker));
        howToAddNewSpeakerTv.setPadding(100,0,0,0);
        howToAddNewSpeakerTv.setSelected(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id) {
                    case R.id.home:
                        Intent i = new Intent(ConfigureActivity.this, AddNewSpeakerToWifiHelpScreenActivity.class);
                        startActivity(i);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpSAMode:
                        Intent helpSaModeIntent = new Intent(ConfigureActivity.this, SAModeHelpScreenActivity.class);
                        startActivity(helpSaModeIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpHNMode:
                        Intent helpHnModeIntent = new Intent(ConfigureActivity.this, HNModeHelpScreenActivity.class);
                        startActivity(helpHnModeIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.helpCreateMaster:
                        Intent helpCreateMasterIntent = new Intent(ConfigureActivity.this, CreateMasterHelpScreenActivity.class);
                        startActivity(helpCreateMasterIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.addNewSpeaker:
                        Intent addNewSpeakerIntent = new Intent(ConfigureActivity.this, AddNewSpeakerHelpScreenActivity.class);
                        startActivity(addNewSpeakerIntent);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.createNewZone:
                        Intent intent = new Intent(ConfigureActivity.this, CreateNewScene.class);
                        startActivity(intent);
                        menuItem.setChecked(false);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.logout:
                        //finish();

                }
                return true;
            }
        });
        View header = navigationView.getHeaderView(0);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.menu_configure_activity, menu);


        new Handler().post(new Runnable() {
            @Override
            public void run() {
                 menuItemView = findViewById(R.id.retry);
                 showViewToolTip(menuItemView,getString(R.string.click_to_retry_finding_your_speakers_in_the_network).replace("\n", "<br/>"), ViewTooltip.Position.BOTTOM, true);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.retry:
                application.getScanThread().UpdateNodes();
                Toast.makeText(this, getString(R.string.searching), Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);


            if (ScanningHandler.getInstance().centralSceneObjectRepo.size() > 0) {
                Intent newIntent = new Intent(ConfigureActivity.this, ActiveScenesListActivity.class);
                startActivity(newIntent);
                finish();

            } else if (ScanningHandler.getInstance().getFreeDeviceList().size() > 0) {
                Intent newIntent = new Intent(ConfigureActivity.this, PlayNewActivity.class);
                startActivity(newIntent);
                finish();
            }

            handler.sendEmptyMessageDelayed(1, 3000);
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        registerForDeviceEvents(this);
        setTheFooterIcons();

        if (handler != null)
            handler.sendEmptyMessageDelayed(1, 3000);

        /*new Handler().post(new Runnable() {
            @Override
            public void run() {
                View dummyToolTipView = findViewById(R.id.dummy);
                showViewToolTip(dummyToolTipView,getString(R.string.click_to_open_menu_help), ViewTooltip.Position.BOTTOM, true);
            }
        });*/


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterForDeviceEvents();
    }

    @Override
    public void onBackPressed() {
        ScanningHandler mScanHandler = ScanningHandler.getInstance();
        if (mScanHandler.centralSceneObjectRepo.size() < 1) {
            Intent intentx = new Intent(Intent.ACTION_MAIN);
            intentx.addCategory(Intent.CATEGORY_HOME);
            intentx.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
            startActivity(intentx);
            finish();
            System.exit(0);

        } else {
            super.onBackPressed();
            finish();
        }
    }

    /*
        @Override
        public void onClick(View v) {

            switch(v.getId()){

                case R.id.favorites_icon:

                    if (LSSDPNodeDB.isCastDevicesExistsInTheNetwork()){

                   *//*     SharedPreferences sharedPreferences = getApplicationContext()
                            .getSharedPreferences(Constants.SHOWN_GOOGLE_TOS, Context.MODE_PRIVATE);
                    String shownGoogle = sharedPreferences.getString(Constants.SHOWN_GOOGLE_TOS, null);*//*
                    // now TOS is recieved in msg box 224, so no need to check shared pref for this dialog
                 *//*   String shownGoogle = "Yes";
                    if (shownGoogle == null) {
                        showGoogleTosDialog(ConfigureActivity.this);
                    }
                    else {
                        Intent intent=new Intent(ConfigureActivity.this,GcastSources.class);
                        startActivity(intent);
                    }*//*

                    //when play cast is clicked, open gCast app
                    openGHomeDialog();
                }
                else {
                    Toast.makeText(this,getString(R.string.favourits_not_implemented), Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.playnew_icon:
   //             Toast.makeText(this, getString(R.string.playnew_button), Toast.LENGTH_SHORT).show();
                Intent newIntent = new Intent(ConfigureActivity.this, CreateNewScene.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(newIntent);
                finish();
                break;

            case R.id.config_item:
                //Toast.makeText(this, "Config", Toast.LENGTH_SHORT).show();
				Intent configIntent = new Intent (ConfigureActivity.this, NewSacActivity.class);
                startActivity(configIntent);
                finish();
                break;

            case R.id.config_button:
             //   Toast.makeText(this, "Config", Toast.LENGTH_SHORT).show();
				 Intent configIntent1 = new Intent (ConfigureActivity.this, NewSacActivity.class);
                startActivity(configIntent1);
                finish();
                break;


        }
    }*/

    public void setTheFooterIcons() {

 /*       if( LSSDPNodeDB.isCastDevicesExistsInTheNetwork()) {

            TextView icon = (TextView) findViewById(R.id.favorites_icon);
            Drawable top = getResources().getDrawable(R.mipmap.ic_cast_black_24dp);
            icon.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
            icon.setText(R.string.favorite_button_playcast);
        }else{
            TextView icon = (TextView) findViewById(R.id.favorites_icon);
            Drawable top = getResources().getDrawable(R.mipmap.ic_favorites);
            icon.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
            icon.setText(R.string.favorite_button);
        }*/

/*
        issue fix for First : When there is no device to be configured as you can see , you should change Favorites to Play Cast.
        Always show play cast, not favourites
*/
        /*TextView icon = (TextView) findViewById(R.id.favorites_icon);
        Drawable top = getResources().getDrawable(R.mipmap.ic_cast_black_24dp);
        icon.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
        icon.setText(R.string.favorite_button_playcast);
        */
    }

    @Override
    public void deviceDiscoveryAfterClearingTheCacheStarted() {

    }

    @Override
    public void newDeviceFound(LSSDPNodes node) {

        if (handler != null)
            handler.removeMessages(1);

        Intent newIntent;
        if (node.getDeviceState().contains("M")) {
            if (!mScanHandler.isIpAvailableInCentralSceneRepo(node.getIP())) {
                SceneObject sceneObjec = new SceneObject(" ", node.getFriendlyname(), 0, node.getIP());
                mScanHandler.putSceneObjectToCentralRepo(node.getIP(), sceneObjec);
                newIntent = new Intent(ConfigureActivity.this, ActiveScenesListActivity.class);
                startActivity(newIntent);
                finish();
            }
            updateSceneObjectAndLaunchActiveScene(node.getIP());
        } else if (node.getDeviceState().equals("F")) {
            LibreLogger.d(this, "free device found");
            newIntent = new Intent(ConfigureActivity.this, PlayNewActivity.class);
            startActivity(newIntent);
            finish();
        }
    }

    /*this method will get called when we are getting master 103*/
    private void updateSceneObjectAndLaunchActiveScene(String ipaddress) {

        LSSDPNodes mMasterNode = mScanHandler.getLSSDPNodeFromCentralDB(ipaddress);

        if (mMasterNode != null) {

            if (!mScanHandler.isIpAvailableInCentralSceneRepo(ipaddress)) {
                SceneObject mMastersceneObjec = new SceneObject(" ", mMasterNode.getFriendlyname(), 0, mMasterNode.getIP());
                mScanHandler.putSceneObjectToCentralRepo(ipaddress, mMastersceneObjec);
            }
            if (LibreApplication.mCleanUpIsDoneButNotRestarted == false) {
                Intent intent = new Intent(ConfigureActivity.this, ActiveScenesListActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }

    }

    @Override
    public void deviceGotRemoved(String ipaddress) {

    }

    @Override
    public void messageRecieved(NettyData packet) {

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (toolTipView != null) {
            toolTipView.remove();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (handler != null)
            handler.removeMessages(1);
    }


    public void showViewToolTip(View view, String text, ViewTooltip.Position position, final boolean shouldRepeat) {
        SharedPreferences sharedPreferences = getSharedPreferences("apppreference", MODE_PRIVATE);

        if (menuItemView ==null)
        {
            //Toast.makeText(this,"Hello", Toast.LENGTH_LONG).show();
            return;
        }

        if (sharedPreferences.getBoolean(ConfigureActivity.class.getSimpleName(),false) == false) {
            sharedPreferences.edit().putBoolean(ConfigureActivity.class.getSimpleName(),true).commit();

                toolTipView = ViewTooltip
                        .on(view)
                        .autoHide(true,3000)
                        .clickToHide(true)
                        .align(ViewTooltip.ALIGN.CENTER)
                        .position(position)
                        .text(text)
                        .textColor(getResources().getColor(R.color.viewToolTipTextColor))
                        .color(getResources().getColor(R.color.viewToolTipColor))
                        .corner(10)
                        .onHide(new ViewTooltip.ListenerHide() {
                            @Override
                            public void onHide(View view) {
                                if (shouldRepeat) {
                                    showViewToolTip(mConfigureButton, getString(R.string.click_to_add_speaker_to_your_wifi), ViewTooltip.Position.BOTTOM, false);
                                }
                            }
                        })
                        .show();
        }

        return;
    }
}