package com.cumulations.grundig.util;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cumulations.grundig.R;
import com.cumulations.grundig.SourceOptions.CumulationsSourcesOptionActivity;

/**
 * Created by cumulations on 22/8/17.
 */

public class ShowLoader {

    public static void showLoader(ImageView loadingBar, TextView loadingText, Context context) {
        loadingBar.setVisibility(View.VISIBLE);
        loadingText.setVisibility(View.VISIBLE);
        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.rotate);
        rotation.setFillAfter(true);
        loadingBar.startAnimation(rotation);
    }

    public static void showLoaderInNowPlaying(ImageView loadingBar, Context context) {
        loadingBar.setVisibility(View.VISIBLE);
        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.rotate);
        rotation.setFillAfter(true);
        loadingBar.startAnimation(rotation);
    }
}
